<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Rules\CurrentPassword;
use App\Services\LogActionServices;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function postChangePassword(Request $request)
    {
        $user = \Auth::user();

        \Validator::make($request->all(), [
            'old_password' => ['required', new CurrentPassword()],
            'new_password' => ['required', 'confirmed'],
        ])->validate();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $oldObject = $user->toArray();

        $user->password = \Hash::make($request->get('new_password'));
        $user->save();

        $newObject = $user->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_GANTI_PASSWORD,
            'update',
            User::class,
            $oldObject,
            $newObject
        );

        $data['data'] = $user;

        return response()->json($data);
    }
}
