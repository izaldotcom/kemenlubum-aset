<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class DarkModeApiController extends Controller
{
    public function get(Request $request)
    {
        if (empty(\Session::get('dark_mode_front'))) {
            $settingDarkMode = Setting::where('setting_key', 'dark_mode_default_front')
                ->first();

            if (empty($settingDarkMode)) {
                \Session::put('dark_mode_front', false);
            } else {
                \Session::put('dark_mode_front', $settingDarkMode->setting_value);
            }
        }

        $result = [
            'data' => \Session::get('dark_mode_front'),
        ];

        return response()->json($result);
    }

    public function post(Request $request)
    {
        \Session::put('dark_mode_front', $request->get('dark_mode'));

        return $this->get($request);
    }
}
