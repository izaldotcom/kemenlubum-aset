<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Maps\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function getCountries(Request $request)
    {
        $slug   = $request->get('slug');
        $search = $request->get('search');

        $query = Country::orderBy('name');

        if (!empty($search)) {
            $query->where('name', 'like', "%{$search}%");
        }

        if (!empty($slug)) {
            $query->whereHas('continent', function ($query) use ($slug) {
                $query->where('slug', $slug);
            });
        }

        return $query->get();
    }
}
