<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Satker;
use Illuminate\Http\Request;

class SatkerController extends Controller
{
    public function getSatkers(Request $request)
    {
        $slug   = $request->get('slug');
        $search = $request->get('search');

        $query = Satker::with(['tanahs', 'gedungs', 'rumahs'])
            ->orderBy('nama_satker');

        if (!empty($search)) {
            $query->where('nama_satker', 'like', "%{$search}%");
        }

        if (!empty($slug)) {
            $query->whereHas('countries', function ($query) use ($slug) {
                $query->where('slug', $slug);
            });
        }

        return $query->get();
    }
}
