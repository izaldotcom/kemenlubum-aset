<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\SummaryAssetServices;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    public function getSummaryAssets(Request $request, $slugSatker = null)
    {
        return response()->json(SummaryAssetServices::getSummaryAsset(null, null, $slugSatker));
    }
}
