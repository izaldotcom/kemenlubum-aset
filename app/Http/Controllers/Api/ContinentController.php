<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Maps\Continent;
use Illuminate\Http\Request;

class ContinentController extends Controller
{
    public function getContinents(Request $request)
    {
        return Continent::orderBy('name')->get();
    }
}
