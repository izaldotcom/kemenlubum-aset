<?php

namespace App\Http\Controllers;

use App\Models\RumahNegara;
use Illuminate\Http\Request;

class RumahNegaraFrontController extends Controller
{
    public function index(Request $request)
    {
        $models = RumahNegara::groupBy('kondisi')
            ->selectRaw('kondisi, COUNT(id) as jumlah')->get()->toArray();

        $kondisis = [];
        $values   = [];
        foreach ($models as $model) {
            $kondisis[] = $model['kondisi'];
            $values[]   = $model['jumlah'];
        }

        return view('asset-rumah-negara', [
            'kondisis' => $kondisis,
            'values'   => $values,
        ]);
    }

    public function kondisi(Request $request)
    {
        $models = RumahNegara::groupBy('kondisi')
            ->selectRaw('kondisi, COUNT(id) as jumlah')->get()->toArray();

        $kondisis = [];
        $values   = [];
        foreach ($models as $model) {
            $kondisis[] = $model['kondisi'];
            $values[]   = $model['jumlah'];
        }

        return view('detail-rumah-negara', [
            'kondisis' => $kondisis,
            'values'   => $values,
        ]);
    }

    public function home(Request $request)
    {
        $models = RumahNegara::groupBy('kondisi')
            ->selectRaw('kondisi, COUNT(id) as jumlah')->get()->toArray();

        $kondisis = [];
        $values   = [];
        foreach ($models as $model) {
            $kondisis[] = $model['kondisi'];
            $values[]   = $model['jumlah'];
        }

        return view('welcome', [
            'kondisis' => $kondisis,
            'values'   => $values,
        ]);
    }
}
