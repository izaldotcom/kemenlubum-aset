<?php

namespace App\Http\Controllers;

use App\Models\Gedung;
use Illuminate\Http\Request;

class KendaraanFrontController extends Controller
{
    public function index(Request $request)
    {
        $models = Gedung::groupBy('kondisi')
            ->selectRaw('kondisi, COUNT(id) as jumlah')->get()->toArray();

        $kondisis = [];
        $values   = [];
        foreach ($models as $model) {
            $kondisis[] = $model['kondisi'];
            $values[]   = $model['jumlah'];
        }

        return view('asset-kendaraan', [
            'kondisis' => $kondisis,
            'values'   => $values,
        ]);
    }

    public function kondisi(Request $request)
    {
        $models = Gedung::groupBy('kondisi')
            ->selectRaw('kondisi, COUNT(id) as jumlah')->get()->toArray();

        $kondisis = [];
        $values   = [];
        foreach ($models as $model) {
            $kondisis[] = $model['kondisi'];
            $values[]   = $model['jumlah'];
        }

        return view('detail-kendaraan', [
            'kondisis' => $kondisis,
            'values'   => $values,
        ]);
    }
}
