<?php

namespace App\Http\Controllers;

use App\Models\Tanah;
use Illuminate\Http\Request;

class TanahFrontController extends Controller
{
    public function index(Request $request)
    {
        $models = Tanah::groupBy('kondisi')
            ->selectRaw('kondisi, COUNT(id) as jumlah')->get()->toArray();

        $kondisis = [];
        $values   = [];
        foreach ($models as $model) {
            $kondisis[] = $model['kondisi'];
            $values[]   = $model['jumlah'];
        }

        return view('asset-tanah', [
            'kondisis' => $kondisis,
            'values'   => $values,
        ]);
    }

    public function kondisi(Request $request)
    {
        $models = Tanah::groupBy('kondisi')
            ->selectRaw('kondisi, COUNT(id) as jumlah')->get()->toArray();

        $kondisis = [];
        $values   = [];
        foreach ($models as $model) {
            $kondisis[] = $model['kondisi'];
            $values[]   = $model['jumlah'];
        }

        return view('detail-tanah', [
            'kondisis' => $kondisis,
            'values'   => $values,
        ]);
    }
}
