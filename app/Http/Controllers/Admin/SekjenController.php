<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Sekjen;
use App\Models\Log\LogInputExcel;
use Illuminate\Http\Request;

class SekjenController extends Controller
{

    public function list(Request $request)
    {
        return view('admin.sekjen.list-sekjen');
    }

    public function inputSekjenExcel(Request $request)
    {
        $logInputExcel = LogInputExcel::orderBy('created_at', 'desc')->take(5)->get();

        return view('admin.kendaraan.input-excel-kendaraan-dinas', [
            'logs' => $logInputExcel,
        ]);
    }

    public function editSekjen(Request $request, $id)
    {
        $Sekjen = Sekjen::where('id', $id)->firstOrFail();

        return view('admin.kendaraan.edit-sekjen', [
            'Sekjen' => $Sekjen,
        ]);
    }
}
