<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Log\LogInputExcel;
use Illuminate\Http\Request;

class PerwakilanKendaraanController extends Controller
{
    public function list(Request $request, $filter = null)
    {
        return view('admin.perwakilanKendaraan.list-perwakilan-kendaraan'
        , [
            'filter' => $filter,
        ]);
    }
}
