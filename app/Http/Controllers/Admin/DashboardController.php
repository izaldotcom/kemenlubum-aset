<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\NotificationServices;
use App\Services\PajakNAsuransiServices;
use App\Services\PengajuanService;
use App\Services\SummaryAssetServices;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
//        \DB::table('kendaraan_dinas')->update([
//            'status_pajak'     => 'clear',
//            'status_pajak_rfs' => 'clear',
//            'status_asuransi'  => 'clear',
//        ]);

        PajakNAsuransiServices::checkForDuePajak();
        PengajuanService::checkForNewPenghapusan();

        $email = "mahar.catur@gmail.com";

//        MailServiceProvider::wrapperMailSend('emailtemplate.email_common', [
//            'introLines' => [
//                "Test Intro Line"
//            ],
//            'level'      => 'primary',
//        ], function ($m) use ($email) {
//            $m->to($email, $email)->subject("Inquiry Subject Email");
//        }, $email, "Inquiry Subject Email", 'test_mail');

        $summary = SummaryAssetServices::getMySummaryAsset();

        return view('admin.dashboard', [
            'summary' => $summary,
        ]);
    }

    public function notification(Request $request)
    {
        $notifications = NotificationServices::getMyNotifications();
        return view('admin.notification', [
            'notifications' => $notifications,
        ]);
    }
}
