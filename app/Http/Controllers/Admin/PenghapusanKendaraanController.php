<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Kendaraan\PengajuanPenghapusan;
use App\Models\Kendaraan\PengajuanPenghapusanDetail;
use App\Models\Kendaraan\PengajuanPenghapusanKendaraan;
use App\Services\PdfMergerServices;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;
\Carbon\Carbon::setLocale('id');

class PenghapusanKendaraanController extends Controller
{
    public function list(Request $request)
    {
        return view('admin.penghapusanKendaraan.list');
    }

    public function listPenghapusan(Request $request, $code = null)
    {
        return view('admin.penghapusanKendaraan.list', [
            'code' => $code,
        ]);
    }
    public function detail(Request $request, $id)
    {
        $pengajuanPenghapusan = PengajuanPenghapusan::where('id', $id)->firstOrFail();

        return view('admin.penghapusanKendaraan.detail', [
            'pengajuanPenghapusan' => $pengajuanPenghapusan,
        ]);
    }

    public function download(Request $request, $id, $step, $regenerate = false)
    {
        $pengajuanPenghapusan = PengajuanPenghapusan::where('id', $id)->firstOrFail();
        if ($regenerate) {
            $pengajuanPenghapusanDetail         = PengajuanPenghapusanDetail::firstOrNew([
                'step'                     => $step,
                'pengajuan_penghapusan_id' => $id,
            ]);
            $pengajuanPenghapusanDetail->status = 'waiting_for_final';

            $title = "{$pengajuanPenghapusan->id}-{$step}-template";

            $listMonth = array("Januari", "Februari", "Maret", "April","Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
$attributeFiller = $pengajuanPenghapusan->getAttributeTemplate($step);

            $hari = "";
            $bulan = "";

            if (!empty($pengajuanPenghapusanDetail->created_at)) {
                switch ($pengajuanPenghapusanDetail->created_at->format('F')) {
                    case 1 :
                        $bulan = "Januari";
                        break;
                    case 2 :
                        $bulan = "Februari";
                        break;
                    case 3 :
                        $bulan = "Maret";
                        break;
                    case 4 :
                        $bulan = "April";
                        break;
                    case 5 :
                        $bulan = "Mei";
                        break;
                    case 6 :
                        $bulan = "Juni";
                        break;
                    case 7 :
                        $bulan = "Juli";
                        break;
                    case 8 :
                        $bulan = "Agustus";
                        break;
                    case 9 :
                        $bulan = "September";
                        break;
                    case 10 :
                        $bulan = "Oktober";
                        break;
                    case 11 :
                        $bulan = "November";
                        break;
                    case 12 :
                        $bulan = "Desember";
                        break;
                }
                $attributeFiller['tanggalSurat'] =  date("d") . " ".  $listMonth[date("n")-1] . " " . date("Y");
                switch ($pengajuanPenghapusanDetail->created_at->format('N')) {
                    case 1 :
                        $hari = "Senin";
                        break;
                    case 2 :
                        $hari = "Selasa";
                        break;
                    case 3 :
                        $hari = "Rabu";
                        break;
                    case 4 :
                        $hari = "Kamis";
                        break;
                    case 5 :
                        $hari = "Jumat";
                        break;
                    case 6 :
                        $hari = "Sabtu";
                        break;
                    case 7 :
                        $hari = "Minggu";
                        break;
                }
                $attributeFiller['tanggalSuratSebut'] = $hari . " , ". date("d") . " ".  $listMonth[date("n")-1] . " " . date("Y");
            } else {
                $attributeFiller['tanggalSurat'] = date("d") . " ".  $listMonth[date("n")-1] . " " . date("Y");
                switch (date('d F Y')) {
                    case 1 :
                        $hari = "Senin";
                        break;
                    case 2 :
                        $hari = "Selasa";
                        break;
                    case 3 :
                        $hari = "Rabu";
                        break;
                    case 4 :
                        $hari = "Kamis";
                        break;
                    case 5 :
                        $hari = "Jumat";
                        break;
                    case 6 :
                        $hari = "Sabtu";
                        break;
                    case 7 :
                        $hari = "Minggu";
                        break;
                }
                $attributeFiller['tanggalSuratSebut'] = $hari . ", " . date("d") . " ".  $listMonth[date("n")-1] . " " . date("Y");
            }

            if ($step == '2') {
                if ($pengajuanPenghapusan->kategori_penghapusan_id == 3)
                $templateProcessor = new TemplateProcessor('template/perwakilan/02_BERITA_ACARA_PENELITIAN_PEMUSNAHAN.docx');
                else if ($pengajuanPenghapusan->kategori_penghapusan_id == 1)
                $templateProcessor = new TemplateProcessor('template/perwakilan/02_BERITA_ACARA_PENELITIAN_TDK_LELANG.docx');
                else if ($pengajuanPenghapusan->kategori_penghapusan_id == 2)
                $templateProcessor = new TemplateProcessor('template/perwakilan/02_BERITA_ACARA_PENELITIAN_SECARA_LELANG.docx');
                else if ($pengajuanPenghapusan->kategori_penghapusan_id == 4)
                    $templateProcessor = new TemplateProcessor('template/perwakilan/02_BERITA_ACARA_PENELITIAN.docx');

                $templateProcessor->setValue('tanggalSurat', $attributeFiller['tanggalSurat']);
                $templateProcessor->setValue('tanggalSuratSebut', $attributeFiller['tanggalSuratSebut']);
                $templateProcessor->setValue('noSurat', $attributeFiller['noSurat']);
                $templateProcessor->setValue('noSuratKepala', $attributeFiller['noSuratKepala']);
                $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);
                $templateProcessor->setValue('alasanKategoriPenghapusan', $attributeFiller['alasanKategoriPenghapusan']);
                $templateProcessor->setValue('namaSatker', $attributeFiller['namaSatker']);
                $templateProcessor->setValue('namaSatkerr', $attributeFiller['namaSatkerr']);

                $templateProcessor->setValue('namaKota', $attributeFiller['namaKota']);
                $templateProcessor->setValue('totalUnit', $attributeFiller['totalUnit']);

                $templateProcessor->cloneRow('rowNama', $attributeFiller['jumlahAnggotas']);
                $templateProcessor->cloneRow('rowNama', $attributeFiller['jumlahAnggotas']);
                $templateProcessor->cloneRow('rowLampiranJabatan', $attributeFiller['jumlahAnggotas']);

                $templateProcessor->cloneRow('rowNo', $attributeFiller['jumlahKendaraans']);

                $i = 1;
                foreach ($attributeFiller['anggotas'] as $anggota) {
                    $attributeFiller['rowNama#' . $i]    = $anggota['nama'];
                    $attributeFiller['rowJabatan#' . $i] = $anggota['jabatan_tim_panitia'];

                    $attributeFiller['rowLampiranNama#' . $i]    = $anggota['nama'];
                    $attributeFiller['rowLampiranJabatan#' . $i] = $anggota['jabatan_tim_panitia'];

                    $templateProcessor->setValue('rowNama#' . $i, $attributeFiller['rowNama#' . $i]);
                    $templateProcessor->setValue('rowJabatan#' . $i, $attributeFiller['rowJabatan#' . $i]);

                    $templateProcessor->setValue('rowLampiranNama#' . $i, $attributeFiller['rowLampiranNama#' . $i]);
                    $templateProcessor->setValue('rowLampiranJabatan#' . $i, $attributeFiller['rowLampiranJabatan#' . $i]);
                    $i = $i + 1;
                }

                $i                   = 1;
                $nilaiPerolehanTotal = 0;
                $nilaiWajarTotal     = 0;
                $nilaiLimitTotal     = 0;
                foreach ($attributeFiller['kendaraans'] as $kendaraan) {
                    /** @var PengajuanPenghapusanKendaraan $kendaraan */
//                    $kendaraanDinas = "Kendaraan Dinas {$kendaraan->kendaraan->merk_type}, nomor rangka {$kendaraan->no_rangka}, tahun pembuatan {$kendaraan->tahun_pembuatan}, ";
                    $kendaraanDinas = "Kendaraan Dinas {$kendaraan->kendaraan->merk_type}, nomor rangka {$kendaraan->no_rangka}, tahun pembuatan {$kendaraan->tahun_pembuatan}.";

//                    $kendaraanDinas .= $kendaraan->alasan_penghapusan . ". ";
//                    $nilaiGanti     = number_format($kendaraan->nilai_ganti, 0, ',', '.');
//                    $nilaiBuku     = number_format($kendaraan->kendaraan->nilai_buku, 0, ',', '.');
//                    $kendaraanDinas .= "beberapa komponen kendaraan perlu mengalami penggantian dengan nilai total sebesar Rp.{$nilaiGanti}. Sementara itu, nilai buku yang dimiliki sebesar Rp.{$nilaiBuku}, sehingga dari segi ekonomis dan umur kendaraan, kendaraan tersebut memerlukan biaya perawatan yang lebih besar dari nilai buku.";
//                    if ($kendaraan->tipe == 'umur') {
//                        $kendaraanDinas .= $kendaraan->alasan_penghapusan;
//                    } elseif ($kendaraan->tipe == 'nilai_limit') {
//                        $nilaiGanti     = number_format($kendaraan->nilai_ganti, 0, ',', '.');
//                        $nilaiWajar     = number_format($kendaraan->nilai_wajar, 0, ',', '.');
//                        $kendaraanDinas .= "beberapa komponen kendaraan perlu mengalami penggantian dengan nilai total sebesar Rp.{$nilaiGanti}. Sementara itu, nilai buku yang dimiliki sebesar Rp.{$nilaiWajar}, sehingga dari segi ekonomis dan umur kendaraan, kendaraan tersebut memerlukan biaya perawatan yang lebih besar dari nilai buku.";
//                    }

                    $attributeFiller['rowKendaraanDinas#' . $i] = $kendaraanDinas;
                    $templateProcessor->setValue('rowKendaraanDinas#' . $i, $attributeFiller['rowKendaraanDinas#' . $i]);


                    $nilaiPerolehanTotal += floatval($kendaraan->kendaraan->nilai_perolehan);
                    $nilaiWajarTotal     += floatval($kendaraan->nilai_wajar);
                    $nilaiLimitTotal     += floatval($kendaraan->nilai_limit);

                    $attributeFiller['rowNo#' . $i]             = $i;
                    $attributeFiller['rowNUP#' . $i]            = $kendaraan->kendaraan->nup;
                    $attributeFiller['rowNamaBarang#' . $i]     = $kendaraan->kendaraan->nama_barang;
                    $attributeFiller['rowKodeBarang#' . $i]     = $kendaraan->kendaraan->kode_barang;
                    $attributeFiller['rowMerk#' . $i]           = $kendaraan->kendaraan->merk_type;
                    $attributeFiller['rowTahun#' . $i]          = (new \DateTime($kendaraan->kendaraan->tanggal_perolehan))->format('Y');
                    $attributeFiller['rowJumlah#' . $i]         = 1;
                    $attributeFiller['rowNilaiPerolehan#' . $i] = number_format($kendaraan->kendaraan->nilai_perolehan, 0, ',', '.');
                    $attributeFiller['rowNilaiWajar#' . $i]     = number_format($kendaraan->nilai_wajar, 0, ',', '.');
                    $attributeFiller['rowNilaiLimit#' . $i]     = number_format($kendaraan->nilai_limit, 0, ',', '.');
                    $attributeFiller['rowKondisiBarang#' . $i]  = $kendaraan->kendaraan->kondisi;
                    $templateProcessor->setValue('rowNo#' . $i, $attributeFiller['rowNo#' . $i]);
                    $templateProcessor->setValue('rowNUP#' . $i, $attributeFiller['rowNUP#' . $i]);
                    $templateProcessor->setValue('rowNamaBarang#' . $i, $attributeFiller['rowNamaBarang#' . $i]);
                    $templateProcessor->setValue('rowKodeBarang#' . $i, $attributeFiller['rowKodeBarang#' . $i]);
                    $templateProcessor->setValue('rowMerk#' . $i, $attributeFiller['rowMerk#' . $i]);
                    $templateProcessor->setValue('rowTahun#' . $i, $attributeFiller['rowTahun#' . $i]);
                    $templateProcessor->setValue('rowJumlah#' . $i, $attributeFiller['rowJumlah#' . $i]);
                    $templateProcessor->setValue('rowNilaiPerolehan#' . $i, $attributeFiller['rowNilaiPerolehan#' . $i]);
                    $templateProcessor->setValue('rowNilaiWajar#' . $i, $attributeFiller['rowNilaiWajar#' . $i]);
                    $templateProcessor->setValue('rowNilaiLimit#' . $i, $attributeFiller['rowNilaiLimit#' . $i]);
                    $templateProcessor->setValue('rowKondisiBarang#' . $i, $attributeFiller['rowKondisiBarang#' . $i]);

                    $i = $i + 1;
                }

                $attributeFiller['rowNilaiPerolehanSum'] = number_format($nilaiPerolehanTotal, 0, ',', '.');
                $templateProcessor->setValue('rowNilaiPerolehanSum', $attributeFiller['rowNilaiPerolehanSum']);
                $attributeFiller['rowNilaiWajarSum'] = number_format($nilaiWajarTotal, 0, ',', '.');
                $templateProcessor->setValue('rowNilaiWajarSum', $attributeFiller['rowNilaiWajarSum']);
                $attributeFiller['rowNilaiLimitSum'] = number_format($nilaiLimitTotal, 0, ',', '.');
                $templateProcessor->setValue('rowNilaiLimitSum', $attributeFiller['rowNilaiLimitSum']);

                $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
                $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
                if (!\File::isDirectory($path)) {
                    \File::makeDirectory($path, 0777, true, true);
                }
                $templateProcessor->saveAs($dirname);

                $file           = new File();
                $file->filename = "{$title}.docx";
                $file->location = $dirname;
                $file->save();

                $pengajuanPenghapusanDetail->template_id = $file->id;
                $pengajuanPenghapusanDetail->save();
            } elseif ($step == '10') {
                $templateProcessor = new TemplateProcessor('template/perwakilan/10_FOTO_NEW.docx');

                $templateProcessor->cloneBlock('cloneBlock', count($attributeFiller['kendaraans']), true, true);

                $i = 1;
                foreach ($attributeFiller['kendaraans'] as $kendaraan) {
                    /** @var PengajuanPenghapusanKendaraan $kendaraan */
                    $kendaraanDinas = "Kendaraan Dinas {$kendaraan->kendaraan->merk_type}, nomor rangka {$kendaraan->no_rangka}, tahun pembuatan {$kendaraan->tahun_pembuatan}, ";

                    if ($kendaraan->tipe == 'umur') {
                        $kendaraanDinas .= $kendaraan->alasan_penghapusan;
                    } elseif ($kendaraan->tipe == 'nilai_limit') {
                        $nilaiGanti     = number_format($kendaraan->nilai_ganti, 0, ',', '.');
                        $nilaiWajar     = number_format($kendaraan->nilai_wajar, 0, ',', '.');
                        $kendaraanDinas .= "beberapa komponen kendaraan perlu mengalami penggantian dengan nilai total sebesar Rp.{$nilaiGanti}. Sementara itu, nilai buku yang dimiliki sebesar Rp.{$nilaiWajar}, sehingga dari segi ekonomis dan umur kendaraan, kendaraan tersebut memerlukan biaya perawatan yang lebih besar dari nilai buku.";
                    }

                    $attributeFiller['rowKendaraanDinas#' . $i] = $kendaraanDinas;
                    $templateProcessor->setValue('rowKendaraanDinas#' . $i, $attributeFiller['rowKendaraanDinas#' . $i]);
                    $attributeFiller['namaSatker#' . $i]     = $pengajuanPenghapusan->satker->nama_satker;
                    $attributeFiller['namaSatkerr#' . $i]     = $pengajuanPenghapusan->satker->nama_satkerr;
                    $attributeFiller['namaKota#' . $i]     = $pengajuanPenghapusan->satker->nama_kota;
                    $attributeFiller['namaBarang#' . $i]     = $kendaraan->kendaraan->nama_barang;
                    $attributeFiller['kodeBarang#' . $i]     = "{$kendaraan->kendaraan->kode_barang} - {$kendaraan->kendaraan->nup}";
                    $attributeFiller['merk#' . $i]           = $kendaraan->kendaraan->merk_type;
                    $attributeFiller['tahunPembuatan#' . $i] = $kendaraan->tahun_pembuatan;
                    $attributeFiller['tahunPerolehan#' . $i] = (new \DateTime($kendaraan->kendaraan->tanggal_perolehan))->format('Y');
                    $templateProcessor->setValue('namaSatker#' . $i, $attributeFiller['namaSatker#' . $i]);
                    $templateProcessor->setValue('namaSatkerr#' . $i, $attributeFiller['namaSatkerr#' . $i]);
                    $templateProcessor->setValue('namaKota#' . $i, $attributeFiller['namaKota#' . $i]);
                    $templateProcessor->setValue('namaBarang#' . $i, $attributeFiller['namaBarang#' . $i]);
                    $templateProcessor->setValue('kodeBarang#' . $i, $attributeFiller['kodeBarang#' . $i]);
                    $templateProcessor->setValue('merk#' . $i, $attributeFiller['merk#' . $i]);
                    $templateProcessor->setValue('tahunPembuatan#' . $i, $attributeFiller['tahunPembuatan#' . $i]);
                    $templateProcessor->setValue('tahunPerolehan#' . $i, $attributeFiller['tahunPerolehan#' . $i]);

                    $kendaraanPhotos = $kendaraan->kendaraan->photoFiles;

                    if (!empty($kendaraanPhotos)) {
                        try {
                            $templateProcessor->cloneRow('foto#' . $i, count($kendaraanPhotos));
                        } catch (\Exception $e) {
                        }
                        $j = 1;
                        foreach ($kendaraanPhotos as $kendaraanPhoto) {
                            /** @var KendaraanPhoto $kendaraanPhoto */
                            $templateProcessor->setImageValue('foto#' . $i . '#' . $j, [
                                'path' => $kendaraanPhoto->file->location, 'ratio' => true,
                            ]);
                            $j++;
                        }
                    }

                    $i = $i + 1;
                }

                $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
                $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
                if (!\File::isDirectory($path)) {
                    \File::makeDirectory($path, 0777, true, true);
                }
                $templateProcessor->saveAs($dirname);

                $file           = new File();
                $file->filename = "{$title}.docx";
                $file->location = $dirname;
                $file->save();

                $pengajuanPenghapusanDetail->template_id = $file->id;
                $pengajuanPenghapusanDetail->save();
            } elseif ($step == '11') {
                if ($pengajuanPenghapusan->kategori_penghapusan_id == 3)
                $templateProcessor = new TemplateProcessor('template/perwakilan/11_SP.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 1)
                $templateProcessor = new TemplateProcessor('template/perwakilan/11_SP Tusi.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2)
                $templateProcessor = new TemplateProcessor('template/perwakilan/11_SP Tusi_Lelang.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4)
                $templateProcessor = new TemplateProcessor('template/perwakilan/11_SP penghapusan Barang Milik Negara (BMN).docx');

                $templateProcessor->setValue('tanggalSurat', $attributeFiller['tanggalSurat']);
                $templateProcessor->setValue('nama', $attributeFiller['nama']);
                $templateProcessor->setValue('nip', $attributeFiller['nip']);
                $templateProcessor->setValue('pangkat', $attributeFiller['pangkat']);
                $templateProcessor->setValue('jabatan', $attributeFiller['jabatan']);
                $templateProcessor->setValue('namaSatker', $attributeFiller['namaSatker']);
                $templateProcessor->setValue('namaSatkerr', $attributeFiller['namaSatkerr']);

                $templateProcessor->setValue('namaKota', $attributeFiller['namaKota']);
                $templateProcessor->setValue('negaraSatker', $attributeFiller['negaraSatker']);
                $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);

                $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
                $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
                if (!\File::isDirectory($path)) {
                    \File::makeDirectory($path, 0777, true, true);
                }
                $templateProcessor->saveAs($dirname);

                $file           = new File();
                $file->filename = "{$title}.docx";
                $file->location = $dirname;
                $file->save();

                $pengajuanPenghapusanDetail->template_id = $file->id;
                $pengajuanPenghapusanDetail->save();
            } elseif ($step == '12') {
                if ($pengajuanPenghapusan->kategori_penghapusan_id == 3)
                $templateProcessor = new TemplateProcessor('template/perwakilan/12_SP.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 1)
                $templateProcessor = new TemplateProcessor('template/perwakilan/12_SP Nilai Limit_Tidak_Lelang.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2)
                $templateProcessor = new TemplateProcessor('template/perwakilan/12_SP Nilai Limit_Lelang.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4)
                $templateProcessor = new TemplateProcessor('template/perwakilan/12_SP Nilai Limit Penghapusan.docx');

                $templateProcessor->setValue('tanggalSurat', $attributeFiller['tanggalSurat']);
                $templateProcessor->setValue('nama', $attributeFiller['nama']);
                $templateProcessor->setValue('nip', $attributeFiller['nip']);
                $templateProcessor->setValue('pangkat', $attributeFiller['pangkat']);
                $templateProcessor->setValue('jabatan', $attributeFiller['jabatan']);
                $templateProcessor->setValue('namaSatker', $attributeFiller['namaSatker']);
                $templateProcessor->setValue('namaSatkerr', $attributeFiller['namaSatkerr']);
                $templateProcessor->setValue('namaKota', $attributeFiller['namaKota']);
                $templateProcessor->setValue('negaraSatker', $attributeFiller['negaraSatker']);
                $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);

                $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
                $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
                if (!\File::isDirectory($path)) {
                    \File::makeDirectory($path, 0777, true, true);
                }
                $templateProcessor->saveAs($dirname);

                $file           = new File();
                $file->filename = "{$title}.docx";
                $file->location = $dirname;
                $file->save();

                $pengajuanPenghapusanDetail->template_id = $file->id;
                $pengajuanPenghapusanDetail->save();

            } elseif ($step == '101') {
                if ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor('template/sekjen/1_Penjualan_Tidak_Lelang_1Unit/01_NOTA_PENGANTAR_SEKJEN_NEW.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/1_Penjualan_Tidak_Lelang_Lebih1Unit/01_NOTA_PENGANTAR_SEKJEN_NEW.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_1Unit/01_NOTA_PENGANTAR_SEKJEN_NEW.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_Lebih1Unit/01_NOTA_PENGANTAR_SEKJEN_NEW.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/3_Pemusnahan_1Unit/01_NOTA_PENGANTAR_SEKJEN_NEW.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/3_Pemusnahan_Lebih1Unit/01_NOTA_PENGANTAR_SEKJEN_NEW.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/4_Penghapusan_1Unit/01_NOTA_PENGANTAR_SEKJEN_NEW.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/4_Penghapusan_Lebih1Unit/01_NOTA_PENGANTAR_SEKJEN_NEW.docx');

                $templateProcessor->setValue('noSuratKeputusan', $attributeFiller['noSuratKeputusan']);
                $templateProcessor->setValue('tanggalBeritaAcara', $attributeFiller['tanggalBeritaAcara']);
                $templateProcessor->setValue('noBeritaAcara', $attributeFiller['noBeritaAcara']);
                $templateProcessor->setValue('noSurat', $attributeFiller['noSurat']);
                $templateProcessor->setValue('lampiran', "1 (satu)");
                $templateProcessor->setValue('tanggalSurat', $attributeFiller['tanggalSurat']);
                $templateProcessor->setValue('namaPernyataan', $attributeFiller['namaPernyataan']);
                $templateProcessor->setValue('namaSekjen', $attributeFiller['namaSekjen']);
                $templateProcessor->setValue('nipSekjen', $attributeFiller['nipSekjen']);
                $templateProcessor->setValue('nipPernyataan', $attributeFiller['nipPernyataan']);
                $templateProcessor->setValue('pangkatPernyataan', $attributeFiller['pangkatPernyataan']);
                $templateProcessor->setValue('jabatanPernyataan', $attributeFiller['jabatanPernyataan']);
                $templateProcessor->setValue('namaSatker', $attributeFiller['namaSatker']);
		$templateProcessor->setValue('namaSatkerr', $attributeFiller['namaSatkerr']);
                $templateProcessor->setValue('namaKota', $attributeFiller['namaKota']);
                $templateProcessor->setValue('negaraSatker', $attributeFiller['negaraSatker']);
                $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);
                $templateProcessor->setValue('totalUnit', $attributeFiller['totalUnit']);
                $templateProcessor->setValue('totalNilaiTaksiran', $attributeFiller['totalNilaiTaksiranFormatted']);
                $templateProcessor->setValue('totalNilaiPerolehan', $attributeFiller['totalNilaiPerolehanFormatted']);
                $templateProcessor->setValue('totalNilaiTaksiranTerbilang', $attributeFiller['totalNilaiTaksiranTerbilang']);
                $templateProcessor->setValue('totalNilaiPerolehanTerbilang', $attributeFiller['totalNilaiPerolehanTerbilang']);
                $templateProcessor->setValue('totalNilaiPerolehan', $attributeFiller['totalNilaiPerolehanFormatted']);
                $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
                $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
                if (!\File::isDirectory($path)) {
                    \File::makeDirectory($path, 0777, true, true);
                }
                $templateProcessor->saveAs($dirname);

                $file           = new File();
                $file->filename = "{$title}.docx";
                $file->location = $dirname;
                $file->save();

                $pengajuanPenghapusanDetail->template_id = $file->id;
                $pengajuanPenghapusanDetail->save();
            } elseif ($step == '102') {
                if ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor('template/sekjen/1_Penjualan_Tidak_Lelang_1Unit/02_SURAT_USULAN_PENJUALAN 1 UNIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/1_Penjualan_Tidak_Lelang_Lebih1Unit/02_SURAT_USULAN_PENJUALAN LEBIH 1 UNIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_1Unit/02_USULAN_PENJUALAN_PUSAT_1_UNIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_Lebih1Unit/02_USULAN_PENJUALAN_PUSAT_LEBIH_1_UNIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/3_Pemusnahan_1Unit/02_SURAT_USULAN_PEMUSNAHAN-1-UNIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/3_Pemusnahan_Lebih1Unit/02_SURAT_USULAN_PEMUSNAHAN-LEBIH-1-UNIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/4_Penghapusan_1Unit/02_SURAT_USULAN_PEMUSNAHAN-LEBIH-1-UNIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/4_Penghapusan_Lebih1Unit/02_SURAT_USULAN_PEMUSNAHAN-LEBIH-1-UNIT.docx');

                $templateProcessor->setValue('noSuratKeputusan', $attributeFiller['noSuratKeputusan']);
                $templateProcessor->setValue('tanggalBeritaAcara', $attributeFiller['tanggalBeritaAcara']);
                $templateProcessor->setValue('totalNilaiTaksiranTerbilang', $attributeFiller['totalNilaiTaksiranTerbilang']);
                $templateProcessor->setValue('totalNilaiPerolehanTerbilang', $attributeFiller['totalNilaiPerolehanTerbilang']);
                $templateProcessor->setValue('noBeritaAcara', $attributeFiller['noBeritaAcara']);
                $templateProcessor->setValue('noSurat', $attributeFiller['noSurat']);
                $templateProcessor->setValue('namaSekjen', $attributeFiller['namaSekjen']);
                $templateProcessor->setValue('nipSekjen', $attributeFiller['nipSekjen']);
                $templateProcessor->setValue('lampiran', "1 (satu)");
                $templateProcessor->setValue('tanggalSurat', $attributeFiller['tanggalSurat']);
                $templateProcessor->setValue('namaPernyataan', $attributeFiller['namaPernyataan']);
                $templateProcessor->setValue('nipPernyataan', $attributeFiller['nipPernyataan']);
                $templateProcessor->setValue('pangkatPernyataan', $attributeFiller['pangkatPernyataan']);
                $templateProcessor->setValue('jabatanPernyataan', $attributeFiller['jabatanPernyataan']);
		$templateProcessor->setValue('alasanKategoriPenghapusan', $attributeFiller['alasanKategoriPenghapusan']);
                $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);
                $templateProcessor->setValue('namaKota', $attributeFiller['namaKota']);
                $templateProcessor->setValue('namaSatker', $attributeFiller['namaSatker']);
		$templateProcessor->setValue('namaSatkerr', $attributeFiller['namaSatkerr']);
                $templateProcessor->setValue('negaraSatker', $attributeFiller['negaraSatker']);
                $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);
                $templateProcessor->setValue('totalUnit', $attributeFiller['totalUnit']);
                $templateProcessor->setValue('totalNilaiTaksiran', $attributeFiller['totalNilaiTaksiranFormatted']);
                $templateProcessor->setValue('totalNilaiPerolehan', $attributeFiller['totalNilaiPerolehanFormatted']);

                $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
                $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
                if (!\File::isDirectory($path)) {
                    \File::makeDirectory($path, 0777, true, true);
                }
                $templateProcessor->saveAs($dirname);

                $file           = new File();
                $file->filename = "{$title}.docx";
                $file->location = $dirname;
                $file->save();

                $pengajuanPenghapusanDetail->template_id = $file->id;
                $pengajuanPenghapusanDetail->save();
            } elseif ($step == '103') {
                if ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor('template/sekjen/1_Penjualan_Tidak_Lelang_1Unit/03_PERNYATAAN_TUSI.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/1_Penjualan_Tidak_Lelang_Lebih1Unit/03_PERNYATAAN_TUSI.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_1Unit/03_PERNYATAAN_TUSI.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_Lebih1Unit/03_PERNYATAAN_TUSI.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/3_Pemusnahan_1Unit/03_PERNYATAAN_TUSI.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/3_Pemusnahan_Lebih1Unit/03_PERNYATAAN_TUSI.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/4_Penghapusan_1Unit/03_PERNYATAAN_TUSI.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/4_Penghapusan_Lebih1Unit/03_PERNYATAAN_TUSI.docx');

                $templateProcessor->setValue('tanggalSurat', $attributeFiller['tanggalSurat']);
                $templateProcessor->setValue('namaPernyataan', $attributeFiller['namaPernyataan']);
                $templateProcessor->setValue('nipPernyataan', $attributeFiller['nipPernyataan']);
                $templateProcessor->setValue('namaSekjen', $attributeFiller['namaSekjen']);
                $templateProcessor->setValue('nipSekjen', $attributeFiller['nipSekjen']);
                $templateProcessor->setValue('pangkatPernyataan', $attributeFiller['pangkatPernyataan']);
                $templateProcessor->setValue('jabatanPernyataan', $attributeFiller['jabatanPernyataan']);
                $templateProcessor->setValue('namaSatker', $attributeFiller['namaSatker']);
		$templateProcessor->setValue('namaSatkerr', $attributeFiller['namaSatkerr']);
                $templateProcessor->setValue('namaKota', $attributeFiller['namaKota']);
                $templateProcessor->setValue('negaraSatker', $attributeFiller['negaraSatker']);
                $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);
                $templateProcessor->setValue('totalUnit', $attributeFiller['totalUnit']);

                $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
                $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
                if (!\File::isDirectory($path)) {
                    \File::makeDirectory($path, 0777, true, true);
                }
                $templateProcessor->saveAs($dirname);

                $file           = new File();
                $file->filename = "{$title}.docx";
                $file->location = $dirname;
                $file->save();

                $pengajuanPenghapusanDetail->template_id = $file->id;
                $pengajuanPenghapusanDetail->save();
            } elseif ($step == '104') {
                if ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor('template/sekjen/1_Penjualan_Tidak_Lelang_1Unit/04_PERNYATAAN_LIMIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/1_Penjualan_Tidak_Lelang_Lebih1Unit/04_PERNYATAAN_LIMIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_1Unit/04_PERNYATAAN_LIMIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_Lebih1Unit/04_PERNYATAAN_LIMIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/3_Pemusnahan_1Unit/04_PERNYATAAN_LIMIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/3_Pemusnahan_Lebih1Unit/04_PERNYATAAN_LIMIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/4_Penghapusan_1Unit/04_PERNYATAAN_LIMIT.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/4_Penghapusan_Lebih1Unit/04_PERNYATAAN_LIMIT.docx');

                $templateProcessor->setValue('tanggalSurat', $attributeFiller['tanggalSurat']);
                $templateProcessor->setValue('namaPernyataan', $attributeFiller['namaPernyataan']);
                $templateProcessor->setValue('nipPernyataan', $attributeFiller['nipPernyataan']);
                $templateProcessor->setValue('namaSekjen', $attributeFiller['namaSekjen']);
                $templateProcessor->setValue('nipSekjen', $attributeFiller['nipSekjen']);
                $templateProcessor->setValue('pangkatPernyataan', $attributeFiller['pangkatPernyataan']);
                $templateProcessor->setValue('jabatanPernyataan', $attributeFiller['jabatanPernyataan']);
                $templateProcessor->setValue('namaSatker', $attributeFiller['namaSatker']);
		$templateProcessor->setValue('namaSatkerr', $attributeFiller['namaSatkerr']);
                $templateProcessor->setValue('namaKota', $attributeFiller['namaKota']);
                $templateProcessor->setValue('negaraSatker', $attributeFiller['negaraSatker']);
                $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);
                $templateProcessor->setValue('totalUnit', $attributeFiller['totalUnit']);

                $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
                $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
                if (!\File::isDirectory($path)) {
                    \File::makeDirectory($path, 0777, true, true);
                }
                $templateProcessor->saveAs($dirname);

                $file           = new File();
                $file->filename = "{$title}.docx";
                $file->location = $dirname;
                $file->save();

                $pengajuanPenghapusanDetail->template_id = $file->id;
                $pengajuanPenghapusanDetail->save();
            } elseif ($step == '105') {
                if ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor('template/sekjen/1_Penjualan_Tidak_Lelang_1Unit/05_PERNYATAAN_PEMUSNAHAN.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 1 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/1_Penjualan_Tidak_Lelang_Lebih1Unit/05_PERNYATAAN_PEMUSNAHAN.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_1Unit/05_PERNYATAAN_PEMUSNAHAN.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 2 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/2_Penjualan_Lelang_Lebih1Unit/05_PERNYATAAN_PEMUSNAHAN.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/3_Pemusnahan_1Unit/05_PERNYATAAN_PEMUSNAHAN.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 3 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/3_Pemusnahan_Lebih1Unit/05_PERNYATAAN_PEMUSNAHAN.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] == 1)
                $templateProcessor = new TemplateProcessor  ('template/sekjen/4_Penghapusan_1Unit/05_PERNYATAAN_PEMUSNAHAN.docx');
                elseif ($pengajuanPenghapusan->kategori_penghapusan_id == 4 && $attributeFiller['totalUnit'] > 1)
                $templateProcessor = new TemplateProcessor ('template/sekjen/4_Penghapusan_Lebih1Unit/05_PERNYATAAN_PEMUSNAHAN.docx');

                $templateProcessor->setValue('tanggalSurat', $attributeFiller['tanggalSurat']);
                $templateProcessor->setValue('namaPernyataan', $attributeFiller['namaPernyataan']);
                $templateProcessor->setValue('nipPernyataan', $attributeFiller['nipPernyataan']);
                $templateProcessor->setValue('namaSekjen', $attributeFiller['namaSekjen']);
                $templateProcessor->setValue('nipSekjen', $attributeFiller['nipSekjen']);
                $templateProcessor->setValue('pangkatPernyataan', $attributeFiller['pangkatPernyataan']);
                $templateProcessor->setValue('jabatanPernyataan', $attributeFiller['jabatanPernyataan']);
                $templateProcessor->setValue('namaSatker', $attributeFiller['namaSatker']);
		$templateProcessor->setValue('namaSatkerr', $attributeFiller['namaSatkerr']);
                $templateProcessor->setValue('namaKota', $attributeFiller['namaKota']);
                $templateProcessor->setValue('negaraSatker', $attributeFiller['negaraSatker']);
                $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);
                $templateProcessor->setValue('totalUnit', $attributeFiller['totalUnit']);

                $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
                $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
                if (!\File::isDirectory($path)) {
                    \File::makeDirectory($path, 0777, true, true);
                }
                $templateProcessor->saveAs($dirname);

                $file           = new File();
                $file->filename = "{$title}.docx";
                $file->location = $dirname;
                $file->save();

                $pengajuanPenghapusanDetail->template_id = $file->id;
                $pengajuanPenghapusanDetail->save();
            }
        } else {
            $pengajuanPenghapusanDetail = PengajuanPenghapusanDetail::where('pengajuan_penghapusan_id', $pengajuanPenghapusan->id)
                ->where('step', $step)
                ->firstOrFail();
        }

        if (in_array($pengajuanPenghapusanDetail->status, ['waiting_for_final', 'rejected'])) {
            $pathToFile = public_path() . '/' . $pengajuanPenghapusanDetail->template->location;
        } else {
            $pathToFile = public_path() . '/' . $pengajuanPenghapusanDetail->file->location;
        }

        if (!file_exists($pathToFile)) {
            \App::abort('404', 'File Not Found');
        }

        return response()->download($pathToFile);
    }

    public function downloadAll(Request $request, $id, $kemkeu = null)
    {
        $pengajuanPenghapusan = PengajuanPenghapusan::where('id', $id)->firstOrFail();

        if (in_array($pengajuanPenghapusan->status, ['approved'])) {
            if (!empty($kemkeu))
                PdfMergerServices::downloadMergedPdfPengajuanPenghapusanKEMKEU($pengajuanPenghapusan);
            else
                PdfMergerServices::downloadMergedPdfPengajuanPenghapusan($pengajuanPenghapusan);
        } else if (in_array($pengajuanPenghapusan->status, ['selesai'])) {
	if (!empty($kemkeu))
                PdfMergerServices::downloadMergedPdfPengajuanPenghapusanKEMKEU($pengajuanPenghapusan);
            else
                PdfMergerServices::downloadMergedPdfPengajuanPenghapusan($pengajuanPenghapusan);
	}
		else {
            \App::abort('404', 'Berkas belum komplit');
        }

        \App::abort('404', 'Berkas belum komplit');
    }

    public function create(Request $request)
    {
        return view('admin.penghapusanKendaraan.create');
    }
}
