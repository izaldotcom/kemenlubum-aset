<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Imports\GedungImport;
use App\Imports\KendaraanDinasImport;
use App\Imports\KendaraanImport;
use App\Imports\RumahNegaraImport;
use App\Imports\TanahImport;
use App\Models\File;
use App\Models\Gedung;
use App\Models\GedungPhoto;
use App\Models\Kendaraan;
use App\Models\KendaraanPhoto;
use App\Models\Log\LogInputExcel;
use App\Models\RumahNegara;
use App\Models\RumahNegaraPhoto;
use App\Models\Satker;
use App\Models\SatkerPhoto;
use App\Models\Tanah;
use App\Models\TanahPhoto;
use App\Services\InputExcelServices;
use App\Services\LogActionServices;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InputExcelApiController extends Controller
{
    public function postInputExcel(Request $request, $tipe)
    {
        \Validator::make($request->all(), [
            'file' => ['required'],
        ])->validate();

        $file = $request->file('file');

        $logInputExcel          = new LogInputExcel();
        $logInputExcel->tipe    = $tipe;
        $logInputExcel->user_id = \Auth::id();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $logInputExcel->status = 'not_complete';

        InputExcelServices::initInputExcelSession();
        InputExcelServices::initInputExcelSessionCounter();

        try {
            if ($tipe == 'rumah') {
                \DB::table('rumah_negaras')->update(['status_sync' => 'none']);
                Excel::import(new RumahNegaraImport(), $file);
                $data_deleted = \DB::table('rumah_negaras')->where('status_sync', '=', 'none')->count();
                \DB::table('rumah_negaras')->where('status_sync', '=', 'none')->delete();
                InputExcelServices::storeInputExcelSessionCounter('data_deleted', $data_deleted);
            } elseif ($tipe == 'gedung') {
                \DB::table('gedungs')->update(['status_sync' => 'none']);
                Excel::import(new GedungImport(), $file);
                $data_deleted = \DB::table('gedungs')->where('status_sync', '=', 'none')->count();
                \DB::table('gedungs')->where('status_sync', '=', 'none')->delete();
                InputExcelServices::storeInputExcelSessionCounter('data_deleted', $data_deleted);
            } elseif ($tipe == 'tanah') {
                \DB::table('tanahs')->update(['status_sync' => 'none']);
                Excel::import(new TanahImport(), $file);
                $data_deleted = \DB::table('tanahs')->where('status_sync', '=', 'none')->count();
                \DB::table('tanahs')->where('status_sync', '=', 'none')->delete();
                InputExcelServices::storeInputExcelSessionCounter('data_deleted', $data_deleted);
            } elseif ($tipe == 'kendaraan') {
//                \DB::table('kendaraans')->update(['status_sync' => 'none']);
                Excel::import(new KendaraanImport(), $file);
                $data_deleted = \DB::table('kendaraans')->where('status_sync', '=', 'none')->count();
                \DB::table('kendaraans')->where('status_sync', '=', 'none')->delete();
                InputExcelServices::storeInputExcelSessionCounter('data_deleted', $data_deleted);
            } elseif ($tipe == 'kendaraan-dinas') {
                \DB::table('kendaraan_dinas')->update(['status_sync' => 'none']);
                Excel::import(new KendaraanDinasImport(), $file);
                $data_deleted = \DB::table('kendaraan_dinas')->where('status_sync', '=', 'none')->count();
                \DB::table('kendaraan_dinas')->where('status_sync', '=', 'none')->delete();
                InputExcelServices::storeInputExcelSessionCounter('data_deleted', $data_deleted);
            }

            if ($file) {
                $dirname  = "input-excels/{$tipe}/";
                $filename = str_random(10) . "." . $file->guessClientExtension();

                $fileModel = new File();

                $fileModel->filename = $filename;
                $fileModel->location = $dirname . $fileModel->filename;

                $file->move($dirname, $fileModel->filename);

                $fileModel->save();

                $logInputExcel->file_id = $fileModel->id;
            }

            $logInputExcel->status = 'success';
        } catch (\Exception $exception) {
            $data['result']        = 'error';
            $logInputExcel->status = 'error';
            $logInputExcel->note   = $exception->getMessage();
        }

        $logInputExcel->data_created = InputExcelServices::getInputExcelSessionCounter()['data_created'];
        $logInputExcel->data_updated = InputExcelServices::getInputExcelSessionCounter()['data_updated'];
        $logInputExcel->data_deleted = InputExcelServices::getInputExcelSessionCounter()['data_deleted'];

        $logInputExcel->save();

        return response()->json($data);
    }

    public function postLocation(Request $request, $tipe, $id)
    {
        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $model = null;
        $class = null;

        if ($tipe == 'rumah') {
            $class = RumahNegara::class;
            $model = RumahNegara::where('id', $id)
                ->firstOrFail();
        } elseif ($tipe == 'gedung') {
            $class = Gedung::class;
            $model = Gedung::where('id', $id)
                ->firstOrFail();
        } elseif ($tipe == 'tanah') {
            $class = Tanah::class;
            $model = Tanah::where('id', $id)
                ->firstOrFail();
        } elseif ($tipe == 'kendaraan') {
            $class = Kendaraan::class;
            $model = Kendaraan::where('id', $id)
                ->firstOrFail();
        } elseif ($tipe == 'satker') {
            $class = Satker::class;
            $model = Satker::where('id', $id)
                ->firstOrFail();
        }

        $oldObject = $model->toArray();

        if (!empty($model)) {
            $model->lat = $request->get('lat');
            $model->lng = $request->get('lng');
            $model->save();
        }

        $newObject = $model->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_UPDATE_LOCATION,
            'update',
            $class,
            $oldObject,
            $newObject
        );

        return response()->json($data);
    }

    public function postPhotos(Request $request, $tipe, $id)
    {
        \Validator::make($request->all(), [
            'file' => ['required'],
            'seed' => ['required'],
        ])->validate();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $model   = null;
        $dirname = "img/{$tipe}/";

        if ($tipe == 'rumah') {
            $model                  = new RumahNegaraPhoto();
            $model->rumah_negara_id = $id;
        } elseif ($tipe == 'gedung') {
            $model            = new GedungPhoto();
            $model->gedung_id = $id;
        } elseif ($tipe == 'tanah') {
            $model           = new TanahPhoto();
            $model->tanah_id = $id;
        } elseif ($tipe == 'kendaraan') {
            $model               = new KendaraanPhoto();
            $model->kendaraan_id = $id;
        } elseif ($tipe == 'satker') {
            $model            = new SatkerPhoto();
            $model->satker_id = $id;
        }


        if (!empty($model)) {
            $file = $request->file('file');

            $model->seed = $request->get("seed");

            if ($file) {
                $filename = str_random(10) . "." . $file->guessClientExtension();

                $fileModel = new File();

                $fileModel->filename = $filename;
                $fileModel->location = $dirname . $fileModel->filename;

                $file->move($dirname, $fileModel->filename);

                $fileModel->save();

                $model->file_id = $fileModel->id;
            }

            $model->save();

            $data = $model;
        }

        return response()->json($data);
    }

    public function deletePhotos(Request $request, $tipe, $id)
    {
        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        if ($tipe == 'rumah') {
            $models = RumahNegaraPhoto::query()
                ->where('id', $id)->firstOrFail();
            $models->delete();
        } elseif ($tipe == 'gedung') {
            $models = GedungPhoto::query()
                ->where('id', $id)->firstOrFail();
            $models->delete();
        } elseif ($tipe == 'tanah') {
            $models = TanahPhoto::query()
                ->where('id', $id)->firstOrFail();
            $models->delete();
        } elseif ($tipe == 'kendaraan') {
            $models = KendaraanPhoto::query()
                ->where('id', $id)->firstOrFail();
            $models->delete();
        } elseif ($tipe == 'satker') {
            $models = SatkerPhoto::query()
                ->where('id', $id)->firstOrFail();
            $models->delete();
        }

        return response()->json($data);
    }

    public function getPhotos(Request $request, $tipe)
    {
        $params = $request->get('params', false);

        if ($tipe == 'rumah') {
            $models = RumahNegaraPhoto::query()
                ->with('file')
                ->where('rumah_negara_id', $params['id']);
        } elseif ($tipe == 'gedung') {
            $models = GedungPhoto::query()
                ->with('file')
                ->where('gedung_id', $params['id']);
        } elseif ($tipe == 'tanah') {
            $models = TanahPhoto::query()
                ->with('file')
                ->where('tanah_id', $params['id']);
        } elseif ($tipe == 'kendaraan') {
            $models = KendaraanPhoto::query()
                ->with('file')
                ->where('kendaraan_id', $params['id']);
        } elseif ($tipe == 'satker') {
            $models = SatkerPhoto::query()
                ->with('file')
                ->where('satker_id', $params['id']);
        }

        $search = $request->get('search', false);
        $order  = $request->get('order', false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch ($key) {
                    default:
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function ($q) use ($search) {
                $q->where('seed', 'like', "%$search%");
            });
        }
        $count = $models->count();

        $page    = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $order_direction = $request->get('order_direction', 'asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case "seed":
                    $models = $models->orderBy($order, $order_direction);
                    break;
                default:
                    break;
            }
        }
        $models = $models->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'data'  => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }
}
