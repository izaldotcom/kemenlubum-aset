<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Barang;
use Illuminate\Http\Request;

class BarangApiController extends Controller
{
    public function get(Request $request)
    {
        $params = $request->get('params', false);
        $models = Barang::query();
//            ->with(['logo', 'thumbnail']);

        $search = $request->get('search', false);
        $order  = $request->get('order', false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch ($key) {
                    default:
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function ($q) use ($search) {
                $q->where('kode_barang', 'like', "%$search%")
                    ->orWhere('nama_barang', 'like', "%$search%");
            });
        }
        $count = $models->count();

        $page    = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $order_direction = $request->get('order_direction', 'asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case "kode_barang":
                case "nama_barang":
                    $models = $models->orderBy($order, $order_direction);
                    break;
                default:
                    break;
            }
        }
        $models = $models->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'data'  => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }
}
