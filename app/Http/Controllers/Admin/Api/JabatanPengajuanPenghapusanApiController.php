<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Kendaraan;

class JabatanPengajuanPenghapusanApiController extends Controller
{
    public function getDropdown()
    {
        return Kendaraan\JabatanPenghapusan::where('is_active', true)
            ->orderBy('seed')
            ->orderBy('name')
            ->get();
    }
}
