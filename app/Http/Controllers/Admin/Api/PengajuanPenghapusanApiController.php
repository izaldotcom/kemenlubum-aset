<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Kendaraan;
use App\Models\Notification\Notification;
use App\Services\LogActionServices;
use Illuminate\Http\Request;
use PhpOffice\PhpWord\TemplateProcessor;

class PengajuanPenghapusanApiController extends Controller
{
    public function get(Request $request)
    {
        $params = $request->get('params', false);
        $models = Kendaraan\PengajuanPenghapusan::query()
            ->with(['satker', 'kategoriPenghapusan', 'uploader', 'approver']);

//        $user = User::where('id', 7)->first();
//        dd($user->hasRole(['admin_aset']));
//        dd($user->wilayah->satkers()->pluck('kode_satker')->toArray());

        if (!\Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan'])) {
            if (\Auth::user()->hasRole(['admin_kendaraan', 'staf_kendaraan', 'admin_satker'])) {
                $kodeSatker = \Auth::user()->satker->kode_satker;
                $models->whereHas('satker', function ($models) use ($kodeSatker) {
                    $models->where('kode_satker', $kodeSatker);
                });
//                $models->where('kode_satker', \Auth::user()->satker->kode_satker);
            }
            if (\Auth::user()->hasRole(['admin_aset'])) {
                $kodeSatker = \Auth::user()->wilayah->satkers()->pluck('kode_satker')->toArray();
                $models->whereHas('satker', function ($models) use ($kodeSatker) {
                    $models->whereIn('kode_satker', $kodeSatker);
                });
//                $models->whereIn('kode_satker', \Auth::user()->wilayah->satkers()->pluck('kode_satker')->toArray());
            }
        }

        $search = $request->get('search', false);
        $order  = $request->get('order', false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch ($key) {
                    default:
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function ($q) use ($search) {
                $q->where('no_pengajuan_penghapusan', 'like', "%$search%")
                    ->orWhere('status', 'like', "%$search%");
            });
        }
        $count = $models->count();

        $page    = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if (!empty($order)) {
            $order_direction = $request->get('order_direction', 'asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case "no_pengajuan_penghapusan":
                case "status":
                    $models = $models->orderBy($order, $order_direction);
                    break;
                default:
                    $models = $models->orderBy('pengajuan_penghapusans.created_at', 'desc');
                    break;
            }
        }

        $models = $models->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'data'  => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }

    public function getVariablePimpinan(Request $request, $id)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $id)->firstOrFail();

        $miscAttribute = $pengajuanPenghapusan->misc_attribute;

        $result = [
            'result' => 'success',
            'data'   => (!empty($miscAttribute) ? $miscAttribute : []),
        ];

        return response()->json($result);
    }
    public function getVariableSignature(Request $request, $id)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $id)->firstOrFail();

        $suratPernyataanSignature = $pengajuanPenghapusan->surat_pernyataan_signature;

        $result = [
            'result' => 'success',
            'data'   => (!empty($suratPernyataanSignature) ? $suratPernyataanSignature : []),
        ];

        return response()->json($result);
    }
    public function getVariablePanitia(Request $request, $id)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $id)->firstOrFail();

        $susunanPanitia = $pengajuanPenghapusan->susunan_panitia;


        $result = [
            'result' => 'success',
            'data'   => (!empty($susunanPanitia) ? $susunanPanitia : []),
        ];

        return response()->json($result);
    }
    public function postEditPenghapusanPanitia(Request $request, $id)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $id)->firstOrFail();
        if (!empty($request->get('susunan_panitia')))
            $susunanPanitia = json_decode($request->get('susunan_panitia'));
        else
            $susunanPanitia = [];

            \Validator::make(array_merge($request->all(), ['susunan_panitia' => $susunanPanitia]), [
                'susunan_panitia'             => 'required|array|min:1',
        ])->validate();
        $pengajuanPenghapusan->susunan_panitia = $susunanPanitia;
        $pengajuanPenghapusan->save();
        $result = [
            'result' => 'success',
        ];
        return response()->json($result);
    }
    public function postEditPenghapusanSignature(Request $request, $id)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $id)->firstOrFail();

        $suratPernyataanSignature = $pengajuanPenghapusan->surat_pernyataan_signature;

        if (empty($suratPernyataanSignature))
            $suratPernyataanSignature = [];
        $pengajuanPenghapusan->surat_pernyataan_signature = [
            'nama'    => $request->get('nama'),
            'nip'     => $request->get('nip'),
            'pangkat' => $request->get('pangkat'),
            'jabatan' => $request->get('jabatan'),

        ];
        $pengajuanPenghapusan->save();
        $result = [
            'result' => 'success',
        ];
        return response()->json($result);
    }
    public function postEditPenghapusanPimpinan(Request $request, $id)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $id)->firstOrFail();

        $miscAttribute = $pengajuanPenghapusan->misc_attribute;

        if (empty($miscAttribute))
            $miscAttribute = [];

        $pengajuanPenghapusan->misc_attribute = [
            'nokeputusan' => $request->get('nokeputusan'),
            'nosurat'     => $request->get('nosurat'),
            'nama'        => $request->get('nama'),
            'nip'         => $request->get('nip'),
            'pangkat'     => $request->get('pangkat'),
            'jabatan'     => $request->get('jabatan'),
            'namasekjen'     => $request->get('namasekjen'),
            'nipsekjen'     => $request->get('nipsekjen'),
        ];

        $pengajuanPenghapusan->save();

        $details = Kendaraan\PengajuanPenghapusanDetail::where('pengajuan_penghapusan_id', $id)
            ->where('step', '>', 100)
            ->update(['status' => 'waiting_for_final']);


        $result = [
            'result' => 'success',
        ];

        return response()->json($result);
    }

    public function postCreateSekjen(Request $request)
    {
        \Validator::make(array_merge($request->all()), [
            'nip_sekjen'     => ['required'],
            'nama_sekjen'     => ['required'],
        ])->validate();

        $Sekjen            = new Kendaraan\Sekjen();
        $Sekjen->nama     = $request->get('nama_sekjen');
        $Sekjen->nip             = $request->get('nip_sekjen');

        $Sekjen->save();

        $data = [
            'result' => 'success',
            'data'   => $Sekjen,
        ];

        return response()->json($data);
    }


    public function postCreate(Request $request)
    {
        if (!empty($request->get('susunan_panitia')))
            $susunanPanitia = json_decode($request->get('susunan_panitia'));
        else
            $susunanPanitia = [];

        \Validator::make(array_merge($request->all(), ['susunan_panitia' => $susunanPanitia]), [
            'kategori_penghapusan_id'     => ['required'],
            'no_sk_kepala_perwakilan'     => ['required'],
            'no_pengajuan_penghapusan'    => ['required'],
            'no_berita_acara'             => ['required'],
            'alasan_kategori_penghapusan' => ['required'],
            'nama'                        => ['required'],
            'nip'                         => ['required'],
            'pangkat'                     => ['required'],
            'jabatan'                     => ['required'],
            'susunan_panitia'             => 'required|array|min:1',
        ])->validate();

        $pengajuanPenghapusan                              = new Kendaraan\PengajuanPenghapusan();
        $pengajuanPenghapusan->user_id                     = \Auth::id();
        $pengajuanPenghapusan->status                      = 'new';
        $pengajuanPenghapusan->kategori_penghapusan_id     = $request->get('kategori_penghapusan_id');
        $pengajuanPenghapusan->no_surat_kepala             = $request->get('no_sk_kepala_perwakilan');
        $pengajuanPenghapusan->no_pengajuan_penghapusan    = $request->get('no_pengajuan_penghapusan');
        $pengajuanPenghapusan->no_berita_acara             = $request->get('no_berita_acara');
        $pengajuanPenghapusan->alasan_kategori_penghapusan = $request->get('alasan_kategori_penghapusan');
        $pengajuanPenghapusan->keterangan                  = $request->get('keterangan');
        $pengajuanPenghapusan->satker_id                   = (empty(\Auth::user()->satker_id) ? 1 : \Auth::user()->satker_id);

        $pengajuanPenghapusan->surat_pernyataan_signature = [
            'nama'    => $request->get('nama'),
            'nip'     => $request->get('nip'),
            'pangkat' => $request->get('pangkat'),
            'jabatan' => $request->get('jabatan'),
        ];

        $pengajuanPenghapusan->susunan_panitia = $susunanPanitia;

        $pengajuanPenghapusan->save();

        $data = [
            'result' => 'success',
            'data'   => $pengajuanPenghapusan,
        ];

        return response()->json($data);
    }

    public function getDetail(Request $request, $id)
    {
        $user                 = \Auth::user();
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $id);
        if ($user->hasRole(['super_admin', 'admin', 'pimpinan'])) {
        } else {
            if ($user->hasRole(['kendaraan_perwakilan_staff'])) {
                $pengajuanPenghapusan = $pengajuanPenghapusan->where('satker_id', $user->satker_id);
            } elseif ($user->hasRole(['admin_wilayah'])) {
                $pengajuanPenghapusan = $pengajuanPenghapusan->whereHas('satker', function ($query) use ($user) {
                    $query->where('wilayah_id', $user->wilayah_id);
                });
            }
        }
        $pengajuanPenghapusan = $pengajuanPenghapusan->firstOrFail();

        $arrPengajuanPenghapusanDetails = [];

        for ($i = 1; $i < 15; $i++) {
            $pengajuanPenghapusanDetail         = Kendaraan\PengajuanPenghapusanDetail::where('pengajuan_penghapusan_id', $pengajuanPenghapusan->id)
                ->where('step', $i)
                ->first();
            $arrPengajuanPenghapusanDetails[$i] = $pengajuanPenghapusanDetail;
        }

        $data = [
            'result' => 'success',
            'data'   => $arrPengajuanPenghapusanDetails,
        ];

        return response()->json($data);
    }


    public function getDetailPimpinan(Request $request, $id)
    {
        $user                 = \Auth::user();
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $id);
        if ($user->hasRole(['super_admin', 'admin', 'pimpinan'])) {
        } else {
            if ($user->hasRole(['kendaraan_perwakilan_staff'])) {
                $pengajuanPenghapusan = $pengajuanPenghapusan->where('satker_id', $user->satker_id);
            } elseif ($user->hasRole(['admin_wilayah'])) {
                $pengajuanPenghapusan = $pengajuanPenghapusan->whereHas('satker', function ($query) use ($user) {
                    $query->where('wilayah_id', $user->wilayah_id);
                });
            }
        }
        $pengajuanPenghapusan = $pengajuanPenghapusan->firstOrFail();

        $arrPengajuanPenghapusanDetails = [];

        for ($i = 101; $i < 106; $i++) {
            $pengajuanPenghapusanDetail         = Kendaraan\PengajuanPenghapusanDetail::where('pengajuan_penghapusan_id', $pengajuanPenghapusan->id)
                ->where('step', $i)
                ->first();
            $arrPengajuanPenghapusanDetails[$i] = $pengajuanPenghapusanDetail;
        }

        $data = [
            'result' => 'success',
            'data'   => $arrPengajuanPenghapusanDetails,
        ];

        return response()->json($data);
    }

    public function getValidate(Request $request, $tipe, $penghapusanid, $step)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::findOrFail($penghapusanid);

        $pengajuanPenghapusanDetail = Kendaraan\PengajuanPenghapusanDetail::where('pengajuan_penghapusan_id', $pengajuanPenghapusan->id)
            ->where('step', '<=', 100)
            ->where('step', $step)->first();

        if (!empty($pengajuanPenghapusanDetail)) {
            $pengajuanPenghapusanDetail->approver_id = \Auth::id();
            if ($tipe == 'approve') {
                $pengajuanPenghapusanDetail->status = 'approved';
            } elseif ($tipe == 'lock'){
                $pengajuanPenghapusanDetail->status = 'locked';
            } elseif ($tipe == 'reject') {
                $pengajuanPenghapusanDetail->status = 'rejected';
            } elseif ($tipe == 'waiting_for_approval')
                $pengajuanPenghapusanDetail->status = 'waiting_for_approval';
            $pengajuanPenghapusanDetail->save();
        }

        $pengajuanPenghapusanDetails = Kendaraan\PengajuanPenghapusanDetail::where('pengajuan_penghapusan_id', $pengajuanPenghapusan->id)
            ->where('step', '<=', 100)
            ->get();
        $counterApproved             = 0;
        if (!empty($pengajuanPenghapusanDetails)) {
            foreach ($pengajuanPenghapusanDetails as $pengajuanPenghapusanDetail) {
                /** @var Kendaraan\PengajuanPenghapusanDetail $pengajuanPenghapusanDetail */
                if ($pengajuanPenghapusanDetail->status == 'approved') {
                    $counterApproved++;
                }
            }
        }
        $allApproved = $counterApproved === count($pengajuanPenghapusanDetails);

        if (count($pengajuanPenghapusanDetails) >= 10 && $allApproved) {
            $pengajuanPenghapusan->status = 'approved';
            $pengajuanPenghapusan->approver_id = \Auth::id();
            $pengajuanPenghapusan->save();
        } elseif ($pengajuanPenghapusanDetail->status == 'rejected'){
            $pengajuanPenghapusan->status = 'rejected';
            $pengajuanPenghapusan->save();
        }
        elseif ($pengajuanPenghapusanDetail->status == 'waiting_for_approval'){
                $pengajuanPenghapusan->status = 'kendaraan';
                $pengajuanPenghapusan->save();
        }else {
            $pengajuanPenghapusan->status = 'kendaraan';
            $pengajuanPenghapusan->save();
        }

        return response()->json($pengajuanPenghapusanDetail);
    }

    public function getKendaraan(Request $request, $id)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::findOrFail($id);

        $pengajuanPenghapusanKendaraans = $pengajuanPenghapusan->pengajuanPenghapusanKendaraans;

        return response()->json($pengajuanPenghapusanKendaraans);
    }

    public function postUpload(Request $request, $penghapusanid, $step)
    {
        \Validator::make($request->all(), [
            'file' => ['required'],
        ])->validate();

        $pengajuanPenghapusan       = Kendaraan\PengajuanPenghapusan::where('id', $penghapusanid)
            ->firstOrFail();
        $pengajuanPenghapusanDetail = Kendaraan\PengajuanPenghapusanDetail::firstOrNew([
            'step'                     => $step,
            'pengajuan_penghapusan_id' => $penghapusanid,
        ]);

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        try {
            $title = "{$pengajuanPenghapusan->id}-{$pengajuanPenghapusanDetail->step}-final";
            $file  = $request->file('file');

            if ($file) {
                $uploadedFilename = $file->getFilename();
                $dirname          = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/";
                $filename         = "{$title}." . $file->guessClientExtension();

                $fileModel = new File();

                $fileModel->filename = $filename;
                $fileModel->location = $dirname . $fileModel->filename;


                $file->move($dirname, $fileModel->filename);

                $fileModel->save();

                $pengajuanPenghapusanDetail->uploaded_filename = $uploadedFilename;
                $pengajuanPenghapusanDetail->file_id           = $fileModel->id;
                $pengajuanPenghapusanDetail->user_id           = \Auth::id();
                $pengajuanPenghapusanDetail->status            = 'waiting_for_approval';
                if ($step > 100)
                    $pengajuanPenghapusanDetail->status = 'approved';
                $pengajuanPenghapusanDetail->save();
            }
        } catch (\Exception $exception) {
            $data['result'] = 'error';
        }

        return response()->json($data);
    }

    public function postTemplate(Request $request, $penghapusanid, $step)
    {
        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $pengajuanPenghapusan               = Kendaraan\PengajuanPenghapusan::where('id', $penghapusanid)
            ->firstOrFail();
        $pengajuanPenghapusanDetail         = Kendaraan\PengajuanPenghapusanDetail::firstOrNew([
            'step'                     => $step,
            'pengajuan_penghapusan_id' => $penghapusanid,
        ]);
        $pengajuanPenghapusanDetail->status = 'waiting_for_final';

        $title = "{$pengajuanPenghapusan->id}-{$step}-template";

        if ($step == '2') {
            $templateProcessor = new TemplateProcessor('template/perwakilan/02_BERITA_ACARA_PENELITIAN.docx');

            $pengajuanPenghapusanDetail->no_surat = $request->get('no_surat');
            $anggotas                             = json_decode($request->get('anggotas', '[]'));
            $kendaraanInputs                      = json_decode($request->get('kendaraans', '[]'));
            $kendaraans                           = $pengajuanPenghapusan->kendaraans;

            $attributeFiller = [
                'namaSatker'                => $pengajuanPenghapusan->satker->nama_satker,
                'namaSatkerr'                => $pengajuanPenghapusan->satker->nama_satkerr,
                'noSurat'                   => $pengajuanPenghapusanDetail->no_surat,
                'noSuratKepala'             => $request->get('no_surat_kepala'),
                'kategoriPenghapusan'       => $pengajuanPenghapusan->kategoriPenghapusan->name,
                'alasanKategoriPenghapusan' => $pengajuanPenghapusan->keterangan,
                'anggotas'                  => $anggotas,
                'jumlahAnggotas'            => count($anggotas),
                'kendaraans'                => $kendaraans,
                'jumlahKendaraans'          => count($kendaraans),
                'totalUnit'                 => count($kendaraans),
            ];

            $attributeFillerArray = $attributeFiller;

            $templateProcessor->setValue('noSurat', $attributeFiller['noSurat']);
            $templateProcessor->setValue('noSuratKepala', $attributeFiller['noSuratKepala']);
            $templateProcessor->setValue('kategoriPenghapusan', $attributeFiller['kategoriPenghapusan']);
            $templateProcessor->setValue('alasanKategoriPenghapusan', $attributeFiller['alasanKategoriPenghapusan']);
            $templateProcessor->setValue('namaSatker', $attributeFiller['namaSatker']);
            $templateProcessor->setValue('namaSatkerr', $attributeFiller['namaSatkerr']);
            $templateProcessor->setValue('totalUnit', $attributeFiller['totalUnit']);

            $templateProcessor->cloneRow('rowNama', $attributeFiller['jumlahAnggotas']);
            $templateProcessor->cloneRow('rowNama', $attributeFiller['jumlahAnggotas']);
            $templateProcessor->cloneRow('tipe', $attributeFiller['jumlahKendaraans']);

            $i = 1;
            foreach ($anggotas as $anggota) {
                $attributeFiller['rowNama#' . $i]    = $anggota->nama;
                $attributeFiller['rowJabatan#' . $i] = $anggota->jabatan;

                $templateProcessor->setValue('rowNama#' . $i, $attributeFiller['rowNama#' . $i]);
                $templateProcessor->setValue('rowJabatan#' . $i, $attributeFiller['rowJabatan#' . $i]);
                $i = $i + 1;
            }

            $i = 1;
            foreach ($kendaraans as $kendaraan) {
                /** @var Kendaraan $kendaraan */
                for ($j = 0; $j < count($kendaraanInputs); $j++) {
                    if ($kendaraanInputs[$j]->id == $kendaraan->id) {
                        break;
                    }
                }
                $attributeFiller['tipe#' . $i]           = $kendaraan->merk_type;
                $attributeFiller['noRangka#' . $i]       = $kendaraanInputs[$j]->no_rangka;
                $attributeFiller['tahunPembuatan#' . $i] = $kendaraanInputs[$j]->tahun_pembuatan;
                $attributeFiller['alasan#' . $i]         = $kendaraanInputs[$j]->alasan;

                $templateProcessor->setValue('tipe#' . $i, $attributeFiller['tipe#' . $i]);
                $templateProcessor->setValue('noRangka#' . $i, $attributeFiller['noRangka#' . $i]);
                $templateProcessor->setValue('tahunPembuatan#' . $i, $attributeFiller['tahunPembuatan#' . $i]);
                $templateProcessor->setValue('alasan#' . $i, $attributeFiller['alasan#' . $i]);
                $i = $i + 1;
            }

            $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}.docx";
            $path    = public_path("pengajuan-penghapusan/{$pengajuanPenghapusan->id}/");
            if (!\File::isDirectory($path)) {
                \File::makeDirectory($path, 0777, true, true);
            }
            $templateProcessor->saveAs($dirname);

            $file           = new File();
            $file->filename = "{$title}.docx";
            $file->save();

            $pengajuanPenghapusanDetail->attribute_fillter       = $attributeFiller;
            $pengajuanPenghapusanDetail->attribute_fillter_array = $attributeFillerArray;
            $pengajuanPenghapusanDetail->template_id             = $file->id;
            $pengajuanPenghapusanDetail->save();

            return response()->json($data);
        }

        return response()->json($data);
    }

    public function postSaveKendaraan(Request $request, $penghapusanid)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $penghapusanid)
            ->firstOrFail();

        $kendaraans = json_decode($request->get('kendaraans', '[]'));

        foreach ($kendaraans as $kendaraan) {
            $pengajuanPenghapusanKendaraan = Kendaraan\PengajuanPenghapusanKendaraan::where('pengajuan_penghapusan_id', $penghapusanid)
                ->where('id', $kendaraan->id)->first();

            if (!empty($pengajuanPenghapusanKendaraan)) {
                $pengajuanPenghapusanKendaraan->alasan_penghapusan = $kendaraan->alasan_penghapusan;
                $pengajuanPenghapusanKendaraan->no_rangka          = $kendaraan->no_rangka;
                $pengajuanPenghapusanKendaraan->tahun_pembuatan    = $kendaraan->tahun_pembuatan;
//                $pengajuanPenghapusanKendaraan->nilai_ganti        = $kendaraan->nilai_ganti;
                $pengajuanPenghapusanKendaraan->nilai_limit = $kendaraan->nilai_limit;
                $pengajuanPenghapusanKendaraan->nilai_wajar = $kendaraan->nilai_wajar;
                $pengajuanPenghapusanKendaraan->tipe        = $kendaraan->tipe;
                $pengajuanPenghapusanKendaraan->save();
            }
        }

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        return response()->json($data);
    }

    public function postAddKendaraan(Request $request, $penghapusanid)
    {
        $pengajuanPenghapusan = Kendaraan\PengajuanPenghapusan::where('id', $penghapusanid)
            ->firstOrFail();

        $ids = json_decode($request->get('ids', '[]'));

        $kendaraans = Kendaraan::whereIn('id', $ids)->get();

        foreach ($kendaraans as $kendaraan) {
            /** @var Kendaraan $kendaraan */
            $pengajuanPenghapusanKendaraan = Kendaraan\PengajuanPenghapusanKendaraan::where('pengajuan_penghapusan_id', $pengajuanPenghapusan->id)
                ->where('kendaraan_id', $kendaraan->id)->first();

            if (empty($pengajuanPenghapusanKendaraan))
                $pengajuanPenghapusanKendaraan = new Kendaraan\PengajuanPenghapusanKendaraan();

            $pengajuanPenghapusanKendaraan->kendaraan_id             = $kendaraan->id;
            $pengajuanPenghapusanKendaraan->pengajuan_penghapusan_id = $pengajuanPenghapusan->id;
            $pengajuanPenghapusanKendaraan->save();
        }

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        return response()->json($data);
    }

    public function postRemoveKendaraan(Request $request, $id)
    {
        $pengajuanPenghapusanKendaraan = Kendaraan\PengajuanPenghapusanKendaraan::findOrFail($id);
        $pengajuanPenghapusanKendaraan->delete();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        return response()->json($data);
    }

    public function postFinishKendaraan(Request $request, $penghapusanid)
    {
        $pengajuanPenghapusan         = Kendaraan\PengajuanPenghapusan::findOrFail($penghapusanid);
        $pengajuanPenghapusan->status = 'kendaraan';
        $pengajuanPenghapusan->save();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        return response()->json($data);
    }
    public function postFinishAll(Request $request, $penghapusanid)
    {
        $pengajuanPenghapusan         = Kendaraan\PengajuanPenghapusan::findOrFail($penghapusanid);
        $pengajuanPenghapusan->status = 'selesai';
        $pengajuanPenghapusan->save();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        return response()->json($data);
    }
}
