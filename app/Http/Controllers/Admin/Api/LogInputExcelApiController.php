<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Log\LogInputExcel;
use Illuminate\Http\Request;

class LogInputExcelApiController extends Controller
{
    public function get(Request $request, $tipe)
    {
        $logInputExcel = LogInputExcel::where('tipe', $tipe)->orderBy('created_at', 'desc')->take(5)->get();

        return response()->json($logInputExcel);
    }
}
