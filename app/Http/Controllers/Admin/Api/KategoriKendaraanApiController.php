<?php


namespace App\Http\Controllers\Admin\Api;
use App\Http\Controllers\Controller;
use App\Models\Kendaraan;

class KategoriKendaraanApiController extends Controller
{
    public function getDropdown()
    {
        return Kendaraan::where('nama_satker', true)
            ->orderBy('kode_barang')
            ->orderBy('nup')
            ->get();
    }
}
