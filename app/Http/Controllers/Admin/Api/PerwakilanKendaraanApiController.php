<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Kendaraan;
use Illuminate\Http\Request;

class PerwakilanKendaraanApiController extends Controller
{
    public function get(Request $request, $kondisi = null)
    {
        $params = $request->get('params', false);
        $models = Kendaraan::query();
//            ->with(['logo', 'thumbnail']);

        if (!empty($kondisi)) {
            switch ($kondisi) {
                case 'berat' :
                    $models->where('kondisi', 'Rusak Berat');
                    break;
            }
        }

        if (!\Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan'])) {
            if (\Auth::user()->hasRole(['admin_kendaraan', 'staf_kendaraan', 'admin_satker'])) {
                $models->where('kode_satker', \Auth::user()->satker->kode_satker);
            }
            if (\Auth::user()->hasRole(['admin_aset'])) {
                $models->whereIn('kode_satker', \Auth::user()->wilayah->satkers()->pluck('kode_satker')->toArray());
            }
        }

        $search = $request->get('search', false);
        $order  = $request->get('order', false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch ($key) {
                    case 'filter' :
                        if (!empty($val))
                            $models->where('kondisi', ($val == "baik" ? "Baik" : ($val == "ringan" ? "Rusak Ringan" : "Rusak Berat")));
                        break;
                    default:
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function ($q) use ($search) {
                $q->where('nama_barang', 'like', "%$search%")
                    ->orWhere('kode_satker', 'like', "%$search%")
                    ->orWhere('nama_satker', 'like', "%$search%")
                    ->orWhere('kondisi', 'like', "%$search%")
                    ->orWhere('merk_type', 'like', "%$search%")
                    ->orWhere('status_penggunaan', 'like', "%$search%")
                    ->orWhere('status_pengelolaan', 'like', "%$search%")
                    ->orWhere('no_bpkb', 'like', "%$search%")
                    ->orWhere('no_polisi', 'like', "%$search%")
                    ->orWhere('pemakai', 'like', "%$search%");
            });
        }
        $count = $models->count();

        $page    = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $order_direction = $request->get('order_direction', 'asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case "nama_barang":
                case "kode_satker":
                case "nama_satker":
                case "kondisi":
                case "merk_type":
                case "status_penggunaan":
                case "status_pengelolaan":
                case "no_bpkb":
                case "no_polisi":
                case "pemakai":
                case "tanggal_rekam_pertama":
                case "tanggal_perolehan":
                case "nilai_perolehan_pertama":
                case "nilai_mutasi":
                case "nilai_perolehan":
                case "nilai_penyusutan":
                case "nilai_buku":
                    $models = $models->orderBy($order, $order_direction);
                    break;
                default:
                    break;
            }
        }
        $models = $models->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'data'  => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }
}
