<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\KendaraanDinas;
use App\Models\Notification\Notification;
use App\Services\LogActionServices;
use Illuminate\Http\Request;

class KendaraanDinasApiController extends Controller
{
    public function get(Request $request)
    {
        $params = $request->get('params', false);
        $models = KendaraanDinas::query();
//            ->with(['logo', 'thumbnail']);

        $search = $request->get('search', false);
        $order  = $request->get('order', false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch ($key) {
                    case 'code':
                        if (!empty($val)) {
                            $notification = Notification::where('code', $val)->first();
                            if (!empty($notification))
                                $models->whereIn('id', $notification->relation_ids);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function ($q) use ($search) {
                $q->where('station', 'like', "%$search%")
                    ->orWhere('kib', 'like', "%$search%")
                    ->orWhere('jenis', 'like', "%$search%")
                    ->orWhere('tipe', 'like', "%$search%")
                    ->orWhere('merk', 'like', "%$search%")
                    ->orWhere('warna', 'like', "%$search%")
                    ->orWhere('tahun', 'like', "%$search%")
                    ->orWhere('bpkb', 'like', "%$search%")
                    ->orWhere('no_mesin', 'like', "%$search%")
                    ->orWhere('no_rangka', 'like', "%$search%")
                    ->orWhere('penanggung_jawab', 'like', "%$search%")
                    ->orWhere('no_polis', 'like', "%$search%")
                    ->orWhere('perusahaan_asuransi', 'like', "%$search%")
                    ->orWhere('premi', 'like', "%$search%")
                    ->orWhere('tertanggung', 'like', "%$search%")
                    ->orWhere('jatuh_tempo_awal', 'like', "%$search%")
                    ->orWhere('jatuh_tempo_akhir', 'like', "%$search%")
                    ->orWhere('no_polisi', 'like', "%$search%")
                    ->orWhere('tanggal_pajak', 'like', "%$search%")
                    ->orWhere('no_polisi_rfs', 'like', "%$search%")
                    ->orWhere('tanggal_pajak_rfs', 'like', "%$search%");
            });
        }

        $count = $models->count();

        $page    = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $order_direction = $request->get('order_direction', 'asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case "station":
                case "kib":
                case "jenis":
                case "tipe":
                case "merk":
                case "warna":
                case "tahun":
                case "bpkb":
                case "no_mesin":
                case "no_rangka":
                case "penanggung_jawab":
                case "no_polis":
                case "perusahaan_asuransi":
                case "premi":
                case "tertanggung":
                case "jatuh_tempo_awal":
                case "jatuh_tempo_akhir":
                case "no_polisi":
                case "tanggal_pajak":
                case "no_polisi_rfs":
                case "tanggal_pajak_rfs":
                    $models = $models->orderBy($order, $order_direction);
                    break;
                default:
                    break;
            }
        }
        $models = $models->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'data'  => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }

    public function post(Request $request)
    {
        \Validator::make($request->all(), [
//            'station'          => ['required'],
//            'kib'              => ['required'],
//            'jenis'            => ['required'],
//            'tipe'             => ['required'],
//            'merk'             => ['required'],
//            'warna'            => ['required'],
//            'tahun'            => ['required'],
//            'bpkb'             => ['required'],
//            'no_mesin'         => ['required'],
//            'no_rangka'        => ['required'],
//            'penanggung_jawab' => ['required'],

//            'no_polis'            => ['required'],
//            'perusahaan_asuransi' => ['required'],
//            'premi'               => ['required'],
//            'tertanggung'         => ['required'],
//            'jatuh_tempo_awal'    => ['required'],
//            'jatuh_tempo_akhir'   => ['required'],
//
//            'no_polisi'         => ['required'],
//            'tanggal_pajak'     => ['required'],
//            'no_polisi_rfs'     => ['required'],
//            'tanggal_pajak_rfs' => ['required'],
        ])->validate();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $kendaraanDinas                   = new KendaraanDinas();
        $kendaraanDinas->station          = $request->get('station');
        $kendaraanDinas->kib              = $request->get('kib');
        $kendaraanDinas->jenis            = $request->get('jenis');
        $kendaraanDinas->tipe             = $request->get('tipe');
        $kendaraanDinas->merk             = $request->get('merk');
        $kendaraanDinas->warna            = $request->get('warna');
        $kendaraanDinas->tahun            = $request->get('tahun');
        $kendaraanDinas->bpkb             = $request->get('bpkb');
        $kendaraanDinas->no_mesin         = $request->get('no_mesin');
        $kendaraanDinas->no_rangka        = $request->get('no_rangka');
        $kendaraanDinas->penanggung_jawab = $request->get('penanggung_jawab');

        $kendaraanDinas->no_polis            = $request->get('no_polis');
        $kendaraanDinas->perusahaan_asuransi = $request->get('perusahaan_asuransi');
        $kendaraanDinas->premi               = $request->get('premi');
        $kendaraanDinas->tertanggung         = $request->get('tertanggung');
        $kendaraanDinas->jatuh_tempo_awal    = new \DateTime($request->get('jatuh_tempo_awal'));
        $kendaraanDinas->jatuh_tempo_akhir   = new \DateTime($request->get('jatuh_tempo_akhir'));

        $kendaraanDinas->no_polisi         = $request->get('no_polisi');
        $kendaraanDinas->tanggal_pajak     = new \DateTime($request->get('tanggal_pajak'));
        $kendaraanDinas->no_polisi_rfs     = $request->get('no_polisi_rfs');
        $kendaraanDinas->tanggal_pajak_rfs = new \DateTime($request->get('tanggal_pajak_rfs'));

        $kendaraanDinas->save();

        $newObject = $kendaraanDinas->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_CREATE_DATA,
            'create',
            KendaraanDinas::class,
            null,
            $newObject
        );

        $data['data'] = $kendaraanDinas;

        return response()->json($data);
    }

    public function put(Request $request, $id)
    {
        \Validator::make($request->all(), [
        ])->validate();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $kendaraanDinas = KendaraanDinas::where('id', $id)->firstOrFail();

        $oldObject = $kendaraanDinas->toArray();

        $kendaraanDinas->station          = $request->get('station') != 'null' ? $request->get('station') : null;
        $kendaraanDinas->kib              = $request->get('kib') != 'null' ? $request->get('kib') : null;
        $kendaraanDinas->jenis            = $request->get('jenis') != 'null' ? $request->get('jenis') : null;
        $kendaraanDinas->tipe             = $request->get('tipe') != 'null' ? $request->get('tipe') : null;
        $kendaraanDinas->merk             = $request->get('merk') != 'null' ? $request->get('merk') : null;
        $kendaraanDinas->warna            = $request->get('warna') != 'null' ? $request->get('warna') : null;
        $kendaraanDinas->tahun            = $request->get('tahun') != 'null' ? $request->get('tahun') : null;
        $kendaraanDinas->bpkb             = $request->get('bpkb') != 'null' ? $request->get('bpkb') : null;
        $kendaraanDinas->no_mesin         = $request->get('no_mesin') != 'null' ? $request->get('no_mesin') : null;
        $kendaraanDinas->no_rangka        = $request->get('no_rangka') != 'null' ? $request->get('no_rangka') : null;
        $kendaraanDinas->penanggung_jawab = $request->get('penanggung_jawab') != 'null' ? $request->get('penanggung_jawab') : null;

        $kendaraanDinas->no_polis            = $request->get('no_polis') != 'null' ? $request->get('no_polis') : null;
        $kendaraanDinas->perusahaan_asuransi = $request->get('perusahaan_asuransi') != 'null' ? $request->get('perusahaan_asuransi') : null;
        $kendaraanDinas->premi               = $request->get('premi') != 'null' ? $request->get('premi') : null;
        $kendaraanDinas->tertanggung         = $request->get('tertanggung') != 'null' ? $request->get('tertanggung') : null;
        $kendaraanDinas->jatuh_tempo_awal    = $request->get('jatuh_tempo_awal') != 'null' ? new \DateTime($request->get('jatuh_tempo_awal')) : null;
        $kendaraanDinas->jatuh_tempo_akhir   = $request->get('jatuh_tempo_akhir') != 'null' ? new \DateTime($request->get('jatuh_tempo_akhir')) : null;

        $kendaraanDinas->no_polisi         = $request->get('no_polisi') != 'null' ? $request->get('no_polisi') : null;
        $kendaraanDinas->tanggal_pajak     = $request->get('tanggal_pajak') != 'null' ? new \DateTime($request->get('tanggal_pajak')) : null;
        $kendaraanDinas->no_polisi_rfs     = $request->get('no_polisi_rfs') != 'null' ? $request->get('no_polisi_rfs') : null;
        $kendaraanDinas->tanggal_pajak_rfs = $request->get('tanggal_pajak_rfs') != 'null' ? new \DateTime($request->get('tanggal_pajak_rfs')) : null;

        $kendaraanDinas->save();

        $newObject = $kendaraanDinas->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_UPDATE_DATA,
            'update',
            KendaraanDinas::class,
            $oldObject,
            $newObject
        );

        $data['data'] = $kendaraanDinas;

        return response()->json($data);
    }

    public function delete(Request $request, $id)
    {
        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $kendaraanDinas = KendaraanDinas::where('id', $id)->firstOrFail();

        $oldObject = $kendaraanDinas->toArray();

        $kendaraanDinas->delete();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_DELETE_DATA,
            'delete',
            KendaraanDinas::class,
            $oldObject
        );

        return response()->json($data);
    }
}
