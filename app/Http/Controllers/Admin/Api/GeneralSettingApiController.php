<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Services\LogActionServices;
use Illuminate\Http\Request;

class GeneralSettingApiController extends Controller
{
    public function get(Request $request)
    {
        $settingDarkMode = Setting::where('setting_key', 'dark_mode_default_front')
            ->first();

        $result = [
            'dark_mode' => empty($settingDarkMode) ? 0 : $settingDarkMode->setting_value,
        ];

        return response()->json($result);
    }

    public function post(Request $request)
    {
        $settingDarkMode = Setting::where('setting_key', 'dark_mode_default_front')
            ->first();

        if (empty($settingDarkMode)) {
            $settingDarkMode              = new Setting();
            $settingDarkMode->setting_key = 'dark_mode_default_front';
        }

        $oldObject = $settingDarkMode->toArray();

        $settingDarkMode->setting_value = $request->get('dark_mode');
        $settingDarkMode->save();

        $newObject = $settingDarkMode->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_GENERAL_SETTING,
            'update',
            Setting::class,
            $oldObject,
            $newObject
        );

        return $this->get($request);
    }
}
