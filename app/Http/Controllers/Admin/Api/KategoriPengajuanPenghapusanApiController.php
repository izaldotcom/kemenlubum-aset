<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Kendaraan;

class KategoriPengajuanPenghapusanApiController extends Controller
{
    public function getDropdown()
    {
        return Kendaraan\KategoriPenghapusan::where('is_active', true)
            ->orderBy('seed')
            ->orderBy('name')
            ->get();
    }
}
