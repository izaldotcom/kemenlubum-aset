<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Role;
use App\Services\LogActionServices;
use App\User;
use Hash;
use Illuminate\Http\Request;

class UserApiController extends Controller
{
    public function get(Request $request)
    {
        $params = $request->get('params', false);
        $models = User::query()
            ->with(['roles'])
            ->where('username', '!=', 'han');
//            ->with(['logo', 'thumbnail']);

        $search = $request->get('search', false);
        $order  = $request->get('order', false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch ($key) {
                    default:
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function ($q) use ($search) {
                $q->where('name', 'like', "%$search%");
            });
        }
        $count = $models->count();

        $page    = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $order_direction = $request->get('order_direction', 'asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case "name":
                    $models = $models->orderBy($order, $order_direction);
                    break;
                default:
                    break;
            }
        }
        $models = $models->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'data'  => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }

    public function putResetPassword(Request $request, $id)
    {
        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $user = User::where('id', $id)->firstOrFail();

        $oldObject = $user->toArray();

        $user->password = Hash::make('123456');
        $user->save();

        $newObject = $user->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_RESET_PASSWORD,
            'update',
            User::class,
            $oldObject,
            $newObject
        );

        return response()->json($data);
    }

    public function post(Request $request)
    {
        $role = Role::where('id', $request->get('role_id'))->firstOrFail();

        $validatorRules = [
            'username' => ['required'],
            'name'     => ['required'],
        ];

        \Validator::make($request->all(), $validatorRules)->validate();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $user                    = new User();
        $user->wilayah_id        = $request->get('wilayah_id');
        $user->satker_id         = $request->get('satker_id');
        $user->username          = $request->get('username');
        $user->password          = Hash::make('123456');
        $user->name              = $request->get('name');
        $user->email             = $request->get('email');
        $user->email_verified_at = date('Y-m-d');
        $user->is_verified       = true;
        $user->is_active         = $request->get('is_active', false) == "true" || $request->get('is_active', false) == "1";

        $user->save();
        $user->roles()->sync([$role->id]);

        $data['data'] = $user;

        $newObject = $user->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_CREATE_DATA,
            'create',
            User::class,
            null,
            $newObject
        );

        $data['data'] = $user;

        return response()->json($data);
    }

    public function put(Request $request, $id)
    {
        $role = Role::where('id', $request->get('role_id'))->firstOrFail();
        $user = User::where('id', $id)->firstOrFail();

        $validatorRules = [
            'username' => ['required'],
            'name'     => ['required'],
        ];

        \Validator::make($request->all(), $validatorRules)->validate();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $oldObject = $user->toArray();

        $user->wilayah_id = !empty($request->get('wilayah_id')) && $request->get('wilayah_id') != "null" ? $request->get('wilayah_id') : null;
        $user->satker_id  = !empty($request->get('satker_id')) && $request->get('satker_id') != "null" ? $request->get('satker_id') : null;
        $user->username   = $request->get('username');
        $user->name       = !empty($request->get('name')) && $request->get('name') != "null" ? $request->get('name') : null;
        $user->email      = !empty($request->get('email')) && $request->get('email') != "null" ? $request->get('email') : null;
        $user->is_active  = $request->get('is_active', false) == "true" || $request->get('is_active', false) == "1";

        $user->save();
        $user->roles()->sync([$role->id]);

        $data['data'] = $user;

        $newObject = $user->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_UPDATE_DATA,
            'update',
            User::class,
            $oldObject,
            $newObject
        );

        $data['data'] = $user;

        return response()->json($data);
    }

    public function delete(Request $request, $id)
    {
        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $user = User::where('id', $id)->firstOrFail();

        $oldObject = $user->toArray();

        $user->delete();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_DELETE_DATA,
            'delete',
            User::class,
            $oldObject
        );

        return response()->json($data);
    }
}
