<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\RumahNegara;
use Illuminate\Http\Request;

class RumahNegaraApiController extends Controller
{
    public function get(Request $request)
    {
        $params = $request->get('params', false);
        $models = RumahNegara::query();
//            ->with(['logo', 'thumbnail']);

        if (!\Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan'])) {
            if (\Auth::user()->hasRole(['admin_bangunan', 'staf_bangunan', 'admin_satker'])) {
                $models->where('kode_satker', \Auth::user()->satker->kode_satker);
            }
            if (\Auth::user()->hasRole(['admin_aset'])) {
                $models->whereIn('kode_satker', \Auth::user()->wilayah->satkers()->pluck('kode_satker')->toArray());
            }
        }

        $search = $request->get('search', false);
        $order  = $request->get('order', false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch ($key) {
                    case 'filter' :
                        if (!empty($val))
                            $models->where('kondisi', ($val == "baik" ? "Baik" : ($val == "ringan" ? "Rusak Ringan" : "Rusak Berat")));
                        break;
                    default:
                        break;
                }
            }
        }

        if ($search != '') {

            $models = $models->where(function ($q) use ($search) {
            $cari1 = explode(" ",$search);
            $cari2 = [];
            for($i=0;$i<count($cari1);$i++){
            $cari2[] = $cari1[$i];
            }
            $cari3 = implode("%",$cari2);
                $q->where('nama_barang', 'like', "%$search%")
                    ->orWhere('kode_satker', 'like', "%$search%")
                    ->orWhere('nama_satker', 'like', "%$cari3%")
                    ->orWhere('kondisi', 'like', "%$search%")
                    ->orWhere('jenis_dokumen', 'like', "%$search%")
                    ->orWhere('jenis_sertifikat', 'like', "%$search%")
                    ->orWhere('kepemilikan', 'like', "%$search%")
                    ->orWhere('kode_kota', 'like', "%$search%")
                    ->orWhere('kode_provinsi', 'like', "%$search%")
                    ->orWhere('uraian_kota', 'like', "%$search%")
                    ->orWhere('alamat', 'like', "%$search%")
                    ->orWhere('jalan', 'like', "%$search%")
                    ->orWhere('status_penggunaan', 'like', "%$search%")
                    ->orWhere('status_pengelolaan', 'like', "%$search%")
                    ->orWhere('no_psp', 'like', "%$search%")
                    ->orWhere('merk_type', 'like', "%$search%");
            });
        }
        $count = $models->count();

        $page    = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $order_direction = $request->get('order_direction', 'asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case "nama_barang":
                    $models = $models->orderBy($order, $order_direction);
                    break;
                default:
                    break;
            }
        }
        $models = $models->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'data'  => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }
}
