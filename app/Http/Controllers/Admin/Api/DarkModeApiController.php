<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Services\LogActionServices;
use Illuminate\Http\Request;

class DarkModeApiController extends Controller
{
    public function get(Request $request)
    {
        $settingDarkMode = Setting::where('setting_key', 'dark_mode')
            ->where('user_id', \Auth::id())
            ->first();

        $result = [
            'data' => empty($settingDarkMode) ? 0 : $settingDarkMode->setting_value,
        ];

        return response()->json($result);
    }

    public function post(Request $request)
    {
        $settingDarkMode = Setting::where('setting_key', 'dark_mode')
            ->where('user_id', \Auth::id())
            ->first();

        if (empty($settingDarkMode)) {
            $settingDarkMode              = new Setting();
            $settingDarkMode->user_id     = \Auth::id();
            $settingDarkMode->setting_key = 'dark_mode';
        }

        $oldObject = $settingDarkMode->toArray();

        $settingDarkMode->setting_value = $request->get('dark_mode');
        $settingDarkMode->save();

        $newObject = $settingDarkMode->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_DARK_MODE_ADMIN,
            'update',
            Setting::class,
            $oldObject,
            $newObject
        );

        return $this->get($request);
    }
}
