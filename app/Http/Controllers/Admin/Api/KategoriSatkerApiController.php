<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Satker;

class KategoriSatkerApiController extends Controller
{
    public function getDropdown()
    {
        return Satker::where('nama_satker', true)
            ->orderBy('category')
            ->orderBy('kode_satker')
            ->get();
    }
}
