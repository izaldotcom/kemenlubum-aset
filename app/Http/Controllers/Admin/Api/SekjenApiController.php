<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Sekjen;
use App\Models\Notification\Notification;
use App\Services\LogActionServices;
use Illuminate\Http\Request;

class SekjenApiController extends Controller
{
    public function get(Request $request)
    {
        $params = $request->get('params', false);
        $models = Sekjen::query();
        $search = $request->get('search', false);
        $order  = $request->get('order', false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch ($key) {
                    default:
                        break;
                }
            }
        }
        if ($search != '') {
            $models = $models->where(function ($q) use ($search) {
                $q->where('nama', 'like', "%$search%")
                  ->orWhere('nip', 'like', "%$search%");
            });
        }
        $count = $models->count();

        $page    = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $order_direction = $request->get('order_direction', 'asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case "nama":
                    $models = $models->orderBy($order, $order_direction);
                    break;
                default:
                    break;
            }
        }
        $models = $models->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'data'  => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }

    public function post(Request $request , $id)
    {
        \Validator::make($request->all(), [
//            'station'          => ['required'],
//            'kib'              => ['required'],
//            'jenis'            => ['required'],
//            'tipe'             => ['required'],
//            'merk'             => ['required'],
//            'warna'            => ['required'],
//            'tahun'            => ['required'],
//            'bpkb'             => ['required'],
//            'no_mesin'         => ['required'],
//            'no_rangka'        => ['required'],
//            'penanggung_jawab' => ['required'],

//            'no_polis'            => ['required'],
//            'perusahaan_asuransi' => ['required'],
//            'premi'               => ['required'],
//            'tertanggung'         => ['required'],
//            'jatuh_tempo_awal'    => ['required'],
//            'jatuh_tempo_akhir'   => ['required'],
//
//            'no_polisi'         => ['required'],
//            'tanggal_pajak'     => ['required'],
//            'no_polisi_rfs'     => ['required'],
//            'tanggal_pajak_rfs' => ['required'],
        ])->validate();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $SekJen                   = new Sekjen();
        $SekJen->station          = $request->get('nama');
        $SekJen->kib              = $request->get('nip');
        $SekJen->save();

        $newObject = $SekJen->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_CREATE_DATA,
            'create',
            KendaraanDinas::class,
            null,
            $newObject
        );

        $data['data'] = $SekJen;

        return response()->json($data);
    }

    public function put(Request $request, $id)
    {
        \Validator::make($request->all(), [
        ])->validate();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $SekJen = Sekjen::where('id', $id)->firstOrFail();

        $oldObject = $SekJen->toArray();

        $SekJen->nama          = $request->get('nama') != 'null' ? $request->get('nama') : null;
        $SekJen->nip              = $request->get('nip') != 'null' ? $request->get('nip') : null;


        $SekJen->save();

        $newObject = $SekJen->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_UPDATE_DATA,
            'update',
            KendaraanDinas::class,
            $oldObject,
            $newObject
        );

        $data['data'] = $SekJen;

        return response()->json($data);
    }

    public function delete(Request $request, $id)
    {
        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $kendaraanDinas = Sekjen::where('id', $id)->firstOrFail();

        $oldObject = $kendaraanDinas->toArray();

        $kendaraanDinas->delete();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_DELETE_DATA,
            'delete',
            Sekjen::class,
            $oldObject
        );

        return response()->json($data);
    }
}
