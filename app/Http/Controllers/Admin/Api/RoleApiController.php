<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Http\Request;

class RoleApiController extends Controller
{
    public function getDropdown(Request $request)
    {
        return Role::query()->where('name', '!=', 'super_admin')->orderBy('display_name')->get();
    }
}
