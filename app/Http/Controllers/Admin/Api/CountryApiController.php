<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Maps\Country;
use Illuminate\Http\Request;

class CountryApiController extends Controller
{
    public function getDropdown(Request $request)
    {
        return Country::query()->orderBy('name')->get();
    }
}
