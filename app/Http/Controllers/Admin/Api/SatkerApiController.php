<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use App\Models\Satker;
use App\Services\LogActionServices;
use Illuminate\Http\Request;

class SatkerApiController extends Controller
{
    public function getDropdown(Request $request)
    {
        return Satker::query()->orderBy('nama_satker')->get();
    }

    public function get(Request $request)
    {
        $params = $request->get('params', false);
        $models = Satker::query()
            ->with(['countries']);

        $search = $request->get('search', false);
        $order  = $request->get('order', false);
        if ($params) {
            foreach ($params as $key => $val) {
                if ($val == '') continue;
                switch ($key) {
                    default:
                        break;
                }
            }
        }

        if ($search != '') {
            $models = $models->where(function ($q) use ($search) {
                $q->where('kode_satker', 'like', "%$search%")
                    ->orWhere('nama_satker', 'like', "%$search%")
                    ->orWhere('lat', 'like', "%$search%")
                    ->orWhere('lng', 'like', "%$search%")
                    ->orWhere('wilayah_id', 'like', "%$search%");
            });
        }
        $count = $models->count();

        $page    = $request->get('page', 1);
        $perpage = $request->get('perpage', 20);

        if ($order) {
            $order_direction = $request->get('order_direction', 'asc');
            if (empty($order_direction)) $order_direction = 'asc';

            switch ($order) {
                case "kode_satker":
                case "nama_satker":
                case "lat":
                case "wilayah_id":
                case "lng":
                    $models = $models->orderBy($order, $order_direction);
                    break;
                default:
                    break;
            }
        }
        $models = $models->skip(($page - 1) * $perpage)->take($perpage)->get();

        $result = [
            'data'  => $models,
            'count' => $count,
        ];

        return response()->json($result);
    }

    public function put(Request $request, $id)
    {
        \Validator::make($request->all(), [
//            'alamat'      => ['required'],
//            'kode_pos'    => ['required'],
//            'email'       => ['required', 'email'],
//            'phone'       => ['required'],
            'country_ids' => ['required'],
        ])->validate();

        $data = [
            'result' => 'success',
            'data'   => [],
        ];

        $countryIds = !empty($request->get('country_ids')) ? explode(',', $request->get('country_ids')) : [];

        $satker = Satker::where('id', $id)->firstOrFail();

        $oldObject = $satker->toArray();

        $satker->alamat   = $request->get('alamat') != 'null' ? $request->get('alamat') : null;
        $satker->email    = $request->get('email') != 'null' ? $request->get('email') : null;
        $satker->kode_pos = $request->get('kode_pos') != 'null' ? $request->get('kode_pos') : null;
        $satker->phone    = $request->get('phone') != 'null' ? $request->get('phone') : null;
        $satker->countries()->sync($countryIds);

        $satker->save();

        $newObject = $satker->toArray();

        LogActionServices::createLogAction(
            LogActionServices::TIPE_UPDATE_DATA,
            'update',
            Satker::class,
            $oldObject,
            $newObject
        );

        $data['data'] = $satker;

        return response()->json($data);
    }
}
