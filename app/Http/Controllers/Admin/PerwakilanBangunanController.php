<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Log\LogInputExcel;
use Illuminate\Http\Request;

class PerwakilanBangunanController extends Controller
{
    public function list(Request $request)
    {
        return view('admin.perwakilanBangunan.list-perwakilan-bangunan');
    }
}
