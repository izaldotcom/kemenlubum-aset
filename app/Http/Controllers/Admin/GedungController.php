<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Log\LogInputExcel;
use Illuminate\Http\Request;

class GedungController extends Controller
{
    public function list(Request $request, $filter = null)
    {
        return view('admin.gedung.list-gedung', [
            'filter' => $filter,
        ]);
    }

    public function inputExcel(Request $request)
    {
        $logInputExcel = LogInputExcel::where('tipe', 'gedung')->orderBy('created_at', 'desc')->take(5)->get();

        return view('admin.gedung.input-excel-gedung', [
            'logs' => $logInputExcel,
        ]);
    }
}
