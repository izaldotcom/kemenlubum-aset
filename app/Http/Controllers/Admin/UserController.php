<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function list(Request $request)
    {
        return view('admin.list-user');
    }

    public function changePassword(Request $request)
    {
        return view('admin.change-password');
    }
}
