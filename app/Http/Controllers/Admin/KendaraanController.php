<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\KendaraanDinas;
use App\Models\Log\LogInputExcel;
use Illuminate\Http\Request;

class KendaraanController extends Controller
{
    public function list(Request $request, $filter = null)
    {
        return view('admin.kendaraan.list-kendaraan', [
            'filter' => $filter,
        ]);
    }

    public function inputExcel(Request $request)
    {
        $logInputExcel = LogInputExcel::where('tipe', 'kendaraan')->orderBy('created_at', 'desc')->take(5)->get();

        return view('admin.kendaraan.input-excel-kendaraan', [
            'logs' => $logInputExcel,
        ]);
    }

    public function listKendaraanDinas(Request $request, $code = null)
    {
        return view('admin.kendaraan.list-kendaraan-dinas', [
            'code' => $code,
        ]);
    }

    public function inputKendaraanDinasExcel(Request $request)
    {
        $logInputExcel = LogInputExcel::orderBy('created_at', 'desc')->take(5)->get();

        return view('admin.kendaraan.input-excel-kendaraan-dinas', [
            'logs' => $logInputExcel,
        ]);
    }

    public function editPajak(Request $request, $id)
    {
        $kendaraanDinas = KendaraanDinas::where('id', $id)->firstOrFail();

        return view('admin.kendaraan.edit-pajak', [
            'kendaraanDinas' => $kendaraanDinas,
        ]);
    }
}
