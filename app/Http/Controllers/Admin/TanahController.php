<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Log\LogInputExcel;
use Illuminate\Http\Request;

class TanahController extends Controller
{
    public function list(Request $request, $filter = null)
    {
        return view('admin.tanah.list-tanah', [
            'filter' => $filter,
        ]);
    }

    public function inputExcel(Request $request)
    {
        $logInputExcel = LogInputExcel::where('tipe', 'tanah')->orderBy('created_at', 'desc')->take(5)->get();

        return view('admin.tanah.input-excel-tanah', [
            'logs' => $logInputExcel,
        ]);
    }
}
