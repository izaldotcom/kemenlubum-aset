<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Log\LogInputExcel;
use Illuminate\Http\Request;

class RumahNegaraController extends Controller
{
    public function list(Request $request, $filter = null)
    {
        return view('admin.rumahNegara.list-rumah-negara', [
            'filter' => $filter,
        ]);
    }

    public function inputExcel(Request $request)
    {
        $logInputExcel = LogInputExcel::where('tipe', 'rumah')->orderBy('created_at', 'desc')->take(5)->get();

        return view('admin.rumahNegara.input-excel-rumah-negara', [
            'logs' => $logInputExcel,
        ]);
    }
}
