<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Log\LogInputExcel;
use Illuminate\Http\Request;

class PerwakilanTanahController extends Controller
{
    public function list(Request $request)
    {
        return view('admin.perwakilanTanah.list-perwakilan-tanah');
    }
}
