<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class IsAuth
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        if (!$user instanceof User) {
            $request->session()->put('url.intended', \URL::current());
//            if (!$request->isXmlHttpRequest())
//                $request->session()->put('url.intended', \URL::current());
            return redirect('/login');
        }

        return $next($request);
    }
}
