<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \Auth::user();

        if ($user instanceof User) {
            if (!$user->hasRole(['admin', 'super_admin'])) {
//                return redirect('/login');
                return abort(404, 'This action is unauthorized.');
            }
        } else {
            $request->session()->put('url.intended', \URL::current());
//            if (!$request->isXmlHttpRequest())
//                $request->session()->put('url.intended', \URL::current());
            return redirect('/login');
        }

        return $next($request);
    }
}
