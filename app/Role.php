<?php

namespace App;

use App\Models\Menu;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'menu_role', 'role_id', 'menu_id');
    }
}