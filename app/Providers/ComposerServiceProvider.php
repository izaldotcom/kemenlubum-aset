<?php

namespace App\Providers;

use App\Services\NotificationServices;
use App\Services\SettingServices;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $dark_mode_user = SettingServices::getSettingByKey('dark_mode', true)->setting_value;
            $view->with('dark_mode_user', $dark_mode_user);

            if (empty(\Session::get('dark_mode_front'))) {
                $dark_mode_front = SettingServices::getSettingByKey('dark_mode_default_front', true)->setting_value;
            } else {
                $dark_mode_front = \Session::get('dark_mode_front');
            }
            $view->with('dark_mode_front', $dark_mode_front);

            if (\Auth::check()) {
                $TotalNotifications = NotificationServices::getMyNotifications();
                $view->with('total_notifications', $TotalNotifications);
            } //my_notifications
            if (\Auth::check()) {
                $myNotifications = NotificationServices::getMyNotifications(5);
                $view->with('my_notifications', $myNotifications);
            }
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
