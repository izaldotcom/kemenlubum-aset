<?php

namespace App\Models\Notification;

use App\Models\Satker;
use App\Models\Wilayah;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $casts = [
        'relation_ids' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function wilayah()
    {
        return $this->belongsTo(Wilayah::class, 'wilayah_id');
    }

    public function satker()
    {
        return $this->belongsTo(Satker::class, 'satker_id');
    }

    public function targets()
    {
        return $this->belongsToMany(User::class, 'notification_targets', 'user_id', 'notification_id')
            ->withPivot('read', 'read_at');
    }
}
