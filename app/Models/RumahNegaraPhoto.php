<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RumahNegaraPhoto extends Model
{
    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function rumahNegara()
    {
        return $this->belongsTo(RumahNegara::class, 'rumah_negara_id');
    }

    public function delete()
    {
        $file = $this->file;
        if (!empty($file)) {
            $file->delete();
        }

        return parent::delete();
    }
}
