<?php

namespace App\Models;

use App\Models\Maps\Country;
use Illuminate\Database\Eloquent\Model;

class Satker extends Model
{
    protected $appends = [
        'car_count',
        'building_count',
        'land_count',
        'main_photo',
        'photos_count',
        'first_country',
    ];

    public function getCarCountAttribute()
    {
        return Kendaraan::where('kode_satker', $this->kode_satker)->count();
        return Kendaraan::where('kode_wilayah', $this->kode_wilayah)->count();
    }

    public function getBuildingCountAttribute()
    {
        $count = Gedung::where('kode_satker', $this->kode_satker)->count();
        $count += RumahNegara::where('kode_satker', $this->kode_satker)->count();
        return $count;
        $countwil = Gedung::where('kode_wilayah', $this->kode_wilayah)->count();
        $countwil += RumahNegara::where('kode_wilayah', $this->kode_wilayah)->count();
        return $countwil;
    }

    public function getLandCountAttribute()
    {
        return Tanah::where('kode_satker', $this->kode_satker)->count();
        return Tanah::where('kode_wilayah', $this->kode_wilayah)->count();
    }

    public function getMainPhotoAttribute()
    {
        $firstPhoto = $this->photoFiles()->first();

        if (empty($firstPhoto))
            return asset("assets/images/kemlu.jpg");
        return $firstPhoto->file->full_url;
    }

    public function getFirstCountryAttribute()
    {
        $firstCountry = $this->countries()->first();

        if (empty($firstCountry))
            $firstCountry = Country::where('slug', 'indonesia')->first();

        return $firstCountry;
    }

    public function getPhotosCountAttribute()
    {
        return $this->photoFiles()->count();
    }

    public function tanahs()
    {
        return $this->hasMany(Tanah::class, 'kode_satker', 'kode_satker');
        return $this->hasMany(Tanah::class, 'kode_wilayah', 'kode_wilayah');
    }

    public function rumahs()
    {
        return $this->hasMany(RumahNegara::class, 'kode_satker', 'kode_satker');
        return $this->hasMany(RumahNegara::class, 'kode_wilayah', 'kode_wilayah');
    }

    public function gedungs()
    {
        return $this->hasMany(Gedung::class, 'kode_satker', 'kode_satker');
        return $this->hasMany(Gedung::class, 'kode_wilayah', 'kode_wilayah');
    }

    public function photoFiles()
    {
        return $this->hasMany(SatkerPhoto::class, 'satker_id')
            ->where('type', 'photo')
            ->orderBy('seed', 'asc');
    }

    public function countries()
    {
        return $this->belongsToMany(Country::class, 'satker_countries', 'satker_id', 'country_id');
    }

    public function wilayah()
    {
        return $this->belongsToMany(Wilayah::class, 'wilayah_id');
    }


}
