<?php

namespace App\Models;

use App\Role;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'menu_role', 'menu_id', 'role_id');
    }
}
