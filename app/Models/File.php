<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $appends = [
        'full_url',
    ];

    public function getFullUrlAttribute()
    {
        return asset($this->location);
    }

    public function delete()
    {
        if (file_exists($this->location))
            @unlink($this->location);

        return parent::delete();
    }
}
