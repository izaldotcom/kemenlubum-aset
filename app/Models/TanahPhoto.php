<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TanahPhoto extends Model
{
    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function tanah()
    {
        return $this->belongsTo(Tanah::class, 'tanah_id');
    }

    public function delete()
    {
        $file = $this->file;
        if (!empty($file)) {
            $file->delete();
        }

        return parent::delete();
    }
}
