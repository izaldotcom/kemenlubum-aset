<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tanah extends Model
{
    protected $appends = [
        'main_photo',
        'photos_count',
    ];

    public function getMainPhotoAttribute()
    {
        $firstPhoto = $this->photoFiles()->first();

        if (empty($firstPhoto))
            return asset("assets/images/kemlu.jpg");
        return $firstPhoto->file->full_url;
    }

    public function getPhotosCountAttribute()
    {
        return $this->photoFiles()->count();
    }

    public function satker()
    {
        return $this->belongsTo(Satker::class, 'kode_satker', 'kode_satker');
    }

    public function photoFiles()
    {
        return $this->hasMany(TanahPhoto::class, 'tanah_id')
            ->where('type', 'photo')
            ->orderBy('seed', 'asc');
    }

    public function documentFiles()
    {
        return $this->hasMany(TanahPhoto::class, 'tanah_id')
            ->where('type', 'document')
            ->orderBy('seed', 'asc');
    }
}
