<?php

namespace App\Models\Maps;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $casts = [
        'geometry' => 'array',
    ];

    use Sluggable;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    public function continent()
    {
        return $this->belongsTo(Continent::class, 'continent_id');
    }
}
