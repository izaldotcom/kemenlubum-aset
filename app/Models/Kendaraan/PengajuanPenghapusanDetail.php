<?php

namespace App\Models\Kendaraan;

use App\Models\File;
use App\User;
use Illuminate\Database\Eloquent\Model;

class PengajuanPenghapusanDetail extends Model
{
    protected $casts = [
        'misc_attribute' => 'array',
    ];

    protected $fillable = [
        'step',
        'pengajuan_penghapusan_id',
    ];

    public function uploader()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function approver()
    {
        return $this->belongsTo(User::class, 'approver_id');
    }

    public function pengajuanPenghapusan()
    {
        return $this->belongsTo(PengajuanPenghapusan::class, 'pengajuan_penghapusan_id');
    }

    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function template()
    {
        return $this->belongsTo(File::class, 'template_id');
    }
}
