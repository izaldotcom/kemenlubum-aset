<?php

namespace App\Models\Kendaraan;

use App\Models\Maps\Country;
use App\Models\Satker;

use App\User;
use Illuminate\Database\Eloquent\Model;
use App\Services\Terbilang;

class PengajuanPenghapusan extends Model
{
    protected $casts = [
        'misc_attribute'             => 'array',
        'susunan_panitia'            => 'array',
        'surat_pernyataan_signature' => 'array',
    ];

    protected $appends = [
        'status_say',
        'status_color',
    ];

    public function satker()
    {
        return $this->belongsTo(Satker::class, 'satker_id');
    }

    public function uploader()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function approver()
    {
        return $this->belongsTo(User::class, 'approver_id');
    }

    public function pengajuanPenghapusanKendaraans()
    {
        return $this->hasMany(PengajuanPenghapusanKendaraan::class, 'pengajuan_penghapusan_id');
    }

    public function kategoriPenghapusan()
    {
        return $this->belongsTo(KategoriPenghapusan::class, 'kategori_penghapusan_id');
    }

    public function getStatusSayAttribute()
    {
        $statusSay = "";

        switch ($this->status) {
            case "kendaraan":
                $statusSay = "Menunggu Berkas Komplit";
                break;
            case "new":
                $statusSay = "Pilih Kendaraan Dahulu!";
                break;
            case "approved":
                $statusSay = "Disetujui";
                break;
            case "rejected":
                $statusSay = "Perbaikan Dokumen";
                break;
            case "selesai":
                $statusSay = "Sudah diajukan ke Kemenkeu";
                break;
            default:
                break;
        }

        return $statusSay;
    }

    public function getStatusColorAttribute()
    {
        $statusColor = "";

        switch ($this->status) {
            case "kendaraan":
                $statusColor = "warning";
                break;
            case "new":
                $statusColor = "warning";
                break;
            case "approved":
                $statusColor = "success";
                break;
            case "rejected":
                    $statusColor = "danger";
                break;
            case "selesai":
                $statusColor = "success";
            break;

            default:
                break;
        }

        return $statusColor;
    }

    public function getAttributeTemplate($template)
    {

        $ter = new Terbilang();

        $attributes = [];

        if ($template == 2) {
            $anggotas   = $this->susunan_panitia;
            $kendaraans = $this->pengajuanPenghapusanKendaraans;

            $attributes = [
                'namaSatker'                => $this->satker->nama_satker,
                'namaSatkerr'                => $this->satker->nama_satkerr,
                'namaKota'                  => $this->satker->nama_kota,
                'tanggalSurat'              => date('d F Y'),
                'noSurat'                   => $this->no_berita_acara,
                'noSuratKepala'             => $this->no_surat_kepala,
                'kategoriPenghapusan'       => $this->kategoriPenghapusan->name,
                'alasanKategoriPenghapusan' => $this->keterangan,
                'anggotas'                  => $anggotas,
                'jumlahAnggotas'            => count($anggotas),
                'kendaraans'                => $kendaraans,
                'jumlahKendaraans'          => count($kendaraans),
                'totalUnit'                 => count($kendaraans),
            ];
        } elseif ($template == 10) {
            $kendaraans = $this->pengajuanPenghapusanKendaraans;

            $attributes = [
                'kendaraans' => $kendaraans,
            ];
        } elseif (in_array($template, [11, 12, 13])) {
            /** @var Country $negaraSatker */
            $negaraSatker = $this->satker->countries()->first();
            if (empty($negaraSatker))
                $negaraSatker = "Jakarta";
            else
                $negaraSatker = $negaraSatker->name;
            $attributes = [
                'tanggalSurat'        => date('d F Y'),
                'namaSatker'          => $this->satker->nama_satker,
                'namaSatkerr'         => $this->satker->nama_satkerr,
                'namaKota'            => $this->satker->nama_kota,
		'alasanKategoriPenghapusan' => $this->keterangan,
                'negaraSatker'        => $negaraSatker,
                'nama'                => $this->surat_pernyataan_signature['nama'],
                'nip'                 => $this->surat_pernyataan_signature['nip'],
                'pangkat'             => $this->surat_pernyataan_signature['pangkat'],
                'jabatan'             => $this->surat_pernyataan_signature['jabatan'],
                'kategoriPenghapusan' => $this->kategoriPenghapusan->name,
            ];
        } elseif (in_array($template, [101, 102, 103, 104, 105])) {
            $kendaraans          = $this->pengajuanPenghapusanKendaraans;
            $totalNilaiPerolehan = 0;
            $totalNilaiTaksiran  = 0;

            if (count($kendaraans) && !empty($kendaraans)) {
                foreach ($kendaraans as $kendaraan) {
                    /** @var PengajuanPenghapusanKendaraan $kendaraan */
                    $tmpTaksiran  = floatval($kendaraan->nilai_limit);
                    $tmpPerolehan = floatval($kendaraan->kendaraan->nilai_perolehan);

                    if (!empty($tmpTaksiran))
                        $totalNilaiTaksiran += $kendaraan->nilai_limit;
                    if (!empty($tmpPerolehan))
                        $totalNilaiPerolehan += $tmpPerolehan;
                }
            }

            /** @var Country $negaraSatker */
            $negaraSatker = $this->satker->countries()->first();
            if (empty($negaraSatker))
                $negaraSatker = "Jakarta";
            else
                $negaraSatker = $negaraSatker->name;

                $nilai_limit = preg_replace('/[^0-9]/','',$kendaraan->nilai_limit);
                $nilai_perolehan = preg_replace('/[^0-9]/','',$kendaraan->kendaraan->nilai_perolehan);

            $attributes = [
                'tanggalSurat'                 => date('d F Y'),
                'tanggalBeritaAcara'           => date('d F Y'),
                'noSuratKeputusan'             => isset($this->misc_attribute['nokeputusan']) ? $this->misc_attribute['nokeputusan'] : "",
                'noBeritaAcara'                => $this->no_berita_acara,
                'namaSatker'                   => $this->satker->nama_satker,
		'namaSatkerr'                  => $this->satker->nama_satkerr,
                'namaKota'                     => $this->satker->nama_kota,
                'negaraSatker'                 => $negaraSatker,
                'noSurat'                      => isset($this->misc_attribute['nosurat']) ? $this->misc_attribute['nosurat'] : "",
                'namaPernyataan'               => isset($this->misc_attribute['nama']) ? $this->misc_attribute['nama'] : "",
                'namaSekjen'                   => isset($this->misc_attribute['namasekjen']) ? $this->misc_attribute['namasekjen'] : "",
                'nipSekjen'                   => isset($this->misc_attribute['nipsekjen']) ? $this->misc_attribute['nipsekjen'] : "",
                'nipPernyataan'                => isset($this->misc_attribute['nip']) ? $this->misc_attribute['nip'] : "",
                'pangkatPernyataan'            => isset($this->misc_attribute['pangkat']) ? $this->misc_attribute['pangkat'] : "",
                'jabatanPernyataan'            => isset($this->misc_attribute['jabatan']) ? $this->misc_attribute['jabatan'] : "",
                'kategoriPenghapusan'          => $this->kategoriPenghapusan->name,
		'alasanKategoriPenghapusan'    => $this->alasan_kategori_penghapusan,
                'totalUnit'                    => count($kendaraans),
                'totalNilaiTaksiran'           => $totalNilaiTaksiran,
                'totalNilaiTaksiranTerbilang'  => $ter->get_convert($kendaraan->nilai_limit),
                'totalNilaiPerolehanTerbilang' => $ter->get_convert($kendaraan->kendaraan->nilai_perolehan),
                'totalNilaiTaksiranFormatted'  => "Rp" . number_format($totalNilaiTaksiran, 2, ',', '.'),
                'totalNilaiPerolehan'          => $totalNilaiPerolehan,
                'totalNilaiPerolehanFormatted' => "Rp" . number_format($totalNilaiPerolehan, 2, ',', '.'),
            ];
        }

        return $attributes;
    }
}
