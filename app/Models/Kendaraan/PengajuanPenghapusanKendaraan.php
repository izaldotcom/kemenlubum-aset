<?php

namespace App\Models\Kendaraan;

use App\Models\Kendaraan;
use Illuminate\Database\Eloquent\Model;

class PengajuanPenghapusanKendaraan extends Model
{
    protected $casts = [
        'misc_attribute' => 'array',
    ];

    protected $with = [
        'kendaraan',
    ];

    public function pengajuanPenghapusan()
    {
        return $this->belongsTo(PengajuanPenghapusan::class, 'pengajuan_penghapusan_id');
    }

    public function kendaraan()
    {
        return $this->belongsTo(Kendaraan::class, 'kendaraan_id');
    }
}
