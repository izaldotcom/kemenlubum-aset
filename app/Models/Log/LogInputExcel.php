<?php

namespace App\Models\Log;

use App\Models\File;
use App\User;
use Illuminate\Database\Eloquent\Model;

class LogInputExcel extends Model
{
    protected $appends = [
        'message_html',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function getMessageAttribute()
    {
        $message = "";
        switch ($this->status) {
            case 'success' :
                $message .= "Success! {$this->user->name} input excel {$this->tipe}";
                break;
            case 'error' :
                $message .= "Failed! {$this->user->name} input excel {$this->tipe} : {$this->note}";
                break;
            default :
                $message .= "Not Completed! {$this->user->name} input excel {$this->tipe}";
                break;
        }
        return $message;
    }

    public function getMessageHtmlAttribute()
    {
        $message = "";
        switch ($this->status) {
            case 'success' :
                $message .= "<strong class='text-success' style='font-style: italic'>Success!</strong> <strong style='text-decoration: underline'>{$this->user->name}</strong> input excel <strong style='text-decoration: underline'>{$this->tipe}</strong>";
                break;
            case 'error' :
                $message .= "<strong class='text-danger' style='font-style: italic'>Failed!</strong> <strong style='text-decoration: underline'>{$this->user->name}</strong> input excel <strong style='text-decoration: underline'>{$this->tipe}</strong> : {$this->note}";
                break;
            default :
                $message .= "<strong class='text-info' style='font-style: italic'>Not Completed!</strong> <strong style='text-decoration: underline'>{$this->user->name}</strong> input excel <strong style='text-decoration: underline'>{$this->tipe}</strong>";
                break;
        }
        return $message;
    }
}
