<?php

namespace App\Models\Log;

use Illuminate\Database\Eloquent\Model;

class LogEmail extends Model
{
    protected $casts = [
        'content' => 'array',
    ];
}
