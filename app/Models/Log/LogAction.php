<?php

namespace App\Models\Log;

use App\Models\Satker;
use App\Models\Wilayah;
use App\User;
use Illuminate\Database\Eloquent\Model;

class LogAction extends Model
{
    protected $casts = [
        'old_data' => 'array',
        'new_data' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function wilayah()
    {
        return $this->belongsTo(Wilayah::class, 'wilayah_id');
    }

    public function satker()
    {
        return $this->belongsTo(Satker::class, 'satker_id');
    }
}
