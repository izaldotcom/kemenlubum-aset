<?php

namespace App\Models;

use App\Models\Kendaraan\PengajuanPenghapusan;
use Illuminate\Database\Eloquent\Model;

class Kendaraan extends Model
{
    protected $appends = [
        'main_photo',
        'photos_count',
    ];

    public function getMainPhotoAttribute()
    {
        $firstPhoto = $this->photoFiles()->first();

        if (empty($firstPhoto))
            return asset("assets/images/kemlu.jpg");
        return $firstPhoto->file->full_url;
    }

    public function getPhotosCountAttribute()
    {
        return $this->photoFiles()->count();
    }

    public function satker()
    {
        return $this->belongsTo(Satker::class, 'kode_satker', 'kode_satker');
    }

    public function photoFiles()
    {
        return $this->hasMany(KendaraanPhoto::class, 'kendaraan_id')
            ->where('type', 'photo')
            ->orderBy('seed', 'asc');
    }

    public function documentFiles()
    {
        return $this->hasMany(KendaraanPhoto::class, 'kendaraan_id')
            ->where('type', 'document')
            ->orderBy('seed', 'asc');
    }

    public function pengajuanPenghapusans()
    {
        return $this->belongsToMany(PengajuanPenghapusan::class, 'pengajuan_penghapusan_kendaraan', 'kendaraan_id', 'pengajuan_penghapusan_id');
    }
}
