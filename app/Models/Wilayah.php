<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wilayah extends Model
{
    public function satkers()
    {
        return $this->hasMany(Satker::class, 'wilayah_id');
    }
}
