<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GedungPhoto extends Model
{
    public function file()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function gedung()
    {
        return $this->belongsTo(Gedung::class, 'gedung_id');
    }

    public function delete()
    {
        $file = $this->file;
        if (!empty($file)) {
            $file->delete();
        }

        return parent::delete();
    }
}
