<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckPajakAsuransi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:pajak';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for check pajak & asuransi kendaraan dinas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }
}
