<?php

namespace App\Imports;

use App\Models\Barang;
use App\Models\RumahNegara;
use App\Models\Satker;
use App\Services\InputExcelServices;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class RumahNegaraImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (empty($row['kode_barang']) || empty($row['nup']) || empty($row['kode_satker']) || empty($row['kib']))
            return null;

        InputExcelServices::storeInputExcelSession($row['kode_satker'], 'rumah_negaras');

        $existedBarang = Barang::where('kode_barang', $row['kode_barang'])->first();
        if (empty($existedBarang)) {
            $existedBarang = new Barang();
        }
        $existedBarang->kode_barang = $row['kode_barang'];
        $existedBarang->nama_barang = $row['nama_barang'];
        $existedBarang->save();

        $existedSatker = Satker::where('kode_satker', $row['kode_satker'])->first();
        if (empty($existedSatker)) {
            $existedSatker = new Satker();
        }
        $existedSatker->kode_satker = $row['kode_satker'];
        $existedSatker->nama_satker = $row['nama_satker'];
        $existedSatker->save();

        $existed = RumahNegara::where('kode_barang', $row['kode_barang'])
            ->where('nup', $row['nup'])
            ->where('kode_satker', $row['kode_satker'])
            ->where('kib', $row['kib'])
            ->first();

        if (!empty($existed)) {
            $this->fillin($existed, $row);
            $existed->save();
            InputExcelServices::storeInputExcelSessionCounter('data_updated', 1);
            return null;
        } else {
            $existed = new RumahNegara();
            $this->fillin($existed, $row);
            InputExcelServices::storeInputExcelSessionCounter('data_created', 1);
            return $existed;
        }
    }

    public function fillin(RumahNegara $model, $row)
    {
        $model->status_sync             = 'sync';
        $model->last_sync               = new \DateTime();
        $model->kode_barang             = $row['kode_barang'];
        $model->nup                     = $row['nup'];
        $model->kode_satker             = $row['kode_satker'];
        $model->nama_satker             = $row['nama_satker'];
        $model->kib                     = $row['kib'];
        $model->nama_barang             = $row['nama_barang'];
        $model->kondisi                 = $row['kondisi'];
        $model->jenis_dokumen           = $row['jenis_dokumen'];
        $model->kepemilikan             = $row['kepemilikan'];
        $model->jenis_sertifikat        = $row['jenis_sertifikat'];
        $model->merk_type               = $row['merktipe'];
        $model->tanggal_rekam_pertama   = (empty($row['tgl_rekam_pertama']) || $row['tgl_rekam_pertama'] < 0) ? null :
            Date::excelToDateTimeObject($row['tgl_rekam_pertama'])->format('Y-m-d');
        $model->tanggal_perolehan       = (empty($row['tgl_perolehan']) || $row['tgl_perolehan'] < 0) ? null :
            Date::excelToDateTimeObject($row['tgl_perolehan'])->format('Y-m-d');
        $model->nilai_perolehan_pertama = $row['nilai_perolehan_pertama'];
        $model->nilai_mutasi            = $row['nilai_mutasi'];
        $model->nilai_perolehan         = $row['nilai_perolehan'];
        $model->nilai_penyusutan        = $row['nilai_penyusutan'];
        $model->nilai_buku              = $row['nilai_buku'];
        $model->kuantitas               = $row['kuantitas'];
        $model->jumlah_foto             = $row['jml_foto'];
        $model->luas_bangunan           = $row['luas_bangunan'];
        $model->luas_dasar_bangunan     = $row['luas_dasar_bangunan'];
        $model->alamat                  = $row['alamat'];
        $model->jalan                   = $row['jalan'];
        $model->kode_kota               = $row['kode_kotakab'];
        $model->uraian_kota             = $row['uraian_kotakabupaten'];
        $model->kode_provinsi           = $row['kode_provinsi'];
        $model->status_penggunaan       = $row['status_penggunaan'];
        $model->status_pengelolaan      = $row['status_pengelolaan'];
        $model->no_psp                  = $row['no_psp'];
        $model->tanggal_psp             = (empty($row['tgl_psp']) || $row['tgl_psp'] < 0) ? null :
            Date::excelToDateTimeObject($row['tgl_psp'])->format('Y-m-d');
        $model->jumlah_kib              = $row['jumlah_kib'];
        $model->sbsk                    = $row['sbsk'];
        $model->optimalisasi            = $row['optimalisasi'];

        return $model;
    }
}
