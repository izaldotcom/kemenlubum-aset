<?php

namespace App\Imports;

use App\Models\PremiKendaraan;
use App\Services\InputExcelServices;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class KendaraanPremiImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (empty($row['nopolis']) || empty($row['no_polisi']))
            return null;

        $existed = PremiKendaraan::where('no_polisi', $row['no_polisi'])
            ->where('no_polis', $row['nopolis'])
            ->first();

        if (!empty($existed)) {
            $this->fillin($existed, $row);
            $existed->save();
            InputExcelServices::storeInputExcelSessionCounter('data_updated', 1);
            return null;
        } else {
            $existed = new PremiKendaraan();
            $this->fillin($existed, $row);
            InputExcelServices::storeInputExcelSessionCounter('data_created', 1);
            return $existed;
        }
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            if (empty($row['nopolis']) || empty($row['no_polisi']))
                continue;

            $existed = PremiKendaraan::where('no_polisi', $row['no_polisi'])
                ->where('no_polis', $row['nopolis'])
                ->first();

            if (!empty($existed)) {
                $this->fillin($existed, $row);
                $existed->save();
                continue;
            } else {
                $existed = new PremiKendaraan();
                $existed = $this->fillin($existed, $row);
                $existed->save();
            }
        }
    }

    public function fillin(PremiKendaraan $model, $row)
    {
        $model->status_sync         = 'sync';
        $model->last_sync           = new \DateTime();
        $model->merk_kendaraan      = $row['merk_kendaraan'];
        $model->jenis               = $row['jenis'];
        $model->tahun               = $row['tahun'];
        $model->no_polisi           = $row['no_polisi'];
        $model->no_polis            = $row['nopolis'];
        $model->pengguna            = $row['pengguna'];
        $model->jatuh_tempo_awal    = (empty($row['jatuh_tempo_awal']) || $row['jatuh_tempo_awal'] < 0) ? null :
            Date::excelToDateTimeObject($row['jatuh_tempo_awal'])->format('Y-m-d');
        $model->jatuh_tempo_akhir   = (empty($row['jatuh_tempo_akhir']) || $row['jatuh_tempo_akhir'] < 0) ? null :
            Date::excelToDateTimeObject($row['jatuh_tempo_akhir'])->format('Y-m-d');
        $model->premi               = $row['premi'];
        $model->tertanggung         = $row['tertanggung'];
        $model->perusahaan_asuransi = $row['perusahaan'];

        return $model;
    }
}
