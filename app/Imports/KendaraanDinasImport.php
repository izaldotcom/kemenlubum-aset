<?php

namespace App\Imports;

use App\Models\KendaraanDinas;
use App\Services\InputExcelServices;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class KendaraanDinasImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        if (empty($row['no_polisi']))
            return null;

        $existed = KendaraanDinas::where('no_polisi', $row['no_polisi'])
            ->first();

        if (!empty($existed)) {
            $this->fillin($existed, $row);
            $existed->save();
            InputExcelServices::storeInputExcelSessionCounter('data_updated', 1);
            return null;
        } else {
            $existed = new KendaraanDinas();
            $this->fillin($existed, $row);
            InputExcelServices::storeInputExcelSessionCounter('data_created', 1);
            return $existed;
        }
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {
        foreach ($collection as $row) {
            if (empty($row['no_polisi']))
                continue;

            $existed = KendaraanDinas::where('no_polisi', $row['no_polisi'])
                ->first();

            if (!empty($existed)) {
                $this->fillin($existed, $row);
                $existed->save();
                InputExcelServices::storeInputExcelSessionCounter('data_updated', 1);
                continue;
            } else {
                $existed = new KendaraanDinas();
                $existed = $this->fillin($existed, $row);
                $existed->save();
                InputExcelServices::storeInputExcelSessionCounter('data_created', 1);
            }
        }
    }

    public function fillin(KendaraanDinas $model, $row)
    {
        $model->status_sync         = 'sync';
        $model->last_sync           = new \DateTime();
        $model->kib                 = ($row['kib'] == '-' ? null : $row['kib']);
        $model->penanggung_jawab    = $row['penanggung_jawab'];
        $model->no_polisi           = $row['no_polisi'];
        $model->no_polisi_rfs       = ($row['no_polisi_rfs'] == '-' ? null : $row['no_polisi_rfs']);
        $model->merk                = $row['merk'];
        $model->jenis               = $row['jenis'];
        $model->tahun               = $row['tahun'];
        $model->warna               = $row['warna'];
        $model->no_rangka           = $row['no_rangka'];
        $model->no_mesin            = $row['no_mesin'];
        $model->bpkb                = $row['bpkb'];
        $model->tipe                = $row['tipe'];
        $model->station             = $row['station'];
        $model->tanggal_pajak       = (empty($row['tgl_pajak']) || $row['tgl_pajak'] < 0 || strtotime($row['tgl_pajak']) !== false) ? null :
            Date::excelToDateTimeObject($row['tgl_pajak'])->format('Y-m-d');
        $model->tanggal_pajak_rfs   = (empty($row['tgl_pajak_rfs']) || $row['tgl_pajak_rfs'] < 0 || strtotime($row['tgl_pajak_rfs']) !== false) ? null :
            Date::excelToDateTimeObject($row['tgl_pajak_rfs'])->format('Y-m-d');
        $model->perusahaan_asuransi = ($row['perusahaan_asuransi'] == '-' ? null : $row['perusahaan_asuransi']);
        if (!empty($model->perusahaan_asuransi)) {
            $model->no_polis          = $row['no_polis'];
            $model->premi             = is_numeric($row['premi']) ? $row['premi'] : null;
            $model->tertanggung       = is_numeric($row['tertanggung']) ? $row['tertanggung'] : null;
            $model->jatuh_tempo_awal  = (empty($row['jatuh_tempo_awal']) || $row['jatuh_tempo_awal'] < 0 || strtotime($row['jatuh_tempo_awal']) !== false) ? null :
                Date::excelToDateTimeObject($row['jatuh_tempo_awal'])->format('Y-m-d');
            $model->jatuh_tempo_akhir = (empty($row['jatuh_tempo_akhir']) || $row['jatuh_tempo_akhir'] < 0 || strtotime($row['jatuh_tempo_akhir']) !== false) ? null :
                Date::excelToDateTimeObject($row['jatuh_tempo_akhir'])->format('Y-m-d');
        } else {
            $model->no_polis          = null;
            $model->premi             = null;
            $model->tertanggung       = null;
            $model->jatuh_tempo_awal  = null;
            $model->jatuh_tempo_akhir = null;
        }

        return $model;
    }
}
