<?php

namespace App\Services;

use App\Models\Kendaraan\PengajuanPenghapusan;
use App\Models\Kendaraan\PengajuanPenghapusanDetail;
use LynX39\LaraPdfMerger\Facades\PdfMerger;

class PdfMergerServices
{
    public static function downloadMergedPdf()
    {
        $pdfMerger = PdfMerger::init(); //Initialize the merger

        $pdfMerger->addPDF('samplepdfs/one.pdf', '1, 3, 4');
        $pdfMerger->addPDF('samplepdfs/two.pdf', '1-2');
        $pdfMerger->addPDF('samplepdfs/three.pdf', 'all');

//You can optionally specify a different orientation for each PDF
        $pdfMerger->addPDF('samplepdfs/one.pdf', '1, 3, 4', 'L');
        $pdfMerger->addPDF('samplepdfs/two.pdf', '1-2', 'P');

        $pdfMerger->merge(); //For a normal merge (No blank page added)

// OR..
        $pdfMerger->duplexMerge(); //Merges your provided PDFs and adds blank pages between documents as needed to allow duplex printing

// optional parameter can be passed to the merge functions for orientation (P for protrait, L for Landscape).
// This will be used for every PDF that doesn't have an orientation specified

        $pdfMerger->save("file_path.pdf");

// OR...
        $pdfMerger->save("file_name.pdf", "download");
// REPLACE 'download' WITH 'browser', 'download', 'string', or 'file' for output options
    }

    public static function downloadMergedPdfPengajuanPenghapusan(PengajuanPenghapusan $pengajuanPenghapusan)
    {
        $details = PengajuanPenghapusanDetail::where('pengajuan_penghapusan_id', $pengajuanPenghapusan->id)
            ->where('status', 'approved')
            ->where('step', '<=', 100)
            ->orderBy('step', 'asc')
            ->get();

        if (!empty($details)) {
            $pdfMerger = PdfMerger::init(); //Initialize the merger
            foreach ($details as $detail) {
                /** @var PengajuanPenghapusanDetail $detail */
                $pdfMerger->addPDF($detail->file->location);
            }

            $pdfMerger->merge();;
//            $pdfMerger->duplexMerge();
            $title   = "{$pengajuanPenghapusan->id}-aggregate.pdf";
            $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}";
            $pdfMerger->save(public_path($dirname));
            $pdfMerger->save($title, "download");
        }
    }

    public static function downloadMergedPdfPengajuanPenghapusanKEMKEU(PengajuanPenghapusan $pengajuanPenghapusan)
    {
        $detailKemkeus = PengajuanPenghapusanDetail::where('pengajuan_penghapusan_id', $pengajuanPenghapusan->id)
            ->where('status', 'approved')
            ->where('step', '>', 100)
            ->orderBy('step', 'asc')
            ->get();
        $details= PengajuanPenghapusanDetail::where('pengajuan_penghapusan_id', $pengajuanPenghapusan->id)
            ->where('status', 'approved')
            ->where('step', '<=', 100)
            ->orderBy('step', 'asc')
            ->get();

        if (!empty($details)) {
            $pdfMerger = PdfMerger::init(); //Initialize the merger
            foreach ($detailKemkeus as $detail) {
                /** @var PengajuanPenghapusanDetail $detail */
                $pdfMerger->addPDF($detail->file->location);
            }
            foreach ($details as $detail) {
                /** @var PengajuanPenghapusanDetail $detail */
                $pdfMerger->addPDF($detail->file->location);
            }

            $pdfMerger->merge();;
//            $pdfMerger->duplexMerge();
            $title   = "{$pengajuanPenghapusan->id}-kemkeu.pdf";
            $dirname = "pengajuan-penghapusan/{$pengajuanPenghapusan->id}/{$title}";
            $pdfMerger->save(public_path($dirname));
            $pdfMerger->save($title, "download");
        }
    }
}
