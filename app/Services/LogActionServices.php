<?php

namespace App\Services;

use App\Models\Log\LogAction;

class LogActionServices
{
    const TIPE_GANTI_PASSWORD  = 'change_password';
    const TIPE_RESET_PASSWORD  = 'reset_password';
    const TIPE_GENERAL_SETTING = 'general_setting';
    const TIPE_DARK_MODE_ADMIN = 'dark_mode_admin';
    const TIPE_CREATE_DATA     = 'create_data';
    const TIPE_UPDATE_DATA     = 'update_data';
    const TIPE_DELETE_DATA     = 'delete_data';
    const TIPE_UPDATE_LOCATION = 'update_location';

    public static function createLogAction($tipe = null, $action = null, $class = null, $old_data = null, $new_data = null)
    {
        $logAction             = new LogAction();
        $logAction->tipe       = $tipe;
        $logAction->action     = $action;
        $logAction->class      = $class;
        $logAction->old_data   = $old_data;
        $logAction->new_data   = $new_data;
        $logAction->user_id    = \Auth::check() ? \Auth::id() : null;
        $logAction->wilayah_id = \Auth::check() ? \Auth::user()->wilayah_id : null;
        $logAction->satker_id  = \Auth::check() ? \Auth::user()->satker_id : null;
        $logAction->save();

        return $logAction;
    }
}
