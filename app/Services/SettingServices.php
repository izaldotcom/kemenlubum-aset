<?php

namespace App\Services;

use App\Models\Setting;

class SettingServices
{
    public static function getSettingByKey($key, $byUser = false)
    {
        if ($byUser)
            $setting = Setting::where('setting_key', $key)->where('user_id', \Auth::id())->first();
        else
            $setting = Setting::where('setting_key', $key)->first();

        if (!$setting) {
            $setting              = new Setting();
            $setting->setting_key = $key;
        }

        return $setting;
    }
}
