<?php

namespace App\Services;

class UserServices
{
    public static function sendEmailActivation($email)
    {
        $hash = \Crypt::encrypt($email);
        $data = [
            'email'      => $email,
            'hash'       => $hash,
            'action_url' => url("/") . "/activation/" . $hash,
        ];

//        MailServiceProvider::wrapperMailSend('emailtemplate2.email_activate', [
//            'introLines' => [
//                __('email.activation.intro'),
//            ],
//            'actionText' => __('email.activation.active'),
//            'actionUrl'  => $data['action_url'],
//            'outroLines' => [__('email.activation.outro')],
//            'level'      => 'primary',
//        ], function ($m) use ($email, $data) {
//            $m->to($email, $email)->subject(__('email.activation.subject'));
//        }, $email, __('email.activation.subject'), 'user_activation');
    }
}
