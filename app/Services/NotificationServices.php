<?php

namespace App\Services;

use App\Models\Notification\Notification;
use App\User;

class NotificationServices
{
    const ACTION_PENGHAPUSAN_TAMBAH  = 'tambah-penghapusan';
    const ACTION_PAJAK_WARNING_1     = 'pajak-warning-1';
    const ACTION_PAJAK_WARNING_2     = 'pajak-warning-2';
    const ACTION_PAJAK_DUE           = 'pajak-due';
    const ACTION_PAJAK_RFS_WARNING_1 = 'pajak-rfs-warning-1';
    const ACTION_PAJAK_RFS_WARNING_2 = 'pajak-rfs-warning-2';
    const ACTION_PAJAK_RFS_DUE       = 'pajak-rfs-due';
    const ACTION_ASURANSI_WARNING_1  = 'asuransi-warning-1';
    const ACTION_ASURANSI_WARNING_2  = 'asuransi-warning-2';
    const ACTION_ASURANSI_DUE        = 'asuransi-due';

    public static function getMyNotifications($limit = null)
    {
        /** @var User $user */
        $user          = \Auth::user();
        $arrActions    = [];
        $notifications = $user->notifications();


//        if ($user->hasRole(['super_admin', 'admin', 'pimpinan', 'admin_pajak_asuransi'])) {
//            $arrActions[] = self::ACTION_PAJAK_DUE;
//            $arrActions[] = self::ACTION_PAJAK_WARNING_1;
//            $arrActions[] = self::ACTION_PAJAK_WARNING_2;
//            $arrActions[] = self::ACTION_PAJAK_RFS_DUE;
//            $arrActions[] = self::ACTION_PAJAK_RFS_WARNING_1;
//            $arrActions[] = self::ACTION_PAJAK_RFS_WARNING_2;
//            $arrActions[] = self::ACTION_ASURANSI_DUE;
//            $arrActions[] = self::ACTION_ASURANSI_WARNING_1;
//            $arrActions[] = self::ACTION_ASURANSI_WARNING_2;
//        }
//
//        $notifications->whereIn('action', $arrActions);

        $notifications->orderBy('created_at', 'DESC');
        if (!empty($limit)) {
            $notifications->take($limit);
        }
        $notifications = $notifications->get();

        return $notifications;
    }

    public static function attachPajakNotificationToUser(Notification $notification)
    {
        $users = User::whereHas('roles', function ($query) {
            $query->whereIn('name', ['super_admin', 'admin', 'pimpinan', 'admin_pajak_asuransi', 'admin_aset']);
        })->pluck('id')->toArray();

        $notification->targets()->sync($users);
    }
   public static function hapus($id)
   {
    $readnotif = Notification::onlyTrashed()->where('id',$id);
    $readnotif->forceDelete();

    return redirect('');
   }

    public static function createGeneralNotification($action = null, $icon = null, $class = null, $title = null, $message = null, $link = null, $relationIds = [])
    {
        $notification               = new Notification();
        $notification->code         = str_random(15);
        $notification->action       = $action;
        $notification->icon         = $icon;
        $notification->class        = $class;
        $notification->title        = $title;
        $notification->message      = $message;
        $notification->link         = $link;
        $notification->relation_ids = $relationIds;
        $notification->save();

        return $notification;
    }

    public static function createPajakDueNotification($kendaraanDinasIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PAJAK_DUE,
            'fa fa-fw fa-2x fa-times text-danger',
            'text-danger',
            'Pajak Due Date',
            'Beberapa pajak kendaraan dinas sudah expired. Tolong dicheck segera!',
            null,
            $kendaraanDinasIds
        );
        $notification->link = route('admin_kendaraan_list_dinas', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }

    public static function createPajakWarning1Notification($kendaraanDinasIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PAJAK_WARNING_1,
            'fa fa-fw fa-2x fa-exclamation-triangle text-info',
            'text-info',
            'Pajak Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_1 . ' hari',
            'Pajak Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_1 . ' hari. Tolong dicheck segera!',
            null,
            $kendaraanDinasIds
        );
        $notification->link = route('admin_kendaraan_list_dinas', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }

    public static function createPajakWarning2Notification($kendaraanDinasIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PAJAK_WARNING_2,
            'fa fa-fw fa-2x fa-exclamation-triangle text-warning',
            'text-warning',
            'Pajak Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_2 . ' hari',
            'Pajak Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_2 . ' hari. Tolong dicheck segera!',
            null,
            $kendaraanDinasIds
        );
        $notification->link = route('admin_kendaraan_list_dinas', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }

    public static function createPajakRFSDueNotification($kendaraanDinasIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PAJAK_DUE,
            'fa fa-fw fa-2x fa-times text-danger',
            'text-danger',
            'Pajak RFS Due Date',
            'Beberapa pajak rfs kendaraan dinas sudah expired. Tolong dicheck segera!',
            null,
            $kendaraanDinasIds
        );
        $notification->link = route('admin_kendaraan_list_dinas', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }

    public static function createPajakRFSWarning1Notification($kendaraanDinasIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PAJAK_WARNING_1,
            'fa fa-fw fa-2x fa-exclamation-triangle text-info',
            'text-info',
            'Pajak RFS Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_1 . ' hari',
            'Pajak RFS Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_1 . ' hari. Tolong dicheck segera!',
            null,
            $kendaraanDinasIds
        );
        $notification->link = route('admin_kendaraan_list_dinas', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }

    public static function createPajakRFSWarning2Notification($kendaraanDinasIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PAJAK_WARNING_2,
            'fa fa-fw fa-2x fa-exclamation-triangle text-warning',
            'text-warning',
            'Pajak RFS Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_2 . ' hari',
            'Pajak RFS Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_2 . ' hari. Tolong dicheck segera!',
            null,
            $kendaraanDinasIds
        );
        $notification->link = route('admin_kendaraan_list_dinas', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }
    public static function createPenghapusanDueNotification($pengajuanServiceIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PENGHAPUSAN_TAMBAH,
            'fa fa-fw fa-2x fa-times text-danger',
            'text-danger',
            'Penambahan Penghapusan',
            'Ada Penghapusan Kendaraan Baru. Tolong dicheck segera!',
            null,
            $pengajuanServiceIds
        );
        $notification->link = route('admin_pengajuan_penghapusan_kendaraan_list-penghapusan', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }


    public static function createAsuransiDueNotification($kendaraanDinasIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PAJAK_DUE,
            'fa fa-fw fa-2x fa-times text-danger',
            'text-danger',
            'Asuransi Due Date',
            'Beberapa asuransi kendaraan dinas sudah expired. Tolong dicheck segera!',
            null,
            $kendaraanDinasIds
        );
        $notification->link = route('admin_kendaraan_list_dinas', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }

    public static function createAsuransiWarning1Notification($kendaraanDinasIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PAJAK_WARNING_1,
            'fa fa-fw fa-2x fa-exclamation-triangle text-info',
            'text-info',
            'Asuransi Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_1 . ' hari',
            'Asuransi Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_1 . ' hari. Tolong dicheck segera!',
            null,
            $kendaraanDinasIds
        );
        $notification->link = route('admin_kendaraan_list_dinas', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }

    public static function createAsuransiWarning2Notification($kendaraanDinasIds = [])
    {
        $notification       = self::createGeneralNotification(
            self::ACTION_PAJAK_WARNING_2,
            'fa fa-fw fa-2x fa-exclamation-triangle text-warning',
            'text-warning',
            'Asuransi Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_2 . ' hari',
            'Asuransi Expired dalam ' . PajakNAsuransiServices::DAYS_WARNING_2 . ' hari. Tolong dicheck segera!',
            null,
            $kendaraanDinasIds
        );
        $notification->link = route('admin_kendaraan_list_dinas', [
            'code' => $notification->code,
        ]);
        $notification->save();

        self::attachPajakNotificationToUser($notification);
    }
}
