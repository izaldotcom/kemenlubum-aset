<?php

namespace App\Services;

class InputExcelServices
{
    const INPUT_EXCEL_SESSION_ID         = 'input-excel-satker';
    const INPUT_EXCEL_COUNTER_SESSION_ID = 'input-excel-counter';

    public static function initInputExcelSession()
    {
        session()->put(self::INPUT_EXCEL_SESSION_ID, []);

        return self::getInputExcelSession();
    }

    public static function getInputExcelSession()
    {
        return session()->get(self::INPUT_EXCEL_SESSION_ID, []);
    }

    public static function storeInputExcelSession($kodeSatker, $tableName)
    {
        $inputExcelSession = self::getInputExcelSession();

        if (!in_array($kodeSatker, $inputExcelSession)) {
            \DB::table($tableName)
                ->where('kode_satker', $kodeSatker)
                ->update(['status_sync' => 'none']);
            $inputExcelSession[] = $kodeSatker;
            session()->put(self::INPUT_EXCEL_SESSION_ID, $inputExcelSession);
        }

        return self::getInputExcelSession();
    }

    public static function initInputExcelSessionCounter()
    {
        session()->put(self::INPUT_EXCEL_COUNTER_SESSION_ID, [
            'data_created' => 0,
            'data_updated' => 0,
            'data_deleted' => 0,
        ]);

        return self::getInputExcelSessionCounter();
    }

    public static function getInputExcelSessionCounter()
    {
        return session()->get(self::INPUT_EXCEL_COUNTER_SESSION_ID, [
            'data_created' => 0,
            'data_updated' => 0,
            'data_deleted' => 0,
        ]);
    }

    public static function storeInputExcelSessionCounter($tipe = 'data_created', $clicker = 1)
    {
        $inputExcelSessionCounter = self::getInputExcelSessionCounter();

        $inputExcelSessionCounter[$tipe] += $clicker;

        session()->put(self::INPUT_EXCEL_COUNTER_SESSION_ID, $inputExcelSessionCounter);

        return self::getInputExcelSessionCounter();
    }
}
