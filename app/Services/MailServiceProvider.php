<?php

namespace App\Services;

use App\Models\Log\LogEmail;

class MailServiceProvider
{
    public static function wrapperMailSend($view, $arr, $callback, $destination, $subject, $type = 'other')
    {
        try {
            \Mail::send($view, $arr, $callback);
        } catch (\Exception $exception) {
            self::createLogEmail($view, $arr, $destination, $subject, $type, 'failed', $exception->getMessage());
            return;
        }
        self::createLogEmail($view, $arr, $destination, $subject, $type);
    }

    public static function createLogEmail($view, $arr, $destination, $subject, $type = 'other', $status = 'success', $exception_message = null)
    {
        $logEmail                    = new LogEmail();
        $logEmail->view              = $view;
        $logEmail->content           = $arr;
        $logEmail->destination       = $destination;
        $logEmail->subject           = $subject;
        $logEmail->type              = $type;
        $logEmail->status            = $status;
        $logEmail->exception_message = $exception_message;
        $logEmail->save();
    }
}
