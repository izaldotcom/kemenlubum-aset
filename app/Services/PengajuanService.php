<?php
namespace App\Services;

use App\Models\Kendaraan\PengajuanPenghapusan;
use App\Models\PenghapusanKendaraan;
use Carbon\Carbon;


class PengajuanService
{
    const STATUS_NEW     = 'new';


    public static function checkForNewPenghapusan()
    {
//        \DB::table('kendaraan_dinas')->update([
//            'status_pajak'     => 'clear',
//            'status_pajak_rfs' => 'clear',
//            'status_asuransi'  => 'clear',
//        ]);
        // PAJAK ==============================================================================
        $pengajuanService = PengajuanPenghapusan::where(
            'created_at' , '<=', Carbon::now()->format('Y-m-d')
            )->whereIn('status', [self::STATUS_NEW])
            ->get();

        if (!empty($pengajuanService)) {
            $pengajuanServiceIds = [];
            foreach ($pengajuanService as $kd) {
                /** @var PengajuanPenghapusan $kd */
                $kd->created_at = Carbon::now();
                $kd->status         = self::STATUS_NEW;
                $kd->save();
                $pengajuanServiceIds[] = $kd->id;
            }
            if (count($pengajuanServiceIds))
                NotificationServices::createPenghapusanDueNotification($pengajuanServiceIds);
        }
    }
}
