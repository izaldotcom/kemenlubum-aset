<?php

namespace App\Services;

use App\Models\Maps\Country;

class FushionTableServices
{
    public static function generateFushionTableData()
    {
        $data          = new \stdClass();
        $data->kind    = "fusiontables#sqlresponse";
        $data->columns = [
            "name",
            "kml_4326",
        ];
        $data->rows    = [];

        $countries = Country::all();

        foreach ($countries as $country) {
            $data->rows[] = [
                $country->name,
                $country->geometry,
            ];
        }

        return $data;
    }

    public static function updateFushionTableData()
    {
        $response = \Curl::to("https://www.googleapis.com/fusiontables/v1/query")
            ->withData([
                'sql' => 'SELECT name, kml_4326 FROM 1foc3xO9DyfSIF6ofvN0kp2bxSfSeKog5FbdWdQ',
                'key' => 'AIzaSyAm9yWCV7JPCTHCJut8whOjARd7pwROFDQ',
            ])
            ->get();

        $countries = json_decode($response)->rows;

        foreach ($countries as $country) {
            $countryDb = Country::where('name', $country[0])->first();

            if (empty($countryDb)) {
                $countryDb = new Country();
            }

            $countryDb->name     = $country[0];
            $countryDb->geometry = $country[1];
            $countryDb->save();
        }

        return self::generateFushionTableData();
    }
}
