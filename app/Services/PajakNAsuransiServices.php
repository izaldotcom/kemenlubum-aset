<?php

namespace App\Services;

use App\Models\KendaraanDinas;
use Carbon\Carbon;

class PajakNAsuransiServices
{
    const STATUS_CLEAR     = 'clear';
    const STATUS_DUE       = 'due';
    const STATUS_WARNING_1 = 'warning01';
    const STATUS_WARNING_2 = 'warning02';

    const DAYS_WARNING_1 = 30;
    const DAYS_WARNING_2 = 15;

    public static function checkForDuePajak()
    {
//        \DB::table('kendaraan_dinas')->update([
//            'status_pajak'     => 'clear',
//            'status_pajak_rfs' => 'clear',
//            'status_asuransi'  => 'clear',
//        ]);
        // PAJAK ==============================================================================
        $kendaraanDinas = KendaraanDinas::where(
            'tanggal_pajak', '<=', Carbon::now()->format('Y-m-d')
        )->whereIn('status_pajak', [self::STATUS_CLEAR, self::STATUS_WARNING_1, self::STATUS_WARNING_2])
            ->get();

        if (!empty($kendaraanDinas)) {
            $kendaraanDinasIds = [];
            foreach ($kendaraanDinas as $kd) {
                /** @var KendaraanDinas $kd */
                $kd->tanggal_status_pajak = Carbon::now();
                $kd->status_pajak         = self::STATUS_DUE;
                $kd->save();
                $kendaraanDinasIds[] = $kd->id;
            }
            if (count($kendaraanDinasIds))
                NotificationServices::createPajakDueNotification($kendaraanDinasIds);
        }

        $kendaraanDinas = KendaraanDinas::where(
            'tanggal_pajak', '<=', Carbon::now()->addDays(self::DAYS_WARNING_2)->format('Y-m-d')
        )->whereIn('status_pajak', [self::STATUS_CLEAR, self::STATUS_WARNING_1])
            ->get();

        if (!empty($kendaraanDinas)) {
            $kendaraanDinasIds = [];
            foreach ($kendaraanDinas as $kd) {
                /** @var KendaraanDinas $kd */
                $kd->tanggal_status_pajak = Carbon::now();
                $kd->status_pajak         = self::STATUS_WARNING_2;
                $kd->save();
                $kendaraanDinasIds[] = $kd->id;
            }
            if (count($kendaraanDinasIds))
                NotificationServices::createPajakWarning2Notification($kendaraanDinasIds);
        }

        $kendaraanDinas = KendaraanDinas::where(
            'tanggal_pajak', '<=', Carbon::now()->addDays(self::DAYS_WARNING_1)->format('Y-m-d')
        )->whereIn('status_pajak', [self::STATUS_CLEAR])
            ->get();

        if (!empty($kendaraanDinas)) {
            $kendaraanDinasIds = [];
            foreach ($kendaraanDinas as $kd) {
                /** @var KendaraanDinas $kd */
                $kd->tanggal_status_pajak = Carbon::now();
                $kd->status_pajak         = self::STATUS_WARNING_1;
                $kd->save();
                $kendaraanDinasIds[] = $kd->id;
            }
            if (count($kendaraanDinasIds))
                NotificationServices::createPajakWarning1Notification($kendaraanDinasIds);
        }

        // PAJAK RFS ==============================================================================

        $kendaraanDinas = KendaraanDinas::where(
            'tanggal_pajak_rfs', '<=', Carbon::now()->format('Y-m-d')
        )->whereIn('status_pajak_rfs', [self::STATUS_CLEAR, self::STATUS_WARNING_1, self::STATUS_WARNING_2])
            ->get();

        if (!empty($kendaraanDinas)) {
            $kendaraanDinasIds = [];
            foreach ($kendaraanDinas as $kd) {
                /** @var KendaraanDinas $kd */
                $kd->tanggal_status_pajak_rfs = Carbon::now();
                $kd->status_pajak_rfs         = self::STATUS_DUE;
                $kd->save();
                $kendaraanDinasIds[] = $kd->id;
            }
            if (count($kendaraanDinasIds))
                NotificationServices::createPajakRFSDueNotification($kendaraanDinasIds);
        }

        $kendaraanDinas = KendaraanDinas::where(
            'tanggal_pajak_rfs', '<=', Carbon::now()->addDays(self::DAYS_WARNING_2)->format('Y-m-d')
        )->whereIn('status_pajak_rfs', [self::STATUS_CLEAR, self::STATUS_WARNING_1])
            ->get();

        if (!empty($kendaraanDinas)) {
            $kendaraanDinasIds = [];
            foreach ($kendaraanDinas as $kd) {
                /** @var KendaraanDinas $kd */
                $kd->tanggal_status_pajak_rfs = Carbon::now();
                $kd->status_pajak_rfs         = self::STATUS_WARNING_2;
                $kd->save();
                $kendaraanDinasIds[] = $kd->id;
            }
            if (count($kendaraanDinasIds))
                NotificationServices::createPajakRFSWarning2Notification($kendaraanDinasIds);
        }

        $kendaraanDinas = KendaraanDinas::where(
            'tanggal_pajak_rfs', '<=', Carbon::now()->addDays(self::DAYS_WARNING_1)->format('Y-m-d')
        )->whereIn('status_pajak_rfs', [self::STATUS_CLEAR])
            ->get();

        if (!empty($kendaraanDinas)) {
            $kendaraanDinasIds = [];
            foreach ($kendaraanDinas as $kd) {
                /** @var KendaraanDinas $kd */
                $kd->tanggal_status_pajak_rfs = Carbon::now();
                $kd->status_pajak_rfs         = self::STATUS_WARNING_1;
                $kd->save();
                $kendaraanDinasIds[] = $kd->id;
            }
            if (count($kendaraanDinasIds))
                NotificationServices::createPajakRFSWarning1Notification($kendaraanDinasIds);
        }

        // ASURANSI ==============================================================================

        $kendaraanDinas = KendaraanDinas::where(
            'jatuh_tempo_akhir', '<=', Carbon::now()->format('Y-m-d')
        )->whereIn('status_asuransi', [self::STATUS_CLEAR, self::STATUS_WARNING_1, self::STATUS_WARNING_2])
            ->get();

        if (!empty($kendaraanDinas)) {
            $kendaraanDinasIds = [];
            foreach ($kendaraanDinas as $kd) {
                /** @var KendaraanDinas $kd */
                $kd->tanggal_status_asuransi = Carbon::now();
                $kd->status_asuransi         = self::STATUS_DUE;
                $kd->save();
                $kendaraanDinasIds[] = $kd->id;
            }
            if (count($kendaraanDinasIds))
                NotificationServices::createAsuransiDueNotification($kendaraanDinasIds);
        }

        $kendaraanDinas = KendaraanDinas::where(
            'jatuh_tempo_akhir', '<=', Carbon::now()->addDays(self::DAYS_WARNING_2)->format('Y-m-d')
        )->whereIn('status_asuransi', [self::STATUS_CLEAR, self::STATUS_WARNING_1])
            ->get();

        if (!empty($kendaraanDinas)) {
            $kendaraanDinasIds = [];
            foreach ($kendaraanDinas as $kd) {
                /** @var KendaraanDinas $kd */
                $kd->tanggal_status_asuransi = Carbon::now();
                $kd->status_asuransi         = self::STATUS_WARNING_2;
                $kd->save();
                $kendaraanDinasIds[] = $kd->id;
            }
            if (count($kendaraanDinasIds))
                NotificationServices::createAsuransiWarning2Notification($kendaraanDinasIds);
        }

        $kendaraanDinas = KendaraanDinas::where(
            'jatuh_tempo_akhir', '<=', Carbon::now()->addDays(self::DAYS_WARNING_1)->format('Y-m-d')
        )->whereIn('status_asuransi', [self::STATUS_CLEAR])
            ->get();

        if (!empty($kendaraanDinas)) {
            $kendaraanDinasIds = [];
            foreach ($kendaraanDinas as $kd) {
                /** @var KendaraanDinas $kd */
                $kd->tanggal_status_asuransi = Carbon::now();
                $kd->status_asuransi         = self::STATUS_WARNING_1;
                $kd->save();
                $kendaraanDinasIds[] = $kd->id;
            }
            if (count($kendaraanDinasIds))
                NotificationServices::createAsuransiWarning1Notification($kendaraanDinasIds);
        }
    }
}
