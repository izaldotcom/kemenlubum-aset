<?php

namespace App\Services;

use App\Models\Gedung;
use App\Models\Kendaraan;
use App\Models\Maps\Country;
use App\Models\RumahNegara;
use App\Models\Satker;
use App\Models\Tanah;
use App\Models\Wilayah;
use App\User;
use Carbon\Carbon;

class SummaryAssetServices
{
    public static function getMySummaryAsset()
    {
        $summary = [];

        if (\Auth::check()) {
            /** @var User $user */
            $user    = \Auth::user();
            $wilayah = $user->wilayah;
            $satker  = $user->satker;

            if (!empty($satker)) {
                $summary = self::getSummaryAsset(null, null, $satker->kode_satker);
            } else {
                $summary = self::getSummaryAsset();
            }
        }

        return $summary;
    }

    public static function getSummaryAsset($slugCountry = null, $slugWilayah = null, $slugSatker = null)
    {
        $country = null;
        $wilayah = null;
        $satker  = null;

        if (!empty($slugCountry))
            $country = Country::where('slug', $slugCountry)->first();

        if (!empty($slugWilayah))
            $wilayah = Wilayah::where('kode_wilayah', $slugWilayah)->first();

        if (!empty($slugSatker))
            $satker = Satker::where('kode_satker', $slugSatker)
            ->orWhere('wilayah_id', $slugSatker)->first()->toArray();

        $tanah     = Tanah::select([
            \DB::raw("COUNT(id) as total"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Baik' THEN 1 ELSE 0 END ) as total_baik"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Rusak Berat' THEN 1 ELSE 0 END ) as total_rusak_berat"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Rusak Ringan' THEN 1 ELSE 0 END ) as total_rusak_ringan"),
        ]);
        $gedung    = Gedung::select([
            \DB::raw("COUNT(id) as total"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Baik' THEN 1 ELSE 0 END ) as total_baik"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Rusak Berat' THEN 1 ELSE 0 END ) as total_rusak_berat"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Rusak Ringan' THEN 1 ELSE 0 END ) as total_rusak_ringan"),
        ]);
        $rumah     = RumahNegara::select([
            \DB::raw("COUNT(id) as total"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Baik' THEN 1 ELSE 0 END ) as total_baik"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Rusak Berat' THEN 1 ELSE 0 END ) as total_rusak_berat"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Rusak Ringan' THEN 1 ELSE 0 END ) as total_rusak_ringan"),
        ]);
        $tahunNow  = Carbon::now()->format('Y');
        $kendaraan = Kendaraan::select([
            \DB::raw("COUNT(id) as total"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Baik' THEN 1 ELSE 0 END ) as total_baik"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Rusak Berat' THEN 1 ELSE 0 END ) as total_rusak_berat"),
            \DB::raw("SUM( CASE WHEN kondisi = 'Rusak Ringan' THEN 1 ELSE 0 END ) as total_rusak_ringan"),

            \DB::raw("SUM( CASE WHEN {$tahunNow} - YEAR(tanggal_perolehan) <= 5 THEN 1 ELSE 0 END ) as total_5_below"),
            \DB::raw("SUM( CASE WHEN {$tahunNow} - YEAR(tanggal_perolehan) > 5 AND {$tahunNow} - YEAR(tanggal_perolehan) <= 10 THEN 1 ELSE 0 END ) as total_5_to_10"),
            \DB::raw("SUM( CASE WHEN {$tahunNow} - YEAR(tanggal_perolehan) > 10 THEN 1 ELSE 0 END ) as total_10_above"),

            \DB::raw("SUM( CASE WHEN kode_barang NOT IN ('3020104001', '3020302001', '3020107006', '3020104002') THEN 1 ELSE 0 END ) as total_empat"),
            \DB::raw("SUM( CASE WHEN kode_barang NOT IN ('3020104001', '3020302001', '3020107006', '3020104002') AND kondisi = 'Baik' THEN 1 ELSE 0 END ) as total_empat_baik"),
            \DB::raw("SUM( CASE WHEN kode_barang NOT IN ('3020104001', '3020302001', '3020107006', '3020104002') AND kondisi = 'Rusak Berat' THEN 1 ELSE 0 END ) as total_empat_rusak_berat"),
            \DB::raw("SUM( CASE WHEN kode_barang NOT IN ('3020104001', '3020302001', '3020107006', '3020104002') AND kondisi = 'Rusak Ringan' THEN 1 ELSE 0 END ) as total_empat_rusak_ringan"),

            \DB::raw("SUM( CASE WHEN kode_barang IN ('3020104001', '3020302001', '3020107006', '3020104002') THEN 1 ELSE 0 END ) as total_dua"),
            \DB::raw("SUM( CASE WHEN kode_barang IN ('3020104001', '3020302001', '3020107006', '3020104002') AND kondisi = 'Baik' THEN 1 ELSE 0 END ) as total_dua_baik"),
            \DB::raw("SUM( CASE WHEN kode_barang IN ('3020104001', '3020302001', '3020107006', '3020104002') AND kondisi = 'Rusak Berat' THEN 1 ELSE 0 END ) as total_dua_rusak_berat"),
            \DB::raw("SUM( CASE WHEN kode_barang IN ('3020104001', '3020302001', '3020107006', '3020104002') AND kondisi = 'Rusak Ringan' THEN 1 ELSE 0 END ) as total_dua_rusak_ringan"),
        ]);
        if (!empty($country)) {
            $tanah->whereHas('satker.countries', function ($query) use ($slugCountry) {
                $query->where('slug', $slugCountry);
            });
            $gedung->whereHas('satker.countries', function ($query) use ($slugCountry) {
                $query->where('slug', $slugCountry);
            });
            $rumah->whereHas('satker.countries', function ($query) use ($slugCountry) {
                $query->where('slug', $slugCountry);
            });
            $kendaraan->whereHas('satker.countries', function ($query) use ($slugCountry) {
                $query->where('slug', $slugCountry);
            });
        }
        if (!empty($wilayah)) {
            $tanah->whereHas('satker.wilayah', function ($query) use ($slugWilayah) {
                $query->where('kode_wilayah', $slugWilayah);
            });
            $gedung->whereHas('satker.wilayah', function ($query) use ($slugWilayah) {
                $query->where('kode_wilayah', $slugWilayah);
            });
            $rumah->whereHas('satker.wilayah', function ($query) use ($slugWilayah) {
                $query->where('kode_wilayah', $slugWilayah);
            });
            $kendaraan->whereHas('satker.wilayah', function ($query) use ($slugWilayah) {
                $query->where('kode_wilayah', $slugWilayah);
            });
        }
        if (!empty($satker)) {
            $tanah->where('kode_satker', $slugSatker);
            $gedung->where('kode_satker', $slugSatker);
            $rumah->where('kode_satker', $slugSatker);
            $kendaraan->where('kode_satker', $slugSatker);
        }

        $gedungSummary = $gedung->first()->toArray();
        $rumahSummary  = $rumah->first()->toArray();

        $bangunanSummary = [
            'total'              => $gedungSummary['total'] + $rumahSummary['total'],
            'total_baik'         => $gedungSummary['total_baik'] + $rumahSummary['total_baik'],
            'total_rusak_berat'  => $gedungSummary['total_rusak_berat'] + $rumahSummary['total_rusak_berat'],
            'total_rusak_ringan' => $gedungSummary['total_rusak_ringan'] + $rumahSummary['total_rusak_ringan'],
        ];

        $summary = [
            'tanah'     => $tanah->first()->toArray(),
            'bangunan'  => $bangunanSummary,
            'mobilitas' => $kendaraan->first()->toArray(),
        ];

        return $summary;
    }
}
