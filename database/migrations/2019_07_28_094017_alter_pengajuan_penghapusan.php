<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPengajuanPenghapusan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan_penghapusans', function (Blueprint $table) {
            $table->longText('alasan_kategori_penghapusan')->nullable();
            $table->longText('misc_attribute')->nullable();
            $table->longText('susunan_panitia')->nullable();
            $table->longText('surat_pernyataan_signature')->nullable();
            $table->dropColumn('attribute_fillter');
        });

        Schema::table('pengajuan_penghapusan_details', function (Blueprint $table) {
            $table->longText('misc_attribute')->nullable();
            $table->dropColumn('attribute_fillter');
            $table->dropColumn('attribute_fillter_array');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
