<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuanPenghapusanKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_penghapusan_kendaraan', function (Blueprint $table) {
            $table->bigInteger('kendaraan_id')->unsigned()->nullable();
            $table->bigInteger('pengajuan_penghapusan_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_penghapusan_kendaraan');
    }
}
