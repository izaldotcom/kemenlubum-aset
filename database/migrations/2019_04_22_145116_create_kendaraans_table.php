<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kendaraans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_barang')->nullable();
            $table->bigInteger('nup')->unsigned()->nullable();
            $table->string('kode_satker')->nullable();
            $table->string('nama_satker')->nullable();
            $table->bigInteger('kib')->unsigned()->nullable();
            $table->string('nama_barang')->nullable();
            $table->string('kondisi')->nullable();
            $table->string('merk_type')->nullable();
            $table->date('tanggal_rekam_pertama')->nullable();
            $table->date('tanggal_perolehan')->nullable();
            $table->decimal('nilai_perolehan_pertama', 50, 2)->default(0)->nullable();
            $table->decimal('nilai_mutasi', 50, 2)->default(0)->nullable();
            $table->decimal('nilai_perolehan', 50, 2)->default(0)->nullable();
            $table->decimal('nilai_penyusutan', 50, 2)->default(0)->nullable();
            $table->decimal('nilai_buku', 50, 2)->default(0)->nullable();
            $table->float('kuantitas')->default(1)->nullable();
            $table->float('jumlah_foto')->default(0)->nullable();
            $table->string('status_penggunaan')->nullable();
            $table->string('status_pengelolaan')->nullable();
            $table->date('tanggal_psp')->nullable();
            $table->string('no_bpkb')->nullable();
            $table->string('no_polisi')->nullable();
            $table->string('pemakai')->nullable();
            $table->float('jumlah_kib')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kendaraans');
    }
}
