<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKeteranganToPengajuanPenghapusansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan_penghapusans', function (Blueprint $table) {
            $table->string('no_pengajuan_penghapusan')->nullable();
            $table->longText('keterangan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pengajuan_penghapusans', function (Blueprint $table) {
            $table->dropColumn('no_pengajuan_penghapusan');
            $table->dropColumn('keterangan');
        });
    }
}
