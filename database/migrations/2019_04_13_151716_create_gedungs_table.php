<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGedungsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gedungs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_barang')->nullable();
            $table->bigInteger('nup')->unsigned()->nullable();
            $table->string('kode_satker')->nullable();
            $table->string('nama_satker')->nullable();
            $table->bigInteger('kib')->unsigned()->nullable();
            $table->string('nama_barang')->nullable();
            $table->string('kondisi')->nullable();
            $table->string('jenis_dokumen')->nullable();
            $table->string('kepemilikan')->nullable();
            $table->string('jenis_sertifikat')->nullable();
            $table->string('merk_type')->nullable();
            $table->date('tanggal_rekam_pertama')->nullable();
            $table->date('tanggal_perolehan')->nullable();
            $table->decimal('nilai_perolehan_pertama', 50, 2)->default(0)->nullable();
            $table->decimal('nilai_mutasi', 50, 2)->default(0)->nullable();
            $table->decimal('nilai_perolehan', 50, 2)->default(0)->nullable();
            $table->decimal('nilai_penyusutan', 50, 2)->default(0)->nullable();
            $table->decimal('nilai_buku', 50, 2)->default(0)->nullable();
            $table->float('kuantitas')->default(1)->nullable();
            $table->float('jumlah_foto')->default(0)->nullable();
            $table->float('jumlah_lantai')->default(0)->nullable();
            $table->float('luas_bangunan')->default(0)->nullable();
            $table->float('luas_dasar_bangunan')->default(0)->nullable();
            $table->text('alamat')->nullable();
            $table->string('jalan')->nullable();
            $table->string('kode_kota')->nullable();
            $table->string('kode_provinsi')->nullable();
            $table->string('uraian_kota')->nullable();
            $table->string('status_penggunaan')->nullable();
            $table->string('status_pengelolaan')->nullable();
            $table->string('no_psp')->nullable();
            $table->date('tanggal_psp')->nullable();
            $table->float('jumlah_kib')->default(0)->nullable();
            $table->float('sbsk')->default(0)->nullable();
            $table->float('optimalisasi')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gedungs');
    }
}
