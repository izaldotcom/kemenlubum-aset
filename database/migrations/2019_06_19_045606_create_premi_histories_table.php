<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premi_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_polis')->nullable();
            $table->string('perusahaan_asuransi')->nullable();
            $table->date('jatuh_tempo_awal')->nullable();
            $table->date('jatuh_tempo_akhir')->nullable();
            $table->decimal('premi', 50, 2)->unsigned()->nullable();
            $table->decimal('tertanggung', 50, 2)->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premi_histories');
    }
}
