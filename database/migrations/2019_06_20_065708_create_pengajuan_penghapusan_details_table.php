<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuanPenghapusanDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_penghapusan_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pengajuan_penghapusan_id')->unsigned()->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('approver_id')->unsigned()->nullable();
            $table->integer('step')->unsigned()->default(1);
            $table->string('status')->default('new');
            $table->longText('attribute_fillter')->nullable();
            $table->bigInteger('file_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_penghapusan_details');
    }
}
