<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePremiKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premi_kendaraans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('merk_kendaraan')->nullable();
            $table->string('jenis')->nullable();
            $table->string('tahun')->nullable();
            $table->string('perusahaan_asuransi')->nullable();
            $table->string('no_polisi')->nullable();
            $table->string('no_polis')->nullable();
            $table->string('pengguna')->nullable();
            $table->date('jatuh_tempo_awal')->nullable();
            $table->date('jatuh_tempo_akhir')->nullable();
            $table->decimal('premi', 50, 2)->unsigned()->nullable();
            $table->decimal('tertanggung', 50, 2)->unsigned()->nullable();
            $table->string('status_sync')->default('sync');
            $table->datetime('last_sync')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premi_kendaraans');
    }
}
