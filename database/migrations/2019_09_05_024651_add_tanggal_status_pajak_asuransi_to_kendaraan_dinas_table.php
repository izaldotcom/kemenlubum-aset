<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTanggalStatusPajakAsuransiToKendaraanDinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kendaraan_dinas', function (Blueprint $table) {
            $table->dateTime('tanggal_status_pajak')->nullable();
            $table->dateTime('tanggal_status_pajak_rfs')->nullable();
            $table->dateTime('tanggal_status_asuransi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kendaraan_dinas', function (Blueprint $table) {
            //
        });
    }
}
