<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewCustomField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan_penghapusans', function (Blueprint $table) {
            $table->longText('attribute_fillter')->nullable();
            $table->integer('total_unit')->unsigned()->default(0);
            $table->decimal('total_harga_perolehan', 50, 2)->unsigned()->default(0);
            $table->decimal('total_harga_taksiran', 50, 2)->unsigned()->default(0);
            $table->dateTime('approved_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
