<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuanPenghapusanKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan_penghapusan_kendaraans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('pengajuan_penghapusan_id')->unsigned()->nullable();
            $table->bigInteger('kendaraan_id')->unsigned()->nullable();
            $table->string('tipe')->default(0);
            $table->float('umur_kendaraan')->default(0);
            $table->decimal('nilai_wajar', 20, 2)->nullable();
            $table->decimal('nilai_limit', 20, 2)->nullable();
            $table->longText('alasan_penghapusan')->nullable();
            $table->longText('misc_attribute')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan_penghapusan_kendaraans');
    }
}
