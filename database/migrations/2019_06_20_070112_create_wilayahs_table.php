<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWilayahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wilayahs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_wilayah')->nullable();
            $table->string('kode_wilayah')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });

        Schema::table('satkers', function (Blueprint $table) {
            $table->bigInteger('wilayah_id')->unsigned()->nullable();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('wilayah_id')->unsigned()->nullable();
            $table->bigInteger('satker_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('wilayah_id');
            $table->dropColumn('satker_id');
        });

        Schema::table('satkers', function (Blueprint $table) {
            $table->dropColumn('wilayah_id');
        });

        Schema::dropIfExists('wilayahs');
    }
}
