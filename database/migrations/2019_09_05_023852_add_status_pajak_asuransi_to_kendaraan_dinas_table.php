<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusPajakAsuransiToKendaraanDinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kendaraan_dinas', function (Blueprint $table) {
            $table->string('status_pajak')->default('clear');
            $table->string('status_pajak_rfs')->default('clear');
            $table->string('status_asuransi')->default('clear');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kendaraan_dinas', function (Blueprint $table) {
            //
        });
    }
}
