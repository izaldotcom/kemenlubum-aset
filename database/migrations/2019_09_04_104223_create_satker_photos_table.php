<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSatkerPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('satker_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('satker_id')->unsigned()->nullable();
            $table->bigInteger('file_id')->unsigned()->nullable();
            $table->integer('seed')->default(0);
            $table->string('type')->default('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('satker_photos');
    }
}
