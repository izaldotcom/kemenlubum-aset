<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNoSuratKepala extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan_penghapusans', function (Blueprint $table) {
            $table->string('no_surat_kepala')->nullable();
        });

        Schema::table('pengajuan_penghapusan_kendaraans', function (Blueprint $table) {
            $table->string('no_rangka')->nullable();
            $table->string('tahun_pembuatan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
