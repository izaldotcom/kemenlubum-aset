<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPivotSyncToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kendaraans', function (Blueprint $table) {
            $table->string('status_sync')->default('sync');
            $table->dateTime('last_sync')->nullable();
        });
        Schema::table('rumah_negaras', function (Blueprint $table) {
            $table->string('status_sync')->default('sync');
            $table->dateTime('last_sync')->nullable();
        });
        Schema::table('tanahs', function (Blueprint $table) {
            $table->string('status_sync')->default('sync');
            $table->dateTime('last_sync')->nullable();
        });
        Schema::table('gedungs', function (Blueprint $table) {
            $table->string('status_sync')->default('sync');
            $table->dateTime('last_sync')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
