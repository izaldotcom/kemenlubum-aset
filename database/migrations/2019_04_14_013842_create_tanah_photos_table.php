<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTanahPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanah_photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tanah_id')->unsigned()->nullable();
            $table->bigInteger('file_id')->unsigned()->nullable();
            $table->integer('seed')->default(0);
            $table->string('type')->default('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanah_photos');
    }
}
