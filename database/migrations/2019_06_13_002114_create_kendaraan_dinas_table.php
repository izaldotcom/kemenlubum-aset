<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKendaraanDinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kendaraan_dinas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipe')->nullable();
            $table->string('station')->nullable();
            $table->string('kib')->nullable();
            $table->string('penanggung_jawab')->nullable();
            $table->string('no_polisi')->nullable();
            $table->string('no_polisi_rfs')->nullable();
            $table->string('merk')->nullable();
            $table->string('jenis')->nullable();
            $table->string('tahun')->nullable();
            $table->string('warna')->nullable();
            $table->string('no_rangka')->nullable();
            $table->string('no_mesin')->nullable();
            $table->string('bpkb')->nullable();
            $table->date('tanggal_pajak')->nullable();
            $table->date('tanggal_pajak_rfs')->nullable();
            $table->string('status_sync')->default('sync');
            $table->datetime('last_sync')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kendaraan_dinas');
    }
}
