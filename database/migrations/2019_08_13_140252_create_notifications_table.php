<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('action')->nullable();
            $table->string('icon')->nullable();
            $table->string('class')->nullable();
            $table->longText('title')->nullable();
            $table->longText('message')->nullable();
            $table->longText('link')->nullable();
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->bigInteger('satker_id')->unsigned()->nullable();
            $table->bigInteger('wilayah_id')->unsigned()->nullable();
            $table->string('read')->default('unread');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
