<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MoveColumnKategoriPenghapusanId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pengajuan_penghapusan_details', function (Blueprint $table) {
            $table->dropColumn('kategori_penghapusan_id');
        });
        Schema::table('pengajuan_penghapusans', function (Blueprint $table) {
            $table->bigInteger('kategori_penghapusan_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
