<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterKendaraanDinasTableToMatchNewestTemplate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kendaraan_dinas', function (Blueprint $table) {
            $table->string('no_polis')->nullable();
            $table->decimal('premi', 50, 2)->nullable();
            $table->decimal('tertanggung', 50, 2)->nullable();
            $table->date('jatuh_tempo_awal')->nullable();
            $table->date('jatuh_tempo_akhir')->nullable();
            $table->string('perusahaan_asuransi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
