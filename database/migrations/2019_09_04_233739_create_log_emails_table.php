<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_emails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('destination')->nullable();
            $table->text('subject')->nullable();
            $table->string('view')->nullable();
            $table->text('content')->nullable();
            $table->integer('retry')->unsigned()->default(0);
            $table->string('status')->default('success');
            $table->text('exception_message')->nullable();
            $table->string('type')->default('other');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_emails');
    }
}
