<?php

use Illuminate\Database\Seeder;

class BaseUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin               = \App\Role::firstOrNew([
            'name' => 'super_admin',
        ]);
        $superAdmin->name         = 'super_admin';
        $superAdmin->display_name = 'Super Administartor';
        $superAdmin->description  = 'Administrator user for XplodeIT-Solution Administrator';
        $superAdmin->save();

        $admin               = \App\Role::firstOrNew([
            'name' => 'admin',
        ]);
        $admin->name         = 'admin';
        $admin->display_name = 'Administrator';
        $admin->description  = 'Administrator privilaged user that can access all feature';
        $admin->save();

        $han = \App\User::firstOrNew([
            'username' => 'han',
        ]);

        /** @var \App\User $han */
        $han->name              = 'Han';
        $han->email             = 'ehs.hantan@gmail.com';
        $han->password          = Hash::make('admin');
        $han->email_verified_at = date('Y-m-d');
        $han->is_verified       = true;
        $han->save();
        $han->attachRole($superAdmin);
    }
}
