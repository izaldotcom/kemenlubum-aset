<?php

use Illuminate\Database\Seeder;

class KategoriPenghapusanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $kategori01               = \App\Models\Kendaraan\KategoriPenghapusan::firstOrNew([
            'name' => 'Penjualan Tidak Secara Lelang',
        ]);
        $kategori01->seed = 1;
        $kategori01->is_active = true;
        $kategori01->save();

        $kategori02               = \App\Models\Kendaraan\KategoriPenghapusan::firstOrNew([
            'name' => 'Penjualan Secara Lelang',
        ]);
        $kategori02->seed = 2;
        $kategori02->is_active = true;
        $kategori02->save();

        $kategori03               = \App\Models\Kendaraan\KategoriPenghapusan::firstOrNew([
            'name' => 'Pemusnahan',
        ]);
        $kategori03->seed = 3;
        $kategori03->is_active = true;
        $kategori03->save();

        $kategori04               = \App\Models\Kendaraan\KategoriPenghapusan::firstOrNew([
            'name' => 'Penghapusan',
        ]);
        $kategori04->seed = 4;
        $kategori04->is_active = true;
        $kategori04->save();
    }
}
