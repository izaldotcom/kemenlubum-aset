<?php

use Illuminate\Database\Seeder;

class CategoryRoleSatkerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('satkers')
            ->whereIn('kode_satker', [
                "011010199403247000KP",
                "011110199404227000KP",
                "011020199637368000KP",
                "011030199637372000KP",
                "011060199667812000KP",
                "011050199667808000KP",
                "011040199606322000KP",
                "011080199404202000KP",
                "011070199667829000KP",
                "011090199404176000KP",
                "011010199677272000KP",
                "011010199651891000KP",
                "011010199651884000KP",
            ])->update(['category' => 'pusat']);
    }
}
