<?php

use Illuminate\Database\Seeder;

class AdminSatkerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin               = \App\Role::firstOrNew([
            'name' => 'admin_satker',
        ]);
        $superAdmin->name         = 'admin_satker';
        $superAdmin->display_name = 'Admin Satker';
        $superAdmin->description  = 'Administrator user for Satker';
        $superAdmin->save();
    }
}
