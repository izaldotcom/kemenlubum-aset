<?php

use Illuminate\Database\Seeder;

class BaseRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin               = \App\Role::firstOrNew([
            'name' => 'super_admin',
        ]);
        $superAdmin->name         = 'super_admin';
        $superAdmin->display_name = 'Super Administartor';
        $superAdmin->description  = 'Administrator user for XplodeIT-Solution Administrator';
        $superAdmin->save();

        $admin               = \App\Role::firstOrNew([
            'name' => 'admin',
        ]);
        $admin->name         = 'admin';
        $admin->display_name = 'Administrator';
        $admin->description  = 'Administrator privilaged user that can access all feature';
        $admin->save();

        $pimpinan               = \App\Role::firstOrNew([
            'name' => 'pimpinan',
        ]);
        $pimpinan->name         = 'pimpinan';
        $pimpinan->display_name = 'Pimpinan';
        $pimpinan->description  = 'User Role Pimpinan';
        $pimpinan->save();

        $staf_kendaraan               = \App\Role::firstOrNew([
            'name' => 'staf_kendaraan',
        ]);
        $staf_kendaraan->name         = 'staf_kendaraan';
        $staf_kendaraan->display_name = 'Staf Kendaraan';
        $staf_kendaraan->description  = 'User Role Staf Kendaraan';
        $staf_kendaraan->save();

        $staf_bangunan               = \App\Role::firstOrNew([
            'name' => 'staf_bangunan',
        ]);
        $staf_bangunan->name         = 'staf_bangunan';
        $staf_bangunan->display_name = 'Staf Bangunan';
        $staf_bangunan->description  = 'User Role Staf Bangunan';
        $staf_bangunan->save();

        $admin_kendaraan               = \App\Role::firstOrNew([
            'name' => 'admin_kendaraan',
        ]);
        $admin_kendaraan->name         = 'admin_kendaraan';
        $admin_kendaraan->display_name = 'Admin Kendaraan';
        $admin_kendaraan->description  = 'User Role Admin Kendaraan';
        $admin_kendaraan->save();

        $admin_bangunan               = \App\Role::firstOrNew([
            'name' => 'admin_bangunan',
        ]);
        $admin_bangunan->name         = 'admin_bangunan';
        $admin_bangunan->display_name = 'Admin Bangunan';
        $admin_bangunan->description  = 'User Role Admin Bangunan';
        $admin_bangunan->save();

        $admin_aset               = \App\Role::firstOrNew([
            'name' => 'admin_aset',
        ]);
        $admin_aset->name         = 'admin_aset';
        $admin_aset->display_name = 'Admin Aset';
        $admin_aset->description  = 'User Role Admin Aset';
        $admin_aset->save();

        $admin_pajak_asuransi               = \App\Role::firstOrNew([
            'name' => 'admin_pajak_asuransi',
        ]);
        $admin_pajak_asuransi->name         = 'admin_pajak_asuransi';
        $admin_pajak_asuransi->display_name = 'Admin Pajak Asuransi';
        $admin_pajak_asuransi->description  = 'User Role Admin Pajak Asuransi';
        $admin_pajak_asuransi->save();
    }
}
