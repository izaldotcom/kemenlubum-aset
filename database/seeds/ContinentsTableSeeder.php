<?php

use Illuminate\Database\Seeder;

class ContinentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $africa       = \App\Models\Maps\Continent::firstOrNew([
            'name' => 'Africa',
        ]);
        $africa->name = "Africa";
        $africa->save();

        $europe       = \App\Models\Maps\Continent::firstOrNew([
            'name' => 'Europe',
        ]);
        $europe->name = "Europe";
        $europe->save();

        $asia       = \App\Models\Maps\Continent::firstOrNew([
            'name' => 'Asia',
        ]);
        $asia->name = "Asia";
        $asia->save();

        $northAmerica       = \App\Models\Maps\Continent::firstOrNew([
            'name' => 'North America',
        ]);
        $northAmerica->name = "North America";
        $northAmerica->save();

        $southAmerica       = \App\Models\Maps\Continent::firstOrNew([
            'name' => 'South America',
        ]);
        $southAmerica->name = "South America";
        $southAmerica->save();

        $australia       = \App\Models\Maps\Continent::firstOrNew([
            'name' => 'Australia',
        ]);
        $australia->name = "Australia";
        $australia->save();

        $antartica       = \App\Models\Maps\Continent::firstOrNew([
            'name' => 'Antarctica',
        ]);
        $antartica->name = "Antarctica";
        $antartica->save();

    }
}
