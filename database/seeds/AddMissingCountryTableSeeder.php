<?php

use App\Models\Maps\Continent;
use Illuminate\Database\Seeder;

class AddMissingCountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singapore               = \App\Models\Maps\Country::firstOrNew([
            'slug' => 'singapore',
        ]);
        $singapore->name         = "Singapore";
        $singapore->continent_id = Continent::where('name', 'Asia')->first()->id;
        $singapore->save();

        $vatican               = \App\Models\Maps\Country::firstOrNew([
            'slug' => 'vatican',
        ]);
        $vatican->name         = "Vatican";
        $vatican->continent_id = Continent::where('name', 'Europe')->first()->id;
        $vatican->save();

    }
}
