<?php

use Illuminate\Database\Seeder;

class WilayahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $wilayah01               = \App\Models\Wilayah::firstOrNew([
            'kode_wilayah' => 'I',
        ]);
        $wilayah01->nama_wilayah = 'Wilayah I - Asia Pasifik';
        $wilayah01->save();

        $wilayah02               = \App\Models\Wilayah::firstOrNew([
            'kode_wilayah' => 'II',
        ]);
        $wilayah02->nama_wilayah = 'Wilayah II - Eropa';
        $wilayah02->save();

        $wilayah03               = \App\Models\Wilayah::firstOrNew([
            'kode_wilayah' => 'III',
        ]);
        $wilayah03->nama_wilayah = 'Wilayah III - Afrika Timur Tengah';
        $wilayah03->save();

        $wilayah04               = \App\Models\Wilayah::firstOrNew([
            'kode_wilayah' => 'IV',
        ]);
        $wilayah04->nama_wilayah = 'Wilayah IV - Amerika dan Pasifik';
        $wilayah04->save();

        \App\Models\Satker::where('kode_satker', 'like', '%404114%')
            ->orWhere('kode_satker', 'like', '%403290%')
            ->orWhere('kode_satker', 'like', '%532612%')
            ->orWhere('kode_satker', 'like', '%403417%')
            ->orWhere('kode_satker', 'like', '%403454%')
            ->orWhere('kode_satker', 'like', '%403423%')
            ->orWhere('kode_satker', 'like', '%632402%')
            ->orWhere('kode_satker', 'like', '%651927%')
            ->orWhere('kode_satker', 'like', '%403505%')
            ->orWhere('kode_satker', 'like', '%549849%')
            ->orWhere('kode_satker', 'like', '%403511%')
            ->orWhere('kode_satker', 'like', '%403520%')
            ->orWhere('kode_satker', 'like', '%576605%')
            ->orWhere('kode_satker', 'like', '%403542%')
            ->orWhere('kode_satker', 'like', '%403551%')
            ->orWhere('kode_satker', 'like', '%403573%')
            ->orWhere('kode_satker', 'like', '%666955%')
            ->orWhere('kode_satker', 'like', '%403602%')
            ->orWhere('kode_satker', 'like', '%403335%')
            ->orWhere('kode_satker', 'like', '%403630%')
            ->orWhere('kode_satker', 'like', '%403567%')
            ->orWhere('kode_satker', 'like', '%403692%')
            ->orWhere('kode_satker', 'like', '%538861%')
            ->orWhere('kode_satker', 'like', '%403721%')
            ->orWhere('kode_satker', 'like', '%403768%')
            ->orWhere('kode_satker', 'like', '%403780%')
            ->orWhere('kode_satker', 'like', '%576590%')
            ->orWhere('kode_satker', 'like', '%403825%')
            ->orWhere('kode_satker', 'like', '%403840%')
            ->orWhere('kode_satker', 'like', '%403737%')
            ->orWhere('kode_satker', 'like', '%677375%')
            ->orWhere('kode_satker', 'like', '%683504%')
            ->orWhere('kode_satker', 'like', '%637368%')
            ->orWhere('kode_satker', 'like', '%606322%')
            ->orWhere('kode_satker', 'like', '%404227%')
            ->orWhere('kode_satker', 'like', '%677272%')
            ->update(['wilayah_id' => $wilayah01->id]);

        \App\Models\Satker::where('kode_satker', 'like', '%560703%')
            ->orWhere('kode_satker', 'like', '%403304%')
            ->orWhere('kode_satker', 'like', '%403341%')
            ->orWhere('kode_satker', 'like', '%403329%')
            ->orWhere('kode_satker', 'like', '%568310%')
            ->orWhere('kode_satker', 'like', '%403350%')
            ->orWhere('kode_satker', 'like', '%403366%')
            ->orWhere('kode_satker', 'like', '%403372%')
            ->orWhere('kode_satker', 'like', '%403460%')
            ->orWhere('kode_satker', 'like', '%403310%')
            ->orWhere('kode_satker', 'like', '%403491%')
            ->orWhere('kode_satker', 'like', '%403950%')
            ->orWhere('kode_satker', 'like', '%403485%')
            ->orWhere('kode_satker', 'like', '%560710%')
            ->orWhere('kode_satker', 'like', '%403913%')
            ->orWhere('kode_satker', 'like', '%632416%')
            ->orWhere('kode_satker', 'like', '%403598%')
            ->orWhere('kode_satker', 'like', '%403938%')
            ->orWhere('kode_satker', 'like', '%404072%')
            ->orWhere('kode_satker', 'like', '%403624%')
            ->orWhere('kode_satker', 'like', '%404057%')
            ->orWhere('kode_satker', 'like', '%403686%')
            ->orWhere('kode_satker', 'like', '%403712%')
            ->orWhere('kode_satker', 'like', '%403743%')
            ->orWhere('kode_satker', 'like', '%403799%')
            ->orWhere('kode_satker', 'like', '%403800%')
            ->orWhere('kode_satker', 'like', '%560724%')
            ->orWhere('kode_satker', 'like', '%403831%')
            ->orWhere('kode_satker', 'like', '%403862%')
            ->orWhere('kode_satker', 'like', '%403856%')
            ->orWhere('kode_satker', 'like', '%677340%')
            ->orWhere('kode_satker', 'like', '%677354%')
            ->orWhere('kode_satker', 'like', '%677286%')
            ->orWhere('kode_satker', 'like', '%677290%')
            ->orWhere('kode_satker', 'like', '%637372%')
            ->orWhere('kode_satker', 'like', '%667808%')
            ->orWhere('kode_satker', 'like', '%667829%')
            ->update(['wilayah_id' => $wilayah02->id]);

        \App\Models\Satker::where('kode_satker', 'like', '%404010%')
            ->orWhere('kode_satker', 'like', '%403582%')
            ->orWhere('kode_satker', 'like', '%403253%')
            ->orWhere('kode_satker', 'like', '%403262%')
            ->orWhere('kode_satker', 'like', '%404120%')
            ->orWhere('kode_satker', 'like', '%403278%')
            ->orWhere('kode_satker', 'like', '%403284%')
            ->orWhere('kode_satker', 'like', '%576612%')
            ->orWhere('kode_satker', 'like', '%403397%')
            ->orWhere('kode_satker', 'like', '%568306%')
            ->orWhere('kode_satker', 'like', '%404032%')
            ->orWhere('kode_satker', 'like', '%403432%')
            ->orWhere('kode_satker', 'like', '%403448%')
            ->orWhere('kode_satker', 'like', '%621871%')
            ->orWhere('kode_satker', 'like', '%651948%')
            ->orWhere('kode_satker', 'like', '%404151%')
            ->orWhere('kode_satker', 'like', '%403479%')
            ->orWhere('kode_satker', 'like', '%403536%')
            ->orWhere('kode_satker', 'like', '%576626%')
            ->orWhere('kode_satker', 'like', '%403944%')
            ->orWhere('kode_satker', 'like', '%404041%')
            ->orWhere('kode_satker', 'like', '%568291%')
            ->orWhere('kode_satker', 'like', '%404139%')
            ->orWhere('kode_satker', 'like', '%404145%')
            ->orWhere('kode_satker', 'like', '%403907%')
            ->orWhere('kode_satker', 'like', '%403922%')
            ->orWhere('kode_satker', 'like', '%403819%')
            ->orWhere('kode_satker', 'like', '%651931%')
            ->orWhere('kode_satker', 'like', '%403969%')
            ->orWhere('kode_satker', 'like', '%532640%')
            ->orWhere('kode_satker', 'like', '%677308%')
            ->orWhere('kode_satker', 'like', '%677329%')
            ->orWhere('kode_satker', 'like', '%677312%')
            ->orWhere('kode_satker', 'like', '%683148%')
            ->orWhere('kode_satker', 'like', '%403247%')
            ->orWhere('kode_satker', 'like', '%404176%')
            ->update(['wilayah_id' => $wilayah03->id]);

        \App\Models\Satker::where('kode_satker', 'like', '%524728%')
            ->orWhere('kode_satker', 'like', '%403975%')
            ->orWhere('kode_satker', 'like', '%403381%')
            ->orWhere('kode_satker', 'like', '%403981%')
            ->orWhere('kode_satker', 'like', '%404063%')
            ->orWhere('kode_satker', 'like', '%404001%')
            ->orWhere('kode_satker', 'like', '%404088%')
            ->orWhere('kode_satker', 'like', '%651906%')
            ->orWhere('kode_satker', 'like', '%404026%')
            ->orWhere('kode_satker', 'like', '%403618%')
            ->orWhere('kode_satker', 'like', '%403649%')
            ->orWhere('kode_satker', 'like', '%403706%')
            ->orWhere('kode_satker', 'like', '%403661%')
            ->orWhere('kode_satker', 'like', '%403670%')
            ->orWhere('kode_satker', 'like', '%403752%')
            ->orWhere('kode_satker', 'like', '%532633%')
            ->orWhere('kode_satker', 'like', '%404094%')
            ->orWhere('kode_satker', 'like', '%404108%')
            ->orWhere('kode_satker', 'like', '%403871%')
            ->orWhere('kode_satker', 'like', '%677361%')
            ->orWhere('kode_satker', 'like', '%677333%')
            ->orWhere('kode_satker', 'like', '%403401%')
            ->orWhere('kode_satker', 'like', '%403990%')
            ->orWhere('kode_satker', 'like', '%404160%')
            ->orWhere('kode_satker', 'like', '%403655%')
            ->orWhere('kode_satker', 'like', '%549853%')
            ->orWhere('kode_satker', 'like', '%651910%')
            ->orWhere('kode_satker', 'like', '%403774%')
            ->orWhere('kode_satker', 'like', '%532654%')
            ->orWhere('kode_satker', 'like', '%403887%')
            ->orWhere('kode_satker', 'like', '%403893%')
            ->orWhere('kode_satker', 'like', '%667812%')
            ->orWhere('kode_satker', 'like', '%404202%')
            ->orWhere('kode_satker', 'like', '%651884%')
            ->orWhere('kode_satker', 'like', '%651891%')
            ->update(['wilayah_id' => $wilayah04->id]);

    }
}
