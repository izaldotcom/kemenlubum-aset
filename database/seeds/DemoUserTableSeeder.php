<?php

use Illuminate\Database\Seeder;

class DemoUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin               = \App\Role::firstOrNew([
            'name' => 'admin',
        ]);
        $admin->name         = 'admin';
        $admin->display_name = 'Administrator';
        $admin->description  = 'Administrator privilaged user that can access all feature';
        $admin->save();

        $han = \App\User::firstOrNew([
            'username' => 'admin',
        ]);
        /** @var \App\User $han */
        $han->name              = 'Administrator';
        $han->email             = 'han@glexindo.com';
        $han->password          = Hash::make('admin');
        $han->email_verified_at = date('Y-m-d');
        $han->is_verified       = true;
        $han->save();
        $han->attachRole($admin);
    }
}
