<?php

use App\Models\Maps\Continent;
use Illuminate\Database\Seeder;

class AddMissingCountry2TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $singapore               = \App\Models\Maps\Country::firstOrNew([
            'slug' => 'bahrain',
        ]);
        $singapore->name         = "Bahrain";
        $singapore->continent_id = Continent::where('name', 'Asia')->first()->id;
        $singapore->save();

        DB::table('satkers')->whereIn('kode_satker', [
            '011010199403907000KP',
//            '011010199404151000KP',
        ])->delete();
    }
}
