<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $username
 * @property int $is_verified
 * @property int $is_active
 * @property int|null $file_id
 * @property int|null $wilayah_id
 * @property int|null $satker_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Notification\Notification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \App\Models\Satker|null $satker
 * @property-read \App\Models\Wilayah|null $wilayah
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSatkerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereWilayahId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User withRole($role)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Menu[] $menus
 * @property-read int|null $menus_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $perms
 * @property-read int|null $perms_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Role whereUpdatedAt($value)
 */
	class Role extends \Eloquent {}
}

namespace App\Models\Maps{
/**
 * App\Models\Maps\Continent
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Continent findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Continent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Continent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Continent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Continent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Continent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Continent whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Continent whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Continent whereUpdatedAt($value)
 */
	class Continent extends \Eloquent {}
}

namespace App\Models\Maps{
/**
 * App\Models\Maps\Country
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $slug
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property array|null $geometry
 * @property int|null $continent_id
 * @property-read \App\Models\Maps\Continent|null $continent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country findSimilarSlugs($attribute, $config, $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country whereContinentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country whereGeometry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Maps\Country whereUpdatedAt($value)
 */
	class Country extends \Eloquent {}
}

namespace App\Models\Kendaraan{
/**
 * App\Models\Kendaraan\PengajuanPenghapusanKendaraan
 *
 * @property int $id
 * @property int|null $pengajuan_penghapusan_id
 * @property int|null $kendaraan_id
 * @property string $tipe
 * @property float $umur_kendaraan
 * @property float|null $nilai_wajar
 * @property float|null $nilai_limit
 * @property string|null $alasan_penghapusan
 * @property array|null $misc_attribute
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $no_rangka
 * @property string|null $tahun_pembuatan
 * @property float|null $nilai_ganti
 * @property-read \App\Models\Kendaraan|null $kendaraan
 * @property-read \App\Models\Kendaraan\PengajuanPenghapusan|null $pengajuanPenghapusan
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereAlasanPenghapusan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereKendaraanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereMiscAttribute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereNilaiGanti($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereNilaiLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereNilaiWajar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereNoRangka($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan wherePengajuanPenghapusanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereTahunPembuatan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereTipe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereUmurKendaraan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan whereUpdatedAt($value)
 */
	class PengajuanPenghapusanKendaraan extends \Eloquent {}
}

namespace App\Models\Kendaraan{
/**
 * App\Models\Kendaraan\KategoriPenghapusan
 *
 * @property int $id
 * @property string|null $name
 * @property int $seed
 * @property int $is_active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\KategoriPenghapusan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\KategoriPenghapusan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\KategoriPenghapusan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\KategoriPenghapusan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\KategoriPenghapusan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\KategoriPenghapusan whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\KategoriPenghapusan whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\KategoriPenghapusan whereSeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\KategoriPenghapusan whereUpdatedAt($value)
 */
	class KategoriPenghapusan extends \Eloquent {}
}

namespace App\Models\Kendaraan{
/**
 * App\Models\Kendaraan\PengajuanPenghapusanDetail
 *
 * @property int $id
 * @property int|null $pengajuan_penghapusan_id
 * @property int|null $user_id
 * @property int|null $approver_id
 * @property int $step
 * @property string $status
 * @property int|null $file_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $no_surat
 * @property int|null $template_id
 * @property array|null $misc_attribute
 * @property string|null $uploaded_filename
 * @property-read \App\User|null $approver
 * @property-read \App\Models\File|null $file
 * @property-read \App\Models\Kendaraan\PengajuanPenghapusan|null $pengajuanPenghapusan
 * @property-read \App\Models\File|null $template
 * @property-read \App\User|null $uploader
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereApproverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereMiscAttribute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereNoSurat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail wherePengajuanPenghapusanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereStep($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereTemplateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereUploadedFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusanDetail whereUserId($value)
 */
	class PengajuanPenghapusanDetail extends \Eloquent {}
}

namespace App\Models\Kendaraan{
/**
 * App\Models\Kendaraan\PengajuanPenghapusan
 *
 * @property int $id
 * @property int|null $satker_id
 * @property int|null $user_id
 * @property int|null $approver_id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $no_pengajuan_penghapusan
 * @property string|null $keterangan
 * @property int|null $kategori_penghapusan_id
 * @property int $total_unit
 * @property float $total_harga_perolehan
 * @property float $total_harga_taksiran
 * @property string|null $approved_at
 * @property string|null $alasan_kategori_penghapusan
 * @property array|null $misc_attribute
 * @property array|null $susunan_panitia
 * @property array|null $surat_pernyataan_signature
 * @property string|null $no_berita_acara
 * @property string|null $no_surat_kepala
 * @property-read \App\User|null $approver
 * @property-read mixed $status_color
 * @property-read mixed $status_say
 * @property-read \App\Models\Kendaraan\KategoriPenghapusan|null $kategoriPenghapusan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kendaraan\PengajuanPenghapusanKendaraan[] $pengajuanPenghapusanKendaraans
 * @property-read int|null $pengajuan_penghapusan_kendaraans_count
 * @property-read \App\Models\Satker|null $satker
 * @property-read \App\User|null $uploader
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereAlasanKategoriPenghapusan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereApprovedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereApproverId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereKategoriPenghapusanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereKeterangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereMiscAttribute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereNoBeritaAcara($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereNoPengajuanPenghapusan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereNoSuratKepala($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereSatkerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereSuratPernyataanSignature($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereSusunanPanitia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereTotalHargaPerolehan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereTotalHargaTaksiran($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereTotalUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PengajuanPenghapusan whereUserId($value)
 */
	class PengajuanPenghapusan extends \Eloquent {}
}

namespace App\Models\Kendaraan{
/**
 * App\Models\Kendaraan\PremiHistory
 *
 * @property int $id
 * @property string|null $no_polis
 * @property string|null $perusahaan_asuransi
 * @property string|null $jatuh_tempo_awal
 * @property string|null $jatuh_tempo_akhir
 * @property float|null $premi
 * @property float|null $tertanggung
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Kendaraan $kendaraan
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory whereJatuhTempoAkhir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory whereJatuhTempoAwal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory whereNoPolis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory wherePerusahaanAsuransi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory wherePremi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory whereTertanggung($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PremiHistory whereUpdatedAt($value)
 */
	class PremiHistory extends \Eloquent {}
}

namespace App\Models\Kendaraan{
/**
 * App\Models\Kendaraan\PajakHistory
 *
 * @property int $id
 * @property string|null $tipe
 * @property string|null $tanggal_perpanjangan
 * @property float|null $nominal
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Kendaraan $kendaraan
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PajakHistory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PajakHistory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PajakHistory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PajakHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PajakHistory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PajakHistory whereNominal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PajakHistory whereTanggalPerpanjangan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PajakHistory whereTipe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan\PajakHistory whereUpdatedAt($value)
 */
	class PajakHistory extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Gedung
 *
 * @property int $id
 * @property string|null $kode_barang
 * @property int|null $nup
 * @property string|null $kode_satker
 * @property string|null $nama_satker
 * * @property string|null $nama_satkerr
 * @property int|null $kib
 * @property string|null $nama_barang
 * @property string|null $kondisi
 * @property string|null $jenis_dokumen
 * @property string|null $kepemilikan
 * @property string|null $jenis_sertifikat
 * @property string|null $merk_type
 * @property string|null $tanggal_rekam_pertama
 * @property string|null $tanggal_perolehan
 * @property float|null $nilai_perolehan_pertama
 * @property float|null $nilai_mutasi
 * @property float|null $nilai_perolehan
 * @property float|null $nilai_penyusutan
 * @property float|null $nilai_buku
 * @property float|null $kuantitas
 * @property float|null $jumlah_foto
 * @property float|null $jumlah_lantai
 * @property float|null $luas_bangunan
 * @property float|null $luas_dasar_bangunan
 * @property string|null $alamat
 * @property string|null $jalan
 * @property string|null $kode_kota
 * @property string|null $kode_provinsi
 * @property string|null $uraian_kota
 * @property string|null $status_penggunaan
 * @property string|null $status_pengelolaan
 * @property string|null $no_psp
 * @property string|null $tanggal_psp
 * @property float|null $jumlah_kib
 * @property float|null $sbsk
 * @property float|null $optimalisasi
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $lat
 * @property string|null $lng
 * @property string $status_sync
 * @property string|null $last_sync
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\GedungPhoto[] $documentFiles
 * @property-read int|null $document_files_count
 * @property-read mixed $main_photo
 * @property-read mixed $photos_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\GedungPhoto[] $photoFiles
 * @property-read int|null $photo_files_count
 * @property-read \App\Models\Satker|null $satker
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereAlamat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereJalan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereJenisDokumen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereJenisSertifikat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereJumlahFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereJumlahKib($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereJumlahLantai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereKepemilikan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereKib($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereKodeBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereKodeKota($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereKodeProvinsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereKodeSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereKondisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereKuantitas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereLastSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereLuasBangunan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereLuasDasarBangunan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereMerkType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereNamaBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereNamaSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereNilaiBuku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereNilaiMutasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereNilaiPenyusutan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereNilaiPerolehan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereNilaiPerolehanPertama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereNoPsp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereNup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereOptimalisasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereSbsk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereStatusPengelolaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereStatusPenggunaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereStatusSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereTanggalPerolehan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereTanggalPsp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereTanggalRekamPertama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Gedung whereUraianKota($value)
 */
	class Gedung extends \Eloquent {}
}

namespace App\Models\Notification{
/**
 * App\Models\Notification\NotificationMessage
 *
 * @property int $id
 * @property string|null $action
 * @property string|null $icon
 * @property string|null $class
 * @property string|null $title
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\NotificationMessage whereUpdatedAt($value)
 */
	class NotificationMessage extends \Eloquent {}
}

namespace App\Models\Notification{
/**
 * App\Models\Notification\Notification
 *
 * @property int $id
 * @property string|null $action
 * @property string|null $icon
 * @property string|null $class
 * @property string|null $title
 * @property string|null $message
 * @property string|null $link
 * @property int|null $user_id
 * @property int|null $satker_id
 * @property int|null $wilayah_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $code
 * @property array|null $relation_ids
 * @property-read \App\Models\Satker|null $satker
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $targets
 * @property-read int|null $targets_count
 * @property-read \App\User|null $user
 * @property-read \App\Models\Wilayah|null $wilayah
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereRelationIds($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereSatkerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Notification\Notification whereWilayahId($value)
 */
	class Notification extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\File
 *
 * @property int $id
 * @property string|null $filename
 * @property string|null $location
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $full_url
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereLocation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\File whereUpdatedAt($value)
 */
	class File extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\KendaraanPhoto
 *
 * @property int $id
 * @property int|null $kendaraan_id
 * @property int|null $file_id
 * @property int $seed
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\File|null $file
 * @property-read \App\Models\Satker|null $kendaraan
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto whereKendaraanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto whereSeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanPhoto whereUpdatedAt($value)
 */
	class KendaraanPhoto extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PremiKendaraan
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiKendaraan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiKendaraan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PremiKendaraan query()
 */
	class PremiKendaraan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RumahNegara
 *
 * @property int $id
 * @property string|null $kode_barang
 * @property int|null $nup
 * @property string|null $kode_satker
 * @property string|null $nama_satker
 *  * @property string|null $nama_satkerr
 * @property int|null $kib
 * @property string|null $nama_barang
 * @property string|null $kondisi
 * @property string|null $jenis_dokumen
 * @property string|null $kepemilikan
 * @property string|null $jenis_sertifikat
 * @property string|null $merk_type
 * @property string|null $tanggal_rekam_pertama
 * @property string|null $tanggal_perolehan
 * @property float|null $nilai_perolehan_pertama
 * @property float|null $nilai_mutasi
 * @property float|null $nilai_perolehan
 * @property float|null $nilai_penyusutan
 * @property float|null $nilai_buku
 * @property float|null $kuantitas
 * @property float|null $jumlah_foto
 * @property float|null $jumlah_lantai
 * @property float|null $luas_bangunan
 * @property float|null $luas_dasar_bangunan
 * @property string|null $alamat
 * @property string|null $jalan
 * @property string|null $kode_kota
 * @property string|null $kode_provinsi
 * @property string|null $uraian_kota
 * @property string|null $status_penggunaan
 * @property string|null $status_pengelolaan
 * @property string|null $no_psp
 * @property string|null $tanggal_psp
 * @property float|null $jumlah_kib
 * @property float|null $sbsk
 * @property float|null $optimalisasi
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $lat
 * @property string|null $lng
 * @property string $status_sync
 * @property string|null $last_sync
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RumahNegaraPhoto[] $documentFiles
 * @property-read int|null $document_files_count
 * @property-read mixed $main_photo
 * @property-read mixed $photos_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RumahNegaraPhoto[] $photoFiles
 * @property-read int|null $photo_files_count
 * @property-read \App\Models\Satker|null $satker
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereAlamat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereJalan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereJenisDokumen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereJenisSertifikat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereJumlahFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereJumlahKib($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereJumlahLantai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereKepemilikan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereKib($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereKodeBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereKodeKota($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereKodeProvinsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereKodeSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereKondisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereKuantitas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereLastSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereLuasBangunan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereLuasDasarBangunan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereMerkType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereNamaBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereNamaSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereNilaiBuku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereNilaiMutasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereNilaiPenyusutan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereNilaiPerolehan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereNilaiPerolehanPertama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereNoPsp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereNup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereOptimalisasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereSbsk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereStatusPengelolaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereStatusPenggunaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereStatusSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereTanggalPerolehan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereTanggalPsp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereTanggalRekamPertama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegara whereUraianKota($value)
 */
	class RumahNegara extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\KendaraanDinas
 *
 * @property int $id
 * @property string|null $tipe
 * @property string|null $station
 * @property string|null $kib
 * @property string|null $penanggung_jawab
 * @property string|null $no_polisi
 * @property string|null $no_polisi_rfs
 * @property string|null $merk
 * @property string|null $jenis
 * @property string|null $tahun
 * @property string|null $warna
 * @property string|null $no_rangka
 * @property string|null $no_mesin
 * @property string|null $bpkb
 * @property string|null $tanggal_pajak
 * @property string|null $tanggal_pajak_rfs
 * @property string $status_sync
 * @property string|null $last_sync
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $no_polis
 * @property float|null $premi
 * @property float|null $tertanggung
 * @property string|null $jatuh_tempo_awal
 * @property string|null $jatuh_tempo_akhir
 * @property string|null $perusahaan_asuransi
 * @property string $status_pajak
 * @property string $status_pajak_rfs
 * @property string $status_asuransi
 * @property string|null $tanggal_status_pajak
 * @property string|null $tanggal_status_pajak_rfs
 * @property string|null $tanggal_status_asuransi
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereBpkb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereJatuhTempoAkhir($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereJatuhTempoAwal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereJenis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereKib($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereLastSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereMerk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereNoMesin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereNoPolis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereNoPolisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereNoPolisiRfs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereNoRangka($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas wherePenanggungJawab($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas wherePerusahaanAsuransi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas wherePremi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereStation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereStatusAsuransi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereStatusPajak($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereStatusPajakRfs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereStatusSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereTahun($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereTanggalPajak($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereTanggalPajakRfs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereTanggalStatusAsuransi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereTanggalStatusPajak($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereTanggalStatusPajakRfs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereTertanggung($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereTipe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\KendaraanDinas whereWarna($value)
 */
	class KendaraanDinas extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Kendaraan
 *
 * @property int $id
 * @property string|null $kode_barang
 * @property int|null $nup
 * @property string|null $kode_satker
 * @property string|null $nama_satker
 *  * @property string|null $nama_satkerr
 * @property int|null $kib
 * @property string|null $nama_barang
 * @property string|null $kondisi
 * @property string|null $merk_type
 * @property string|null $tanggal_rekam_pertama
 * @property string|null $tanggal_perolehan
 * @property float|null $nilai_perolehan_pertama
 * @property float|null $nilai_mutasi
 * @property float|null $nilai_perolehan
 * @property float|null $nilai_penyusutan
 * @property float|null $nilai_buku
 * @property float|null $kuantitas
 * @property float|null $jumlah_foto
 * @property string|null $status_penggunaan
 * @property string|null $status_pengelolaan
 * @property string|null $tanggal_psp
 * @property string|null $no_bpkb
 * @property string|null $no_polisi
 * @property string|null $pemakai
 * @property float|null $jumlah_kib
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $no_psp
 * @property string|null $lat
 * @property string|null $lng
 * @property string $status_sync
 * @property string|null $last_sync
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KendaraanPhoto[] $documentFiles
 * @property-read int|null $document_files_count
 * @property-read mixed $main_photo
 * @property-read mixed $photos_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Kendaraan\PengajuanPenghapusan[] $pengajuanPenghapusans
 * @property-read int|null $pengajuan_penghapusans_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\KendaraanPhoto[] $photoFiles
 * @property-read int|null $photo_files_count
 * @property-read \App\Models\Satker|null $satker
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereJumlahFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereJumlahKib($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereKib($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereKodeBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereKodeSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereKondisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereKuantitas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereLastSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereMerkType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNamaBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNamaSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNilaiBuku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNilaiMutasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNilaiPenyusutan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNilaiPerolehan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNilaiPerolehanPertama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNoBpkb($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNoPolisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNoPsp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereNup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan wherePemakai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereStatusPengelolaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereStatusPenggunaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereStatusSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereTanggalPerolehan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereTanggalPsp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereTanggalRekamPertama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Kendaraan whereUpdatedAt($value)
 */
	class Kendaraan extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\SatkerPhoto
 *
 * @property int $id
 * @property int|null $satker_id
 * @property int|null $file_id
 * @property int $seed
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\File|null $file
 * @property-read \App\Models\Satker|null $satker
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto whereSatkerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto whereSeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SatkerPhoto whereUpdatedAt($value)
 */
	class SatkerPhoto extends \Eloquent {}
}



namespace App\Models{
/**
 * App\Models\Barang
 *
 * @property int $id
 * @property string|null $kode_barang
 * @property string|null $nama_barang
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereKodeBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereNamaBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Barang whereUpdatedAt($value)
 */
	class Barang extends \Eloquent {}
}

namespace App\Models{
    /**
     * App\Models\Sekjen
     *
     * @property int $id
     * @property string|null $nama
     * @property string|null $nip
     * @property \Illuminate\Support\Carbon|null $created_at
     * @property \Illuminate\Support\Carbon|null $updated_at
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sekjen newModelQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sekjen newQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sekjen query()
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sekjen whereCreatedAt($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sekjen whereId($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sekjen wherenama($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sekjen wherenip($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Sekjen whereUpdatedAt($value)
     */
        class Sekjen extends \Eloquent {}
    }

namespace App\Models{
/**
 * App\Models\Satker
 *
 * @property int $id
 * @property string|null $kode_satker
 * @property string|null $nama_satker
 *  * @property string|null $nama_satkerr
 * @property string|null $lat
 * @property string|null $lng
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $alamat
 * @property string|null $kode_pos
 * @property string|null $email
 * @property string|null $phone
 * @property int|null $country_id
 * @property int|null $wilayah_id
 * @property string $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Maps\Country[] $countries
 * @property-read int|null $countries_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Gedung[] $gedungs
 * @property-read int|null $gedungs_count
 * @property-read mixed $building_count
 * @property-read mixed $car_count
 * @property-read mixed $first_country
 * @property-read mixed $land_count
 * @property-read mixed $main_photo
 * @property-read mixed $photos_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SatkerPhoto[] $photoFiles
 * @property-read int|null $photo_files_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\RumahNegara[] $rumahs
 * @property-read int|null $rumahs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Tanah[] $tanahs
 * @property-read int|null $tanahs_count
 * @property-read \App\Models\Wilayah|null $wilayah
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereAlamat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereKodePos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereKodeSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereNamaSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Satker whereWilayahId($value)
 */
	class Satker extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Menu
 *
 * @property int $id
 * @property string|null $icon
 * @property string|null $title
 * @property string|null $route
 * @property string|null $url
 * @property string|null $type
 * @property int $order
 * @property int|null $parent_id
 * @property int $show
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereRoute($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereShow($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Menu whereUrl($value)
 */
	class Menu extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TanahPhoto
 *
 * @property int $id
 * @property int|null $tanah_id
 * @property int|null $file_id
 * @property int $seed
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\File|null $file
 * @property-read \App\Models\Tanah|null $tanah
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto whereSeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto whereTanahId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TanahPhoto whereUpdatedAt($value)
 */
	class TanahPhoto extends \Eloquent {}
}

namespace App\Models\Log{
/**
 * App\Models\Log\LogAction
 *
 * @property int $id
 * @property string|null $tipe
 * @property string|null $action
 * @property string|null $class
 * @property array|null $old_data
 * @property array|null $new_data
 * @property int|null $user_id
 * @property int|null $satker_id
 * @property int|null $wilayah_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Satker|null $satker
 * @property-read \App\User|null $user
 * @property-read \App\Models\Wilayah|null $wilayah
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereAction($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereNewData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereOldData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereSatkerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereTipe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogAction whereWilayahId($value)
 */
	class LogAction extends \Eloquent {}
}

namespace App\Models\Log{
/**
 * App\Models\Log\LogEmail
 *
 * @property int $id
 * @property string|null $destination
 * @property string|null $subject
 * @property string|null $view
 * @property array|null $content
 * @property int $retry
 * @property string $status
 * @property string|null $exception_message
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereDestination($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereExceptionMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereRetry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogEmail whereView($value)
 */
	class LogEmail extends \Eloquent {}
}

namespace App\Models\Log{
/**
 * App\Models\Log\LogInputExcel
 *
 * @property int $id
 * @property int|null $file_id
 * @property int|null $user_id
 * @property string|null $tipe
 * @property float $data_updated
 * @property float $data_created
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float $data_deleted
 * @property string $status
 * @property string|null $note
 * @property-read \App\Models\File|null $file
 * @property-read mixed $message
 * @property-read mixed $message_html
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereDataCreated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereDataDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereDataUpdated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereTipe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\LogInputExcel whereUserId($value)
 */
	class LogInputExcel extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\RumahNegaraPhoto
 *
 * @property int $id
 * @property int|null $rumah_negara_id
 * @property int|null $file_id
 * @property int $seed
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\File|null $file
 * @property-read \App\Models\RumahNegara|null $rumahNegara
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto whereRumahNegaraId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto whereSeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RumahNegaraPhoto whereUpdatedAt($value)
 */
	class RumahNegaraPhoto extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Tanah
 *
 * @property int $id
 * @property string|null $kode_barang
 * @property int|null $nup
 * @property string|null $kode_satker
 * @property string|null $nama_satker
 *  * @property string|null $nama_satker
 * @property int|null $kib
 * @property string|null $nama_barang
 * @property string|null $kondisi
 * @property string|null $jenis_dokumen
 * @property string|null $kepemilikan
 * @property string|null $jenis_sertifikat
 * @property string|null $merk_type
 * @property string|null $tanggal_rekam_pertama
 * @property string|null $tanggal_perolehan
 * @property float|null $nilai_perolehan_pertama
 * @property float|null $nilai_mutasi
 * @property float|null $nilai_perolehan
 * @property float|null $nilai_penyusutan
 * @property float|null $nilai_buku
 * @property float|null $kuantitas
 * @property float|null $jumlah_foto
 * @property float|null $jumlah_lantai
 * @property float|null $luas_tanah_seluruhnya
 * @property float|null $luas_tanah_bangunan
 * @property float|null $luas_tanah_lingkungan
 * @property float|null $luas_lahan_kosong
 * @property string|null $alamat
 * @property string|null $rt_rw
 * @property string|null $kelurahan
 * @property string|null $kecamatan
 * @property string|null $kode_kota
 * @property string|null $kota
 * @property string|null $kode_provinsi
 * @property string|null $provinsi
 * @property string|null $kode_pos
 * @property string|null $alamat_lain
 * @property string|null $status_penggunaan
 * @property string|null $status_pengelolaan
 * @property string|null $no_psp
 * @property string|null $tanggal_psp
 * @property float|null $jumlah_kib
 * @property float|null $sbsk
 * @property float|null $optimalisasi
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $lat
 * @property string|null $lng
 * @property string $status_sync
 * @property string|null $last_sync
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TanahPhoto[] $documentFiles
 * @property-read int|null $document_files_count
 * @property-read mixed $main_photo
 * @property-read mixed $photos_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TanahPhoto[] $photoFiles
 * @property-read int|null $photo_files_count
 * @property-read \App\Models\Satker|null $satker
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereAlamat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereAlamatLain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereJenisDokumen($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereJenisSertifikat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereJumlahFoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereJumlahKib($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereJumlahLantai($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKecamatan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKelurahan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKepemilikan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKib($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKodeBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKodeKota($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKodePos($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKodeProvinsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKodeSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKondisi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKota($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereKuantitas($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereLastSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereLat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereLng($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereLuasLahanKosong($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereLuasTanahBangunan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereLuasTanahLingkungan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereLuasTanahSeluruhnya($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereMerkType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereNamaBarang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereNamaSatker($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereNilaiBuku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereNilaiMutasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereNilaiPenyusutan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereNilaiPerolehan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereNilaiPerolehanPertama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereNoPsp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereNup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereOptimalisasi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereProvinsi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereRtRw($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereSbsk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereStatusPengelolaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereStatusPenggunaan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereStatusSync($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereTanggalPerolehan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereTanggalPsp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereTanggalRekamPertama($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Tanah whereUpdatedAt($value)
 */
	class Tanah extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Setting
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $setting_key
 * @property string|null $setting_value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereSettingKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereSettingValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereUserId($value)
 */
	class Setting extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\GedungPhoto
 *
 * @property int $id
 * @property int|null $gedung_id
 * @property int|null $file_id
 * @property int $seed
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\File|null $file
 * @property-read \App\Models\Gedung|null $gedung
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto whereFileId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto whereGedungId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto whereSeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\GedungPhoto whereUpdatedAt($value)
 */
	class GedungPhoto extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Wilayah
 *
 * @property int $id
 * @property string|null $nama_wilayah
 * @property string|null $kode_wilayah
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Satker[] $satkers
 * @property-read int|null $satkers_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah whereKodeWilayah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah whereNamaWilayah($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Wilayah whereUpdatedAt($value)
 */
	class Wilayah extends \Eloquent {}
}

namespace App{
/**
 * App\Permission
 *
 * @property int $id
 * @property string $name
 * @property string|null $display_name
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Permission whereUpdatedAt($value)
 */
	class Permission extends \Eloquent {}
}

