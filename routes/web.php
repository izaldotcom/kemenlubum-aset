<?php
use App\Services\Terbilang;

Route::get('/test',function(){
  $ter = new Terbilang();
  return  $ter->penyebut(656500000000);
});

Route::get('/bypass-google', function () {
    \App\Services\FushionTableServices::updateFushionTableData();
    return 'success';
});
Route::get('/google-fushion', function () {
    return new \Illuminate\Http\JsonResponse(\App\Services\FushionTableServices::generateFushionTableData());
});

Route::group(['middleware' => ['isAuth']], function () {
    Route::get('/assets-management', 'RumahNegaraFrontController@home');


    Route::get('/logout', 'Auth\LoginController@logout');
    Route::get('/', function () {
        return view('front');
    });

    Route::group(['prefix' => 'admin', 'middleware' => ['isAuth']], function () {
        Route::get('/', 'Admin\DashboardController@index')->name('admin_dashboard');
        Route::get('/notification', 'Admin\DashboardController@notification')->name('admin_notification');

        // Route::get('/email', function () {
        //     return view('emailtemplate/email');
        // });

        Route::group(['prefix' => 'pengajuan-penghapusan-kendaraan', 'middleware' => ['role:super_admin|admin|pimpinan|admin_aset|staf_kendaraan|admin_satker']], function () {
            Route::get('/create', 'Admin\PenghapusanKendaraanController@create')->name('admin_pengajuan_penghapusan_kendaraan_create');
            Route::get('/', 'Admin\PenghapusanKendaraanController@list')->name('admin_pengajuan_penghapusan_kendaraan_list');
            Route::get('/list-penghapusan/{code?}', 'Admin\PenghapusanKendaraanController@listPenghapusan')->name('admin_pengajuan_penghapusan_kendaraan_list-penghapusan');
            Route::get('/detail/{id}', 'Admin\PenghapusanKendaraanController@detail')->name('admin_pengajuan_penghapusan_kendaraan_detail');
            Route::get('/download/{id}/{step}/{regenerate?}', 'Admin\PenghapusanKendaraanController@download')->name('admin_pengajuan_penghapusan_kendaraan_download');
            Route::get('/download-all/{id}/{kemkeu?}', 'Admin\PenghapusanKendaraanController@downloadAll')->name('admin_pengajuan_penghapusan_kendaraan_download_all');
        });

        Route::group(['prefix' => 'kendaraan'], function () {
            Route::group(['middleware' => ['role:super_admin|admin|pimpinan|admin_aset|admin_kendaraan|staf_kendaraan|admin_satker']], function () {
                Route::get('/list/{filter?}', 'Admin\KendaraanController@list')->name('admin_kendaraan_list');
                Route::get('/input-excel', 'Admin\KendaraanController@inputExcel')->name('admin_kendaraan_input_excel');
            });

            Route::group(['middleware' => ['role:super_admin|admin|pimpinan|admin_pajak_asuransi']], function () {
                Route::get('/list-dinas/{code?}', 'Admin\KendaraanController@listKendaraanDinas')->name('admin_kendaraan_list_dinas');
                Route::get('/input-excel-dinas', 'Admin\KendaraanController@inputKendaraanDinasExcel')->name('admin_kendaraan_input_excel_dinas');
                Route::get('/edit-pajak/{id}', 'Admin\KendaraanController@editPajak')->name('admin_kendaraan_edit_pajak');
            });
        });

        Route::group(['middleware' => ['role:super_admin|admin|pimpinan|admin_aset|admin_bangunan|staf_bangunan|admin_satker']], function () {
            Route::group(['prefix' => 'rumah-negara'], function () {
                Route::get('/list/{filter?}', 'Admin\RumahNegaraController@list')->name('admin_rumah_negara_list');
                Route::get('/input-excel', 'Admin\RumahNegaraController@inputExcel')->name('admin_rumah_negara_input_excel');
            });

            Route::group(['prefix' => 'gedung'], function () {
                Route::get('/list/{filter?}', 'Admin\GedungController@list')->name('admin_gedung_list');
                Route::get('/input-excel', 'Admin\GedungController@inputExcel')->name('admin_gedung_input_excel');
            });

            Route::group(['prefix' => 'tanah'], function () {
                Route::get('/list/{filter?}', 'Admin\TanahController@list')->name('admin_tanah_list');
                Route::get('/input-excel', 'Admin\TanahController@inputExcel')->name('admin_tanah_input_excel');
            });
        });

        Route::group(['middleware' => ['role:super_admin|admin|pimpinan']], function () {
            Route::group(['prefix' => 'user'], function () {
                Route::get('/list', 'Admin\UserController@list')->name('admin_user_list');
            });

            Route::group(['prefix' => 'wilayah'], function () {
                Route::get('/list', 'Admin\WilayahController@list')->name('admin_wilayah_list');
            });

            Route::group(['prefix' => 'barang'], function () {
                Route::get('/list', 'Admin\BarangController@list')->name('admin_barang_list');
            });

            Route::group(['prefix' => 'satker'], function () {
                Route::get('/list', 'Admin\SatkerController@list')->name('admin_satker_list');
            });
            Route::group(['prefix' => 'setting'], function () {
                Route::get('/general', 'Admin\SettingController@generalSetting')->name('admin_setting_general');
            });

        });
        Route::get('/list', 'Admin\SekjenController@list')->name('admin_sekjen_list');
        Route::group(['prefix' => 'perwakilan'], function () {
            Route::get('/Pkendaraan/{filter?}', 'Admin\PerwakilanKendaraanController@list')->name('admin_kendaraannew_list');
            Route::get('/Pkendaraan/{kondisi?}', "Admin\Api\PerwakilanKendaraanApiController@get")->name('admin_api_get_perwakilan_kendaraan');
            Route::get('/Ptanah/{filter?}', 'Admin\PerwakilanTanahController@list')->name('admin_tanahnew_list');
            Route::get('/Pbangunan/{filter?}', 'Admin\PerwakilanBangunanController@list')->name('admin_bangunannew_list');
            Route::get('/Pbangunan/{kondisi?}', "Admin\Api\PerwakilanBangunanApiController@get")->name('admin_api_get_perwakilan_bangunan');
        });

        Route::get('/change-password', 'Admin\UserController@changePassword')->name('admin_change_password');

        Route::get('/searches', function () {
            return view('searches');
        })->name('admin_searches');

        Route::group(['prefix' => 'api'], function () {
            Route::group(['prefix' => 'pengajuan-penghapusan'], function () {
                Route::post('/create', "Admin\Api\PengajuanPenghapusanApiController@postCreate")->name('admin_api_post_pengajuan_penghapusan_create');
                Route::get('/get', "Admin\Api\PengajuanPenghapusanApiController@get")->name('admin_api_get_pengajuan_penghapusan');
                Route::get('/get-detail/{id}', "Admin\Api\PengajuanPenghapusanApiController@getDetail")->name('admin_api_get_pengajuan_penghapusan_detail');
                Route::get('/get-variable-pimpinan/{id}', "Admin\Api\PengajuanPenghapusanApiController@getVariablePimpinan")->name('admin_api_get_pengajuan_penghapusan_variable_pimpinan');
                Route::get('/get-variable-signature/{id}', "Admin\Api\PengajuanPenghapusanApiController@getVariableSignature")->name('admin_api_get_pengajuan_penghapusan_variable_signature');
                Route::get('/get-variable-panitia/{id}', "Admin\Api\PengajuanPenghapusanApiController@getVariablePanitia")->name('admin_api_get_pengajuan_penghapusan_variable_panitia');
                Route::get('/get-detail-pimpinan/{id}', "Admin\Api\PengajuanPenghapusanApiController@getDetailPimpinan")->name('admin_api_get_pengajuan_penghapusan_detail_pimpinan');
                Route::get('/get-detail-signature/{id}', "Admin\Api\PengajuanPenghapusanApiController@getDetailSignature")->name('admin_api_get_pengajuan_penghapusan_detail_signature');
                Route::get('/get-detail-panitia/{id}', "Admin\Api\PengajuanPenghapusanApiController@getDetailPanitia")->name('admin_api_get_pengajuan_penghapusan_detail_panitia');
                Route::get('/get-kendaraan/{id}', "Admin\Api\PengajuanPenghapusanApiController@getKendaraan")->name('admin_api_get_pengajuan_penghapusan_kendaraan');
                Route::post('/edit-penghapusan-pimpinan/{penghapusanid}', "Admin\Api\PengajuanPenghapusanApiController@postEditPenghapusanPimpinan")->name('admin_api_post_pengajuan_penghapusan_edit_penghapusan_pimpinan');
                Route::post('/edit-penghapusan-panitia/{penghapusanid}', "Admin\Api\PengajuanPenghapusanApiController@postEditPenghapusanPanitia")->name('admin_api_post_pengajuan_penghapusan_edit_penghapusan_panitia');
                Route::post('/edit-penghapusan-signature/{penghapusanid}', "Admin\Api\PengajuanPenghapusanApiController@postEditPenghapusanSignature")->name('admin_api_post_pengajuan_penghapusan_edit_penghapusan_signature');
                Route::post('/finish-kendaraan/{penghapusanid}', "Admin\Api\PengajuanPenghapusanApiController@postFinishKendaraan")->name('admin_api_post_pengajuan_penghapusan_finish_kendaraan');
                Route::post('/finish-all/{penghapusanid}', "Admin\Api\PengajuanPenghapusanApiController@postFinishAll")->name('admin_api_post_pengajuan_penghapusan_finish_all');
                Route::post('/save-kendaraan/{penghapusanid}', "Admin\Api\PengajuanPenghapusanApiController@postSaveKendaraan")->name('admin_api_post_pengajuan_penghapusan_save_kendaraan');
                Route::post('/add-kendaraan/{penghapusanid}', "Admin\Api\PengajuanPenghapusanApiController@postAddKendaraan")->name('admin_api_post_pengajuan_penghapusan_add_kendaraan');
                Route::post('/remove-kendaraan/{id}', "Admin\Api\PengajuanPenghapusanApiController@postRemoveKendaraan")->name('admin_api_post_pengajuan_penghapusan_remove_kendaraan');
                Route::post('/upload/{penghapusanid}/{step}', "Admin\Api\PengajuanPenghapusanApiController@postUpload")->name('admin_api_post_pengajuan_penghapusan_upload');
                Route::post('/template/{penghapusanid}/{step}', "Admin\Api\PengajuanPenghapusanApiController@postTemplate")->name('admin_api_post_pengajuan_penghapusan_template');
                Route::get('/{tipe}/{penghapusanid}/{step}', "Admin\Api\PengajuanPenghapusanApiController@getValidate")->name('admin_api_get_pengajuan_penghapusan_validate');

            });

            Route::group(['prefix' => 'kategori-pengajuan-penghapusan'], function () {
                Route::get('/dropdown', "Admin\Api\KategoriPengajuanPenghapusanApiController@getDropdown")->name('admin_api_get_kategori_penghapusan_dropdown');
            });

            Route::group(['prefix' => 'jabatan-pengajuan-penghapusan'], function () {
                Route::get('/dropdown', "Admin\Api\JabatanPengajuanPenghapusanApiController@getDropdown")->name('admin_api_get_jabatan_penghapusan_dropdown');
            });

            Route::group(['prefix' => 'PerwakilanKendaraan'], function () {
                Route::get('/dropdown', "Admin\Api\KategoriKendaraanApiController@getDropdown")->name('admin_api_get_kategori_perwakilan_kendaraan_dropdown');
            });

            Route::group(['prefix' => 'input-excel'], function () {
                Route::post('/{tipe}', "Admin\Api\InputExcelApiController@postInputExcel")->name('admin_api_post_input_excel');
            });


            Route::group(['prefix' => 'log-input-excel'], function () {
                Route::get('/{tipe}', "Admin\Api\LogInputExcelApiController@get")->name('admin_api_get_log_input_excel');
            });

            Route::group(['prefix' => 'location'], function () {
                Route::post('/{tipe}/{id}', "Admin\Api\InputExcelApiController@postLocation")->name('admin_api_post_location');
            });

            Route::group(['prefix' => 'photos'], function () {
                Route::get('/{tipe}', "Admin\Api\InputExcelApiController@getPhotos")->name('admin_api_get_photos');
                Route::post('/{tipe}/{id}', "Admin\Api\InputExcelApiController@postPhotos")->name('admin_api_post_photos');
                Route::delete('/{tipe}/{id}', "Admin\Api\InputExcelApiController@deletePhotos")->name('admin_api_delete_photos');
            });

            Route::group(['prefix' => 'dark-mode'], function () {
                Route::get('/', "Admin\Api\DarkModeApiController@get")->name('admin_api_get_dark_mode');
                Route::post('/', "Admin\Api\DarkModeApiController@post")->name('admin_api_post_dark_mode');
            });

            Route::group(['prefix' => 'general-setting'], function () {
                Route::get('/', "Admin\Api\GeneralSettingApiController@get")->name('admin_api_get_general_setting');
                Route::post('/', "Admin\Api\GeneralSettingApiController@post")->name('admin_api_post_general_setting');
            });

            Route::group(['prefix' => 'wilayah'], function () {
                Route::get('/dropdown', "Admin\Api\WIlayahApiController@getDropdown")->name('admin_api_get_dropdown_wilayah');
                Route::get('/', "Admin\Api\WIlayahApiController@get")->name('admin_api_get_wilayah');
            });

            Route::group(['prefix' => 'rumah-negara'], function () {
                Route::get('/', "Admin\Api\RumahNegaraApiController@get")->name('admin_api_get_rumah_negara');
                Route::delete('/{id}', "Admin\Api\RumahNegaraApiController@delete")->name('admin_api_delete_rumah_negara');
            });

            Route::group(['prefix' => 'tanah'], function () {
                Route::get('/', "Admin\Api\TanahApiController@get")->name('admin_api_get_tanah');
                Route::delete('/{id}', "Admin\Api\TanahApiController@delete")->name('admin_api_delete_tanah');
            });

            Route::group(['prefix' => 'gedung'], function () {
                Route::get('/', "Admin\Api\GedungApiController@get")->name('admin_api_get_gedung');
                Route::delete('/{id}', "Admin\Api\Gedung@delete")->name('admin_api_delete_gedung');
            });

            Route::group(['prefix' => 'kendaraan'], function () {
                Route::get('/{kondisi?}', "Admin\Api\KendaraanApiController@get")->name('admin_api_get_kendaraan');
                Route::delete('/{id}', "Admin\Api\Kendaraan@delete")->name('admin_api_delete_kendaraan');
            });




            Route::group(['prefix' => 'kendaraan-dinas'], function () {
                Route::get('/', "Admin\Api\KendaraanDinasApiController@get")->name('admin_api_get_kendaraan_dinas');
                Route::post('/', "Admin\Api\KendaraanDinasApiController@post")->name('admin_api_post_kendaraan_dinas');
                Route::put('/{id}', "Admin\Api\KendaraanDinasApiController@put")->name('admin_api_put_kendaraan_dinas');
                Route::delete('/{id}', "Admin\Api\KendaraanDinasApiController@delete")->name('admin_api_delete_kendaraan_dinas');
            });

            Route::group(['prefix' => 'user'], function () {
                Route::get('/', "Admin\Api\UserApiController@get")->name('admin_api_get_user');
                Route::post('/', "Admin\Api\UserApiController@post")->name('admin_api_post_user');
                Route::put('/{id}', "Admin\Api\UserApiController@put")->name('admin_api_put_user');
                Route::put('/reset-password/{id}', "Admin\Api\UserApiController@putResetPassword")->name('admin_api_put_reset_password_user');
                Route::delete('/{id}', "Admin\Api\UserApiController@delete")->name('admin_api_delete_user');
            });

            Route::group(['prefix' => 'barang'], function () {
                Route::get('/', "Admin\Api\BarangApiController@get")->name('admin_api_get_barang');
                Route::post('/', "Admin\Api\BarangApiController@post")->name('admin_api_post_barang');
                Route::put('/{id}', "Admin\Api\BarangApiController@put")->name('admin_api_put_barang');
                Route::delete('/{id}', "Admin\Api\BarangApiController@delete")->name('admin_api_delete_barang');
            });
            Route::group(['prefix' => 'kendaraan'], function () {
                Route::delete('/{id}', "Admin\Api\KendaraanApiController@delete")->name('admin_api_delete_kendaraan');
            });
            Route::group(['prefix' => 'gedung'], function () {
                Route::delete('/{id}', "Admin\Api\GedungApiController@delete")->name('admin_api_delete_gedung');
            });
            Route::group(['prefix' => 'tanah'], function () {
                Route::delete('/{id}', "Admin\Api\TanahApiController@delete")->name('admin_api_delete_tanah');
            });
            Route::group(['prefix' => 'rumah-negara'], function () {
                Route::delete('/{id}', "Admin\Api\RumahNegaraApiController@delete")->name('admin_api_delete_rumah_negara');
            });
            Route::group(['prefix' => 'Satker'], function () {
                Route::delete('/{id}', "Admin\Api\Satker@delete")->name('admin_api_delete_satker');
            });
            Route::group(['prefix' => 'role'], function () {
                Route::get('/dropdown', "Admin\Api\RoleApiController@getDropdown")->name('admin_api_get_dropdown_role');
            });

            Route::group(['prefix' => 'barang'], function () {
                Route::get('/', "Admin\Api\BarangApiController@get")->name('admin_api_get_barang');
            });


            Route::group(['prefix' => 'country'], function () {
                Route::get('/dropdown', "Admin\Api\CountryApiController@getDropdown")->name('admin_api_get_country_dropdown');
            });

            Route::group(['prefix' => 'satker'], function () {
                Route::get('/dropdown', "Admin\Api\SatkerApiController@getDropdown")->name('admin_api_get_dropdown_satker');
                Route::get('/', "Admin\Api\SatkerApiController@get")->name('admin_api_get_satker');
                Route::put('/{id}', "Admin\Api\SatkerApiController@put")->name('admin_api_put_satker');
                Route::delete('/{id}', "Admin\Api\Satker@delete")->name('admin_api_delete_satker');
            });
        });

    });
    Route::get('/upload/hapus/{id}', 'NotificationsController@hapus');
    Route::group(['prefix' => 'api'], function () {
        Route::group(['prefix' => 'dark-mode'], function () {
            Route::get('/', "Api\DarkModeApiController@get")->name('api_get_dark_mode');
            Route::post('/', "Api\DarkModeApiController@post")->name('api_post_dark_mode');
        });
        Route::get('/', "Admin\Api\SekjenApiController@get")->name('admin_api_get_sekjen');
        Route::post('/', "Admin\Api\SekjenApiController@post")->name('admin_api_post_sekjen');
        Route::put('/{id}', "Admin\Api\SekjenApiController@put")->name('admin_api_put_sekjen');
        Route::delete('/{id}', "Admin\Api\SekjenApiController@delete")->name('admin_api_delete_sekjen');

        Route::group(['prefix' => 'summary-asset'], function () {
//        Route::get('/{slugCountry?}/{slugSatker?}', "Api\AssetController@getSummaryAssets")->name('api_asset_get_summary_assets');
            Route::get('/{slugSatker?}', "Api\AssetController@getSummaryAssets")->name('api_asset_get_summary_assets');
        });

        Route::group(['prefix' => 'continents'], function () {
            Route::get('/', "Api\ContinentController@getContinents")->name('api_continent_get_continents');
        });
        Route::group(['prefix' => 'countries'], function () {
            Route::get('/', "Api\CountryController@getCountries")->name('api_country_get_countries');
        });
        Route::group(['prefix' => 'satkers'], function () {
            Route::get('/', "Api\SatkerController@getSatkers")->name('api_satker_get_satkers');
        });
        Route::group(['prefix' => 'change-password'], function () {
            Route::post('/', "Api\ProfileController@postChangePassword")->name('api_post_change_password');
        });

        Route::post('/login', 'Auth\LoginController@login')->name('api_post_login');
    });
});

Auth::routes();
