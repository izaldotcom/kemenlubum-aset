@extends('layouts.base')

@section('content')
    <div class="main-menu">
        <div class="row d-flex justify-content-center" style="margin-top: 2rem">
            <div class="col-md-8">
                <form action="be_forms_elements_bootstrap.html" method="post" onsubmit="return false;">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-secondary">Hasil pencarian dari</button>
                                </div>
                                <input type="text" class="form-control" id="example-input2-group2"
                                       name="example-input2-group2" placeholder="Rumah Negara">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row d-flex justify-content-center" style="margin-top: 2rem">
            <div class="banner-kota col-11 col-md-8">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey hasil-survey mb-3">
                        <a href="{{ url('/detail-survey') }}">
                            <div class="card justify-content-center p-3">
                                <div class="img-survey mb-3">
                                    <img class="card-img-top" src="{{ asset('assets/images/hasil-survey.png') }}" alt=""
                                         style="height: 125px;width: 125px;">
                                </div>
                                <p class="card-text text-image">Hasil Survey</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey hasil-survey mb-3">
                        <a href="{{ url('/detail-survey') }}">
                            <div class="card justify-content-center p-3">
                                <div class="img-survey mb-3">
                                    <img class="card-img-top" src="{{ asset('assets/images/data-survey.png') }}" alt=""
                                         style="height: 125px;width: 125px;">
                                </div>
                                <p class="card-text text-image">Data Survey</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey hasil-survey mb-3">
                        <a href="{{ url('/detail-survey') }}">
                            <div class="card justify-content-center p-3">
                                <div class="img-survey mb-3">
                                    <img class="card-img-top" src="{{ asset('assets/images/hasil-survey.png') }}" alt=""
                                         style="height: 125px;width: 125px;">
                                </div>
                                <p class="card-text text-image">Hasil Survey</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey hasil-survey mb-3">
                        <a href="{{ url('/detail-survey') }}">
                            <div class="card justify-content-center p-3">
                                <div class="img-survey mb-3">
                                    <img class="card-img-top" src="{{ asset('assets/images/data-survey.png') }}" alt=""
                                         style="height: 125px;width: 125px;">
                                </div>
                                <p class="card-text text-image">Data Survey</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey hasil-survey mb-3">
                        <a href="{{ url('/detail-survey') }}">
                            <div class="card justify-content-center p-3">
                                <div class="img-survey mb-3">
                                    <img class="card-img-top" src="{{ asset('assets/images/hasil-survey.png') }}" alt=""
                                         style="height: 125px;width: 125px;">
                                </div>
                                <p class="card-text text-image">Hasil Survey</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey hasil-survey mb-3">
                        <a href="{{ url('/detail-survey') }}">
                            <div class="card justify-content-center p-3">
                                <div class="img-survey mb-3">
                                    <img class="card-img-top" src="{{ asset('assets/images/data-survey.png') }}" alt=""
                                         style="height: 125px;width: 125px;">
                                </div>
                                <p class="card-text text-image">Data Survey</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey hasil-survey mb-3">
                        <a href="{{ url('/detail-survey') }}">
                            <div class="card justify-content-center p-3">
                                <div class="img-survey mb-3">
                                    <img class="card-img-top" src="{{ asset('assets/images/hasil-survey.png') }}" alt=""
                                         style="height: 125px;width: 125px;">
                                </div>
                                <p class="card-text text-image">Hasil Survey</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey hasil-survey mb-3">
                        <a href="{{ url('/detail-survey') }}">
                            <div class="card justify-content-center p-3">
                                <div class="img-survey mb-3">
                                    <img class="card-img-top" src="{{ asset('assets/images/data-survey.png') }}" alt=""
                                         style="height: 125px;width: 125px;">
                                </div>
                                <p class="card-text text-image">Data Survey</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="row d-flex justify-content-center align-items-baseline flex-row h-100 hidden-md-up">
            <div class="banner-kota col-11 h-50 align-items-center d-flex">
                <div class="banner-center">
                    <div class="title-banner">SURVEY</div>
                    <img src="{{ asset('assets/images/kota.png') }}" alt="" class="img-fluid">
                </div>
            </div>
            <div class="banner-kota col-11 col-sm-8 h-50">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey hasil-survey mr-3 px-0">
                        <a href="{{ url('/detail-survey') }}" class="d-block"><img src="{{ asset('assets/images/hasil-survey.png') }}" alt="">
                            <span class="text-image">Hasil Survey</span>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-5 col-5 text-center btn-survey data-survey ml-3 px-0">
                        <a href="{{ url('/detail-survey') }}" class="d-block"><img src="{{ asset('assets/images/data-survey.png') }}" alt="">
                            <span class="text-image">Daftar Survey</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>  --}}
    </div>
@endsection