@extends('layouts.base-login')

@section('content')
    <div class="main-menu d-flex justify-content-center align-items-center">
        <div class="card login-dashboard">
            <div class="card-body">
                <img src="{{ asset('assets/images/user.png') }}" alt="" class="img-user">
                <div class="title-login">{{ __('Login') }}</div>
                <form method="POST" action="{{ url('/login') }}">
                    @csrf
                    <div class="form-group row d-flex justify-content-center">
                        <div class="input-group col-10 px-0">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><img
                                            src="{{ asset('assets/images/user-login.png') }}" alt=""></span>
                            </div>
                            <input placeholder="Masukkan Username" id="email" type="text"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-login"
                                   name="username" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row d-flex justify-content-center">
                        <div class="input-group col-10 px-0">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><img
                                            src="{{ asset('assets/images/password-login.png') }}" alt=""></span>
                            </div>
                            <input placeholder="Masukkan Password" id="password" type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-login"
                                   name="password" value="{{ old('password') }}" required autofocus>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                               <strong>{{ $errors->first('password') }}</strong>
                           </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row d-flex justify-content-center mt-4">
                        <div class="col-10 px-0">
                            <button type="submit" class="btn btn-submit w-100">
                                {{ __('Login') }}
                            </button>
                        </div>
                    </div>
                    <div class="form-group row mx-ins">
                        <div class="col-md-6 offset-md-1 px-0">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember"
                                       id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    Ingat Saya
                                </label>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group row">
                        <div class="col-md-8 offset-md-1">
                            @if (Route::has('password.request'))
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </div> --}}
                </form>
            </div>
        </div>
    </div>
    {{-- <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
@endsection
