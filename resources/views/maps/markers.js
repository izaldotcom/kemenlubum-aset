var markers = [
  {
    "name":"SEKRETARIAT JENDERAL KEMENTERIAN LUAR NEGERI",
    "city":"Jakarta",
    "lat":21.5568642,
    "lng":39.2271978,
    "alt":5282,
    "tz":"Jakarta"
  },{
    "name":"Kedutaan Besar RI di Addis Ababa",
    "city":"Addis Ababa, Etiopia",
    "lat":8.9806034,
    "lng":38.75776050000002,
    "alt":20,
    "tz":"Addis Ababa, Etiopia"
  },{
    "name":"Kedutaan Besar RI di Alger",
    "city":"Alger, Aljazair",
    "lat":36.753768,
    "lng":3.0587560999999823,
    "tz":"Alger, Aljazair"
  },{
    "name":"Kedutaan Besar RI di Ankara",
    "city":"Ankara, Turki",
    "lat":39.8848512,
    "lng":32.85691120000001,
    "tz":"Ankara, Turki"
  },{
    "name":"Kedutaan Besar RI di Baghdad",
    "city":"Baghdad, Irak",
    "lat":33.319356,
    "lng":44.38635380000005,
    "tz":"Baghdad, Irak"
  },{
    "name":"Kedutaan Besar RI di Bangkok",
    "city":"Bangkok, China",
    "lat":13.7512607,
    "lng":100.53656430000001,
    "tz":"Bangkok, China"
  },{
    "name":"KBRI Beograd",
    "city":"Beograd, Serbia",
    "lat":44.78770219999999,
    "lng":20.456334200000015,
    "tz":"America/Godthab"
  },{
    "name":"Konsulat Jenderal RI di Frankfurt",
    "city":"Frankfurt, Jerman",
    "lat":50.1224546,
    "lng":8.653083299999935,
    "tz":"Frankfurt, Jerman"
  },{
    "name":"Kedutaan Besar RI di Bern",
    "city":"Bern, Belanda",
    "lat":46.9371738,
    "lng":7.466225699999995,
    "tz":"Bern, Belanda"
  },{
    "name":"Konsulat Jenderal RI di Mumbai",
    "city":"Mumbai, India",
    "lat":18.9656426,
    "lng":72.8093576,
    "tz":"Mumbai, India"
  },{
    "name":"Kedutaan Besar RI di Berlin",
    "city":"Berlin, Jerman",
    "lat":52.5294319,
    "lng":13.363261299999976,
    "tz":"Berlin, Jerman"
  },{
    "name":"Kedutaan Besar RI di Brussels",
    "city":"Brussels, Jerman",
    "lat":50.8384919,
    "lng":4.434669399999962,
    "tz":"Brussels, Jerman"
  },{
    "name":"Kedutaan Besar RI di Bucharest",
    "city":"București, Rumania",
    "lat":44.4565141,
    "lng":26.089190199999962,
    "tz":"București, Rumania"
  },{
    "name":"Kedutaan Besar RI di Budapest",
    "city":"Budapest, Városligeti fasor 26, 1068 Hungaria",
    "lat":47.5095366,
    "lng":19.0763508,
    "tz":"Budapest, Városligeti fasor 26, 1068 Hungaria"
  },{
    "name":"Kedutaan Besar RI di Buenos Aires",
    "city":"Buonos Aires, Brazil",
    "lat":-34.57940839999999,
    "lng":-58.400031799999965,
    "tz":"Buonos Aires, Brazil"
  },{
    "name":"Kedutaan Besar RI di Kairo",
    "city":"Kairo, Mesir",
    "lat":30.0349349,
    "lng":31.23106729999995,
    "tz":"Kairo, Mesir]"
  },{
    "name":"Kedutaan Besar RI di Kairo",
    "city":"Kairo, Mesir",
    "lat":30.0349349,
    "lng":31.23106729999995,
    "tz":"Kairo, Mesir"
  },{
    "name":"Kedutaan Besar RI di Canberra",
    "city":"Yarralumla ACT 2600, Australia",
    "lat":-35.30396700000001,
    "lng":149.116596,
    "tz":"Yarralumla ACT 2600, Australia"
  },{
    "name":"KBRI Colombo",
    "city":"Kolombo, Sri Lanka",
    "lat":6.9019569,
    "lng":79.87501840000004,
    "tz":"Kolombo, Sri Lanka"
  },{
    "name":"Kedutaan Besar RI di Dhaka",
    "city":"Dhaka 1212, Bangladesh",
    "lat":23.795737,
    "lng":90.41273899999999,
    "tz":"Dhaka 1212, Bangladesh"
  },{
    "name":"Kedutaan Besar RI di Damascus",
    "city":"Damaskus, Suriah",
    "lat":33.496819,
    "lng":36.250725399999965,
    "tz":"Damaskus, Suriah"
  },{
    "name":"Kedutaan Besar RI di Dar Es Salaam",
    "city":" Ali Hassan Mwinyi Rd, Dar es Salaam 0572, Tanzania",
    "lat":-6.8072247,
    "lng":39.28615869999999,
    "tz":" Ali Hassan Mwinyi Rd, Dar es Salaam 0572, Tanzania"
  },{
    "name":"Konsulat Jenderal RI di Davao City",
    "city":" 31 6th Ave, Talomo, Davao City, 8000 Davao del Sur, Filipina",
    "lat":14.5580728,
    "lng":121.0151912,
    "tz":" 31 6th Ave, Talomo, Davao City, 8000 Davao del Sur, Filipina"
  },{
    "name":"Kedutaan Besar RI di Den Haag",
    "city":" Den Haag, Belanda",
    "lat":52.08614,
    "lng":4.2887210000000096,
    "tz":" Den Haag, Belanda"
  },{
    "name":"Konsulat Jenderal RI Jeddah",
    "city":" Jeddah Arab Saudi",
    "lat":21.5568642,
    "lng":39.2271978,
    "tz":" Jeddah Arab Saudi" 
  },{
    "name":"Perutusan Tetap RI pada PBB dan OI lainnya di Jenewa",
    "city":" Jenewa, Republik Federasi Swiss.",
    "lat":46.2040552,
    "lng":6.126648100000011,
    "tz":"Jenewa, Republik Federasi Swiss." 
  },{
    "name":"Konsulat Jenderal RI di Hamburg",
    "city":"Hamburg, Republik Federal Jerman.",
    "lat":53.5973291,
    "lng":9.99385570000004,
    "tz":"Hamburg, Republik Federal Jerman." 
  },{
    "name":"Kedutaan Besar RI di Hanoi",
    "city":"Hanoi, Filipina",
    "lat":21.0210017,
    "lng":105.85350849999998,
    "tz":"Hanoi, Filipina"
  },{
    "name":"Konsulat Jenderal RI di Hong Kong",
    "city":"Hongkong",
    "lat":22.278354,
    "lng":114.18642899999998,
    "tz":"Hongkong"
  },{
    "name":"Kedutaan Besar RI di Islamabad",
    "city":"ISLAMABAD, Pakistan.",
    "lat":33.72243719999999,
    "lng":73.10379460000001,
    "tz":"ISLAMABAD, Pakistan."
  },{
    "name":"Kedutaan Besar RI di Kabul",
    "city":"KABUL, NEGARA REPUBLIK ISLAM AFGHANISTAN.",
    "lat":34.53080699999999,
    "lng":69.17900680000002,
    "tz":"KABUL, NEGARA REPUBLIK ISLAM AFGHANISTAN."
  },{
    "name":"Konsulat Jenderal RI di Karachi",
    "city":"Konsulat Jenderal Republik Indonesia Di Karachi, Republik Islam Pakistan.",
    "lat":24.82035609999999,
    "lng":67.03007630000002,
    "tz":"Konsulat Jenderal Republik Indonesia Di Karachi, Republik Islam Pakistan."
  },{
    "name":"Konsulat Jenderal RI di Kota Kinabalu",
    "city":"Konsulat Jenderal Republik Indonesia di KOTA KINABALU SABAH, MALAYSIA.",
    "lat":5.970191700000002,
    "lng":116.0708396,
    "tz":"Konsulat Jenderal Republik Indonesia di KOTA KINABALU SABAH, MALAYSIA."
  },{
    "name":"Konsulat Jenderal RI di Osaka",
    "city":"Osaka, Jepang",
    "lat":34.6880588,
    "lng":135.48494729999993,
    "tz":"Osaka, Jepang"
  },{
    "name":"Konsulat Jenderal RI di Osaka",
    "city":"Osaka, Jepang",
    "lat":34.6880588,
    "lng":135.48494729999993,
    "tz":"Osaka, Jepang"
  },{
    "name":"Kedutaan Besar Republik Indonesia Kuala Lumpur",
    "city":"Kuala Lumpur",
    "lat":3.1466944,
    "lng":101.72173129999999,
    "tz":"Kuala Lumpur"
  },{
    "name":"Kedutaan Besar RI di Abuja",
    "city":"Abuja, Merangkap Republik Ghana, Republik Liberia",
    "lat":9.095041,
    "lng":7.495096999999987,
    "tz":"Abuja, Merangkap Republik Ghana, Republik Liberia"
  },{
    "name":"Kedutaan Besar RI di London",
    "city":"London, Inggris",
    "lat":51.49695209999999,
    "lng":-0.13095029999999497,
    "tz":"London, Inggris"
  },{
    "name":"Kedutaan Besar RI di Manila",
    "city":"Manila, Filipina",
    "lat":14.5580728,
    "lng":121.0151912,
    "tz":"London, Inggris"
  },{
    "name":"KBRI Mexico",
    "city":"Mexico",
    "lat":19.4279345,
    "lng":-99.19725979999998,
    "tz":"Mexico"
  },{
    "name":"Kedutaan Besar RI di Moskow",
    "city":"Rusia",
    "lat":55.7380238,
    "lng":37.63015870000004,
    "tz":"Rusia"
  },{
    "name":"Kedutaan Besar RI di New Delhi",
    "city":"New Delhi, India",
    "lat":28.602185,
    "lng":77.19454819999999,
    "tz":"New Delhi, India"
  },{
    "name":"Kedutaan Besar RI di Ottawa",
    "city":"Otawa, Kanada",
    "lat":45.410279,
    "lng":-75.73424899999998,
    "tz":"Otawa, Kanada"
  }

];