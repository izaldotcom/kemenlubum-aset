@extends('layouts.base')

@section('styles')
    <link rel="stylesheet" id="css-main" href="{{ asset('assets-admin/css/codebase.css') }}">
    <style>
        #page-container.main-content-boxed > #main-container .content, #page-container.main-content-boxed > #page-footer .content, #page-container.main-content-boxed > #page-header .content, #page-container.main-content-boxed > #page-header .content-header {
            max-width: 100%;
        }

        @media (min-width: 768px) {
            .content-heading {
                margin-bottom: 25px;
                padding-top: 0;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-menu">
        <div class="row d-flex justify-content-center">
            <div class="banner-kota col-11 align-items-center d-flex justify-content-around"
                 style="margin-top: 2rem;margin-bottom: 4rem">
                <div class="banner-center w-100">
                    <div class="title-banner">TANAH BERDASARKAN KONDISI</div>
                    <img src="{{ asset('assets/images/kota.png') }}" alt="" class="img-fluid w-100">
                </div>
            </div>
            <div class="banner-kota col-11 ">
                <div id="page-container" class="enable-page-overlay side-scroll page-header-modern main-content-boxed">
                    <main id="main-container">
                        <div class="content p-0">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="block">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">Pie</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option"
                                                        data-toggle="block-option" data-action="state_toggle"
                                                        data-action-mode="demo">
                                                    <i class="si si-refresh"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content">
                                            <div class="row items-push-2x text-center invisible" data-toggle="appear">
                                                <div class="col-12">
                                                    <!-- Sparkline Pie 1 Container -->
                                                    <span class="js-slc-pie1">100,25,26,18</span>
                                                    <div class="font-w600 mt-10">
                                                        <i class="fa fa-ticket text-muted mr-5"></i> Kondisi
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <!-- Pie Chart -->
                                    <div class="block">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">Pie</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option"
                                                        data-toggle="block-option" data-action="state_toggle"
                                                        data-action-mode="demo">
                                                    <i class="si si-refresh"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content block-content-full text-center">
                                            <!-- Pie Chart Container -->
                                            <canvas class="js-chartjs-pie"></canvas>
                                        </div>
                                    </div>
                                    <!-- END Pie Chart -->
                                </div>
                                <div class="col-xl-6">
                                    <!-- Polar Area Chart -->
                                    <div class="block">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">Polar Area</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option"
                                                        data-toggle="block-option" data-action="state_toggle"
                                                        data-action-mode="demo">
                                                    <i class="si si-refresh"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content block-content-full text-center">
                                            <!-- Polar Area Chart Container -->
                                            <canvas class="js-chartjs-polar"></canvas>
                                        </div>
                                    </div>
                                    <!-- END Polar Area Chart -->
                                </div>
                                <div class="col-xl-6">
                                    <!-- Bars Chart -->
                                    <div class="block">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">Bars</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option"
                                                        data-toggle="block-option" data-action="state_toggle"
                                                        data-action-mode="demo">
                                                    <i class="si si-refresh"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content block-content-full text-center">
                                            <canvas class="js-chartjs-bars01"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets-admin/js/codebase.core.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/codebase.app.min.js') }}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('assets-admin/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.stack.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.resize.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('assets-admin/js/pages/be_comp_charts.min.js') }}"></script>

    <!-- Page JS Helpers (Easy Pie Chart Plugin) -->
    <script>jQuery(function () {
            Codebase.helpers('easy-pie-chart');
        });</script>

    <script>
        // Set Global Chart.js configuration
        Chart.defaults.global.defaultFontColor           = '#555555';
        Chart.defaults.scale.gridLines.color             = "rgba(0,0,0,.04)";
        Chart.defaults.scale.gridLines.zeroLineColor     = "rgba(0,0,0,.1)";
        Chart.defaults.scale.ticks.beginAtZero           = true;
        Chart.defaults.global.elements.line.borderWidth  = 2;
        Chart.defaults.global.elements.point.radius      = 5;
        Chart.defaults.global.elements.point.hoverRadius = 7;
        Chart.defaults.global.tooltips.cornerRadius      = 3;
        Chart.defaults.global.legend.labels.boxWidth     = 12;

        // Get Chart Containers
        let chartLinesCon = jQuery('.js-chartjs-lines');
        let chartBarsCon  = jQuery('.js-chartjs-bars01');
        let chartRadarCon = jQuery('.js-chartjs-radar');
        let chartPolarCon = jQuery('.js-chartjs-polar');
        let chartPieCon   = jQuery('.js-chartjs-pie');
        let chartDonutCon = jQuery('.js-chartjs-donut');

        // Lines/Bar/Radar Chart Data
        let chartLinesBarsRadarData = {
            labels  : [
                @foreach($kondisis as $kondisi)
                    '{{ $kondisi }}',
                @endforeach
            ],
            datasets: [
                {
                    label                    : 'Total',
                    fill                     : true,
                    backgroundColor          : 'rgba(66,165,245,.75)',
                    borderColor              : 'rgba(66,165,245,1)',
                    pointBackgroundColor     : 'rgba(66,165,245,1)',
                    pointBorderColor         : '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor    : 'rgba(66,165,245,1)',
                    data                     : [
                        @foreach($values as $value)
                            '{{ $value }}',
                        @endforeach
                    ]
                }
            ]
        };

        // Polar/Pie/Donut Data
        let chartPolarPieDonutData = {
            labels  : [
                'Earnings',
                'Sales',
                'Tickets'
            ],
            datasets: [{
                data                : [
                    50,
                    25,
                    25
                ],
                backgroundColor     : [
                    'rgba(156,204,101,1)',
                    'rgba(255,202,40,1)',
                    'rgba(239,83,80,1)'
                ],
                hoverBackgroundColor: [
                    'rgba(156,204,101,.5)',
                    'rgba(255,202,40,.5)',
                    'rgba(239,83,80,.5)'
                ]
            }]
        };

        // Init Charts
        let chartLines, chartBars, chartRadar, chartPolar, chartPie, chartDonut;

        if (chartLinesCon.length) {
            chartLines = new Chart(chartLinesCon, {type: 'line', data: chartLinesBarsRadarData});
        }

        if (chartBarsCon.length) {
            chartBars = new Chart(chartBarsCon, {type: 'bar', data: chartLinesBarsRadarData});
        }

        if (chartRadarCon.length) {
            chartRadar = new Chart(chartRadarCon, {type: 'radar', data: chartLinesBarsRadarData});
        }

        if (chartPolarCon.length) {
            chartPolar = new Chart(chartPolarCon, {type: 'polarArea', data: chartPolarPieDonutData});
        }

        if (chartPieCon.length) {
            chartPie = new Chart(chartPieCon, {type: 'pie', data: chartPolarPieDonutData});
        }

        if (chartDonutCon.length) {
            chartDonut = new Chart(chartDonutCon, {type: 'doughnut', data: chartPolarPieDonutData});
        }
    </script>
@endsection