@extends('admin.layouts.base')

@section('content')
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dasbor</a>
        <span class="breadcrumb-item active">Input Excel Asset Rumah Negara</span>
    </nav>

    <div class="row invisible" data-toggle="appear">
        <!-- Row #2 -->
        <div class="col-md-5">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Import Excel File</h3>
                </div>
                <div class="block-content">
                    <form-input-excel tipe="rumah"></form-input-excel>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Log Action</h3>
                </div>
                <div class="block-content block-content-full">
                    <log-import-excel tipe="rumah"></log-import-excel>
                </div>
            </div>
        </div>
        <!-- END Row #2 -->
    </div>
@endsection