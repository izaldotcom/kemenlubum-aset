@extends('admin.layouts.base')

@section('scripts')
    <script type="text/javascript">
        $(document).ready(() => {
            window.app.params.filter = "{{ !empty($filter) ? $filter : ""  }}";
            window.eventHub.$emit('refresh-ajaxtable', 'data-rumah-negara');
        });
    </script>
@endsection

@section('content')
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dasbor</a>
        <span class="breadcrumb-item active">Daftar Asset Rumah Negara</span>
    </nav>

    <div class="row">
        <!-- Row #2 -->
        <div class="col-md-12">
            <div class="block" id="mythead">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Daftar Asset Rumah Negara</h3>
                </div>
                <div class="block-content long-table">
                    <ajax-table
                            :url="'{{ route('admin_api_get_rumah_negara' ) }}'"
                            :oid="'data-rumah-negara'"
                            :params="params"
                            :config="{
                                  autoload: false,
                                  show_all: false,
                                  has_number: true,
                                  has_entry_page: false,
                                  has_pagination: true,
                                  has_action: true,
                                  has_search_input: true,
                                  has_search_header: false,
                                  custom_header: '',
                                  default_sort: 'id',
                                  default_sort_dir: 'desc',
                                  custom_empty_page: false,
                                  search_placeholder: 'Cari',
                                  class: {
                                    table: ['table-condensed'],
                                    wrapper: ['table-responsive'],
                                  }
                            }"
                            :rowparams="{tanggal: params.nama_kedutaan}"
                            :rowtemplate="'tr-data-rumah-negara'"
                            :columns="{
                                  'kode_barang': 'Kode Barang',
                                  'nup': 'NUP',
                                  'kode_satker': 'Kode Satker',
                                  'nama_satker': 'Nama Satker',
                                  'kib': 'KIB',
                                  'nama_barang': 'Nama Barang',
                                  'kondisi': 'Kondisi',
                                  'jenis_dokumen': 'Jenis Dokumen',
                                  'kepemilikan': 'Kepemilikan',
                                  'jenis_sertifikat': 'Jenis Sertifikat',
                                  'merk_type': 'Merk/Type',
                                  'tanggal_rekam_pertama': 'Tgl Rekam Pertama',
                                  'tanggal_perolehan': 'Tgl Perolehan',
                                  'nilai_perolehan_pertama': 'Nilai Perolehan Pertama',
                                  'nilai_mutasi': 'Nilai Mutasi',
                                  'nilai_perolehan': 'Nilai Perolehan',
                                  'nilai_penyusutan': 'Nilai Penyusutan',
                                  'nilai_buku': 'Nilai Buku',
                                  'kuantitas': 'Kuantitas',
                                  'jumlah_foto': 'Jumlah Foto',
                                  'jumlah_lantai': 'Jumlah Lantai',
                                  'luas_bangunan': 'Luas Bangunan',
                                  'luas_dasar_bangunan': 'Luas Dasar Bangunan',
                                  'alamat': 'Alamat',
                                  'jalan': 'Jalan',
                                  'kode_kota': 'Kode Kota',
                                  'kode_provinsi': 'Kode Provinsi',
                                  'uraian_kota': 'Uraian Kota',
                                  'status_penggunaan': 'Status Penggunaan',
                                  'status_pengelolaan': 'Status Pengelolaan',
                                  'no_psp': 'No. PSP',
                                  'tanggal_psp': 'Tgl PSP',
                                  'jumlah_kib': 'Jumlah KIB',
                                  'sbsk': 'SBSK',
                                  'optimalisasi': 'Optimalisasi',
                            }">
                    </ajax-table>
                </div>
            </div>
        </div>
    </div>

    {{--    <div class="row" data-toggle="appear">--}}
    {{--        <div class="col-md-12">--}}
    {{--            <div class="block">--}}
    {{--                <div class="block-header block-header-default">--}}
    {{--                    <h3 class="block-title">Log Action</h3>--}}
    {{--                </div>--}}
    {{--                <div class="block-content block-content-full">--}}
    {{--                    <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->--}}
    {{--                    <table class="table table-bordered table-striped table-vcenter table-sm-responsive">--}}
    {{--                        <tbody>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Januari 2019 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Maret 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Gedung</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Februari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">11 Februari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Gedung</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">19 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Gedung</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">10 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">05 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Gedung</td>--}}
    {{--                        </tr>--}}
    {{--                        </tbody>--}}
    {{--                    </table>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection

@section('modal')
    <modal-edit-long-lat tipe="rumah"></modal-edit-long-lat>
    <modal-edit-photos tipe="rumah"></modal-edit-photos>
@endsection
