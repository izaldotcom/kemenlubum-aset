@extends('admin.layouts.base')

@section('content')
    <div class="row invisible" data-toggle="appear">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">General Setting</h3>
                </div>
                <div class="block-content block-content-full">
                    <div class="row">
                        <div class="col-md-6">
                            <form-general-setting></form-general-setting>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
