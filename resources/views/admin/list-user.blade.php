@extends('admin.layouts.base')

@section('content')
    <div class="row invisible" data-toggle="appear">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Daftar Users</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-info min-width-125 mb-3"
                            onclick="fireUpModalAddUser()">+ Add New Users
                    </button>
                    <ajax-table
                        :url="'{{ route('admin_api_get_user' ) }}'"
                        :oid="'data-user'"
                        :params="params"
                        :config="{
                                  autoload: true,
                                  show_all: false,
                                  has_number: true,
                                  has_entry_page: false,
                                  has_pagination: true,
                                  has_action: true,
                                  has_search_input: true,
                                  has_search_header: false,
                                  custom_header: '',
                                  default_sort: 'id',
                                  default_sort_dir: 'desc',
                                  custom_empty_page: false,
                                  search_placeholder: 'Cari',
                                  class: {
                                    table: ['table-condensed'],
                                    wrapper: ['table-responsive'],
                                  }
                            }"
                        :rowparams="{tanggal: params.nama_kedutaan}"
                        :rowtemplate="'tr-data-user'"
                        :columns="{
                                  'username': 'Username',
                                  'role_name': 'Role',
                                  'name': 'Name',
                                  'email': 'E-mail',
                                  'is_active': 'Active',
                            }">
                    </ajax-table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <modal-add-user></modal-add-user>
    <modal-edit-user></modal-edit-user>
    {{--    <modal-delete-user></modal-delete-user>--}}
    <modal-reset-password></modal-reset-password>
@endsection

@section('scripts')
    <script type="text/javascript">
        function fireUpModalAddUser() {
            window.eventHub.$emit('open-modal', 'modal-add-user');
        }
    </script>
@endsection
