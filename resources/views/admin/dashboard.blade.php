@extends('admin.layouts.base')

@section('content')
    <div class="row js-appear-enabled animated fadeIn content-chart" data-toggle="appear">
        <div class="col-6 col-xl-4">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-yellow block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <img src="{{ asset('assets/svg/land-w.svg') }}" alt="">
                    </div>
                    <div class="font-size-h3 font-w600 js-count-to-enabled" data-toggle="countTo" data-speed="1000"
                         data-to="{{ $summary['tanah']['total'] }}">
                        {{ number_format($summary['tanah']['total'], 0, ',', '.') }}
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Tanah</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-4">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-blue block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <img src="{{ asset('assets/svg/building-w.svg') }}" alt="">
                    </div>
                    <div class="font-size-h3 font-w600">
                        <span data-toggle="countTo" data-speed="1000" data-to="{{ $summary['bangunan']['total'] }}"
                              class="js-count-to-enabled">
                            {{ number_format($summary['bangunan']['total'], 0, ',', '.') }}
                        </span>
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Bangunan</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-4">
            <a class="block block-link-shadow text-right" href="javascript:void(0)">
                <div class="block-green2 block-content block-content-full clearfix">
                    <div class="float-left mt-10 d-none d-sm-block">
                        <img src="{{ asset('assets/svg/car-w.svg') }}" alt="">
                    </div>
                    <div class="font-size-h3 font-w600 js-count-to-enabled" data-toggle="countTo" data-speed="1000"
                         data-to="{{ $summary['mobilitas']['total'] }}">
                        {{ number_format($summary['mobilitas']['total'], 0, ',', '.') }}
                    </div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Mobilitas</div>
                </div>
            </a>
        </div>
    </div>
    <div class="row" id="printMe8">
        <div class="col-xl-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Summary Keseluruhan Aset</h3>
                </div>
                <div class="block-content" id="printMe">
                    <div class="row items-push-1x text-center">
                        <div class="col-12 mx-auto">
                        <div id="summaryAllBar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
                     <!-- <div class="col-xl-4">
                        <div class="block">
                            <div class="block-header block-header-default">
                                <h3 class="block-title">Tanah</h3>

                           </div>
                           <div class="block-content">
                               <div class="row items-push-1x text-center">
                                    <div class="col-12 mx-auto">
                                        <GChart
                                            type="PieChart"
                                            :data="summaryTanahBar"
                                           :options="barTanahOptions"
                                        />
                                    </div>
                                </div>
                            </div>
                       </div>
                   </div> -->
        <div class="col-xl-4">
            <!-- Avatar -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Bangunan</h3>
                                            <!-- <div class="block-options"> -->
                                                <!-- <button type="button" class="js-pie-randomize btn-block-option" -->
                                                        <!-- data-toggle="tooltip" title="Randomize">-->
                                                    <!-- <i class="fa fa-random"></i>-->
                                                <!-- </button>-->
                                            <!--</div>-->
                </div>
                <div class="block-content" id="printMe2">
                    <div class="row items-push-1x text-center">
                        <div class="col-12 mx-auto">
                            <div id="summaryBangunanBar"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Avatar -->
        </div>

        <div class="col-xl-4">
            <!-- Avatar -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Mobilitas</h3>
                                            <!-- <div class="block-options"> -->
                                                <!-- <button type="button" class="js-pie-randomize btn-block-option" -->
                                                        <!-- data-toggle="tooltip" title="Randomize">-->
                                                    <!-- <i class="fa fa-random"></i>-->
                                                <!-- </button>-->
                                            <!--</div>-->
                </div>
                <div class="block-content" id="printMe3">
                    <div class="row items-push-1x text-center">
                        <div class="col-12 mx-auto">
                            <div id="summaryMobilitasBar"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Avatar -->
        </div>

        <div class="col-xl-4">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Kendaraan Berdasarkan Usia</h3>
                    <!--                        <div class="block-options">-->
                    <!--                            <button type="button" class="js-pie-randomize btn-block-option"-->
                    <!--                                    data-toggle="tooltip" title="Randomize">-->
                    <!--                                <i class="fa fa-random"></i>-->
                    <!--                            </button>-->
                    <!--                        </div>-->
                </div>
                <div class="block-content" id="printMe4">
                    <div class="row items-push-1x text-center">
                        <div class="col-12 mx-auto">
                            <div id="summaryKendaraanUsia"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4">
            <!-- Avatar -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Pembagian Kendaraan</h3>
                                            <!-- <div class="block-options"> -->
                                                <!-- <button type="button" class="js-pie-randomize btn-block-option" -->
                                                        <!-- data-toggle="tooltip" title="Randomize">-->
                                                    <!-- <i class="fa fa-random"></i>-->
                                                <!-- </button>-->
                                            <!--</div>-->
                </div>
                <div class="block-content" id="printMe5">
                    <div class="row items-push-1x text-center">
                        <div class="col-12 mx-auto">
                            <div id="summaryKendaraanRoda"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Avatar -->
        </div>

        <div class="col-xl-4">
            <!-- Avatar -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Kendaraan Roda 4 / Lebih</h3>
                    <!--                        <div class="block-options">-->
                    <!--                            <button type="button" class="js-pie-randomize btn-block-option"-->
                    <!--                                    data-toggle="tooltip" title="Randomize">-->
                    <!--                                <i class="fa fa-random"></i>-->
                    <!--                            </button>-->
                    <!--                        </div>-->
                </div>
                <div class="block-content" id="printMe7">
                    <div class="row items-push-1x text-center">
                        <div class="col-12 mx-auto">
                            <div id="summaryKendaraanRodaEmpat"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Avatar -->
        </div>

        <div class="col-xl-4">
            <!-- Avatar -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Kendaraan Roda < 4</h3>
                    <!--                        <div class="block-options">-->
                    <!--                            <button type="button" class="js-pie-randomize btn-block-option"-->
                    <!--                                    data-toggle="tooltip" title="Randomize">-->
                    <!--                                <i class="fa fa-random"></i>-->
                    <!--                            </button>-->
                    <!--                        </div>-->
                </div>
                <div class="block-content" id="printMe6">
                    <div class="row items-push-1x text-center">
                        <div class="col-12 mx-auto">
                            <div id="summaryKendaraanRodaDua"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Avatar -->
        </div>
    </div>

{{--        Commented Sementara--}}
{{--        <div class="row" data-toggle="appear">--}}
{{--            <div class="col-6 col-md-4 col-xl-2">--}}
{{--                <a class="block block-link-shadow text-center" href="be_pages_generic_inbox.html">--}}
{{--                    <div class="block-content ribbon ribbon-bookmark ribbon-success ribbon-left">--}}
{{--                        <div class="ribbon-box">15</div>--}}
{{--                        <p class="mt-5">--}}
{{--                            <i class="si si-envelope-letter fa-3x"></i>--}}
{{--                        </p>--}}
{{--                        <p class="font-w600">Inbox</p>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="col-6 col-md-4 col-xl-2">--}}
{{--                <a class="block block-link-shadow text-center" href="be_pages_generic_profile.html">--}}
{{--                    <div class="block-content">--}}
{{--                        <p class="mt-5">--}}
{{--                            <i class="si si-user fa-3x"></i>--}}
{{--                        </p>--}}
{{--                        <p class="font-w600">Profile</p>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="col-6 col-md-4 col-xl-2">--}}
{{--                <a class="block block-link-shadow text-center" href="be_pages_forum_categories.html">--}}
{{--                    <div class="block-content ribbon ribbon-bookmark ribbon-primary ribbon-left">--}}
{{--                        <div class="ribbon-box">3</div>--}}
{{--                        <p class="mt-5">--}}
{{--                            <i class="si si-bubbles fa-3x"></i>--}}
{{--                        </p>--}}
{{--                        <p class="font-w600">Forum</p>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="col-6 col-md-4 col-xl-2">--}}
{{--                <a class="block block-link-shadow text-center" href="be_pages_generic_search.html">--}}
{{--                    <div class="block-content">--}}
{{--                        <p class="mt-5">--}}
{{--                            <i class="si si-magnifier fa-3x"></i>--}}
{{--                        </p>--}}
{{--                        <p class="font-w600">Search</p>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="col-6 col-md-4 col-xl-2">--}}
{{--                <a class="block block-link-shadow text-center" href="be_comp_charts.html">--}}
{{--                    <div class="block-content">--}}
{{--                        <p class="mt-5">--}}
{{--                            <i class="si si-bar-chart fa-3x"></i>--}}
{{--                        </p>--}}
{{--                        <p class="font-w600">Live Stats</p>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--            <div class="col-6 col-md-4 col-xl-2">--}}
{{--                <a class="block block-link-shadow text-center" href="javascript:void(0)">--}}
{{--                    <div class="block-content">--}}
{{--                        <p class="mt-5">--}}
{{--                            <i class="si si-settings fa-3x"></i>--}}
{{--                        </p>--}}
{{--                        <p class="font-w600">Settings</p>--}}
{{--                    </div>--}}
{{--                </a>--}}
{{--            </div>--}}
{{--        </div>--}}
@endsection
