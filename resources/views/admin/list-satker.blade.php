@extends('admin.layouts.base')

@section('content')
    <div class="row invisible" data-toggle="appear">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Daftar Satker</h3>
                </div>
                <div class="block-content block-content-full long-table">
                    <ajax-table
                        :url="'{{ route('admin_api_get_satker' ) }}'"
                        :oid="'data-satker'"
                        :params="params"
                        :config="{
                                  autoload: true,
                                  show_all: false,
                                  has_number: true,
                                  has_entry_page: false,
                                  has_pagination: true,
                                  has_action: true,
                                  has_search_input: true,
                                  has_search_header: false,
                                  custom_header: '',
                                  default_sort: 'id',
                                  default_sort_dir: 'desc',
                                  custom_empty_page: false,
                                  search_placeholder: 'Cari',
                                  class: {
                                    table: ['table-condensed'],
                                    wrapper: ['table-responsive'],
                                  }
                            }"
                        :rowparams="{tanggal: params.nama_kedutaan}"
                        :rowtemplate="'tr-data-satker'"
                        :columns="{
                                  'kode_satker': 'Kode',
                                  'nama_satker': 'Nama',
                                  'countries': 'Countries',
                                  'lat': 'Location',
                            }">
                    </ajax-table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <modal-edit-long-lat tipe="satker"></modal-edit-long-lat>
    <modal-edit-satker></modal-edit-satker>
    <modal-edit-photos tipe="satker"></modal-edit-photos>
@endsection
