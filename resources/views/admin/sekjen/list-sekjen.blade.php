@extends('admin.layouts.base')

@section('scripts')
    <script type="text/javascript">
        function fireUpModalAddSekjen() {
            window.eventHub.$emit('open-modal', 'modal-add-sekjen');
        }
    </script>
    <script type="text/javascript">
        $(document).ready(() => {
            window.app.params.code = "{{ !empty($code) ? $code : ""  }}";
            window.eventHub.$emit('refresh-ajaxtable', 'data-sekjen');
        });
    </script>
@endsection

@section('content')
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dasbor</a>
        <span class="breadcrumb-item active">Daftar Sekertariat Jenderal</span>
    </nav>

    <div class="row">
        <!-- Row #2 -->
        <div class="col-md-12">
            <div class="block" id="mythead">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Daftar Sekertariat Jenderal</h3>
                </div>
                <div class="block-content long-table">
                    <button type="button" class="btn btn-info min-width-125 mb-3"
                            onclick="fireUpModalAddSekjen()">+ Add Sekertariat Jenderal
                    </button>
                    <ajax-table
                        :url="'{{ route('admin_api_get_sekjen')}}'"
                        :oid="'data-sekjen'"
                        :params="params"
                        :config="{
                                  autoload: false,
                                  show_all: false,
                                  has_number: true,
                                  has_entry_page: false,
                                  has_pagination: true,
                                  has_action: true,
                                  has_search_input: true,
                                  has_search_header: false,
                                  custom_header: '',
                                  default_sort: 'id',
                                  default_sort_dir: 'desc',
                                  custom_empty_page: false,
                                  search_placeholder: 'Cari',
                                  class: {
                                    table: ['table-condensed'],
                                    wrapper: ['table-responsive'],
                                  }
                            }"
                            :rowparams="{tanggal: params.nama_kedutaan}"
                        :rowtemplate="'tr-data-sekjen'"
                        :columns="{
                                  'nama': 'nama',
                                  'nip': 'nip',
                            }">
                    </ajax-table>
                </div>
            </div>
        </div>
    </div>

    {{--    <div class="row" data-toggle="appear">--}}
    {{--        <div class="col-md-12">--}}
    {{--            <div class="block">--}}
    {{--                <div class="block-header block-header-default">--}}
    {{--                    <h3 class="block-title">Log Action</h3>--}}
    {{--                </div>--}}
    {{--                <div class="block-content block-content-full">--}}
    {{--                    <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->--}}
    {{--                    <table class="table table-bordered table-striped table-vcenter table-sm-responsive">--}}
    {{--                        <tbody>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Januari 2019 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Maret 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Kendaraan</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Februari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">11 Februari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Kendaraan</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">19 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Kendaraan</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">10 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">05 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Kendaraan</td>--}}
    {{--                        </tr>--}}
    {{--                        </tbody>--}}
    {{--                    </table>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection

@section('modal')
    <modal-add-sekjen></modal-add-sekjen>
    <modal-edit-sekjen></modal-edit-sekjen>
    <modal-delete-sekjen></modal-delete-sekjen>
@endsection
