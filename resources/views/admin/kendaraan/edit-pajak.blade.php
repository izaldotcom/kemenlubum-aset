@extends('admin.layouts.base')

@section('content')
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dasbor</a>
        <a class="breadcrumb-item" href="{{ route('admin_kendaraan_list_dinas') }}">Daftar Pajak & Asuransi</a>
        <span class="breadcrumb-item active">Edit Pajak & Asuransi</span>
    </nav>

    <div class="row">
        <!-- Row #2 -->
        <div class="col-md-12">
            <div class="block" id="mythead">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Edit Pajak & Asuransi</h3>
                </div>
                <div class="block-content long-table">
                </div>
            </div>
        </div>
    </div>

    {{--    <div class="row" data-toggle="appear">--}}
    {{--        <div class="col-md-12">--}}
    {{--            <div class="block">--}}
    {{--                <div class="block-header block-header-default">--}}
    {{--                    <h3 class="block-title">Log Action</h3>--}}
    {{--                </div>--}}
    {{--                <div class="block-content block-content-full">--}}
    {{--                    <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->--}}
    {{--                    <table class="table table-bordered table-striped table-vcenter table-sm-responsive">--}}
    {{--                        <tbody>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Januari 2019 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Maret 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Kendaraan</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Februari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">11 Februari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Kendaraan</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">19 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Kendaraan</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">10 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">05 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Kendaraan</td>--}}
    {{--                        </tr>--}}
    {{--                        </tbody>--}}
    {{--                    </table>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection

@section('modal')
    <modal-add-kendaraan-dinas></modal-add-kendaraan-dinas>
    <modal-edit-kendaraan-dinas></modal-edit-kendaraan-dinas>
    <modal-delete-kendaraan-dinas></modal-delete-kendaraan-dinas>
@endsection

@section('scripts')
    <script type="text/javascript">
        function fireUpModalAddKendaraanDinas() {
            window.eventHub.$emit('open-modal', 'modal-add-kendaraan-dinas');
        }
    </script>
@endsection
