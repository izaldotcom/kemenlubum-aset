@extends('admin.layouts.base')

@section('content')
    <div class="main-menu d-flex justify-content-center align-items-center" style="margin: 4rem 0">
        <div class="col-md-5">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title text-centert">Ganti Password</h3>
                </div>
                <div class="block-content">
                    <form-change-password></form-change-password>
                </div>
            </div>
        </div>
    </div>
@endsection
