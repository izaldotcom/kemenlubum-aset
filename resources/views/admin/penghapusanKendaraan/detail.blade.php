@extends('admin.layouts.base')

@section('content')
    @php
        /** @var \App\Models\Kendaraan\PengajuanPenghapusan $pengajuanPenghapusan */
    @endphp

    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dasbor</a>
        <a class="breadcrumb-item" href="{{ route('admin_pengajuan_penghapusan_kendaraan_list') }}">Daftar Pengajuan
            Penghapusan</a>
        <span
            class="breadcrumb-item active">Detail Pengajuan Penghapusan {{ ($pengajuanPenghapusan->status == 'new') ? '(Pilih Kendaraan)' : '' }} </span>
    </nav>

    <div class="row">
        <div class="col-xl-12">
            <div class="block" id="mythead">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Pengajuan Penghapusan
                        - {{ ($pengajuanPenghapusan->status == 'new') ? '(Pilih Kendaraan)' : '' }}</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-md-6">
                            <strong>No Pengajuan
                                Penghapusan:</strong> {{ $pengajuanPenghapusan->no_pengajuan_penghapusan }}
                        </div>
                        <div class="col-md-6">
                            <strong>Kategori
                                Penghapusan:</strong> {{ $pengajuanPenghapusan->kategoriPenghapusan->name }}
                        </div>
                        <div class="col-md-12">
                            <hr/>
                            <div class="alert alert-{{ $pengajuanPenghapusan->status_color }}">
                                {{ $pengajuanPenghapusan->status_say }}
                            </div>
                        </div>
                        @if ($pengajuanPenghapusan->status == 'new')
                            <div class="col-md-12">
                                <pilih-kendaraan-penghapusan
                                    penghapusanid="{{ $pengajuanPenghapusan->id }}"></pilih-kendaraan-penghapusan>
                            </div>

                        @elseif ($pengajuanPenghapusan->status == 'kendaraan')
                            {{--                            <div class="col-md-12 mb-10">--}}
                            {{--                                                                <button class="btn btn-danger">Kembali ke pemilihan kendaraan</button>--}}
                            {{--                                <a href="{{ route('admin_pengajuan_penghapusan_kendaraan_download_all', ['id' => $pengajuanPenghapusan->id]) }}">--}}
                            {{--                                    <button type="button" class="btn btn-info">Download & Merge All</button>--}}
                            {{--                                </a>--}}
                            {{--                            </div>--}}
                        @elseif ($pengajuanPenghapusan->status == 'approved')
                            <div class="col-md-12 mb-10">
                                {{--                                <button class="btn btn-danger">Kembali ke pemilihan kendaraan</button>--}}
                                @if (Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan', 'admin_aset']))
                                    <a href="{{ route('admin_pengajuan_penghapusan_kendaraan_download_all', ['id' => $pengajuanPenghapusan->id]) }}">
                                        <button type="button" class="btn btn-info">Download & Merge All</button>
                                    </a>
                                    <button type="button" class="btn btn-warning"
                                            onclick="fireUpModalEditPenghapusanPimpinan()">
                                        Edit Variable Surat Sekjen
                                    </button>
                                    <a href="{{ route('admin_pengajuan_penghapusan_kendaraan_download_all', ['id' => $pengajuanPenghapusan->id, 'kemkeu' => 1]) }}">
                                        <button type="button" class="btn btn-success">Download & Merge All for KEMKEU
                                        </button>
                                    </a>
                                @endif
                            </div>
                            @elseif ($pengajuanPenghapusan->status == 'selesai')
                            <div class="col-md-12 mb-10">
                                {{--                                <button class="btn btn-danger">Kembali ke pemilihan kendaraan</button>--}}
                                    <a href="{{ route('admin_pengajuan_penghapusan_kendaraan_download_all', ['id' => $pengajuanPenghapusan->id]) }}">
                                        <button type="button" class="btn btn-info">Download & Merge All</button>
                                    </a>
                                    <a href="{{ route('admin_pengajuan_penghapusan_kendaraan_download_all', ['id' => $pengajuanPenghapusan->id, 'kemkeu' => 1]) }}">
                                        <button type="button" class="btn btn-success">Download & Merge All for KEMKEU
                                        </button>
                                    </a>
                            </div>
                            @endif
                    </div>
                </div>
            </div>
        </div>

        @if (in_array($pengajuanPenghapusan->status, ['approved']))
            @if (Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan', 'admin_aset']))
                <div class="col-xl-12">
                    <div class="block" id="mythead">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Detail Pengajuan Penghapusan Surat Pernyataan</h3>
                        </div>
                        <div class="block-content">
                            <detail-penghapusan-pimpinan penghapusanid="{{ $pengajuanPenghapusan->id }}"
                                                         approver="1"></detail-penghapusan-pimpinan>
                        </div>
                    </div>
                </div>
            @endif
        @endif

        @if ($pengajuanPenghapusan->status != 'new')
            <div class="col-xl-12">
                <div class="block" id="mythead">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Detail Pengajuan Penghapusan</h3>
                    </div>
                    <div class="block-content">
                        <detail-penghapusan penghapusanid="{{ $pengajuanPenghapusan->id }}"
                                            approver="1"></detail-penghapusan>
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        function fireUpModalAddKendaraanPenghapusan() {
            window.eventHub.$emit('open-modal', 'modal-add-kendaraan-penghapusan');
        }
        function fireModalEditKendaraan(){
                window.eventHub.$emit('open-modal', 'modal-edit-penghapusan-panitia')
            }
        function fireModalEditSignature(){
                window.eventHub.$emit('open-modal', 'modal-edit-penghapusan-signature')
            }
        function fireUpModalEditPenghapusanPimpinan() {
            window.eventHub.$emit('open-modal', 'modal-edit-penghapusan-pimpinan');
        }

    </script>
@endsection

@section('modal')
    <modal-edit-penghapusan-pimpinan penghapusanid="{{ $pengajuanPenghapusan->id }}"></modal-edit-penghapusan-pimpinan>
    <modal-template-penghapusan-detail penghapusanid="{{ $pengajuanPenghapusan->id }}"></modal-template-penghapusan-detail>
        <modal-edit-penghapusan-panitia penghapusanid="{{ $pengajuanPenghapusan->id }}"> </modal-edit-penghapusan-panitia>
        <modal-edit-penghapusan-signature penghapusanid="{{ $pengajuanPenghapusan->id }}"> </modal-edit-penghapusan-signature>
    <modal-upload-penghapusan-detail penghapusanid="{{ $pengajuanPenghapusan->id }}"></modal-upload-penghapusan-detail>
    <modal-add-kendaraan-penghapusan penghapusanid="{{ $pengajuanPenghapusan->id }}"></modal-add-kendaraan-penghapusan>
@endsection
