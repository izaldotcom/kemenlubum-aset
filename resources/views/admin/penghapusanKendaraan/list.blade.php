@extends('admin.layouts.base')

@section('content')
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dasbor</a>
        <span class="breadcrumb-item active">Daftar Pengajuan Penghapusan</span>
    </nav>

    <div class="row">
        <!-- Row #2 -->
        <div class="col-md-12">
            <div class="block" id="mythead">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Daftar Pengajuan Penghapusan</h3>
                </div>
                <div class="block-content long-table">
                    <ajax-table
                            :url="'{{ route('admin_api_get_pengajuan_penghapusan' ) }}'"
                            :oid="'data-pengajuan-penghapusan'"
                            :params="params"
                            :config="{
                                  autoload: true,
                                  show_all: false,
                                  has_number: true,
                                  has_entry_page: false,
                                  has_pagination: true,
                                  has_action: true,
                                  has_search_input: true,
                                  has_search_header: false,
                                  custom_header: '',
                                  default_sort: 'id',
                                  default_sort_dir: 'desc',
                                  custom_empty_page: false,
                                  search_placeholder: 'Cari',
                                  class: {
                                    table: ['table-condensed'],
                                    wrapper: ['table-responsive'],
                                  }
                            }"
                            :rowparams="{tanggal: params.nama_kedutaan}"
                            :rowtemplate="'tr-data-pengajuan-penghapusan'"
                            :columns="{
                                  'created_at': 'Tanggal Pengajuan',
                                  'no_pengajuan_penghapusan': 'No.',
                                  'kategori_penghapusan': 'Kategori',
                                  'nama_satker': 'Satker',
                                  'uploader': 'Creator',
                                  'status': 'Status',
                                  'status_approve': 'Approver',
                            }">
                    </ajax-table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <modal-edit-long-lat tipe="kendaraan"></modal-edit-long-lat>
    <modal-edit-photos tipe="kendaraan"></modal-edit-photos>
@endsection
