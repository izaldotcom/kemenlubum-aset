@extends('admin.layouts.base')
@section('content')
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dasbor</a>
        <span class="breadcrumb-item active">Create Pengajuan Penghapusan</span>
    </nav>

    <div class="row">
        <!-- Row #2 -->
        <div class="col-md-12">
            <div class="block" id="mythead">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Create Pengajuan Penghapusan</h3>
                </div>
                <div class="block-content">
                    <ul class="create-list-penghapusan m-0 p-0">
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-success mr-5 mb-5">
                                    <i class="fa fa-check"></i>
                                </button>
                                <div class="medias-body ml-4">1. Surat Keputusan Kepala Perwakilan Republik Indonesia
                                    Jeddah tentang Perubahan Atas Keputusan Kepala Perwakilan Republik Indonesia Nomor
                                    004/SK-KONJEN/1/2016 tentang Tim Inventarisasi dan Penghapusan Barang Milik Negara
                                    Konsulat Jendral Republik Indonesia di Jeddah
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-success mr-5 mb-5">
                                    <i class="fa fa-check"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">2. Berita Acara Penelitian beserta dengan
                                    lampirannya
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-success mr-5 mb-5">
                                    <i class="fa fa-check"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">3. Laporan Barang Kuasa Pengguna Semester I TA 2016
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-danger mr-5 mb-5">
                                    <i class="fa fa-times"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">4. Surat Keterangan Kondisi Kendaraan dari bengkel
                                    resmi
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-success mr-5 mb-5">
                                    <i class="fa fa-check"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">5. Fotocopy Bukti Kepemilikan Kendaraan Bermotor
                                    (STNK)
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-success mr-5 mb-5">
                                    <i class="fa fa-check"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">6. Kartu Identitas Barang (KIB), <i>print out</i> dari
                                    Aplikasi SIMAK-BMN
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-danger mr-5 mb-5">
                                    <i class="fa fa-times"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">7. Buku Barang (BB) tentang kendaraan yang akan
                                    dihapus
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-success mr-5 mb-5">
                                    <i class="fa fa-check"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">8. Laporan Kondisi Barang Kendaraan Dinas yang akan
                                    dihapuskan
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-success mr-5 mb-5">
                                    <i class="fa fa-check"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">9. <i>Listing</i> Sejarah Transaksi BMN <i>print
                                        out</i> dari Aplikasi SIMAK-BMN
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-danger mr-5 mb-5">
                                    <i class="fa fa-times"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">10. Foto Berwarna kendaraan dinas yang akan
                                    dihapuskan
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-danger mr-5 mb-5">
                                    <i class="fa fa-times"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">11. Surat Pernyataan Kepala Perwakilan yang menyatakan
                                    bahwa penghapusan kendaraan dinas tidak akan mengganggu tugas dan fungsi perwakilan
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-success mr-5 mb-5">
                                    <i class="fa fa-check"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">12. Surat Pernyataan Kepala Perwakilan yang menyatakan
                                    bertanggungjawab atas besaran nilai/taksiran bukan merupakan nilai wajar hasil
                                    penertiban BMN, tapi nilai pasar sekarang
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-success mr-5 mb-5">
                                    <i class="fa fa-check"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">13. Surat Pernyataan Kepala Perwakilan yang menyatakan
                                    Kerajaan Saudi Arabia tidak memiliki sistem lelang
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="media">
                                <button type="button" class="btn btn-circle btn-alt-danger mr-5 mb-5">
                                    <i class="fa fa-times"></i>
                                </button>
                                <div class="media-body ml-4 pt-2">14. Surat Pernyataan Kepala Perwakilan
                                    bertanggungjawab atas keabsahan dokumen penghapusan 6 (enam) unit kendaraan dinas
                                    pada Perwakilan RI Jeddah
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="row" data-toggle="appear">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Log Action</h3>
                </div>
                <div class="block-content block-content-full">
                    <!-- DataTables functionality is initialized with .js-dataTable-full class in js/pages/be_tables_datatables.min.js which was auto compiled from _es6/pages/be_tables_datatables.js -->
                    <table class="table table-bordered table-striped table-vcenter table-sm-responsive">
                        <tbody>
                        <tr>
                            <td class="date">12 Januari 2019 11.12.56</td>
                            <td class="inf">Susan Day at Rumah Negara</td>
                        </tr>
                        <tr>
                            <td class="date">12 Maret 2018 11.12.56</td>
                            <td class="inf">Ralph Murray at Kendaraan</td>
                        </tr>
                        <tr>
                            <td class="date">12 Februari 2018 11.12.56</td>
                            <td class="inf">Susan Day at Rumah Negara</td>
                        </tr>
                        <tr>
                            <td class="date">11 Februari 2018 11.12.56</td>
                            <td class="inf">Ralph Murray at Kendaraan</td>
                        </tr>
                        <tr>
                            <td class="date">19 Januari 2018 11.12.56</td>
                            <td class="inf">Ralph Murray at Rumah Negara</td>
                        </tr>
                        <tr>
                            <td class="date">12 Januari 2018 11.12.56</td>
                            <td class="inf">Ralph Murray at Kendaraan</td>
                        </tr>
                        <tr>
                            <td class="date">10 Januari 2018 11.12.56</td>
                            <td class="inf">Susan Day at Rumah Negara</td>
                        </tr>
                        <tr>
                            <td class="date">05 Januari 2018 11.12.56</td>
                            <td class="inf">Ralph Murray at Kendaraan</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> --}}
@endsection

@section('modal')
    <modal-edit-long-lat tipe="kendaraan"></modal-edit-long-lat>
    <modal-edit-photos tipe="kendaraan"></modal-edit-photos>
@endsection
