@extends('admin.layouts.base-maps')

@section('styles')
    <link href="{{ asset('assets-admin/css/keyframe.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" id="css-main" href="{{ asset('assets-admin/css/style-darkmode2.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets-admin/css/style-custom2.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/css/dnSlide.css') }}">
    <style>
        #page-container.main-content-boxed > #main-container .content, #page-container.main-content-boxed > #page-footer .content, #page-container.main-content-boxed > #page-header .content, #page-container.main-content-boxed > #page-header .content-header {
            max-width: 100%;
        }

        .input-group-append {
            margin-top: 1px;
        }

        #slider-filter .owl-item.active {
            border: solid 0.75vh rgba(0, 150, 255, 1);
            box-shadow: 0 0 2.5vh rgba(0, 200, 255, 1);
            cursor: hand;
        }

        @media (min-width: 768px) {
            .content-heading {
                margin-bottom: 25px;
                padding-top: 0;
            }
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            {{-- <div class="sidebar-maps">
                <div class="open-sidebar"><i class="fa fa-angle-right"></i></div>
                <div data-toggle="slimscroll" data-height="calc(100vh - 75px)">
                    <div class="content-filter-maps">
                        <div class="search-map">
                            <div class="input-group">
                              <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                              <div class="input-group-append">
                                <span class="input-group-text"><i class="fa fa-search"></i></span>
                                <span class="input-group-text"><i class="fa fa-times"></i></span>
                              </div>
                            </div>
                        </div>
                        <button class="hero-header fade-in">
                            <img src="{{ asset('assets/images/kemlu.jpg') }}" class="img-fluid">
                        </button>
                        <button class="hero-header-directions">
                           <div class="hero-header-directions-base">
                              <div class="hero-header-directions-icon" style="background-image:url({{ asset('assets/images/directions.png') }});"></div>
                           </div>
                           <label class="hero-header-directions-label">Directions</label>
                        </button>
                        <div class="hero-header-description">
                          <h1 class="hero-header-title">Ministry Of Foreign Affairs</h1>
                          <h2 class="hero-header-subtitle mb-0">
                              <span class="hero-header-subtitle-badge">Kementerian Luar Negeri Republik Indonesia</span>
                          </h2>
                          <a class="widget-pane-link">Government office</a>
                          <div class="hero-header-description-container mt-2">
                             <div class="weather">
                                <div class="weather-link">
                                   <a href="" class="weather-link-href">
                                      <div class="weather-icon">
                                          <img src="{{ asset('assets/images/partly-cloudy.png') }}" alt="Jakarta weather">
                                      </div>
                                   </a>
                                </div>
                                <div class="weather-text">
                                   <div>Partly Cloudy · 27°C</div>
                                   <div>9:52 PM</div>
                                </div>
                             </div>
                          </div>
                        </div>
                        <div class="action-maps">
                            <div class="row d-flex justify-content-center m-0">
                                <div class="col-3 py-2">
                                    <a href="">
                                        <div class="d-flex card flex-column">
                                        <div class="img text-center mb-2">
                                            <img src="{{ asset('assets/images/save.png') }}" alt="" height="24" width="20">
                                        </div>
                                        <div class="blue-button-text text-center">Save</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 py-2">
                                    <a href="">
                                        <div class="d-flex card flex-column">
                                        <div class="img text-center mb-2">
                                            <img src="{{ asset('assets/images/nearby.png') }}" alt="" height="24" width="20">
                                        </div>
                                        <div class="blue-button-text text-center">Nearby</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 py-2">
                                    <a href="">
                                        <div class="d-flex card flex-column">
                                        <div class="img text-center mb-2">
                                            <img src="{{ asset('assets/images/send.png') }}" alt="" height="24" width="20">
                                        </div>
                                        <div class="blue-button-text text-center">Send To Your Phone</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-3 py-2">
                                    <a href="">
                                        <div class="d-flex card flex-column">
                                        <div class="img text-center mb-2">
                                            <img src="{{ asset('assets/images/share.png') }}" alt="" height="24" width="20">
                                        </div>
                                        <div class="blue-button-text text-center">Share</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                       <div class="divider"></div>
                        <div class="info-hoverable px-3">
                            <a href="#">
                                <div class="media py-2">
                                    <img src="{{ asset('assets/images/marker.png') }}" alt="" class="mr-3" width="15">
                                    <span class="media-body">Jl. Taman Pejambon No.6, RT.9/RW.5, Senen, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10410</span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="media py-2">
                                    <img src="{{ asset('assets/images/plus-code.png') }}" alt="" class="mr-3" width="15">
                                    <span class="media-body">RRGM+7F Jakarta</span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="media py-2">
                                    <img src="{{ asset('assets/images/web.png') }}" alt="" class="mr-3" width="15">
                                    <span class="media-body">kemlu.go.id</span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="media py-2">
                                    <img src="{{ asset('assets/images/clock.png') }}" alt="" class="mr-3" width="15">
                                    <span class="media-body">Opens at 8.00 AM</span>
                                </div>
                            </a>
                            <a href="#">
                                <div class="media py-2">
                                    <img src="{{ asset('assets/images/label.png') }}" alt="" class="mr-3" width="15">
                                    <span class="media-body">Add a label</span>
                                </div>
                            </a>
                        </div>
                       <div class="divider"></div>
                        <div class="subheader">
                           <h2 class="subheader-header">Photos</h2>
                        </div>
                        <div class="img-container">
                            <div class="img-left">
                                <button class="button">
                                    <div class="image-pack-image-container" style="width: 211px;height: 120px;">
                                        <img src="{{ asset('assets/images/jalan.jpg') }}" alt="" style="width: 211px;height: 120px;">
                                    </div>
                                    <div class="shade"></div>
                                    <div class="label">
                                        <span class="pano"></span>
                                        <span class="gm2-body-2"></span>
                                    </div>
                                </button>
                            </div>
                            <div class="img-right">
                                <button class="button">
                                    <div class="image-pack-image-container" style="width: 141px;height:120px">
                                        <img src="{{ asset('assets/images/gedung.jpg') }}" alt="" style="width: 141px;height:120px">
                                    </div>
                                    <div class="shade"></div>
                                    <div class="label">
                                        <span class="pano d-none"></span>
                                        <span class="gm2-body-2">12 Photos</span>
                                    </div>
                                </button>
                            </div>
                        </div>
                       <div class="divider"></div>
                        <div class="subheader">
                           <h2 class="subheader-header">People also search for</h2>
                        </div>
                        <div class="img-container">
                            <div class="row">
                                <div class="col-3">
                                    <div class="card w-100">
                                      <img class="card-img-top" src="{{ asset('assets/images/gedung-k.jpg') }}" alt="Card image cap" width="72" height="72">
                                      <div class="card-body">
                                        <div class="related-place-title">Ministry Of Finance</div>
                                        <div class="related-place-category">Department of finance</div>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="card w-100">
                                      <img class="card-img-top" src="{{ asset('assets/images/gedung-k.jpg') }}" alt="Card image cap" width="72" height="72">
                                      <div class="card-body">
                                        <div class="related-place-title">Ministry Of Finance</div>
                                        <div class="related-place-category">Department of finance</div>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="card w-100">
                                      <img class="card-img-top" src="{{ asset('assets/images/gedung-k.jpg') }}" alt="Card image cap" width="72" height="72">
                                      <div class="card-body">
                                        <div class="related-place-title">Ministry Of Finance</div>
                                        <div class="related-place-category">Department of finance</div>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="card w-100">
                                      <img class="card-img-top" src="{{ asset('assets/images/gedung-k.jpg') }}" alt="Card image cap" width="72" height="72">
                                      <div class="card-body">
                                        <div class="related-place-title">Ministry Of Finance</div>
                                        <div class="related-place-category">Department of finance</div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row d-flex justify-content-center">
                            <button class="btn btn-secondary btn-sm my-3" id="view-list-more">
                                <span>View More</span>
                            </button>
                        </div>
                    </div>
                    <div class="content-list-maps">
                        <div class="search-map">
                            <div class="input-group">
                              <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                              <div class="input-group-append">
                                <span class="input-group-text"><i class="fa fa-search"></i></span>
                                <a href="#" class="close-list-maps"><span class="input-group-text"><i class="fa fa-times"></i></span></a>
                              </div>
                            </div>
                        </div>
                        <div class="info-hoverable">
                            <a href="#" class="px-3 close-list-maps">
                                <div class="media py-2">
                                    <div class="media-body">
                                        <h1 class="hero-header-title">Ministry Of Foreign Affairs</h1>
                                        <span class="mb-2">Jl. Taman Pejambon No.6, RT.9/RW.5, Senen, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10410</span>
                                        <span>Opens at 8.00 AM</span>
                                    </div>
                                    <img src="{{ asset('assets/images/g9280.jpg') }}" alt="" class="ml-3" width="92">
                                </div>
                            </a>
                            <a href="#" class="px-3 close-list-maps">
                                <div class="media py-2">
                                    <div class="media-body">
                                        <h1 class="hero-header-title">Ministry Of Foreign Affairs</h1>
                                        <span class="mb-2">Jl. Taman Pejambon No.6, RT.9/RW.5, Senen, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10410</span>
                                        <span>Opens at 8.00 AM</span>
                                    </div>
                                    <img src="{{ asset('assets/images/g9280.jpg') }}" alt="" class="ml-3" width="92">
                                </div>
                            </a>
                            <a href="#" class="px-3 close-list-maps">
                                <div class="media py-2">
                                    <div class="media-body">
                                        <h1 class="hero-header-title">Ministry Of Foreign Affairs</h1>
                                        <span class="mb-2">Jl. Taman Pejambon No.6, RT.9/RW.5, Senen, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10410</span>
                                        <span>Opens at 8.00 AM</span>
                                    </div>
                                    <img src="{{ asset('assets/images/g9280.jpg') }}" alt="" class="ml-3" width="92">
                                </div>
                            </a>
                            <a href="#" class="px-3 close-list-maps">
                                <div class="media py-2">
                                    <div class="media-body">
                                        <h1 class="hero-header-title">Ministry Of Foreign Affairs</h1>
                                        <span class="mb-2">Jl. Taman Pejambon No.6, RT.9/RW.5, Senen, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10410</span>
                                        <span>Opens at 8.00 AM</span>
                                    </div>
                                    <img src="{{ asset('assets/images/g9280.jpg') }}" alt="" class="ml-3" width="92">
                                </div>
                            </a>
                            <a href="#" class="px-3 close-list-maps">
                                <div class="media py-2">
                                    <div class="media-body">
                                        <h1 class="hero-header-title">Ministry Of Foreign Affairs</h1>
                                        <span class="mb-2">Jl. Taman Pejambon No.6, RT.9/RW.5, Senen, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10410</span>
                                        <span>Opens at 8.00 AM</span>
                                    </div>
                                    <img src="{{ asset('assets/images/g9280.jpg') }}" alt="" class="ml-3" width="92">
                                </div>
                            </a>
                            <a href="#" class="px-3 close-list-maps">
                                <div class="media py-2">
                                    <div class="media-body">
                                        <h1 class="hero-header-title">Ministry Of Foreign Affairs</h1>
                                        <span class="mb-2">Jl. Taman Pejambon No.6, RT.9/RW.5, Senen, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10410</span>
                                        <span>Opens at 8.00 AM</span>
                                    </div>
                                    <img src="{{ asset('assets/images/g9280.jpg') }}" alt="" class="ml-3" width="92">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div> --}}
            <backend-maps-filter></backend-maps-filter>
            <maps-kemlu></maps-kemlu>
        </div>
    </div>
@endsection

@section('scripts')

@endsection