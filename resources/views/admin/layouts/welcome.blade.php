@extends('layouts.base')

@section('styles')
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/css/owl.carousel.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets-admin/css/codebase.css') }}">
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/css/style-custom.css') }}">

  <!-- Mapbox GL -->

    {{-- <link rel="stylesheet" id="css-main" href="{{ asset('assets/css/dnSlide.css') }}"> --}}

    <style>
        #page-container.main-content-boxed > #main-container .content, #page-container.main-content-boxed > #page-footer .content, #page-container.main-content-boxed > #page-header .content, #page-container.main-content-boxed > #page-header .content-header {
            max-width: 100%;
        }
        .custom-popup .leaflet-popup-content-wrapper {
      background: #2c3e50;
      color: #fff;
      font-size: 16px;
      line-height: 24px;
      border-radius: 0px;
    }

    .custom-popup .leaflet-popup-content-wrapper a {
      color: rgba(255, 255, 255, 0.1);
    }

    .custom-popup .leaflet-popup-tip-container {
      width: 30px;
      height: 15px;
    }

    .custom-popup .leaflet-popup-tip {
      background: transparent;
      border: none;
      box-shadow: none;
    }

    /* css to customize Leaflet default styles  */
    .custom .leaflet-popup-tip,
    .custom .leaflet-popup-content-wrapper {
      background: #0b3654;
      color: #ffffff;
    }

    .waseman {
      text-align: center;
      padding: 10px 0;
    }

    .facebook,
    .twitter,
    .instagram,
    .telegram {
      padding: 10px 15px;
      background: #555956;
      text-decoration: none;
      text-transform: uppercase;
      font-size: 10px;
      color: white;
      border-radius: 3px;
    }

    .facebook:hover,
    .twitter:hover,
    .instagram:hover,
    .telegram:hover {
      background: #c97d18;
    }



        .input-group-append {
            margin-top: 1px;
        }

        #slider-filter .owl-item.active {
            border: solid 0.75vh rgba(0, 150, 255, 1);
            box-shadow: 0 0 2.5vh rgba(0, 200, 255, 1);
            cursor: hand;
        }
        #mapid { min-height: 500px; }

        @media (min-width: 768px) {
            .content-heading {
                margin-bottom: 25px;
                padding-top: 0;
            }
        }
        .marker_kemlu {
          background: #e93434;
        }
    </style>
@endsection

@section('content')
    <div class="main-menu">
        <div class="row d-flex justify-content-center maps-top">
        <front-maps-filter darkmodefront="{{ $dark_mode_front == 'true' ? 1 : 0 }}"></front-maps-filter>
        <div id="map"
        style="width: 2000px; height: 900px; border: 0px;">

                    <!-- <front-maps darkmodefront=""></front-maps> -->

        </div>
    </div>
        <div class="row d-flex justify-content-center mt-4 chart-bottom">
           {{-- <div class="bottom-menu back-maps">
                 <button class="bottom-button" id="chart-open">
                    <i class="bottom-menu-icon p-4">
                        <i class="fa fa-arrow-up"></i>
                    </i>
                </button>
            </div> --}}
             <div class="banner-kota col-12 px-0">
                <div id="page-container" class="enable-page-overlay side-scroll page-header-modern main-content-boxed">
                    <main id="main-container" class="mt-0">
                        <front-maps-filter></front-maps-filter>
                        <front-maps-chart darkmodefront="{{ $dark_mode_front == 'true' ? 1 : 0 }}" class="enable-page-overlay side-scroll page-header-modern main-content-boxed"></front-maps-chart>
                    </main>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
{{-- <script src="{{ asset('assets-admin/js/codebase.core.min.js') }}"></script> --}}
{{-- <script src="{{ asset('assets-admin/js/codebase.app.min.js') }}"></script> --}}


    <!-- Page JS Plugins -->
    <script src="{{ asset('assets-admin/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/chartjs/Chart.bundle.min.js') }}"></script>

    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.stack.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.resize.min.js') }}"></script>
    {{-- <link rel="stylesheet" href="https://d19vzq90twjlae.cloudfront.net/leaflet-0.7.3/leaflet.css" /> --}}
      {{-- <link rel="stylesheet" href="MarkerCluster.css" /> --}}
        {{-- <script src="https://d19vzq90twjlae.cloudfront.net/leaflet-0.7.3/leaflet.js"></script> --}}
          {{-- <script src="leaflet.markercluster.js"></script> --}}
            {{-- <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
        // $(document).ready(function () {
        //     $('.vue-map .show-marker-info').hover(function () {
        //         alert('hello');
        //     });
        // });
        $(document).ready(function(){
           // $(".vue-map").mouseover(function(tooltip) {
           //     var marker = $(this).find(".marker-image");
           //     var markerA = $(this).find(".card-info");
           //     var tooltip = $(this).find(".show-marker-info").find(".card-info");
           //     var tooltipA = $(this).find(".show-marker-info").find(".arrow-info");

           //     var tipX = marker.outerWidth() + 0;
           //       var tipY = marker.outerHeight() - 200;

           //       var tipAX = markerA.outerWidth() + 0;
           //         var tipAY = markerA.outerHeight() - 0;

           //       tooltip.css({ top: tipY, left: tipX });

           //       tooltipA.css({ top: tipAY, left: tipAX });

           //       var tooltip_rect = tooltip[0].getBoundingClientRect();
           //       // Corrections if out of window
           //       if ((tooltip_rect.x + tooltip_rect.width) > $(window).width())
           //         tipX = -tooltip_rect.width - 0;
           //       if ((tooltip_rect.y + tooltip_rect.height) > $(window).height())
           //         tipY = -tooltip_rect.height + 100;

           //       var tooltip_rectA = tooltipA[0].getBoundingClientRect();

           //       if (tooltip_rectA.y < 0)
           //         tipAY = tipAY - 200;

           //       tooltip.css({ top: tipY, left: tipX });

           //       tooltipA.css({ top: tipAY, left: tipAX });
           // });
           $(".vue-map").mouseover(function(tooltip) {
               var marker = $(this).find(".marker-image");
               var markerA = $(this).find(".card-info-list");
               var markerD = $(this).find(".card-info-detail");
               var tooltip = $(this).find(".show-marker-info").find(".card-info");
               var tooltipA = $(this).find(".show-marker-info").find(".arrow-info-list");
               var tooltipD = $(this).find(".show-marker-info").find(".arrow-info-detail");

               var tipX = marker.outerWidth() + 0;             // 5px on the right of the ktooltip
                 var tipY = marker.outerHeight() - 104;                                  // 40px on the top of the

                 var tipAX = markerA.outerWidth() - 0;             // 5px on the right of the ktooltip
                   var tipAY = markerA.outerHeight() - 0;

                   var tipDX = markerD.outerWidth() + 0;             // 5px on the right of the ktooltip
                     var tipDY = markerD.outerHeight() - 0;

                 tooltip.css({ top: tipY, left: tipX });          // Position tooltip

                 tooltipA.css({ top: tipAY, left: tipAX });          // Position tooltip

                 tooltipD.css({ top: tipDY, left: tipDX });          // Position tooltip


                 // Get calculated tooltip coordinates and size
                 var tooltip_rect = tooltip[0].getBoundingClientRect();
                 // Corrections if out of window
                 if ((tooltip_rect.x + tooltip_rect.width) > $(window).width()) // Out on the right
                   tipX = -tooltip_rect.width - 0; // Simulate a "right: tipX" position
                 if ((tooltip_rect.y + tooltip_rect.height) > $(window).height()) // Out on the right
                   tipY = -tooltip_rect.height + 100; // Simulate a "right: tipX" position

                 // Apply corrected position
                 tooltip.css({ top: tipY, left: tipX });

                 // Apply corrected position
                 tooltipA.css({ top: tipAY, left: tipAX });
                 tooltipD.css({ top: tipDY, left: tipDX });


           });
           $(".vue-map").mouseover(function(tooltip) {
               var marker = $(this).find(".marker-image");
               var markerA = $(this).find(".card-info-list");
               var markerD = $(this).find(".card-info-detail");
               var tooltip = $(this).find(".show-marker-info:hover").find(".card-info");
               var tooltipA = $(this).find(".show-marker-info:hover").find(".arrow-info-list");
               var tooltipD = $(this).find(".show-marker-info:hover").find(".arrow-info-detail");

               var tipX = marker.outerWidth() + 0;             // 5px on the right of the ktooltip
                 var tipY = marker.outerHeight() - 104;                                  // 40px on the top of the
                 // var tipZ = marker.outerHeight() - 104;                                  // 40px on the top of the

                 var tipAX = markerA.outerWidth() - 406;             // 5px on the right of the ktooltip
                   var tipAY = markerA.outerHeight() - 425;

                   var tipDX = markerD.outerWidth() - 406;             // 5px on the right of the ktooltip
                     var tipDY = markerD.outerHeight() - 320;

                 tooltip.css({ top: tipY, left: tipX });          // Position tooltip

                 tooltipA.css({ top: tipAY, left: tipAX });          // Position tooltip

                 tooltipD.css({ top: tipDY, left: tipDX });          // Position tooltip


                 // Get calculated tooltip coordinates and size
                 var tooltip_rect = tooltip[0].getBoundingClientRect();
                 // Corrections if out of window
                 if ((tooltip_rect.x + tooltip_rect.width) > $(window).width()) // Out on the right
                   tipX = -tooltip_rect.width - 0; // Simulate a "right: tipX" position
                 if ((tooltip_rect.y + tooltip_rect.height) > $(window).height() + 150) // Out on the right
                   tipY = -tooltip_rect.height + 100; // Simulate a "right: tipX" position
                 if ((tooltip_rect.y + tooltip_rect.height) > $(window).height() + 400)
                 tipZ = tooltip_rect.height + 100;
                 if ((tooltip_rect.y + tooltip_rect.height) > $(window).height() + 250)
                   tipZ = -tooltip_rect.height + 100;
                 if (tooltip_rect.y < 0)
                   tipY = tipY - tooltip_rect.y;

                 var tooltip_rectA = tooltipA[0].getBoundingClientRect();

                 if ((tooltip_rectA.x + tooltip_rectA.width) > $(window).width())
                   tipAX = -tooltip_rectA.width + 438;
                 if ((tooltip_rectA.y + tooltip_rectA.height) > $(window).height())
                   tipAY = -tooltip_rectA.height + 117;


                 // Apply corrected position
                 tooltip.css({ top: tipY, left: tipX});

                 if (tipX < 0)
                  tipAX = tipAX + 376;
                 if (tipY < -113)
                   tipAY = tipAY + 320;
                 // if (tipZ < -113)
                 //   tipAY = tipAY + 320;

                 // Apply corrected position
                 tooltipA.css({ top: tipAY, left: tipAX });


                 if (tipX < 0)
                  tipDX = tipDX + 377;
                 if (tipY < -113)
                   tipDY = tipDY + 215;

                 // Apply corrected position
                 tooltipD.css({ top: tipDY, left: tipDX });
           });
        });
        $(document).ready(function(){
           $(".vue-map").mouseover(function() {
               $(this).find(".z-marker:hover").parent().addClass('z-marker-index')
           });
           $(".vue-map").mouseout(function() {
               $(this).find(".z-marker").parent().removeClass('z-marker-index')
           });
        });

    </script>
    <!-- Page JS Code -->
    {{-- <script src="{{ asset('assets-admin/js/pages/be_comp_charts.min.js') }}"></script> --}}
    <script>
        $(document).ready(function (e) {
            $('.btn-effect.effect-land').on('click', function () {
                $(this).toggleClass('active');
                $('.btn-effect.effect-building').removeClass('active');
                $('.btn-effect.effect-car').removeClass('active');
            });
            $('.btn-effect.effect-building').on('click', function () {
                $(this).toggleClass('active');
                $('.btn-effect.effect-land').removeClass('active');
                $('.btn-effect.effect-car').removeClass('active');
            });
            $('.btn-effect.effect-car').on('click', function () {
                $(this).toggleClass('active');
                $('.btn-effect.effect-land').removeClass('active');
                $('.btn-effect.effect-building').removeClass('active');
            });

            $('#chart-open').on('click', function () {
                $(this).toggleClass('active');
                $(".maps-top").toggleClass('active');
                $(".chart-bottom").toggleClass('active');
            });
        });
    </script>
<!-- Make sure you put this AFTER Leaflet's CSS -->
<script src="{{ asset('assets-admin/leaflet0.77/leaflet.js') }}"></script>
<link rel="stylesheet"  href="{{ asset('assets-admin/leaflet0.77/leaflet.css') }}">
<link href="https://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css"
        rel="stylesheet"
        type="text/css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/MarkerCluster.css"
        rel="stylesheet"
        type="text/css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/MarkerCluster.Default.css"
        rel="stylesheet"
        type="text/css">
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'
        type='text/javascript'></script>
  <script src='https://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js'
        type='text/javascript'></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/0.4.0/leaflet.markercluster.js'
        type='text/javascript'></script>

<script>
     var map = L.map( 'map', {
  center: [1.0, 1.0],
  minZoom: 3,
  closePopupOnClick: false,
  zoom: 5, zoomControl: false, attributionControl: false, dragging: true, tap:true,
         tapTolerance: 30,
         trackResize: true,
         worldCopyJump: false,
         continuousWorld: false
});
     map.fitWorld();
     map.fitWorld({
         animate: true
     });


    // L.marker([40.712, -74.227]).addTo(map);
    // L.marker([40.774, -74.125]).addTo(map);
    // L.marker([-6.174320631252695, 106.8339781761963]).addTo(map);
    // L.marker([8.9806034, 38.75776050000002]).addTo(map);
    // L.marker([36.753768, 3.0587560999999823]).addTo(map);
    // L.marker ([39.8848512, 32.85691120000001]).addTo(map);
    // L.marker([33.319356, 44.38635380000005]).addTo(map);
    // L.marker([13.7512607, 100.53656430000001]).addTo(map);
    // L.marker([44.78770219999999, 20.456334200000015]).addTo(map);
    // L.marker([50.1224546, 8.653083299999935]).addTo(map);
    // L.marker([46.9371738, 7.466225699999995]).addTo(map);
    // L.marker([13.7512607, 72.8093576]).addTo(map);
    // L.marker([18.9656426, 72.8093576]).addTo(map);
    // L.marker([52.5294319, 13.363261299999976]).addTo(map);
    // L.marker([50.8384919, 4.434669399999962]).addTo(map);
    // L.marker([44.4565141, 26.089190199999962]).addTo(map);
    // L.marker([47.5095366, 19.0763508]).addTo(map);
    // L.marker([-34.57940839999999, -58.400031799999965]).addTo(map);
    // L.marker([30.0349349, 31.23106729999995]).addTo(map);
    // L.marker([-35.30396700000001, 149.116596]).addTo(map);
    // L.marker([6.9019569, 26.089190199999962]).addTo(map);
    // L.marker([33.496819, -6.8072247]).addTo(map);
    // L.marker([-6.8072247, 39.28615869999999]).addTo(map);
    // L.marker([14.5580728, 121.0151912]).addTo(map);
    // L.marker([52.08614, 4.2887210000000096]).addTo(map);
    // L.marker([21.5568642, 39.2271978]).addTo(map);
    // L.marker([46.2040552, 6.126648100000011]).addTo(map);
    // L.marker([53.5973291, 26.089190199999962]).addTo(map);


//     var myIcon = L.icon({
//     iconUrl: myURL + 'images/pin24.png',
//     iconRetinaUrl: myURL + 'images/pin.png',
//     iconSize: [29, 24],
//     // iconAnchor: [9, 21],
//     // popupAnchor: [0, -14]
// });
// var markerClusters = L.markerClusterGroup();
// for ( var i = 0; i < marker.length; ++i )
// {
//   var m = L.marker( [markers[i].lat, markers[i].lng], {icon: myIcon} )
//                   .bindPopup( popup );

//   markerClusters.addLayer( m );
// }

// map.addLayer( markerClusters );

//./assets-admin/newtask/{z}/{x}/{y}.png
//https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}
L.tileLayer('./assets-admin/mapbox/{z}/{x}/{y}.png', {
  attribution: '',
 ext: 'png',
    tms: false,
    continuousWorld: false,
    noWrap:true,
}).addTo(map);

    var markers = [
  {
    "name":"Kedutaan Besar RI di Addis Ababa",
    "city":"Addis Ababa, Etiopia",
    "knd":"2",
    "tnh":"17",
    "gd":"12",
    "lat":8.9806034,
    "lng":38.75776050000002,
    "alt":20,
    "tz":"Addis Ababa, Etiopia"
  },{
    "name":"Kedutaan Besar RI di Alger",
    "city":"Alger, Aljazair",
    "knd":"1",
    "tnh":"3",
    "gd":"11",
    "lat":36.753768,
    "lng":3.0587560999999823,
    "tz":"Alger, Aljazair"
  },{
    "name":"Kedutaan Besar RI di Ankara",
    "city":"Ankara, Turki",
    "knd":"2",
    "tnh":"4",
    "gd":"7",
    "lat":39.8848512,
    "lng":32.85691120000001,
    "tz":"Ankara, Turki"
  },{
    "name":"Kedutaan Besar RI di Baghdad",
    "city":"Baghdad, Irak",
    "knd":"3",
    "tnh":"0",
    "gd":"0",
    "lat":33.319356,
    "lng":44.38635380000005,
    "tz":"Baghdad, Irak"
  },{
    "name":"Kedutaan Besar RI di Bangkok",
    "city":"Bangkok, China",
    "knd":"20",
    "tnh":"1",
    "gd":"20",
    "lat":13.7512607,
    "lng":100.53656430000001,
    "tz":"Bangkok, China"
  },{
    "name":"KBRI Beograd",
    "city":"Beograd, Serbia",
    "knd":"8",
    "tnh":"0",
    "gd":"5",
    "lat":44.78770219999999,
    "lng":20.456334200000015,
    "tz":"America/Godthab"
  },{
    "name":"Konsulat Jenderal RI di Frankfurt",
    "city":"Frankfurt, Jerman",
    "knd":"6",
    "tnh":"0",
    "gd":"0",
    "lat":50.1224546,
    "lng":8.653083299999935,
    "tz":"Frankfurt, Jerman"
  },{
    "name":"Kedutaan Besar RI di Bern",
    "city":"Bern, Belanda",
    "knd":"",
    "tnh":"",
    "gd":"",
    "lat":46.9371738,
    "lng":7.466225699999995,
    "tz":"Bern, Belanda"
  },{
    "name":"Kedutaan Besar RI di Berlin",
    "city":"Berlin, Jerman",
    "knd":"7",
    "tnh":"2",
    "gd":"2",
    "lat":52.5294319,
    "lng":13.363261299999976,
    "tz":"Berlin, Jerman"
  },{
    "name":"Kedutaan Besar RI di Brussels",
    "city":"Brussels, Jerman",
    "knd":"10",
    "tnh":"3",
    "gd":"11",
    "lat":50.8384919,
    "lng":4.434669399999962,
    "tz":"Brussels, Jerman"
  },{
    "name":"Kedutaan Besar RI di Bucharest",
    "city":"Bucure?ti, Rumania",
    "knd":"6",
    "tnh":"0",
    "gd":"0",
    "lat":44.4565141,
    "lng":26.089190199999962,
    "tz":"Bucure?ti, Rumania"
  },{
    "name":"Kedutaan Besar RI di Budapest",
    "city":"Budapest, Városligeti fasor 26, 1068 Hungaria",
    "knd":"10",
    "tnh":"0",
    "gd":"1",
    "lat":47.5095366,
    "lng":19.0763508,
    "tz":"Budapest, Városligeti fasor 26, 1068 Hungaria"
  },{
    "name":"Kedutaan Besar RI di Antananarivo",
    "city":"Tanarive, Afrika",
    "knd":"8",
    "tnh":"1",
    "gd":"10",
    "lat":-18.8796762,
    "lng":47.522258299999976,
    "tz":"Tanarive, Afrika"
  }
  ,{
    "name":"Kedutaan Besar RI di Santiago",
    "city":"Santiago",
    "knd":"7",
    "tnh":"0",
    "gd":"0",
    "lat":-33.4216872,
    "lng":-70.61278440000001,
    "tz":"Santiago"
  }
  ,{
    "name":"Kedutaan Besar RI di Brasillia",
    "city":"Brasillia, Brazil",
    "knd":"7",
    "tnh":"0",
    "gd":"13",
    "lat":-15.8163986,
    "lng":-47.88519550000001,
    "tz":"Brasillia, Brazil"
  }
  ,{
    "name":"Kedutaan Besar RI di Bandar Seri Bengawan",
    "city":"Bandar Seri Bengawan",
    "knd":"17",
    "tnh":"3",
    "gd":"10",
    "lat":4.921751999999999,
    "lng":114.96094019999998,
    "tz":"Bandar Seri Bengawan"
  }
  ,{
    "name":"Kedutaan Besar RI di Buenos Aires",
    "city":"Buonos Aires, Brazil",
    "knd":"6",
    "tnh":"2",
    "gd":"2",
    "lat":-34.57940839999999,
    "lng":-58.400031799999965,
    "tz":"Buonos Aires, Brazil"
  },{
    "name":"Kedutaan Besar RI di Kairo",
    "city":"Kairo, Mesir",
    "knd":"12",
    "tnh":"1",
    "gd":"10",
    "lat":30.0349349,
    "lng":31.23106729999995,
    "tz":"Kairo, Mesir"
  },{
    "name":"Kedutaan Besar RI di Canberra",
    "city":"Yarralumla ACT 2600, Australia",
    "knd":"10",
    "tnh":"2",
    "gd":"5",
    "lat":-35.30396700000001,
    "lng":149.116596,
    "tz":"Yarralumla ACT 2600, Australia"
  },{
    "name":"KBRI Colombo",
    "city":"Kolombo, Sri Lanka",
    "knd":"8",
    "tnh":"3",
    "gd":"20",
    "lat":6.9019569,
    "lng":79.87501840000004,
    "tz":"Kolombo, Sri Lanka"
  },{
    "name":"Kedutaan Besar RI di Dhaka",
    "city":"Dhaka 1212, Bangladesh",
    "knd":"12",
    "tnh":"1",
    "gd":"3",
    "tanah":"1",
    "bangunan":"3",
    "mobilitas":"12",
    "lat":23.795737,
    "lng":90.41273899999999,
    "tz":"Dhaka 1212, Bangladesh"
  },{
    "name":"Kedutaan Besar RI di Damascus",
    "city":"Damaskus, Suriah",
    "knd":"8",
    "tnh":"1",
    "gd":"12",
    "lat":33.496819,
    "lng":36.250725399999965,
    "tz":"Damaskus, Suriah"
  },{
    "name":"Kedutaan Besar RI di Dar Es Salaam",
    "city":" Ali Hassan Mwinyi Rd, Dar es Salaam 0572, Tanzania",
    "knd":"8",
    "tnh":"1",
    "gd":"12",
    "lat":-6.8072247,
    "lng":39.28615869999999,
    "tz":" Ali Hassan Mwinyi Rd, Dar es Salaam 0572, Tanzania"
  },{
    "name":"Konsulat Jenderal RI di Davao City",
    "city":" 31 6th Ave, Talomo, Davao City, 8000 Davao del Sur, Filipina",
    "knd":"9",
    "tnh":"2",
    "gd":"6",
    "lat":14.5580728,
    "lng":121.0151912,
    "tz":" 31 6th Ave, Talomo, Davao City, 8000 Davao del Sur, Filipina"
  },{
    "name":"Kedutaan Besar RI di Den Haag",
    "city":" Den Haag, Belanda",
    "knd":"17",
    "tnh":"7",
    "gd":"14",
    "lat":52.08614,
    "lng":4.2887210000000096,
    "tz":" Den Haag, Belanda"
  },{
    "name":"Konsulat Jenderal RI Jeddah",
    "city":" Jeddah Arab Saudi",
    "knd":"20",
    "tnh":"2",
    "gd":"15",
    "lat":21.5568642,
    "lng":39.2271978,
    "tz":" Jeddah Arab Saudi"
  },{
    "name":"Perutusan Tetap RI pada PBB dan OI lainnya di Jenewa",
    "city":" Jenewa, Republik Federasi Swiss.",
    "knd":"17",
    "tnh":"4",
    "gd":"4",
    "lat":46.2040552,
    "lng":6.126648100000011,
    "tz":"Jenewa, Republik Federasi Swiss."
  },{
    "name":"Konsulat Jenderal RI di Hamburg",
    "city":"Hamburg, Republik Federal Jerman.",
    "knd":"8",
    "tnh":"2",
    "gd":"8",
    "lat":53.5973291,
    "lng":9.99385570000004,
    "tz":"Hamburg, Republik Federal Jerman."
  },{
    "name":"Kedutaan Besar RI di Hanoi",
    "city":"Hanoi, Filipina",
    "knd":"",
    "tnh":"",
    "gd":"",
    "lat":21.0210017,
    "lng":105.85350849999998,
    "tz":"Hanoi, Filipina"
  },{
    "name":"Konsulat Jenderal RI di Hong Kong",
    "city":"Hongkong",
    "knd":"13",
    "tnh":"0",
    "gd":"0",
    "lat":22.278354,
    "lng":114.18642899999998,
    "tz":"Hongkong"
  },{
    "name":"Kedutaan Besar RI di Islamabad",
    "city":"ISLAMABAD, Pakistan.",
    "knd":"17",
    "tnh":"1",
    "gd":"17",
    "lat":33.72243719999999,
    "lng":73.10379460000001,
    "tz":"ISLAMABAD, Pakistan."
  },{
    "name":"Kedutaan Besar RI di Kabul",
    "city":"KABUL, NEGARA REPUBLIK ISLAM AFGHANISTAN.",
    "knd":"15",
    "tnh":"0",
    "gd":"4",
    "lat":34.53080699999999,
    "lng":69.17900680000002,
    "tz":"KABUL, NEGARA REPUBLIK ISLAM AFGHANISTAN."
  },{
    "name":"Konsulat Jenderal RI di Kota Kinabalu",
    "city":"Konsulat Jenderal Republik Indonesia di KOTA KINABALU SABAH, MALAYSIA.",
    "knd":"12",
    "tnh":"2",
    "gd":"17",
    "lat":5.970191700000002,
    "lng":116.0708396,
    "tz":"Konsulat Jenderal Republik Indonesia di KOTA KINABALU SABAH, MALAYSIA."
  },{
    "name":"Konsulat Jenderal RI di Osaka",
    "city":"Osaka, Jepang",
    "knd":"6",
    "tnh":"1",
    "gd":"1",
    "lat":34.6880588,
    "lng":135.48494729999993,
    "tz":"Osaka, Jepang"
  },{
    "name":"Kedutaan Besar Republik Indonesia Kuala Lumpur",
    "city":"Kuala Lumpur",
    "knd":"30",
    "tnh":"5",
    "gd":"15",
    "lat":3.1466944,
    "lng":101.72173129999999,
    "tz":"Kuala Lumpur"
  },{
    "name":"Kedutaan Besar RI di Abuja",
    "city":"Abuja, Merangkap Republik Ghana, Republik Liberia",
    "knd":"15",
    "tnh":"3",
    "gd":"8",
    "lat":9.095041,
    "lng":7.495096999999987,
    "tz":"Abuja, Merangkap Republik Ghana, Republik Liberia"
  },{
    "name":"Kedutaan Besar RI di London",
    "city":"London, Inggris",
    "knd":"14",
    "tnh":"4",
    "gd":"11",
    "lat":51.49695209999999,
    "lng":-0.13095029999999497,
    "tz":"London, Inggris"
  },{
    "name":"KBRI Mexico",
    "city":"Mexico",
    "knd":"9",
    "tnh":"2",
    "gd":"2",
    "lat":19.4279345,
    "lng":-99.19725979999998,
    "tz":"Mexico"
  },{
    "name":"Kedutaan Besar RI di Moskow",
    "city":"Rusia",
    "knd":"11",
    "tnh":"0",
    "gd":"0",
    "lat":55.7380238,
    "lng":37.63015870000004,
    "tz":"Rusia"
  },{
    "name":"Kedutaan Besar RI di New Delhi",
    "city":"New Delhi, India",
    "knd":"14",
    "tnh":"0",
    "gd":"11",
    "lat":28.602185,
    "lng":77.19454819999999,
    "tz":"New Delhi, India"
  },{
    "name":"Konsulat Jenderal RI di New York",
    "city":"New York, Amerika Serikat",
    "knd":"8",
    "tnh":"2",
    "gd":"2",
    "lat":40.76971959999999,
    "lng":-73.96791759999996,
    "tz":"New York, Amerika Serikat"
  },{
    "name":"Konsulat Jenderal RI di Noumea",
    "city":"NOUMEA, NEW CALEDONIA ",
    "knd":"8",
    "tnh":"2",
    "gd":"4",
    "tanah":"2",
    "bangunan":"4",
    "monilitas":"8",
    "lat":-22.2852996,
    "lng":166.44560479999996,
    "tz":"NOUMEA, NEW CALEDONIA "
  },{
      "name":"Kedutaan Besar RI di Ottawa",
      "city":"Otawa, Kanada",
      "knd":"11",
    "tnh":"2",
    "gd":"3",
      "lat":45.410279,
      "lng":-75.73424899999998,
      "tz":"Otawa, Kanada"
    },{
        "name":"Kedutaan Besar RI di Paramaribo",
        "city":"REPUBLIK KOOPERATIF GUYANA DAN CARICOM Republik Suriname",
        "knd":"7",
    "tnh":"2",
    "gd":"2",
        "lat":45.410279,
        "lng":-75.73424899999998,
        "tz":"REPUBLIK KOOPERATIF GUYANA DAN CARICOM Republik Suriname"
      },{
        "name":"Perutusan Tetap RI pada PBB dan OI lainnya di New York",
        "city":"New York, Amerika Serikat",
        "knd":"19",
    "tnh":"4",
    "gd":"4",
        "lat":40.7469315,
        "lng":-73.97249299999999,
        "tz":"New York, Amerika Serikat"
      },{
        "name":"Kedutaan Besar RI di Praha",
        "city":"Praha, Yunani",
        "knd":"11",
    "tnh":"2",
    "gd":"4",
        "lat":50.070991,
        "lng":14.372992000000067,
        "tz":"Praha, Yunani"
      },{
        "name":"Kedutaan Besar RI di Pyong Yang",
        "city":"Korea Utara",
        "knd":"5",
    "tnh":"0",
    "gd":"0",
        "lat":39.0224524,
        "lng":125.79390269999999,
        "tz":"Korea Utara"
      },{
        "name":"Kedutaan Besar RI di Yangon",
        "city":"Myanmar",
        "knd":"27",
    "tnh":"3",
    "gd":"14",
        "lat":16.7897298,
        "lng":96.14200649999998,
        "tz":"Myanmar"
      },{
        "name":"Kedutaan Besar RI di Roma",
        "city":"Italy",
        "knd":"9",
    "tnh":"3",
    "gd":"3",
        "lat":41.9104905,
        "lng":12.493474800000058,
        "tz":"Italy"
      },{
        "name":"Kedutaan Besar RI di Sofia",
        "city":"Bulgaria",
        "knd":"8",
    "tnh":"0",
    "gd":"0",
        "lat":42.6496097,
        "lng":23.336676699999998,
        "tz":"Bulgaria"
      },{
        "name":"Kedutaan Besar RI di Stockholm",
        "city":"Swedia",
        "knd":"9",
    "tnh":"1",
    "gd":"1",
        "lat":59.33209229999999,
        "lng":18.04936859999998,
        "tz":"Swedia"
      },{
        "name":"Kedutaan Besar RI di Tripoli",
        "city":"Libya",
        "knd":"7",
    "tnh":"0",
    "gd":"0",
        "lat":32.8872094,
        "lng":13.191338299999984,
        "tz":"Libya"
      },{
        "name":"Kedutaan Besar RI di Tunis",
        "city":"Tunisia",
        "knd":"12",
    "tnh":"2",
    "gd":"3",
        "lat":36.8354493,
        "lng":10.230932199999984,
        "tz":"Tunisia"
      },{
        "name":"Kedutaan Besar RI di Vientiane",
        "city":"Thailand",
        "knd":"13",
    "tnh":"3",
    "gd":"9",
        "lat":17.9733244,
        "lng":102.62233079999999,
        "tz":"Thailand"
      },{
        "name":"Kedutaan Besar RI di Warsawa",
        "city":"Polandia",
        "knd":"9",
    "tnh":"0",
    "gd":"0",
        "lat":52.23634699999999,
        "lng":21.048448000000008,
        "tz":"Polandia"
      },{
        "name":"Kedutaan Besar RI di Washington DC",
        "city":"AS",
        "knd":"22",
    "tnh":"3",
    "gd":"7",
        "lat":38.91027439999999,
        "lng":-77.04627690000001,
        "tz":"AS"
      },{
        "name":"Kedutaan Besar RI di Wina",
        "city":"Austria",
        "knd":"12",
    "tnh":"2",
    "gd":"2",
        "lat":48.2323374,
        "lng":16.33929749999993,
        "tz":"Austria"
      },{
        "name":"Kedutaan Besar RI di Windhoek",
        "city":"Namibia",
        "knd":"2",
    "tnh":"7",
    "gd":"6",
        "lat":-22.5595726,
        "lng":17.096294599999965,
        "tz":"Namibia"
      },{
        "name":"Kedutaan Besar RI di Zagreb",
        "city":"Croatia",
        "knd":"5",
    "tnh":"0",
    "gd":"0",
        "lat":45.82488670000001,
        "lng":15.980649099999937,
        "tz":"Croatia"
      },{
        "name":"Kedutaan Besar RI Seoul",
        "city":"Korea",
        "knd":"13",
    "tnh":"1",
    "gd":"5",
        "lat":37.5185014,
        "lng":126.93158440000002,
        "tz":"Korea"
      },{
        "name":"Konsulat Jenderal RI di Capetown",
        "city":"?South Africa",
        "knd":"6",
    "tnh":"3",
    "gd":"5",
        "lat":37.5185014,
        "lng":18.47801000000004,
        "tz":"?South Africa"
      },{
        "name":"Konsulat Jenderal RI di Chicago",
        "city":"U.S.",
        "knd":"8",
    "tnh":"1",
    "gd":"1",
        "lat":41.886629,
        "lng":-87.63450799999998,
        "tz":"U.S."
      },{
        "name":"Konsulat Jenderal RI di Davao City",
        "city":"Filipina",
        "knd":"9",
    "tnh":"2",
    "gd":"6",
        "lat":14.5580728,
        "lng":121.0151912,
        "tz":"Filipina"
      },{
        "name":"Konsulat Jenderal RI di Dubai",
        "city":"United Arab Emirates",
        "knd":"10",
    "tnh":"0",
    "gd":"0",
        "lat":25.248032,
        "lng":55.28072259999999,
        "tz":"United Arab Emirates"
      },{
        "name":"Konsulat Jenderal RI di Guangzhou",
        "city":"Tiongkok",
        "knd":"6",
    "tnh":"0",
    "gd":"0",
        "lat":23.12911,
        "lng":113.26438499999995,
        "tz":"Tiongkok"
      },{
        "name":"Konsulat Jenderal RI di Hamburg",
        "city":"Germany",
        "knd":"8",
    "tnh":"2",
    "gd":"8",
        "lat":53.5973291,
        "lng":9.99385570000004,
        "tz":"Germany"
      },{
        "name":"Konsulat Jenderal RI di Ho Chi Minh City",
        "city":"Vietnam",
        "knd":"5",
    "tnh":"0",
    "gd":"0",
        "lat":10.7852926,
        "lng":106.69728090000001,
        "tz":"Vietnam"
      },{
        "name":"Konsulat Jenderal RI di Hong Kong",
        "city":"Hong Kong",
        "knd":"9",
    "tnh":"0",
    "gd":"2",
        "lat":22.278354,
        "lng":114.18642899999998,
        "tz":"Hong Kong"
      },{
        "name":"Konsulat Jenderal RI di Houston",
        "city":"Harris?, ?Fort Bend?, ?Montgomery",
        "knd":"10",
    "tnh":"2",
    "gd":"4",
        "lat":29.7284738,
        "lng":-95.56863620000001,
        "tz":"Harris?, ?Fort Bend?, ?Montgomery"
      },{
        "name":"Konsulat Jenderal RI di Istanbul",
        "city":"Turkey",
        "knd":"4",
    "tnh":"0",
    "gd":"0",
        "lat":41.0684788,
        "lng":29.007785600000034,
        "tz":"Turkey"
      },{
        "name":"Konsulat Jenderal RI di Johor Bahru",
        "city":"Malaysia",
        "knd":"11",
    "tnh":"0",
    "gd":"0",
        "lat":1.4699546,
        "lng":103.75588570000002,
        "tz":"Malaysia"
      },{
        "name":"Konsulat Jenderal RI di Karachi",
        "city":"Pakistan",
        "knd":"8",
    "tnh":"0",
    "gd":"4",
        "lat":24.82035609999999,
        "lng":67.03007630000002,
        "tz":"Pakistan"
      },{
        "name":"Konsulat Jenderal RI di Kota Kinabalu",
        "city":"Malaysia",
        "knd":"17",
    "tnh":"2",
    "gd":"12",
        "lat":5.970191700000002,
        "lng":116.0708396,
        "tz":"Malaysia"
      },{
        "name":"Konsulat Jenderal RI di Los Angeles",
        "city":"AS",
        "knd":"14",
    "tnh":"3",
    "gd":"5",
        "lat":34.0522342,
        "lng":-118.2436849,
        "tz":"AS"
      },{
        "name":"Konsulat Jenderal RI di Marseille",
        "city":"France",
        "knd":"5",
    "tnh":"2",
    "gd":"2",
        "lat":43.2685261,
        "lng":5.3865677000000005,
        "tz":"France"
      },{
        "name":"Konsulat Jenderal RI di Melbourne",
        "city":"Australia",
        "knd":"5",
    "tnh":"2",
    "gd":"2",
        "lat":-37.851694,
        "lng":144.979424,
        "tz":"Australia"
      },{
        "name":"Konsulat Jenderal RI di Mumbai",
        "city":"India",
        "knd":"7",
    "tnh":"1",
    "gd":"5",
        "lat":18.9656426,
        "lng":72.8093576,
        "tz":"India"
      },{
        "name":"Konsulat Jenderal RI di New York",
        "city":"AS",
        "knd":"8",
    "tnh":"2",
    "gd":"2",
        "lat":40.76971959999999,
        "lng":-73.96791759999996,
        "tz":"AS"
      },{
        "name":"Konsulat Jenderal RI di Penang",
        "city":"Malaysia",
        "knd":"9",
    "tnh":"2",
    "gd":"3",
        "lat":5.432615,
        "lng":100.30451619999997,
        "tz":"Malaysia"
      },{
        "name":"Konsulat Jenderal RI di Perth",
        "city":"Australia",
        "knd":"5",
    "tnh":"2",
    "gd":"4",
        "lat":-31.9599344,
        "lng":115.87285900000006,
        "tz":"Australia"
      },{
        "name":"Konsulat Jenderal RI di San Fransisco",
        "city":"AS",
        "knd":"11",
    "tnh":"2",
    "gd":"2",
        "lat":37.80452160000001,
        "lng":-122.41656510000001,
        "tz":"AS"
      },{
        "name":"Konsulat Jenderal RI di Shanghai",
        "city":"Tiongkok",
        "knd":"5",
    "tnh":"0",
    "gd":"0",
        "lat":31.230416,
        "lng":121.473701,
        "tz":"Tiongkok"
      },{
        "name":"Konsulat Jenderal RI di Sydney",
        "city":"Australia",
        "knd":"7",
    "tnh":"2",
    "gd":"2",
        "lat":-33.94206739999999,
        "lng":151.2446698,
        "tz":"Australia"
      },{
        "name":"Konsulat Jenderal RI di Toronto",
        "city":"Canada",
        "knd":"5",
    "tnh":"2",
    "gd":"2",
        "lat":43.653144,
        "lng":-79.37262900000002,
        "tz":"Canada"
      },{
        "name":"Konsulat Jenderal RI di Vancouver",
        "city":"Canada",
        "knd":"7",
    "tnh":"2",
    "gd":"2",
        "lat":49.290268,
        "lng":-123.13204200000001,
        "tz":"Canada"
      },{
        "name":"Konsulat Jenderal RI Kuching",
        "city":"Malaysia",
        "knd":"6",
    "tnh":"0",
    "gd":"1",
        "lat":1.5071398,
        "lng":110.37041049999993,
        "tz":"Malaysia"
      },{
        "name":"Konsulat RI di Darwin",
        "city":"Australia",
        "knd":"7",
    "tnh":"3",
    "gd":"3",
        "lat":-12.46407,
        "lng":130.84596999999997,
        "tz":"Australia"
      },{
        "name":"Konsulat RI di Songkhla",
        "city":"Thailand",
        "knd":"8",
    "tnh":"0",
    "gd":"0",
        "lat":7.207608,
        "lng":100.591947,
        "tz":"Thailand"
      },{
        "name":"Konsulat RI di Tawau",
        "city":"Malaysia",
        "knd":"15",
    "tnh":"0",
    "gd":"0",
        "lat":4.2620634,
        "lng":117.90341969999997,
        "tz":"Malaysia"
      },{
        "name":"Konsulat RI di Vanimo",
        "city":"Papua New Guinea",
        "knd":"5",
    "tnh":"3",
    "gd":"0",
        "lat":-2.689676699999999,
        "lng":141.2998457,
        "tz":"Papua New Guinea"
      },{
        "name":"Perutusan Tetap RI pada PBB dan OI lainnya di New York",
        "city":"AS",
        "knd":"19",
    "tnh":"4",
    "gd":"4",
        "lat":40.7469315,
        "lng":-73.97249299999999,
        "tz":"AS"
      },{
        "name":"Perwakilan RI di Phnom Penh",
        "city":"Cambodia",
        "knd":"9",
    "tnh":"0",
    "gd":"0",
        "lat":11.5388759,
        "lng":104.9276337,
        "tz":"Cambodia"
      },{
        "name":"PTRI ASEAN",
        "city":"Indonesia",
        "knd":"5",
    "tnh":"0",
    "gd":"0",
        "lat":-6.229532999999999,
        "lng":126.93158440000002,
        "tz":"Indonesia"
      },{
        "name":"PUSAT PENDIDIKAN DAN PELATIHAN KEMENLU",
        "city":"Indonesia",
        "knd":"13",
    "tnh":"0",
    "gd":"0",
        "lat":-6.229568078256203,
        "lng":106.79839212883599,
        "tz":"Indonesia"
      },{
        "name":"PUSTIK KP",
        "city":"Indonesia",
        "knd":"21",
    "tnh":"0",
    "gd":"2",
        "lat":-6.1743131,
        "lng":106.83325620000005,
        "tz":"Indonesia"
      },{
        "name":"SEKRETARIAT JENDERAL KEMENTERIAN LUAR NEGERI",
        "city":"Indonesia",
        "knd":"224",
    "tnh":"18",
    "gd":"903",
        "lat":-6.174320631252695,
        "lng":106.8339781761963,
        "tz":"Indonesia"
      }
];
var markerClusters = L.markerClusterGroup();

for ( var i = 0; i < markers.length; ++i )
{
    var customOptions = {
  'maxWidth': '500',
  'className': 'custom',
  closeButton: false,
  autoClose: false
}
var DetriIcon = L.icon({
  iconUrl: './assets/images/maps-marker.ico',
  iconSize: [38, 38], // size of the icon
  popupAnchor: [0, -15]
});
    var popup = markers[i].name +
              '</br> Kantor </center>' + markers[i].tz +
              '</br> <img src="{{ asset('./assets/images/kendaraan-maps.png')}}" alt="" class="img-fluid w-5"> ' + markers[i].knd +
              '</br> <img src="{{ asset('./assets/images/tanah-maps.png')}}" alt="" class="img-fluid w-5"> ' + markers[i].tnh +
              '</br> <img src="{{ asset('./assets/images/gedung-maps.png')}}" alt="" class="img-fluid w-5"> ' + markers[i].gd;


var m = L.marker( [markers[i].lat, markers[i].lng] , {
  icon: DetriIcon
} ).bindPopup( popup, customOptions );
m.on('mouseover', m.openPopup.bind(m));
m.on('mouseout', m.closePopup.bind(m));

  markerClusters.addLayer( m );
}

map.addLayer( markerClusters );
map.setMaxBounds(map.getBounds());


    // markers.addLayer(lokasi);
		// map.addLayer(markers);
		// map.fitBounds(markers.getBounds());



    // map.fitBounds(ck(function(){ map.fitBounds(bounds, {padding: [50, 50]}); });
    // $('#fit2').click(function(){ map.fitBounds(bounds, {padding: [50, 50]}); });
    // $('#fit2').click(function(){ map.fitBounds(bounds, {padding: [50, 50]}); });

    // L.marker([38.9188702,-77.0708398]).addTo(map);




    // $.getJSON("neigborhoods.geojson,function(hoodData)"){
    // L.geoJson(hoodData, {
    // style:function (feature)
    // var fillColor,
    // density = feature.properties.density;
    // if( density > 80) fillColor = "#006837"
    // elseif (density > 40) fillColor = "#31a354"
    // elseif ( density > 20 ) fillColor = "#78c679";
    // elseif ( density > 10 ) fillColor = "#c2e699";
    // elseif ( density > 0 ) fillColor = "#ffffcc";
    // else fillColor = "#f7f7f7";  // no data
    // return { color: "#999", weight: 1, fillColor: fillColor, fillOpacity: .6 };
    //   },
    //   onEachFeature: function(feature, layer){
    //       var ratIcon = L.Icon({
    //           iconUrl:'http://https://andywoodruff.com/maptime-leaflet/rat.png',
    //           iconSize:[60,50]
    //       });
    //       var rodents = L.geoJson(data,{
    //     pointToLayer: function(feature,latlng){
    //     var marker = L.marker(latlng,{icon: ratIcon});
    //     marker.bindPopup(feature.properties.Location + '<br/>' + feature.properties.OPEN_DT);
    //     return marker;
    //       }
    //     });
    //     var clusters = L.markerClusterGroup();
    // clusters.addLayer(rodents);
    // map.addLayer(clusters);
    //     });
    // }

    // axios.get('{{ route('api.outlets.index') }}')
    // .then(function (response) {
    //     console.log(response.data);
    //     L.geoJSON(response.data, {
    //         pointToLayer: function(geoJsonPoint, latlng) {
    //             return L.marker(latlng);
    //         }
    //     })
    //     .bindPopup(function (layer) {
    //         return layer.feature.properties.map_popup_content;
    //     }).addTo(map);
    // })
    // .catch(function (error) {
    //     console.log(error);
    // });

    // @can('create', new App\Outlet)
    // var theMarker;

    // map.on('click', function(e) {
    //     let latitude = e.latlng.lat.toString().substring(0, 15);
    //     let longitude = e.latlng.lng.toString().substring(0, 15);

    //     if (theMarker != undefined) {
    //         map.removeLayer(theMarker);
    //     };

    //     var popupContent = "Your location : " + latitude + ", " + longitude + ".";
    //     popupContent += '<br><a href="{{ route('outlets.create') }}?latitude=' + latitude + '&longitude=' + longitude + '"></a>';

    //     theMarker = L.marker([latitude, longitude]).addTo(map);
    //     theMarker.bindPopup(popupContent)
    //     .openPopup();
    // });
    // @endcan



</script>



    <script src="{{ asset('assets/js/jquery.owl.carousel.js') }}"></script>
    <script>
        $(document).ready(function(){
          $('#owl-slider-filter').each(function(){
            $(this).owlCarousel({
              stagePadding: 50,
              loop:true,
              margin:10,
              nav:true,
              items: 1,
            });
          });

          $('#owl-slider-maps').each(function(){
            $(this).owlCarousel({
              stagePadding: 50,
              loop:true,
              margin:10,
              nav:true,
              items: 1,
            });
          });
        });
    </script>
    <!-- Page JS Helpers (Easy Pie Chart Plugin) -->
    {{-- <script>jQuery(function () {
            Codebase.helpers('easy-pie-chart');
        });</script> --}}

@endsection
