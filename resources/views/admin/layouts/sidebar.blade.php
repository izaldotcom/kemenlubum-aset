<nav id="sidebar">
    <div class="sidebar-content">
        <div class="content-side content-side-full content-side-user px-10 align-parent">
            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                <img class="img-avatar img-avatar32" src="{{ asset('assets-admin/media/avatars/avatar15.jpg') }}"
                     alt="">
            </div>

            <div class="sidebar-mini-hidden-b text-center">
                <a class="img-link" href="{{ route('admin_dashboard') }}">
                    <img class="img-avatar" src="{{ asset('assets-admin/media/avatars/avatar1.jpg') }}" alt="">
                </a>
                <ul class="list-inline mt-10">
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase"
                           href="{{ route('admin_dashboard') }}">{{ Auth::user()->name }}</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark" href="{{ url('/logout') }}">
                            <i class="si si-logout" style="color: red; font-weight: bold;"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="content-side content-side-full">
            <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout"
                    data-action="sidebar_close">
                <i class="fa fa-times text-danger"></i>
            </button>
            <ul class="nav-main">
                <li class="nav-main-heading"><span class="sidebar-mini-visible">MN</span><span
                        class="sidebar-mini-hidden">Menu</span></li>
                <li>
                    <a class="" href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i><span
                            class="sidebar-mini-hide">Dasbor</span></a>
                </li>
                {{-- <li>
                    <a class="" href="{{ url('/admin/notification') }}"><i class="fa fa-bell-o"></i><span
                                class="sidebar-mini-hide">Notification</span></a>
                </li> --}}
                @if (Auth::user()->hasRole(['super_admin', 'admin', 'admin_aset', 'admin_satker']))
                <li>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-target"></i><span
                            class="sidebar-mini-hide">Pengajuan Penghapusan Kendaraan</span></a>
                    <ul>
                        <li>
                            <a href="#" onclick="fireCreatePengajuanPenghapusan()">Create Pengajuan
                                Penghapusan</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_pengajuan_penghapusan_kendaraan_list') }}">List Pengajuan
                                Penghapusan</a>
                        </li>
                    </ul>
                </li>
                @endif
                {{--                <li>--}}
                {{--                    <a class="" href="{{ url('/notification') }}"><i class="fa fa-bell"></i><span--}}
                {{--                                class="sidebar-mini-hide">Notification</span></a>--}}
                {{--                </li>--}}
                <li class="nav-main-heading"><span class="sidebar-mini-visible">AS</span><span
                        class="sidebar-mini-hidden">Daftar Aset</span></li>
                @if (Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan', 'admin_aset', 'admin_kendaraan', 'staf_kendaraan', 'admin_satker']))
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-target"></i><span
                                class="sidebar-mini-hide">Kendaraan</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_kendaraan_list') }}">Daftar Kendaraan</a>
                            </li>
                            @if (Auth::user()->hasRole(['super_admin', 'admin', 'admin_aset', 'admin_kendaraan', 'admin_satker']))
                            <li>
                                <a href="{{ route('admin_kendaraan_input_excel') }}">Input Excel</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if (Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan', 'admin_pajak_asuransi','admin_kendaraan','staf_kendaraan']))
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-target"></i><span
                                class="sidebar-mini-hide">Pajak & Asuransi</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_kendaraan_list_dinas') }}">Daftar Pajak & Asuransi</a>
                            </li>
                            @if (Auth::user()->hasRole(['super_admin','admin']))
                                <li>
                                    <a href="{{ route('admin_kendaraan_input_excel_dinas') }}">Input Pajak & Asuransi
                                        Excel</a>
                                </li>
                            @endif
                        </ul>
                    </li>
                @endif
                @if (Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan', 'admin_aset', 'admin_bangunan', 'staf_bangunan', 'admin_satker']))
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-home"></i><span
                                class="sidebar-mini-hide">Rumah Negara</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_rumah_negara_list') }}">Daftar Rumah</a>
                            </li>
                        @if (Auth::user()->hasRole(['super_admin', 'admin', 'admin_aset', 'admin_bangunan', 'admin_satker']))
                            <li>
                                <a href="{{ route('admin_rumah_negara_input_excel') }}">Input Excel</a>
                            </li>
                        @endif
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="fa fa-building"></i><span
                                class="sidebar-mini-hide">Gedung</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_gedung_list') }}">Daftar Gedung</a>
                            </li>
                            @if (Auth::user()->hasRole(['super_admin', 'admin', 'admin_aset', 'admin_bangunan', 'admin_satker']))
                            <li>
                                <a href="{{ route('admin_gedung_input_excel') }}">Input Excel</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-flag"></i><span
                                class="sidebar-mini-hide">Tanah</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_tanah_list') }}">Daftar Tanah</a>
                            </li>
                            @if (Auth::user()->hasRole(['super_admin', 'admin', 'admin_aset', 'admin_bangunan', 'admin_satker']))
                            <li>
                                <a href="{{ route('admin_tanah_input_excel') }}">Input Excel</a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @if (Auth::user()->hasRole(['super_admin', 'admin', 'admin_aset']))
                    <li>
                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-directions"></i><span
                            class="sidebar-mini-hide">Profile Perwakilan</span></a>
                    <ul>
                        <li>
                            <a href="{{ route('admin_kendaraannew_list') }}">Perwakilan Berdasarkan Kendaraan</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_bangunannew_list') }}">Perwakilan Berdasarkan Bangunan</a>
                        </li>
                        <li>
                            <a href="{{ route('admin_tanahnew_list') }}">Perwakilan Berdasarkan Tanah</a>
                        </li>
                    </ul>
                    </li>
                    @endif
                @endif
                @if (Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan']))
                    <li class="nav-main-heading"><span class="sidebar-mini-visible">AD</span><span
                            class="sidebar-mini-hidden">Administration</span></li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-user"></i><span
                                class="sidebar-mini-hide">Users</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_user_list') }}">Daftar Users</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-user"></i><span
                                class="sidebar-mini-hide">Sekertariat Jenderal</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_sekjen_list') }}">Daftar Sekertariat Jenderal</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-book-open"></i><span
                                class="sidebar-mini-hide">Barang</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_barang_list') }}">Daftar Barang</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-home"></i><span
                                class="sidebar-mini-hide">Satker</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_satker_list') }}">Daftar Satker</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-home"></i><span
                                class="sidebar-mini-hide">Wilayah</span></a>
                        <ul>
                            <li>
                                <a href="{{ route('admin_wilayah_list') }}">Daftar Wilayah</a>
                            </li>
                        </ul>
                    </li>
                @endif
                <li class="nav-main-heading"><span class="sidebar-mini-visible">AD</span><span
                        class="sidebar-mini-hidden">Pengaturan</span></li>
                @if (Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan']))
                    <li>
                        <a href="{{ route('admin_setting_general') }}"><i class="fa fa-gears"></i><span
                                class="sidebar-mini-hide">Pengaturan Umum</span></a>
                    </li>
                @endif
                <li>
                    <a href="{{ route('admin_change_password') }}"><i class="fa fa-key"></i><span
                            class="sidebar-mini-hide">Ganti Password</span></a>
                </li>
                <dark-mode-toogle></dark-mode-toogle>
            </ul>
        </div>
    </div>
</nav>
