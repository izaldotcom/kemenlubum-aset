<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin Panel</title>
    @include('admin.layouts.htmlheader')
</head>
<body class="body-inverse">
<div id="page-container"
     class="sidebar-o sidebar-inverse enable-page-overlay side-scroll page-header-modern main-content-boxed">
    <div id="app">

        <!-- =============================================== -->
    @include('admin.layouts.sidebar')
    <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <!-- Main Container -->
        <main id="main-container" class="main-default main-inverse">
            <header class="header-body d-flex align-items-center">
                <a href="{{ url('/admin') }}" class="img-garuda">
                    <img src="{{ asset('assets/images/garuda.png') }}" width="50">
                </a>
                <div class="header-command">
                    {{-- <div class="title-dashboard">DASHBOARD</div> --}}
                    <div class="title-content">Sistem Manajemen Aset</div>
                    <div class="title-content">Kementerian Luar Negeri Republik Indonesia</div>
                </div>
                <div class="img-ssl">
                    {{--                    <img src="{{ asset('assets/images/ssl-logo.png') }}">--}}
                </div>
                {{-- <div class="search-bar">
                   <input class="form-search" placeholder="Cari Menu">
                </div> --}}
            </header>
            <!-- Page Content -->
            <div class="content">
                @include('admin.layouts.header')
                @yield('content')
                <footer id="page-footer" class="opacity-0">
                    <div class="content py-20 font-size-xs clearfix">
                        <div class="float-left">
                            <a class="font-w600" href="{{ route('admin_dashboard') }}" target="_blank">Kementerian Luar
                                Negeri</a> &copy; <span class="js-year-copy">2019</span>
                        </div>
                    </div>
                </footer>
            </div>
            <!-- /.content -->

        </main>
        @yield('modal')
        <modal-create-penghapusan></modal-create-penghapusan>
        <modal-pemilihan-perwakilan-kendaraan></modal-pemilihan-perwakilan-kendaraan>
        <modal-pemilihan-perwakilan-bangunan></modal-pemilihan-perwakilan-bangunan>
        <modal-pemilihan-perwakilan-tanah></modal-pemilihan-perwakilan-tanah>
    </div>
</div>
<!-- /.wrapper -->
<script type="text/javascript">
    window.base_url   = "{{ url('/') }}";
    window.approveron = {{ Auth::user()->hasRole(['super_admin', 'admin', 'admin_aset']) ? 1 : 0 }};
    window.base_path  = "{{ asset('/') }}";
    window.csrf_token = "{{ csrf_token() }}";
</script>

<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
    integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
    crossorigin=""></script>

    <script src="https://www.gstatic.com/charts/loader.js"></script>
@isset($summary)
           <script>
        //{{ $summary['tanah']['total'] }}
           var summaryAllBar =  [
            ['Aset', 'Baik', {role: "annotation"}, 'Rusak Ringan', {role: "annotation"}, 'Rusak Berat', {role: "annotation"},],
            ['Tanah', parseFloat({{ $summary['tanah']['total_baik'] }}), parseFloat({{ $summary['tanah']['total_baik'] }}), parseFloat({{ $summary['tanah']['total_rusak_ringan'] }}), parseFloat({{ $summary['tanah']['total_rusak_ringan'] }}), parseFloat({{ $summary['tanah']['total_rusak_berat'] }}), parseFloat({{ $summary['tanah']['total_rusak_berat'] }})],
            ['Bangunan', parseFloat( {{ $summary['bangunan']['total_baik'] }} ), parseFloat({{ $summary['bangunan']['total_baik'] }}), parseFloat({{ $summary['bangunan']['total_rusak_ringan'] }}), parseFloat({{ $summary['bangunan']['total_rusak_ringan'] }}), parseFloat({{ $summary['bangunan']['total_rusak_berat'] }}), parseFloat({{ $summary['bangunan']['total_rusak_berat']}})],
            ['Mobilitas', parseFloat({{ $summary['mobilitas']['total_baik'] }}), parseFloat({{ $summary['mobilitas']['total_baik'] }}), parseFloat({{ $summary['mobilitas']['total_rusak_ringan'] }}), parseFloat({{ $summary['mobilitas']['total_rusak_ringan'] }}), parseFloat({{ $summary['mobilitas']['total_rusak_berat'] }}), parseFloat({{ $summary['mobilitas']['total_rusak_berat'] }})],
                        ];
           google.charts.load('current', {'packages':['corechart']});
           google.charts.setOnLoadCallback(drawChart);
           function drawChart()
           {
            var data = google.visualization.arrayToDataTable(summaryAllBar);

                var options = {
                      title: 'Keseluruhan Aset',
                      subtitle: 'Berdasarkan kondisi aset',
                      //is3D:true,
                      height:400,
                    colors         : ['#1b9e77', '#FA7D0E', '#FF0000'],
                    backgroundColor: {
                    'fill'       : '#FCFCFC',
                        'fillOpacity': 0
                    },
                     };
                var chart = new google.visualization.BarChart(document.getElementById('summaryAllBar'));
                chart.draw(data, options);
           }
           </script>


           <script>

           var summaryBangunanBar =  [
            ['Kondisi', 'Jumlah', {role: "annotation"}, {role: 'style'}],
                            ['Baik', parseFloat( {{ $summary['bangunan']['total_baik'] }} ), parseFloat( {{ $summary['bangunan']['total_baik'] }} ), '#1b9e77'],            // RGB value
                            ['Rusak Ringan', parseFloat({{ $summary['bangunan']['total_rusak_ringan'] }}), parseFloat({{ $summary['bangunan']['total_rusak_ringan'] }}), '#FA7D0E'],            // English color name
                            ['Rusak Berat', parseFloat({{ $summary['bangunan']['total_rusak_berat'] }}), parseFloat({{ $summary['bangunan']['total_rusak_berat'] }}), '#FF0000'],
                        ];
           google.charts.load('current', {'packages':['corechart']});
           google.charts.setOnLoadCallback(drawChart);
           function drawChart()
           {
            var data = google.visualization.arrayToDataTable(summaryBangunanBar);

                var options = {
                    title   : 'Summary Aset Bangunan',
                    subtitle: 'Berdasarkan kondisi aset',
                    bars           : 'vertical',
                    pieSliceText   : 'label',
                    height         : 300,
                    pieHole: 0.4,
                    colors         : ['#1b9e77', '#FA7D0E', '#FF0000'],
                    backgroundColor: {
                        'fill'       : '#FCFCFC',
                        'fillOpacity': 1
                    },
                     };
                var chart = new google.visualization.PieChart(document.getElementById('summaryBangunanBar'));
                chart.draw(data, options);
           }
           </script>


           <script>
var summaryMobilitasBar =  [
 ['Kondisi', 'Jumlah', {role: "annotation"}, {role: 'style'}],
                 ['Baik', parseFloat( {{ $summary['mobilitas']['total_baik'] }} ), parseFloat( {{ $summary['mobilitas']['total_baik'] }} ), '#1b9e77'],            // RGB value
                 ['Rusak Ringan', parseFloat({{ $summary['mobilitas']['total_rusak_ringan'] }}), parseFloat({{ $summary['mobilitas']['total_rusak_ringan'] }}), '#FA7D0E'],            // English color name
                 ['Rusak Berat', parseFloat({{ $summary['mobilitas']['total_rusak_berat'] }}), parseFloat({{ $summary['mobilitas']['total_rusak_berat'] }}), '#FF0000'],
             ];
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart()
{
 var data = google.visualization.arrayToDataTable(summaryMobilitasBar);

     var options = {
         title   : 'Summary Aset Mobilitas',
         subtitle: 'Berdasarkan kondisi aset',
         bars           : 'vertical',
         pieSliceText   : 'label',
         height         : 300,
         pieHole: 0.4,
         colors         : ['#1b9e77', '#FA7D0E', '#FF0000'],

          };
     var chart = new google.visualization.PieChart(document.getElementById('summaryMobilitasBar'));
     chart.draw(data, options);
}
</script>
<script>
var summaryKendaraanUsia =  [
            ['Usia', 'Jumlah', {role: "annotation"}, {role: 'style'}],
                 ['0-5', parseFloat( {{ $summary['mobilitas']['total_5_below'] }} ), parseFloat( {{ $summary['mobilitas']['total_5_below'] }} ), '#1b9e77'],            // RGB value
                 ['6-10', parseFloat({{ $summary['mobilitas']['total_5_to_10'] }}), parseFloat({{ $summary['mobilitas']['total_5_to_10'] }}), '#FA7D0E'],
                 ['>10', parseFloat({{ $summary['mobilitas']['total_10_above'] }}), parseFloat({{ $summary['mobilitas']['total_10_above'] }}), '#FF0000'],
             ];
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart()
{
 var data = google.visualization.arrayToDataTable(summaryKendaraanUsia);

     var options = {
         title   : 'Summary Berdasarkan Usia',
         subtitle: 'Summary Berdasarkan Usia',
         bars           : 'vertical',
         pieSliceText   : 'label',
         height         : 300,
         pieHole: 0.4,
         colors         : ['#1b9e77', '#FA7D0E', '#FF0000'],

          };
     var chart = new google.visualization.PieChart(document.getElementById('summaryKendaraanUsia'));
     chart.draw(data, options);
}
</script>
<script>
var summaryKendaraanRoda =  [
                 ['Roda', 'Jumlah', {role: "annotation"}, {role: 'style'}],
                 ['< Empat :', parseFloat({{ $summary['mobilitas']['total_dua'] }}), parseFloat({{ $summary['mobilitas']['total_dua'] }}), '#FA7D0E'],
                 ['Empat/Lebih :', parseFloat({{ $summary['mobilitas']['total_empat']}}), parseFloat({{ $summary['mobilitas']['total_empat'] }} ), '#1b9e77'],
             ];
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart()
{
 var data = google.visualization.arrayToDataTable(summaryKendaraanRoda);

     var options = {
         title   : 'Pembagian Kendaraan',
         subtitle: 'Pembagian Kendaraan',
         bars           : 'vertical',
         pieSliceText   : 'label',
         height         : 300,
         pieHole: 0.4,
         colors         : ['#1b9e77', '#FA7D0E', '#FF0000'],

          };
     var chart = new google.visualization.PieChart(document.getElementById('summaryKendaraanRoda'));
     chart.draw(data, options);
}
</script>
<script>
var summaryKendaraanRodaEmpat =  [
                 ['Roda', 'Jumlah', {role: "annotation"}, {role: 'style'}],
                 ['Baik', parseFloat( {{ $summary['mobilitas']['total_empat_baik'] }} ), parseFloat( {{ $summary['mobilitas']['total_empat_baik'] }} ), '#1b9e77'],
                 ['Rusak Ringan', parseFloat({{ $summary['mobilitas']['total_empat_rusak_ringan'] }}), parseFloat({{ $summary['mobilitas']['total_empat_rusak_ringan'] }}), '#FA7D0E'],
                 ['Rusak Berat', parseFloat({{ $summary['mobilitas']['total_empat_rusak_berat'] }}), parseFloat({{ $summary['mobilitas']['total_empat_rusak_berat'] }}), '#FA7D0E'],
             ];
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart()
{
 var data = google.visualization.arrayToDataTable(summaryKendaraanRodaEmpat);

     var options = {
         title   : 'Summary Kendaraan Roda 4 > Lebih',
         subtitle: 'Berdasarkan Summary Kendaraan Roda 4 > Lebih',
         bars           : 'vertical',
         pieSliceText   : 'label',
         height         : 300,
         pieHole: 0.4,
         colors         : ['#1b9e77', '#FA7D0E', '#FF0000'],

          };
     var chart = new google.visualization.PieChart(document.getElementById('summaryKendaraanRodaEmpat'));
     chart.draw(data, options);
}
</script>
<script>
var summaryKendaraanRodaDua =  [
                 ['Roda', 'Jumlah', {role: "annotation"}, {role: 'style'}],
                 ['Baik', parseFloat( {{ $summary['mobilitas']['total_dua_baik'] }} ), parseFloat( {{ $summary['mobilitas']['total_dua_baik'] }} ), '#1b9e77'],
                 ['Rusak Ringan', parseFloat({{ $summary['mobilitas']['total_dua_rusak_ringan'] }}), parseFloat({{ $summary['mobilitas']['total_dua_rusak_ringan'] }}), '#FA7D0E'],
                 ['Rusak Berat', parseFloat({{ $summary['mobilitas']['total_dua_rusak_berat'] }}), parseFloat({{ $summary['mobilitas']['total_dua_rusak_berat'] }}), '#FA7D0E'],
             ];
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart()
{
 var data = google.visualization.arrayToDataTable(summaryKendaraanRodaDua);

     var options = {
         title   : 'Summary Kendaraan Roda < 4 ',
         subtitle: 'Summary Kendaraan Roda < 4',
         bars           : 'vertical',
         pieSliceText   : 'label',
         height         : 300,
         pieHole: 0.4,
         colors         : ['#1b9e77', '#FA7D0E', '#FF0000'],

          };
     var chart = new google.visualization.PieChart(document.getElementById('summaryKendaraanRodaDua'));
     chart.draw(data, options);
}
</script>
@endisset

<script src="{{ asset('js/app-admin.js') }}"></script>
@include('admin.layouts.basescripts')
<script>
    $.fn.equalHeights = function () {
        var max_height = 0;
        $(this).each(function () {
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function () {
            $(this).height(max_height);
        });
    };
    $(window).resize(function () {
        $('.long-table tbody > tr > td').equalHeights();
        $('.long-table thead > tr > th').equalHeights();
    });
    $(document).ready(function () {
        setTimeout(function () {
            $('.long-table tbody > tr > td').equalHeights();
            $('.long-table thead > tr > th').equalHeights();
        }, 700);
        $('.modal').on('show.bs.modal', function () {
            setTimeout(function () {
                $('.long-table tbody > tr > td').equalHeights();
                $('.long-table thead > tr > th').equalHeights();
            }, 200);
        });
    });
    $('input.dark-mode:checkbox').change(function () {
        if ($(this).is(":checked")) {
            $('body').addClass("body-inverse");
            $('.main-default').addClass("main-inverse");
            $('.sidebar-o').addClass("sidebar-inverse");
            $('.modal-default').addClass("modal-inverse");
        } else {
            $('body').removeClass("body-inverse");
            $('.main-default').removeClass("main-inverse");
            $('.sidebar-o').removeClass("sidebar-inverse");
            $('.modal-default').removeClass("modal-inverse");

        }
    });
    $(document).ready(function () {
        $('#page-header-notifications').on('click', function () {
            $(this).next().toggleClass('active-user');
            $('.dropdown-menu').removeClass('active-notif');
        });
        $('#page-header-user-dropdown').on('click', function () {
            $(this).next().toggleClass('active-notif');
            $('.dropdown-menu').removeClass('active-user');
        });
    });

    function fireCreatePengajuanPenghapusan() {
        window.eventHub.$emit('open-modal', 'modal-create-penghapusan');
    }
    function fireCreatesekjen() {
        window.eventHub.$emit('open-modal', 'modal-create-sekjen');
    }
    function firePerwakilanKendaraan() {
        window.eventHub.$emit('open-modal', 'modal-pemilihan-perwakilan-kendaraan');
    }
    function firePerwakilanBangunan() {
        window.eventHub.$emit('open-modal', 'modal-pemilihan-perwakilan-bangunan');
    }
    function firePerwakilanTanah() {
        window.eventHub.$emit('open-modal', 'modal-pemilihan-perwakilan-tanah');
    }
</script>
</body>
</html>

