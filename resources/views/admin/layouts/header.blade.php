<header id="page-header">
    <div class="content-header p-0 px-3">
        <div class="content-header-section">
            <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout"
                    data-action="sidebar_toggle">
                <i class="fa fa-navicon"></i>
            </button>
            {{--            <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout"--}}
            {{--                    data-action="header_search_on">--}}
            {{--                <i class="fa fa-search"></i>--}}
            {{--            </button>--}}
        </div>

        <div class="content-header-section">
            <div class="btn-group" role="group">
                <div class="login-bar">
                    <a href="{{ url('/assets-management') }}">
                        <button class="btn btn-success px-4"> PETA
                        </button>
                    </a>
                </div>
                <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user d-sm-none"></i>
                    <span class="d-none d-sm-inline-block">{{ Auth::user()->name }}</span>
                    <i class="fa fa-angle-down ml-5"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right min-width-200"
                     aria-labelledby="page-header-user-dropdown">
                    <h5 class="h6 text-center py-10 mb-5 border-b text-uppercase">Pengguna</h5>
                    <a class="dropdown-item" href="{{ route('admin_change_password') }}">
                        <i class="si si-key mr-5"></i> Ganti Password
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ url('logout') }}">
                        <i class="si si-logout mr-5"></i> Keluar
                    </a>
                </div>
            </div>
            @if (Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan', 'admin_aset', 'admin_kendaraan', 'staf_kendaraan']))
            <div class="notif-admin btn-group " role="group">
                <button type="button " class="btn btn-rounded btn-dual-secondary" id="page-header-notifications"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-bell-o"></i>
                    <span class="badge badge-primary badge-pill">
                        {{ count($total_notifications) }}
                    </span>
                </button>
                <div class="dropdown-menu dropdown-menu-right min-width-300 dropdown-menu-not"
                     aria-labelledby="page-header-notifications">
                    <h5 class="h6 text-center py-10 mb-0 border-b text-uppercase">Notifications</h5>
                    <ul class="list-unstyled">
                        @foreach($my_notifications as $my_notification)
                            @php
                                /** @var \App\Models\Notification\Notification $my_notification */
                            @endphp
                            {{--                        <li class="table-active">--}}
                            <li>
                                <a class="text-body-color-dark media mb-10" href="{{ $my_notification->link }}">
                                    <div class="ml-5 mr-15">
                                        <i class="{{ $my_notification->icon }}"></i>
                                    </div>
                                    <div class="media-body pr-10">
                                        <p class="mb-0">
                                            {{ $my_notification->message }}
                                        </p>
                                        <div class="text-muted font-size-sm font-italic">
                                            {{ $my_notification->created_at->format('d M Y H:i:s') }}
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item text-center mb-0" href="{{ url('/admin/notification') }}">
                        View All
                    </a>
                </div>
            </div>
            @endif
        </div>
    </div>
    <div id="page-header-search" class="overlay-header">
        <div class="content-header content-header-fullrow">
            <form action="" method="post">
                <div class="input-group" id="search-bar">
                    <div class="input-group-prepend">
                        <button type="button" class="btn btn-secondary" data-toggle="layout"
                                data-action="header_search_off">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                    <input id="cari-menu" type="text" class="form-control" placeholder="Search or hit ESC..">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-secondary">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                    <div class="dropdown-search">
                        <a href="" class="dropdown-item no-hide"><i class="fa fa-home mr-3"></i>Rumah Negara</a>
                        <a href="" class="dropdown-item no-hide"><i class="fa fa-building mr-3"></i>Gedung</a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="page-header-loader" class="overlay-header bg-primary">
        <div class="content-header content-header-fullrow text-center">
            <div class="content-header-item">
                <i class="fa fa-sun-o fa-spin text-white"></i>
            </div>
        </div>
    </div>
</header>
