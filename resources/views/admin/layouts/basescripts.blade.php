<script src="{{ asset('assets-admin/js/codebase.core.min.js') }}"></script>
<!--
Codebase JS

Custom functionality including Blocks/Layout API as well as other vital and optional helpers
webpack is putting everything together at assets/_es6/main/app.js
-->
<script src="{{ asset('assets-admin/js/codebase.app.min.js') }}"></script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/chartjs/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/slick/slick.min.js') }}"></script>
<!-- Page JS Helpers (Slick Slider plugin) -->
<script>jQuery(function () {
        Codebase.helpers('slick');
    });</script>
<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_pages_dashboard.min.js') }}"></script>
<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_ui_icons.min.js') }}"></script>
<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-ui/jquery.ui.touch-punch.min.js') }}"></script>

<!-- Page JS Helpers (jQueryUI Plugin) -->
<script>jQuery(function () {
        Codebase.helpers('draggable-items');
    });</script>


<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/chartjs/Chart.bundle.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_blocks_widgets_stats.min.js') }}"></script>

<!-- Page JS Helpers (Easy Pie Chart Plugin) -->
<script>jQuery(function () {
        Codebase.helpers('easy-pie-chart');
    });</script>

<script src="{{ asset('assets-admin/js/plugins/bootstrap-notify/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/es6-promise/es6-promise.auto.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_ui_activity.min.js') }}"></script>

<!-- Page JS Helpers (BS Notify Plugin) -->
<script>jQuery(function () {
        Codebase.helpers('notify');
    });</script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

<!-- Page JS Helpers (Magnific Popup plugin) -->
<script>jQuery(function () {
        Codebase.helpers('magnific-popup');
    });</script>

<!-- Page JS Helpers (Table Tools helper) -->
<script>jQuery(function () {
        Codebase.helpers('table-tools');
    });</script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_tables_datatables.min.js') }}"></script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-tags-input/jquery.tagsinput.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-auto-complete/jquery.auto-complete.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/masked-inputs/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/dropzonejs/dropzone.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/pwstrength-bootstrap/pwstrength-bootstrap.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_forms_plugins.min.js') }}"></script>

<!-- Page JS Helpers (BS Datepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins) -->
<script>jQuery(function () {
        Codebase.helpers(['datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });</script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/simplemde/simplemde.min.js') }}"></script>

<!-- Page JS Helpers (Summernote + CKEditor + SimpleMDE plugins) -->
<script>jQuery(function () {
        Codebase.helpers(['summernote', 'ckeditor', 'simplemde']);
    });</script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-validation/additional-methods.js') }}"></script>

<!-- Page JS Helpers (Select2 plugin) -->
<script>jQuery(function () {
        Codebase.helpers('select2');
    });</script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_forms_validation.min.js') }}"></script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-validation/additional-methods.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_forms_wizard.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_comp_chat.min.js') }}"></script>
<script>
    jQuery(function () {
        // Add demonstration headers and messages for Chat #1
        BeCompChat.addHeader(1, '5 hours ago');
        BeCompChat.addMessage(1, 'Hi Admin!');
        BeCompChat.addMessage(1, 'Can you help me out?');
        BeCompChat.addMessage(1, 'I\'m having some issues with the code and I would really like your opinion!!');
        BeCompChat.addHeader(1, '2 hours ago');
        BeCompChat.addMessage(1, 'Hey Jeffrey!', 'self');
        BeCompChat.addMessage(1, 'Sure thing, let me know!', 'self');

        // Add demonstration headers and messages for Chat #2
        BeCompChat.addHeader(2, 'Yesterday');
        BeCompChat.addMessage(2, 'Hi Admin!');
        BeCompChat.addMessage(2, 'How are you?');
        BeCompChat.addHeader(2, 'Today');
        BeCompChat.addMessage(2, 'I\'m fine, thanks!', 'self');
        BeCompChat.addMessage(2, 'I hope you are doing great, too!', 'self');
    });
</script>
<script>
    jQuery(function () {
        // Add demonstration headers and messages for Chat #3
        BeCompChat.addMessage(3, 'Hey Jack!', 'self');
        BeCompChat.addMessage(3, 'Can you contact me asap?', 'self');

        // Add demonstration headers and messages for Chat #4
        BeCompChat.addHeader(4, 'Yesterday');
        BeCompChat.addMessage(4, 'Hi Admin!');
        BeCompChat.addMessage(4, 'How are you?');
        BeCompChat.addHeader(4, 'Today');
        BeCompChat.addMessage(4, 'I\'m fine, thanks!', 'self');

        // Add demonstration headers and messages for Chat #5
        BeCompChat.addMessage(5, 'We will have to move forward as soon as possible!');
        BeCompChat.addMessage(5, 'Is your team you ready?');
        BeCompChat.addHeader(5, 'Today');
        BeCompChat.addMessage(5, 'Absolutely!', 'self');

        // Add demonstration headers and messages for Chat #6
        BeCompChat.addHeader(6, 'No new messages!');
    });
</script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/chartjs/Chart.bundle.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.pie.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.stack.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.resize.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_comp_charts.min.js') }}"></script>

<!-- Page JS Helpers (Easy Pie Chart Plugin) -->
<script>jQuery(function () {
        Codebase.helpers('easy-pie-chart');
    });</script>
<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

<!-- Page JS Helpers (Magnific Popup plugin) -->
<script>jQuery(function () {
        Codebase.helpers('magnific-popup');
    });</script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Page JS Helpers (SlimScroll plugin) -->
<script>jQuery(function () {
        Codebase.helpers(['slimscroll']);
    });</script>
<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/jquery-raty/jquery.raty.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_comp_rating.min.js') }}"></script>

<script src="{{ asset('assets-admin/js/codebase.app.min.js') }}"></script>

<!-- Page JS Helpers (Content Filtering helper) -->
<script>jQuery(function () {
        Codebase.helpers('content-filter');
    });</script>
<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/fullcalendar/fullcalendar.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_comp_calendar.min.js') }}"></script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/cropperjs/cropper.min.js') }}"></script>

<!-- Page JS Code -->
{{-- <script src="{{ asset('assets-admin/js/pages/be_comp_image_cropper.min.js') }}"></script> --}}

<!-- For more info please have a look at https://developers.google.com/maps/documentation/javascript/get-api-key#key -->
<script src="{{ asset('assets-admin/js/plugins/gmapsjs/gmaps.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_comp_maps_google.min.js') }}"></script>

<!-- Page JS Plugins -->
{{-- <script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/dist/jquery-jvectormap.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/maps/jquery-jvectormap-au-mill-en.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/maps/jquery-jvectormap-cn-mill-en.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/maps/jquery-jvectormap-de-mill-en.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/maps/jquery-jvectormap-europe-mill-en.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/maps/jquery-jvectormap-fr-mill-en.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/maps/jquery-jvectormap-in-mill-en.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/maps/jquery-jvectormap-us-aea-en.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/maps/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-jvectormap/maps/jquery-jvectormap-za-mill-en.js') }}"></script> --}}
<!-- Page JS Code -->
{{-- <script src="{{ asset('assets-admin/js/pages/be_comp_maps_vector.min.js') }}"></script> --}}

<!-- Page JS Plugins -->
<!-- Google Maps API Key (you will have to obtain a Google Maps API key to use Google Maps) -->
<!-- For more info please have a look at https://developers.google.com/maps/documentation/javascript/get-api-key#key -->
<script src="{{ asset('assets-admin/js/plugins/gmapsjs/gmaps.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_pages_generic_contact.min.js') }}"></script>
<script>jQuery(function () {
        Codebase.helpers('table-tools');
    });</script>
<!-- Page JS Helpers (Content Filtering helper) -->
<script>jQuery(function () {
        Codebase.helpers('content-filter');
    });</script>

<!-- Page JS Plugins -->
<script src="{{ asset('assets-admin/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets-admin/js/plugins/jquery-ui/jquery.ui.touch-punch.min.js') }}"></script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_pages_generic_scrumboard.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.show-marker').hover(function () {
            $(this).next().addClass('active')
            if ($(this).next().hasClass('active')) {
                $(this).parent().addClass('z-marker')
                $(this).next().hover(function () {
                    $(this).addClass('active')
                    $(this).parent().addClass('z-marker')
                }, function () {
                    $(this).removeClass('active')
                    $(this).parent().removeClass('z-marker')
                });
            } else {
                $(this).parent().removeClass('z-marker')
            }
        }, function () {
            $(this).next().removeClass('active')
            $(this).parent().removeClass('z-marker')
        });
    });
</script>
<script src="{{ asset('assets/js/dnSlide.js') }}"></script>
<script>
    jQuery(document).ready(function ($) {
        $(".dnSlide-main").each(function (index, el) {
            var setting = {
                "response"     : true,
                afterClickBtnFn: function (i) {
                    console.log(i);
                }
            };
            switch (index) {
                case 1:
                    setting.verticalAlign = "top";
                    setting.switching     = "custom";
                    setting.precentWidth  = "80%";
                    setting.autoPlay      = false;
                    // var api = $(el).dnSlide(setting).data( "dnSlide" );
                    // $(".hide").on("click",function(){
                    //     api.hide(function(){
                    //         alert('HIDEEN！！！');
                    //     });
                    // });
                    // $(".show").on("click",function(){
                    //     api.show(function(){
                    //         alert('SHOW！！！');
                    //     });
                    // });
                    break;
                case 0:
                    setting.autoPlay = true;
                    $(el).dnSlide(setting);
                    break;
                case 2:
                    setting.verticalAlign = "bottom";
                    $(el).dnSlide(setting);
                    break;
                default:
                    $(el).dnSlide(setting);
                    break;
            }
        });
    });
</script>

<!-- Page JS Code -->
<script src="{{ asset('assets-admin/js/pages/be_pages_generic_todo.min.js') }}"></script>
<script>

    var h = $(window).height() - 40;
    // $('#map-landscape').css({"opacity" : "0", "width": "0"})
    // $('#map-potrait').css({"opacity" : "0", "width": "0"})

    var width  = $(window).width();
    var height = $(window).height();

    if (width < height) {
        $('#map-landscape').css({"opacity": "0", "width": "0"})
        $('#map-potrait').css({"opacity": "1", "width": "75%", "transform": "scale(1,1)"})
        $('.img-ssl').addClass("mobile").css({'opacity': 1})
    } else {
        $('#map-potrait').css({"opacity": "0", "width": "0"})
        $('#map-landscape').css({"opacity": "1", "width": "50%", "transform": "scale(1,1)"})
        $('.img-ssl').removeClass("mobile").css({'opacity': 1})
    }

    window.onresize = function (event) {
        // $('#map-landscape').css({"opacity" : "0", "width": "0"})
        // $('#map-potrait').css({"opacity" : "0", "width": "0"})

        var width  = $(window).width();
        var height = $(window).height();

        if (width < height) {
            $('#map-landscape').css({"opacity": "0", "width": "0"})
            $('#map-potrait').css({"opacity": "1", "width": "75%", "transform": "scale(1,1)"})
            $('.img-ssl').addClass("mobile").css({'opacity': 1})
        } else {
            $('#map-potrait').css({"opacity": "0", "width": "0"})
            $('#map-landscape').css({"opacity": "1", "width": "50%", "transform": "scale(1,1)"})
            $('.img-ssl').removeClass("mobile").css({'opacity': 1})
        }
    }

    $(document).ready(function () {
        $("input#cari-menu").focusin(function () {
            $(".dropdown-search").addClass('open-dropdown');
            $('.dropdown-search').hover(function () {
                    $(".dropdown-search").addClass('open-dropdown');
                },
                function () {
                    $(".dropdown-search").removeClass('open-dropdown');
                });
        });
        $("input#cari-menu").click(function () {
            $(".dropdown-search").addClass('open-dropdown');
            $('.dropdown-search').hover(function () {
                    $(".dropdown-search").addClass('open-dropdown');
                },
                function () {
                    $(".dropdown-search").removeClass('open-dropdown');
                });
        });
    });
    $(document).ready(function () {
        $("input#cari-menu-filter").focusin(function () {
            $(".dropdown-search").addClass('open-dropdown-filter');
            $('.dropdown-search').hover(function () {
                    $(".dropdown-search-filter").addClass('open-dropdown-filter');
                },
                function () {
                    $(".dropdown-search-filter").removeClass('open-dropdown-filter');
                });
        });
        $("input#cari-menu-filter").click(function () {
            $(".dropdown-search-filter").addClass('open-dropdown-filter');
            $('.dropdown-search-filter').hover(function () {
                    $(".dropdown-search-filter").addClass('open-dropdown-filter');
                },
                function () {
                    $(".dropdown-search-filter").removeClass('open-dropdown-filter');
                });
        });
    });

</script>
<script>
    $(document).ready(function () {
        $('.open-sidebar').on('click', function () {
            $(this).toggleClass('active');
            $('.sidebar-maps').toggleClass('active');
            $('#scrollbar').scrollTop(0);
        });
        $('#view-list-more').on('click', function () {
            $('.content-list-maps').show();
            $('.content-filter-country').hide();
            $('#scrollbar').scrollTop(0);
            $('.content-filter-maps').hide();
        });
        $('.close-list-maps').on('click', function () {
            $('.content-list-maps').hide();
            $('.content-filter-country').hide();
            $('#scrollbar').scrollTop(0);
            $('.content-filter-maps').show();
        });
        $('.open-country').on('click', function () {
            $('.content-list-maps').hide();
            $('.content-filter-maps').hide();
            $('#scrollbar').scrollTop(0);
            $('.content-filter-country').show();
        });
        $('.list-detail-country').on('click', function () {
            $('.content-list-maps').show();
            $('.content-filter-country').hide();
            $('#scrollbar').scrollTop(0);
            $('.content-filter-maps').hide();
        });
    });
    // document.querySelector('.slimScrollDiv').scrollIntoView({
    //     behavior: 'smooth'
    // });
</script>

<script src="{{ asset('assets/js/vue-ladda.js') }}"></script>
<script src="{{ asset('assets/js/autoNumeric.min.js') }}"></script>

@yield('scripts')
