<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Admin Panel</title>
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    @include('admin.layouts.htmlheader')
    <style>
        #map-canvas {
            height: 100vh;
            width: 100vw;
        }
    </style>
</head>
<body class="body-inverse">
<div id="page-container" class="enable-page-overlay side-scroll page-header-modern main-content-boxed">
    <div id="app">
        <!-- Main Container -->
        <main id="main-container">
            <header class="header-body d-flex align-items-center">
                <a href="{{ url('/admin') }}" class="img-garuda">
                    <img src="{{ asset('assets/images/garuda.png') }}" width="50">
                </a>
                <div class="header-command">
                    {{-- <div class="title-dashboard">DASHBOARD</div> --}}
                    <div class="title-content">Sistem Aset Manajemen</div>
                    <div class="title-content">Kementerian Luar Negeri Republik Indonesia</div>
                </div>
                <div class="img-ssl">
{{--                    <img src="{{ asset('assets/images/ssl-logo.png') }}">--}}
                </div>
                {{-- <div class="search-bar">
                   <input class="form-search" placeholder="Cari Menu">
                </div> --}}
            </header>
            <!-- Page Content -->
            <div class="w-100 main-inverse">
                @yield('content')
            </div>
        </main>
    </div>
</div>
<!-- /.wrapper -->
<script type="text/javascript">
    window.base_url   = "{{ url('/') }}";
    window.base_path  = "{{ asset('/') }}";
    window.csrf_token = "{{ csrf_token() }}";
</script>

<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
    integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
    crossorigin=""></script>
    <script>
    var map = L.map('mapid').setView([{{ config('leaflet.map_center_latitude') }}, {{ config('leaflet.map_center_longitude') }}], {{ config('leaflet.zoom_level') }});
    var baseUrl = "{{ url('/') }}";

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    axios.get('{{ route('api.outlets.index') }}')
    .then(function (response) {
        console.log(response.data);
        L.geoJSON(response.data, {
            pointToLayer: function(geoJsonPoint, latlng) {
                return L.marker(latlng);
            }
        })
        .bindPopup(function (layer) {
            return layer.feature.properties.map_popup_content;
        }).addTo(map);
    })
    .catch(function (error) {
        console.log(error);
    });

    @can('create', new App\Outlet)
    var theMarker;

    map.on('click', function(e) {
        let latitude = e.latlng.lat.toString().substring(0, 15);
        let longitude = e.latlng.lng.toString().substring(0, 15);

        if (theMarker != undefined) {
            map.removeLayer(theMarker);
        };

        var popupContent = "Your location : " + latitude + ", " + longitude + ".";
        popupContent += '<br><a href="{{ route('outlets.create') }}?latitude=' + latitude + '&longitude=' + longitude + '">Add new outlet here</a>';

        theMarker = L.marker([latitude, longitude]).addTo(map);
        theMarker.bindPopup(popupContent)
        .openPopup();
    });
    @endcan
</script>
<script src="{{ asset('js/app-admin.js') }}"></script>
@include('admin.layouts.basescripts')
</body>
</html>

