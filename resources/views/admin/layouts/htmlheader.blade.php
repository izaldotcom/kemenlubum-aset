<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="{{ asset('assets/images/garuda.png') }}">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/slick/slick.css') }}">
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/slick/slick-theme.css') }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/sweetalert2/sweetalert2.min.css') }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/magnific-popup/magnific-popup.css') }}">
<!-- Fonts and Codebase framework -->
<!-- Page JS Plugins CSS -->
<link rel="stylesheet"
      href="{{ asset('assets-admin/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<link rel="stylesheet"
      href="{{ asset('assets-admin/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/jquery-tags-input/jquery.tagsinput.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/jquery-auto-complete/jquery.auto-complete.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/ion-rangeslider/css/ion.rangeSlider.css') }}">
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css') }}">
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/dropzonejs/dist/dropzone.css') }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/summernote/summernote-bs4.css') }}">
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/simplemde/simplemde.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/select2/css/select2.css') }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/fullcalendar/fullcalendar.min.css') }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/cropperjs/cropper.min.css') }}">
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="{{ asset('assets-admin/js/plugins/jquery-jvectormap/dist/jquery-jvectormap.css') }}">

{{-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"> --}}
<link rel="stylesheet" id="css-main" href="{{ asset('assets/css/fonts.css') }}">
<link rel="stylesheet" id="css-main" href="{{ asset('assets-admin/css/codebase.css') }}">
<link rel="stylesheet" id="css-main" href="{{ asset('assets-admin/css/style-custom.css') }}">
<link rel="stylesheet" id="css-main" href="{{ asset('assets/css/header.css') }}">

<link rel="stylesheet" href="{{ asset('assets/css/vue-multiselect.min.css') }}">

@if ($dark_mode_user == 'true')
    <link rel="stylesheet" id="css-main" href="{{ asset('assets-admin/css/style-darkmode.css') }}">
@endif

@yield('styles')
