@extends('admin.layouts.base')

@section('content')
    <div class="row invisible" data-toggle="appear">
        <div class="col-md-12">
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Daftar Barang</h3>
                </div>
                <div class="block-content block-content-full">
                    <ajax-table
                            :url="'{{ route('admin_api_get_barang' ) }}'"
                            :oid="'data-barang'"
                            :params="params"
                            :config="{
                                  autoload: true,
                                  show_all: false,
                                  has_number: true,
                                  has_entry_page: false,
                                  has_pagination: true,
                                  has_action: true,
                                  has_search_input: true,
                                  has_search_header: false,
                                  custom_header: '',
                                  default_sort: 'id',
                                  default_sort_dir: 's',
                                  custom_empty_page: false,
                                  search_placeholder: 'Cari',
                                  class: {
                                    table: ['table-condensed'],
                                    wrapper: ['table-responsive'],
                                  }
                            }"
                            :rowparams="{tanggal: params.nama_kedutaan}"
                            :rowtemplate="'tr-data-barang'"
                            :columns="{
                                  'kode_barang': 'Kode',
                                  'nama_barang': 'Nama',
                            }">
                    </ajax-table>
                </div>
            </div>
        </div>
    </div>
@endsection
