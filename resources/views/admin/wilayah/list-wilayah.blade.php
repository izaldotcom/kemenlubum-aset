@extends('admin.layouts.base')

@section('content')
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dasbor</a>
        <span class="breadcrumb-item active">List Asset Wilayah</span>
    </nav>

    <div class="row">
        <!-- Row #2 -->s
        <div class="col-md-12">
            <div class="block" id="mythead">
                <div class="block-header block-header-default">
                    <h3 class="block-title">List Asset Wilayah</h3>
                </div>
                <div class="block-content long-table">
                    <ajax-table
                            :url="'{{ route('admin_api_get_wilayah' ) }}'"
                            :oid="'data-wilayah'"
                            :params="params"
                            :config="{
                                  autoload: true,
                                  show_all: false,
                                  has_number: true,
                                  has_entry_page: false,
                                  has_pagination: true,
                                  has_action: true,
                                  has_search_input: true,
                                  has_search_header: false,
                                  custom_header: '',
                                  default_sort: 'id',
                                  default_sort_dir: 'desc',
                                  custom_empty_page: false,
                                  search_placeholder: 'Cari',
                                  class: {
                                    table: ['table-condensed'],
                                    wrapper: ['table-responsive'],
                                  }
                            }"
                            :rowparams="{tanggal: params.nama_kedutaan}"
                            :rowtemplate="'tr-data-wilayah'"
                            :columns="{
                                  'kode_wilayah': 'Kode Wilayah',
                                  'nama_wilayah': 'Nama Wilayah',
                                  'active': 'Active',
                            }">
                    </ajax-table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <modal-edit-long-lat tipe="rumah"></modal-edit-long-lat>
    <modal-edit-photos tipe="rumah"></modal-edit-photos>
@endsection