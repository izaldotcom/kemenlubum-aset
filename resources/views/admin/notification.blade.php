@extends('admin.layouts.base')

@section('styles')
    <style>
        .custom-checkbox .custom-control-input:checked ~ .custom-control-label::after {
            background-image: none;
        }

        .custom-checkbox .custom-control-label::before {
            border-radius: 50px;
        }

        .custom-control-label::before {
            background-color: #dee2e6;
        }

        .custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
            background-color: #c3c5c7;
        }

        .custom-checkbox label,
        .custom-checkbox label:focus {
            text-decoration: none;
            outline: none;
        }

        .custom-checkbox .custom-control-input:checked ~ .custom-control-label::before {
            background-color: #c3c5c7;
        }

        .custom-control-input:focus ~ .custom-control-label::before {
            box-shadow: 0 0 0 0px #f0f2f5, 0 0 0 0px #e83f3f;
        }
    </style>
@endsection

@section('content')
    <h2 class="content-heading mb-3 border-bottom-0 notif-header">
        <button type="button" class="btn btn-rounded btn-success"><span class="mr-2">{{ count($notifications) }}</span> New Notification</button>
    </h2>
    <div class="row">
        <div class="col-md-12 col-xl-12">
            <div class="block notification">
                <div class="block-content">

                    <table class="js-table-checkable table table-hover table-vcenter">
                        <tbody>
                        @foreach($notifications as $notification)
                            @php
                                /** @var \App\Models\Notification\Notification $notification */
                            @endphp
                            <tr class="table-active">
                                <td class="font-w600" style="width: 80px;">
                                    {{-- <img class="img-avatar" src="{{ asset('assets-admin/media/various/little-face.png') }}" alt=""> --}}
                                    <div class="ml-5 mr-15"><i class="{{ $notification->icon }}"></i></div>
                                </td>
                                <td>
                                    <a class="font-w600" data-toggle="modal" data-target="#modal-message"
                                       href="{{ $notification->link }}">
                                        {{ $notification->title }}
                                    </a>
                                    <div class="text-muted mt-5">
                                        {{ $notification->message }}
                                    </div>
                                </td>
                                <td class="d-none d-xl-table-cell font-w600 font-size-sm text-muted"
                                    style="width: 120px;">
                                    {{ $notification->created_at->format('d M Y H:i:s') }}
                                </td>
                                <td class="d-none d-sm-table-cell ">
                                    <a  type="button"  href="/upload/hapus/{{ $notification->id }}" class="btn btn-rounded btn-alt-secondary float-right">
                                        <i class="fa fa-times text-danger mx-5"></i>
                                    </a>
                                    {{-- <div class="custom-control custom-checkbox custom-control-inline float-right">
                                        <input class="custom-control-input" type="checkbox"
                                               name="example-inline-checkbox1"
                                               id="example-inline-checkbox1" value="unread" checked="">
                                        <label class="custom-control-label" for="example-inline-checkbox1"></label>
                                    </div> --}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
