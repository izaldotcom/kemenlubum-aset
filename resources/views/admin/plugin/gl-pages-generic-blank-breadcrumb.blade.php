@extends('admin.layouts.base')

@section('content')
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Generic</a>
        <span class="breadcrumb-item active">Blank Page</span>
    </nav>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Blank
                <small>Get Started</small>
            </h3>
        </div>
        <div class="block-content">
            <p>Create your own awesome project!</p>
        </div>
    </div>
@endsection