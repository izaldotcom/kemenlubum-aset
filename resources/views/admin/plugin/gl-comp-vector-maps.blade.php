@extends('admin.layouts.base')

@section('content')
    <div class="content">
        <!-- For more info and examples you can check out http://jvectormap.com/documentation/ -->
        <h2 class="content-heading">Vector Maps</h2>

        <!-- Maps Row -->
        <div class="row">
            <div class="col-xl-12">
                <!-- World Map -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">World</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <!-- World Map Container -->
                        <div class="js-vector-map-world" style="height: 500px;"></div>
                    </div>
                </div>
                <!-- END World Map -->
            </div>
            <div class="col-xl-6">
                <!-- Europe Map -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Europe</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <!-- Europe Map Container -->
                        <div class="js-vector-map-europe" style="height: 350px;"></div>
                    </div>
                </div>
                <!-- END Europe Map -->
            </div>
            <div class="col-xl-6">
                <!-- USA Map -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">USA</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <!-- USA Map Container -->
                        <div class="js-vector-map-usa" style="height: 350px;"></div>
                    </div>
                </div>
                <!-- END USA Map -->
            </div>
            <div class="col-xl-6">
                <!-- India Map -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">India</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <!-- India Map Container -->
                        <div class="js-vector-map-india" style="height: 350px;"></div>
                    </div>
                </div>
                <!-- END India Map -->
            </div>
            <div class="col-xl-6">
                <!-- China Map -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">China</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <!-- China Map Container -->
                        <div class="js-vector-map-china" style="height: 350px;"></div>
                    </div>
                </div>
                <!-- END China Map -->
            </div>
            <div class="col-xl-6">
                <!-- Australia Map -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Australia</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <!-- Australia Map Container -->
                        <div class="js-vector-map-australia" style="height: 350px;"></div>
                    </div>
                </div>
                <!-- END Australia Map -->
            </div>
            <div class="col-xl-6">
                <!-- South Africa Map -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">South Africa</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <!-- South Africa Map Container -->
                        <div class="js-vector-map-south-africa" style="height: 350px;"></div>
                    </div>
                </div>
                <!-- END South Africa Map -->
            </div>
            <div class="col-xl-6">
                <!-- France Map -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">France</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <!-- France Map Container -->
                        <div class="js-vector-map-france" style="height: 350px;"></div>
                    </div>
                </div>
                <!-- END France Map -->
            </div>
            <div class="col-xl-6">
                <!-- Germany Map -->
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Germany</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option">
                                <i class="si si-wrench"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content block-content-full">
                        <!-- Germany Map Container -->
                        <div class="js-vector-map-germany" style="height: 350px;"></div>
                    </div>
                </div>
                <!-- END Germany Map -->
            </div>
        </div>
@endsection