@extends('admin.layouts.base')

@section('content')
    <div class="js-chat-container content content-full invisible p-0" data-toggle="appear" data-chat-height="auto">
        <!-- Multiple Chat (auto height) -->
        <div class="block mb-0">
            <ul class="js-chat-head nav nav-tabs nav-tabs-alt bg-body-light" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#chat-window1">
                        <img class="img-avatar img-avatar16" src="assets/media/avatars/avatar14.jpg" alt="">
                        <span class="ml-5">Jeffrey</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#chat-window2">
                        <img class="img-avatar img-avatar16" src="assets/media/avatars/avatar7.jpg" alt="">
                        <span class="ml-5">Megan</span>
                    </a>
                </li>
                <li class="nav-item ml-auto">
                    <a class="nav-link" href="#chat-people">
                        <i class="si si-users"></i>
                    </a>
                </li>
            </ul>
            <div class="tab-content overflow-hidden">
                <!-- Chat Window #1 -->
                <div class="tab-pane fade show active" id="chat-window1" role="tabpanel">
                    <!-- Messages (demonstration messages are added with JS code at the bottom of this page) -->
                    <div class="js-chat-talk block-content block-content-full text-wrap-break-word overflow-y-auto"
                         data-chat-id="1"></div>

                    <!-- Chat Input -->
                    <div class="js-chat-form block-content block-content-full block-content-sm bg-body-light">
                        <form action="be_comp_chat_multiple.html" method="post">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="fa fa-comment text-primary"></i>
                                  </span>
                                </div>
                                <input class="js-chat-input form-control" type="text" data-target-chat-id="1"
                                       placeholder="Type your message and hit enter..">
                            </div>
                        </form>
                    </div>
                    <!-- END Chat Input -->
                </div>
                <!-- END Chat Window #1 -->

                <!-- Chat Window #2 -->
                <div class="tab-pane fade" id="chat-window2" role="tabpanel">
                    <!-- Messages (demonstration messages are added with JS code at the bottom of this page) -->
                    <div class="js-chat-talk block-content block-content-full text-wrap-break-word overflow-y-auto"
                         data-chat-id="2"></div>

                    <!-- Chat Input -->
                    <div class="js-chat-form block-content block-content-full block-content-sm bg-body-light">
                        <form action="be_comp_chat_multiple.html" method="post">
                            <div class="input-group input-group-lg">
                                <div class="input-group-prepend">
                                  <span class="input-group-text">
                                      <i class="fa fa-comment text-primary"></i>
                                  </span>
                                </div>
                                <input class="js-chat-input form-control" type="text" data-target-chat-id="2"
                                       placeholder="Type your message and hit enter..">
                            </div>
                        </form>
                    </div>
                    <!-- END Chat Input -->
                </div>
                <!-- END Chat Window #2 -->

                <!-- People -->
                <div class="tab-pane fade fade-left" id="chat-people" role="tabpanel">
                    <div class="js-chat-people block-content block-content-full overflow-y-auto bg-pattern"
                         style="background-image: url('assets/media/various/bg-pattern-inverse.png');">
                        <div class="row mb-0">
                            <div class="col-md-4">
                                <h3 class="h1 font-w300 text-success">Online</h3>
                                <ul class="nav-users push">
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar5.jpg" alt="">
                                            <i class="fa fa-circle text-success"></i> Betty Kelley
                                            <div class="font-w400 font-size-xs text-muted">Photographer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar9.jpg" alt="">
                                            <i class="fa fa-circle text-success"></i> Thomas Riley
                                            <div class="font-w400 font-size-xs text-muted">Web Designer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar12.jpg" alt="">
                                            <i class="fa fa-circle text-success"></i> Albert Ray
                                            <div class="font-w400 font-size-xs text-muted">Web Designer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar10.jpg" alt="">
                                            <i class="fa fa-circle text-success"></i> Jack Estrada
                                            <div class="font-w400 font-size-xs text-muted">Web Developer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar1.jpg" alt="">
                                            <i class="fa fa-circle text-success"></i> Lisa Jenkins
                                            <div class="font-w400 font-size-xs text-muted">UI Designer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar15.jpg" alt="">
                                            <i class="fa fa-circle text-success"></i> Jack Estrada
                                            <div class="font-w400 font-size-xs text-muted">Copywriter</div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h3 class="h1 font-w300 text-warning">Away</h3>
                                <ul class="nav-users mt-10 push">
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar10.jpg" alt="">
                                            <i class="fa fa-circle text-warning"></i> Jose Parker
                                            <div class="font-w400 font-size-xs text-muted">Web Designer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar12.jpg" alt="">
                                            <i class="fa fa-circle text-warning"></i> David Fuller
                                            <div class="font-w400 font-size-xs text-muted">Web Developer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar5.jpg" alt="">
                                            <i class="fa fa-circle text-warning"></i> Laura Carr
                                            <div class="font-w400 font-size-xs text-muted">UI Designer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar11.jpg" alt="">
                                            <i class="fa fa-circle text-warning"></i> Brian Cruz
                                            <div class="font-w400 font-size-xs text-muted">Copywriter</div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h3 class="h1 font-w300 text-muted">Offline</h3>
                                <ul class="nav-users mt-10 push">
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar5.jpg" alt="">
                                            <i class="fa fa-circle text-muted"></i> Andrea Gardner
                                            <div class="font-w400 font-size-xs text-muted">Photographer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar9.jpg" alt="">
                                            <i class="fa fa-circle text-muted"></i> Jose Mills
                                            <div class="font-w400 font-size-xs text-muted">Web Designer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar13.jpg" alt="">
                                            <i class="fa fa-circle text-muted"></i> Jose Wagner
                                            <div class="font-w400 font-size-xs text-muted">Web Designer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar14.jpg" alt="">
                                            <i class="fa fa-circle text-muted"></i> David Fuller
                                            <div class="font-w400 font-size-xs text-muted">Web Developer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar7.jpg" alt="">
                                            <i class="fa fa-circle text-muted"></i> Lori Moore
                                            <div class="font-w400 font-size-xs text-muted">UI Designer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar11.jpg" alt="">
                                            <i class="fa fa-circle text-muted"></i> Jack Greene
                                            <div class="font-w400 font-size-xs text-muted">Copywriter</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar7.jpg" alt="">
                                            <i class="fa fa-circle text-muted"></i> Helen Jacobs
                                            <div class="font-w400 font-size-xs text-muted">UI Designer</div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <img class="img-avatar" src="assets/media/avatars/avatar10.jpg" alt="">
                                            <i class="fa fa-circle text-muted"></i> Adam McCoy
                                            <div class="font-w400 font-size-xs text-muted">Copywriter</div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END People -->
            </div>
        </div>
        <!-- END Multiple Chat (auto height) -->
    </div>
@endsection