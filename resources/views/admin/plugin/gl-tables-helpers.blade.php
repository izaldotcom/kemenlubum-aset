@extends('admin.layouts.base')

@section('content')
    <!-- Table Sections (.js-table-sections class is initialized in Helpers.tableToolsSections()) -->
    <h2 class="content-heading">Table Sections</h2>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">
                <code>.js-table-sections</code>
            </h3>
        </div>
        <div class="block-content">
            <!--
            Separate your table content with multiple tbody sections. Add one row and add the class .js-table-sections-header to a
            tbody section to make it clickable. It will then toggle the next tbody section which can have multiple rows. Eg:

            <tbody class="js-table-sections-header">One row</tbody>
            <tbody>Multiple rows</tbody>
            <tbody class="js-table-sections-header">One row</tbody>
            <tbody>Multiple rows</tbody>
            <tbody class="js-table-sections-header">One row</tbody>
            <tbody>Multiple rows</tbody>

            You can also add the class .show in your tbody.js-table-sections-header to make the next tbody section visible by default
            -->
            <table class="js-table-sections table table-hover">
                <thead>
                <tr>
                    <th style="width: 30px;"></th>
                    <th>Name</th>
                    <th style="width: 15%;">Access</th>
                    <th class="d-none d-sm-table-cell" style="width: 20%;">Date</th>
                </tr>
                </thead>
                <tbody class="js-table-sections-header show table-active">
                <tr>
                    <td class="text-center">
                        <i class="fa fa-angle-right"></i>
                    </td>
                    <td class="font-w600">Albert Ray</td>
                    <td>
                        <span class="badge badge-primary">Personal</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">October 2, 2017 12:16</em>
                    </td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $281,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 3, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $140,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 8, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $203,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 12, 2017 12:16</span>
                    </td>
                </tr>
                </tbody>
                <tbody class="js-table-sections-header">
                <tr>
                    <td class="text-center">
                        <i class="fa fa-angle-right"></i>
                    </td>
                    <td class="font-w600">Judy Ford</td>
                    <td>
                        <span class="badge badge-info">Business</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">October 10, 2017 12:16</em>
                    </td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $167,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 28, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $136,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 18, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $294,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 24, 2017 12:16</span>
                    </td>
                </tr>
                </tbody>
                <tbody class="js-table-sections-header">
                <tr>
                    <td class="text-center">
                        <i class="fa fa-angle-right"></i>
                    </td>
                    <td class="font-w600">Thomas Riley</td>
                    <td>
                        <span class="badge badge-warning">Trial</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">October 2, 2017 12:16</em>
                    </td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $270,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 7, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $156,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 27, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $291,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 9, 2017 12:16</span>
                    </td>
                </tr>
                </tbody>
                <tbody class="js-table-sections-header">
                <tr>
                    <td class="text-center">
                        <i class="fa fa-angle-right"></i>
                    </td>
                    <td class="font-w600">Laura Carr</td>
                    <td>
                        <span class="badge badge-danger">Disabled</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">October 25, 2017 12:16</em>
                    </td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $223,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 4, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $221,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 5, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $299,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 8, 2017 12:16</span>
                    </td>
                </tr>
                </tbody>
                <tbody class="js-table-sections-header">
                <tr>
                    <td class="text-center">
                        <i class="fa fa-angle-right"></i>
                    </td>
                    <td class="font-w600">Helen Jacobs</td>
                    <td>
                        <span class="badge badge-success">VIP</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">October 14, 2017 12:16</em>
                    </td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $268,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 14, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $295,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 9, 2017 12:16</span>
                    </td>
                </tr>
                <tr>
                    <td class="text-center"></td>
                    <td class="font-w600 text-success">+ $292,00</td>
                    <td class="font-size-sm">Stripe</td>
                    <td class="d-none d-sm-table-cell">
                        <span class="font-size-sm text-muted">October 16, 2017 12:16</span>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Table Sections -->

    <!-- Checkable Table (.js-table-checkable class is initialized in Helpers.tableToolsCheckable()) -->
    <h2 class="content-heading">Checkable Table</h2>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">
                <code>.js-table-checkable</code>
            </h3>
        </div>
        <div class="block-content">
            <!-- If you put a checkbox in thead section, it will automatically toggle all tbody section checkboxes -->
            <table class="js-table-checkable table table-hover">
                <thead>
                <tr>
                    <th class="text-center" style="width: 70px;">
                        <label class="css-control css-control-primary css-checkbox py-0">
                            <input type="checkbox" class="css-control-input" id="check-all" name="check-all">
                            <span class="css-control-indicator"></span>
                        </label>
                    </th>
                    <th>Name</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Access</th>
                    <th class="d-none d-sm-table-cell" style="width: 20%;">Date</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-center">
                        <label class="css-control css-control-primary css-checkbox">
                            <input type="checkbox" class="css-control-input" id="row_1" name="row_1">
                            <span class="css-control-indicator"></span>
                        </label>
                    </td>
                    <td>
                        <p class="font-w600 mb-10">Jack Estrada</p>
                        <p class="text-muted mb-0">Customer details and further information</p>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <span class="badge badge-info">Business</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">November 16, 2017 13:17</em>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <label class="css-control css-control-primary css-checkbox">
                            <input type="checkbox" class="css-control-input" id="row_2" name="row_2">
                            <span class="css-control-indicator"></span>
                        </label>
                    </td>
                    <td>
                        <p class="font-w600 mb-10">Lisa Jenkins</p>
                        <p class="text-muted mb-0">Customer details and further information</p>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <span class="badge badge-success">VIP</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">November 8, 2017 13:17</em>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <label class="css-control css-control-primary css-checkbox">
                            <input type="checkbox" class="css-control-input" id="row_3" name="row_3">
                            <span class="css-control-indicator"></span>
                        </label>
                    </td>
                    <td>
                        <p class="font-w600 mb-10">Laura Carr</p>
                        <p class="text-muted mb-0">Customer details and further information</p>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <span class="badge badge-warning">Trial</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">November 6, 2017 13:17</em>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <label class="css-control css-control-primary css-checkbox">
                            <input type="checkbox" class="css-control-input" id="row_4" name="row_4">
                            <span class="css-control-indicator"></span>
                        </label>
                    </td>
                    <td>
                        <p class="font-w600 mb-10">Marie Duncan</p>
                        <p class="text-muted mb-0">Customer details and further information</p>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <span class="badge badge-primary">Personal</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">November 2, 2017 13:17</em>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <label class="css-control css-control-primary css-checkbox">
                            <input type="checkbox" class="css-control-input" id="row_5" name="row_5">
                            <span class="css-control-indicator"></span>
                        </label>
                    </td>
                    <td>
                        <p class="font-w600 mb-10">Carol Ray</p>
                        <p class="text-muted mb-0">Customer details and further information</p>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <span class="badge badge-primary">Personal</span>
                    </td>
                    <td class="d-none d-sm-table-cell">
                        <em class="text-muted">November 9, 2017 13:17</em>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection