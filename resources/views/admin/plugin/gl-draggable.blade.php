@extends('admin.layouts.base')

@section('content')
    <h2 class="content-heading">Draggable Blocks
        <small>with jQueryUI</small>
    </h2>
    <div class="row js-draggable-items">
        <div class="col-md-4 draggable-column">
            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->
        </div>
        <div class="col-md-4 draggable-column">
            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->
        </div>
        <div class="col-md-4 draggable-column">
            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->

            <!-- Block -->
            <div class="block draggable-item">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Block</h3>
                    <div class="block-options">
                        <a class="btn-block-option draggable-handler" href="javascript:void(0)">
                            <i class="si si-cursor-move"></i>
                        </a>
                    </div>
                </div>
                <div class="block-content">
                    <p>Draggable &amp; Sortable..</p>
                </div>
            </div>
            <!-- END Block -->
        </div>
    </div>
@endsection
