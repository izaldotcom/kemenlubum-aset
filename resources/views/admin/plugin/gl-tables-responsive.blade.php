@extends('admin.layouts.base')

@section('content')
    <h2 class="content-heading">Responsive Tables</h2>

    <!-- Full Table -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Full Table</h3>
        </div>
        <div class="block-content">
            <p>The first way to make a table responsive, is to wrap it with <code>&lt;div class=&quot;table-responsive&quot;&gt;&lt;/div&gt;</code>.
                This way the table will be horizontally scrollable and all data will be accessible on smaller screens (&lt;
                768px).</p>
            <div class="table-responsive">
                <table class="table table-striped table-vcenter">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                        <th>Name</th>
                        <th style="width: 30%;">Email</th>
                        <th style="width: 15%;">Access</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">
                            <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar12.jpg" alt="">
                        </td>
                        <td class="font-w600">Brian Stevens</td>
                        <td>customer1@example.com</td>
                        <td>
                            <span class="badge badge-success">VIP</span>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Delete">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar13.jpg" alt="">
                        </td>
                        <td class="font-w600">David Fuller</td>
                        <td>customer2@example.com</td>
                        <td>
                            <span class="badge badge-success">VIP</span>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Delete">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar16.jpg" alt="">
                        </td>
                        <td class="font-w600">Jack Estrada</td>
                        <td>customer3@example.com</td>
                        <td>
                            <span class="badge badge-info">Business</span>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Delete">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar15.jpg" alt="">
                        </td>
                        <td class="font-w600">Ryan Flores</td>
                        <td>customer4@example.com</td>
                        <td>
                            <span class="badge badge-danger">Disabled</span>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Delete">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar14.jpg" alt="">
                        </td>
                        <td class="font-w600">Jesse Fisher</td>
                        <td>customer5@example.com</td>
                        <td>
                            <span class="badge badge-primary">Personal</span>
                        </td>
                        <td class="text-center">
                            <div class="btn-group">
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Edit">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip"
                                        title="Delete">
                                    <i class="fa fa-times"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Full Table -->

    <!-- Partial Table -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Partial Table</h3>
        </div>
        <div class="block-content">
            <p>The second way is to use <a href="be_ui_grid.html#cb-grid-rutil">responsive utility CSS classes</a> for
                hiding columns in various screen resolutions. This way you can hide less important columns and keep the
                most valuable on smaller screens. At the following example the <strong>Access</strong> column isn't
                visible on small and extra small screens and <strong>Email</strong> column isn't visible on extra small
                screens.</p>
            <table class="table table-striped table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;"><i class="si si-user"></i></th>
                    <th>Name</th>
                    <th class="d-none d-sm-table-cell" style="width: 30%;">Email</th>
                    <th class="d-none d-md-table-cell" style="width: 15%;">Access</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-center">
                        <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar14.jpg" alt="">
                    </td>
                    <td class="font-w600">Carl Wells</td>
                    <td class="d-none d-sm-table-cell">client1@example.com</td>
                    <td class="d-none d-md-table-cell">
                        <span class="badge badge-primary">Personal</span>
                    </td>
                    <td class="text-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar10.jpg" alt="">
                    </td>
                    <td class="font-w600">Jesse Fisher</td>
                    <td class="d-none d-sm-table-cell">client2@example.com</td>
                    <td class="d-none d-md-table-cell">
                        <span class="badge badge-info">Business</span>
                    </td>
                    <td class="text-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar11.jpg" alt="">
                    </td>
                    <td class="font-w600">Thomas Riley</td>
                    <td class="d-none d-sm-table-cell">client3@example.com</td>
                    <td class="d-none d-md-table-cell">
                        <span class="badge badge-warning">Trial</span>
                    </td>
                    <td class="text-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar12.jpg" alt="">
                    </td>
                    <td class="font-w600">Justin Hunt</td>
                    <td class="d-none d-sm-table-cell">client4@example.com</td>
                    <td class="d-none d-md-table-cell">
                        <span class="badge badge-info">Business</span>
                    </td>
                    <td class="text-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <img class="img-avatar img-avatar48" src="assets/media/avatars/avatar14.jpg" alt="">
                    </td>
                    <td class="font-w600">Wayne Garcia</td>
                    <td class="d-none d-sm-table-cell">client5@example.com</td>
                    <td class="d-none d-md-table-cell">
                        <span class="badge badge-warning">Trial</span>
                    </td>
                    <td class="text-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Delete">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection