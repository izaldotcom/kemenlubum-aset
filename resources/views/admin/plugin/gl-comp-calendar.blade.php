@extends('admin.layouts.base')

@section('content')
    <div class="block">
        <div class="block-content">
            <div class="row items-push">
                <div class="col-xl-9">
                    <!-- Calendar Container -->
                    <div class="js-calendar"></div>
                </div>
                <div class="col-xl-3 d-none d-xl-block">
                    <!-- Add Event Form -->
                    <form class="js-form-add-event mb-30" action="be_comp_calendar.html" method="post">
                        <div class="input-group">
                            <input type="text" class="js-add-event form-control" placeholder="Add Event..">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">
                                    <i class="fa fa-plus-circle"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- END Add Event Form -->

                    <!-- Event List -->
                    <ul class="js-events list list-events">
                        <li class="bg-info-light">Project Mars</li>
                        <li class="bg-success-light">Cinema</li>
                        <li class="bg-danger-light">Project X</li>
                        <li class="bg-warning-light">Skype Meeting</li>
                        <li class="bg-info-light">Codename PX</li>
                        <li class="bg-success-light">Weekend Adventure</li>
                        <li class="bg-warning-light">Meeting</li>
                        <li class="bg-success-light">Walk the dog</li>
                        <li class="bg-info-light">AI schedule</li>
                    </ul>
                    <div class="text-center">
                        <em class="font-size-xs text-muted"><i class="fa fa-arrows"></i> Drag and drop events on the
                            calendar</em>
                    </div>
                    <!-- END Event List -->
                </div>
            </div>
        </div>
    </div>
@endsection