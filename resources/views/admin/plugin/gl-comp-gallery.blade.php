@extends('admin.layouts.base')

@section('content')
    <!-- For more info and examples you can check out http://dimsemenov.com/plugins/magnific-popup/ -->
    <h2 class="content-heading">Gallery
        <small>Simple</small>
    </h2>
    <div class="row items-push js-gallery img-fluid-100">
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo17@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo17.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo18@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo18.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo19@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo19.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo20@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo20.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo21@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo21.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo22@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo22.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo23@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo23.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo24@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo24.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo25@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo25.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo26@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo26.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo27@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo27.jpg') }}" alt="">
            </a>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <a class="img-link img-link-zoom-in img-thumb img-lightbox"
               href="{{ asset('assets-admin/media/photos/photo28@2x.jpg') }}">
                <img class="img-fluid" src="{{ asset('assets-admin/media/photos/photo28.jpg') }}" alt="">
            </a>
        </div>
    </div>
    <!-- END Simple Gallery -->

    <!-- Advanced Gallery (.js-gallery class is initialized in Helpers.magnific()) -->
    <!-- For more info and examples you can check out http://dimsemenov.com/plugins/magnific-popup/ -->
    <h2 class="content-heading">Gallery
        <small>Advanced</small>
    </h2>
    <div class="row items-push js-gallery">
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo15.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo15@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo16.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo16@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo14.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo14@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo13.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo13@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo12.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo12@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo11.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo11@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo10.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo10@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo9.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo9@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo8.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo8@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo7.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo7@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo6.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo6@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xl-3 animated fadeIn">
            <div class="options-container fx-item-zoom-in fx-overlay-slide-down">
                <img class="img-fluid options-item" src="{{ asset('assets-admin/media/photos/photo5.jpg') }}" alt="">
                <div class="options-overlay bg-black-op-75">
                    <div class="options-overlay-content">
                        <h3 class="h4 text-white mb-5">Image</h3>
                        <h4 class="h6 text-white-op mb-15">More Details</h4>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-primary min-width-75 img-lightbox"
                           href="{{ asset('assets-admin/media/photos/photo5@2x.jpg') }}">
                            <i class="fa fa-search-plus"></i> View
                        </a>
                        <a class="btn btn-sm btn-rounded btn-noborder btn-alt-success min-width-75"
                           href="javascript:void(0)"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection