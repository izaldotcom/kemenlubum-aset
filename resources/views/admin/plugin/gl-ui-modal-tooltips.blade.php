@extends('admin.layouts.base')

@section('content')
    <h2 class="content-heading">Tooltips</h2>
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <!-- Top Tooltip -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Top</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-danger" data-toggle="tooltip" data-placement="top"
                            title="Top Tooltip">Show Tooltip
                    </button>
                </div>
            </div>
            <!-- END Top Tooltip -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Right Tooltip -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Right</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-danger" data-toggle="tooltip" data-placement="right"
                            title="Right Tooltip">Show Tooltip
                    </button>
                </div>
            </div>
            <!-- END Right Tooltip -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Bottom Tooltip -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Bottom</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-danger" data-toggle="tooltip" data-placement="bottom"
                            title="Bottom Tooltip">Show Tooltip
                    </button>
                </div>
            </div>
            <!-- END Bottom Tooltip -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Left Tooltip -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Left</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-danger" data-toggle="tooltip" data-placement="left"
                            title="Left Tooltip">Show Tooltip
                    </button>
                </div>
            </div>
            <!-- END Left Tooltip -->
        </div>
    </div>
    <!-- END Tooltips -->

    <!-- Popovers -->
    <!-- Bootstrap Popover (data-toggle="popover" and .js-popover class is initialized in Helpers.coreBootstrapPopover()) -->
    <!-- For advanced poover usage you can check out https://getbootstrap.com/docs/4.1/components/popovers/ -->
    <h2 class="content-heading">Popovers</h2>
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <!-- Top Popover -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Top</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-success" data-toggle="popover" title="Top Popover"
                            data-placement="top"
                            data-content="This is example content. You can put a description or more info here.">Show
                        Popover
                    </button>
                </div>
            </div>
            <!-- END Top Popover -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Right Popover -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Right</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-success" data-toggle="popover" title="Right Popover"
                            data-placement="right"
                            data-content="This is example content. You can put a description or more info here.">Show
                        Popover
                    </button>
                </div>
            </div>
            <!-- END Right Popover -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Bottom Popover -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Bottom</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-success" data-toggle="popover" title="Bottom Popover"
                            data-placement="bottom"
                            data-content="This is example content. You can put a description or more info here.">Show
                        Popover
                    </button>
                </div>
            </div>
            <!-- END Bottom Popover -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Left Popover -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Left</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-success" data-toggle="popover" title="Left Popover"
                            data-placement="left"
                            data-content="This is example content. You can put a description or more info here.">Show
                        Popover
                    </button>
                </div>
            </div>
            <!-- END Left Popover -->
        </div>
    </div>
    <!-- END Popovers -->

    <!-- Bootstrap Modals -->
    <!-- For advanced modal usage you can check out https://getbootstrap.com/docs/4.1/components/modal/ -->
    <h2 class="content-heading">Bootstrap Modals</h2>
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <!-- Normal Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Normal</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-info" data-toggle="modal" data-target="#modal-normal">
                        Launch Modal
                    </button>
                </div>
            </div>
            <!-- END Normal Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Small Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Small</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-info" data-toggle="modal" data-target="#modal-small">Launch
                        Modal
                    </button>
                </div>
            </div>
            <!-- END Small Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Large Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Large</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-info" data-toggle="modal" data-target="#modal-large">Launch
                        Modal
                    </button>
                </div>
            </div>
            <!-- END Large Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Fade In Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Fade In
                        <small>Effect</small>
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-info" data-toggle="modal" data-target="#modal-fadein">
                        Launch Modal
                    </button>
                </div>
            </div>
            <!-- END Fade In Modal -->
        </div>
    </div>
    <!-- Bootstrap Modals -->

    <!-- Extra Modal Options -->
    <h2 class="content-heading">Extra Modal Options</h2>
    <div class="row">
        <div class="col-md-6 col-xl-3">
            <!-- Top Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Top Position</h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-warning" data-toggle="modal" data-target="#modal-top">
                        Launch Modal
                    </button>
                </div>
            </div>
            <!-- END Top Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Pop In Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Pop In
                        <small>Effect</small>
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-warning" data-toggle="modal" data-target="#modal-popin">
                        Launch Modal
                    </button>
                </div>
            </div>
            <!-- END Pop In Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Pop Out Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Pop Out
                        <small>Effect</small>
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-warning" data-toggle="modal" data-target="#modal-popout">
                        Launch Modal
                    </button>
                </div>
            </div>
            <!-- END Pop Out Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Slide Up Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Slide Up
                        <small>Effect</small>
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-warning" data-toggle="modal" data-target="#modal-slideup">
                        Launch Modal
                    </button>
                </div>
            </div>
            <!-- END Slide Up Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Slide Right Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Slide Right
                        <small>Effect</small>
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-warning" data-toggle="modal"
                            data-target="#modal-slideright">Launch Modal
                    </button>
                </div>
            </div>
            <!-- END Slide Right Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- Slide Left Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Slide Left
                        <small>Effect</small>
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-warning" data-toggle="modal"
                            data-target="#modal-slideleft">Launch Modal
                    </button>
                </div>
            </div>
            <!-- END Slide Left Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- From Right Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">From Right
                        <small>Effect</small>
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-warning" data-toggle="modal"
                            data-target="#modal-fromright">Launch Modal
                    </button>
                </div>
            </div>
            <!-- END From Right Modal -->
        </div>
        <div class="col-md-6 col-xl-3">
            <!-- From Left Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">From Left
                        <small>Effect</small>
                    </h3>
                </div>
                <div class="block-content block-content-full">
                    <button type="button" class="btn btn-alt-warning" data-toggle="modal" data-target="#modal-fromleft">
                        Launch Modal
                    </button>
                </div>
            </div>
            <!-- END From Left Modal -->
        </div>
    </div>
    <!-- Extra Modal Options -->

    <!-- JS Modal Methods Modals -->
    <!-- For all modal methods you can check out https://getbootstrap.com/docs/4.1/components/modal/#via-javascript -->
    <h2 class="content-heading">JS Modal Methods</h2>
    <div class="row">
        <div class="col-xl-4">
            <!-- Show Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Show Modal</h3>
                </div>
                <div class="block-content block-content-full">
                    <code>jQuery('#myModalId').modal('show');</code>
                </div>
            </div>
            <!-- END Show Modal -->
        </div>
        <div class="col-xl-4">
            <!-- Hide Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Hide Modal</h3>
                </div>
                <div class="block-content block-content-full">
                    <code>jQuery('#myModalId').modal('hide');</code>
                </div>
            </div>
            <!-- END Hide Modal -->
        </div>
        <div class="col-xl-4">
            <!-- Toggle Modal -->
            <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Toggle Modal</h3>
                </div>
                <div class="block-content block-content-full">
                    <code>jQuery('#myModalId').modal('toggle');</code>
                </div>
            </div>
            <!-- END Toggle Modal -->
        </div>
    </div>
@endsection