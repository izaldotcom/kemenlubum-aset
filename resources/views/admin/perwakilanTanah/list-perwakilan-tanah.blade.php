@extends('admin.layouts.base')

@section('scripts')
    <script type="text/javascript">
        $(document).ready(() => {
            window.app.params.filter = "{{ !empty($filter) ? $filter : ""  }}";
            window.eventHub.$emit('refresh-ajaxtable', 'data-tanah');
        });

    </script>
     <script type="text/javascript">
        window.onload = function () {
          var table = document.getElementById('data-table'),
          download = document.getElementById('download');

          download.addEventListener('click', function () {
            window.open('data:application/vnd.ms-excel,' + encodeURIComponent(table.outerHTML));
          });
        }
      </script>
<script>
    function exportHTML(){
     var header = "<html xmlns:o='urn:schemas-microsoft-com:office:office' "+
          "xmlns:w='urn:schemas-microsoft-com:office:word' "+
          "xmlns='http://www.w3.org/TR/REC-html40'>"+
          "<head><meta charset='utf-8'><title>Export HTML to Word Document with JavaScript</title></head><body>";
     var footer = "</body></html>";
     var sourceHTML = header+document.getElementById("source-html").innerHTML+footer;

     var source = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(sourceHTML);
     var fileDownload = document.createElement("a");
     document.body.appendChild(fileDownload);
     fileDownload.href = source;
     fileDownload.download = 'document.doc';
     fileDownload.click();
     document.body.removeChild(fileDownload);
  }
  </script>
@endsection

@section('content')
    <nav class="breadcrumb bg-white push">
        <a class="breadcrumb-item" href="javascript:void(0)">Dasbor</a>
        <span class="breadcrumb-item active">Daftar Perwakilan Asset Tanah</span>
    </nav>

    <div class="row">
        <!-- Row #2 -->
        <div class="col-md-12">
            <div class="block" id="mythead">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Daftar Perwakilan Asset Tanah</h3>
                </div>
                <button id="btn-export" class="btn btn-success px-4" onclick="exportHTML();">Export</button>
                <div class="block-content long-table">
                    <ajax-tables
                            :url="'{{ route('admin_api_get_tanah' ) }}'"
                            :oid="'data-tanah'"
                            :params="params"
                            :config="{
                                  autoload: false,
                                  show_all: false,
                                  has_number: true,
                                  has_entry_page: false,
                                  has_pagination: false,
                                  has_action: false,
                                  has_search_input: false,
                                  has_search_header: true,
                                  custom_header: '',
                                  default_sort: 'id',
                                  default_sort_dir: 'desc',
                                  custom_empty_page: false,
                                  search_placeholder: 'Cari',
                                  class: {
                                    table: ['table-condensed'],
                                    wrapper: ['table-responsive'],
                                  }
                            }" id="source-html"
                            :rowparams="{tanggal: params.nama_kedutaan}"
                            :rowtemplate="'tr-data-perwakilan-tanah'"
                            :columns="{
                                  'nama_satker': 'Nama Satker',
                                  'nama_barang': 'Nama Barang',
                                  'kondisi': 'Kondisi',
                                  'merk_type': 'Merk/Type',
                            }">
                    </ajax-tables>
                </div>
            </div>
        </div>
    </div>

    {{--    <div class="row" data-toggle="appear">--}}
    {{--        <div class="col-md-12">--}}
    {{--            <div class="block">--}}
    {{--                <div class="block-header block-header-default">--}}
    {{--                    <h3 class="block-title">Log Action</h3>--}}
    {{--                </div>--}}
    {{--                <div class="block-content block-content-full">--}}
    {{--                    <table class="table table-bordered table-striped table-vcenter table-sm-responsive">--}}
    {{--                        <tbody>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Januari 2019 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Maret 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Gedung</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Februari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">11 Februari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Gedung</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">19 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">12 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Gedung</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">10 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Susan Day at Rumah Negara</td>--}}
    {{--                        </tr>--}}
    {{--                        <tr>--}}
    {{--                            <td class="date">05 Januari 2018 11.12.56</td>--}}
    {{--                            <td class="inf">Ralph Murray at Gedung</td>--}}
    {{--                        </tr>--}}
    {{--                        </tbody>--}}
    {{--                    </table>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection

@section('modal')
    <modal-edit-long-lat tipe="tanah"></modal-edit-long-lat>
    <modal-edit-photos tipe="tanah"></modal-edit-photos>
@endsection
