@extends('layouts.base')

@section('styles')
    <link rel="stylesheet" id="css-main" href="{{ asset('assets-admin/css/codebase.css') }}">
    <style>
        #page-container.main-content-boxed > #main-container .content, #page-container.main-content-boxed > #page-footer .content, #page-container.main-content-boxed > #page-header .content, #page-container.main-content-boxed > #page-header .content-header {
            max-width: 100%;
        }

        @media (min-width: 768px) {
            .content-heading {
                margin-bottom: 25px;
                padding-top: 0;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-menu">
        <div class="row d-flex justify-content-center">
            <div class="banner-kota col-11 align-items-center d-flex justify-content-around"
                 style="margin-top: 2rem;margin-bottom: 4rem">
                <div class="banner-center w-100">
                    <div class="title-banner">GEDUNG BERDASARKAN KONDISI</div>
                    <img src="{{ asset('assets/images/kota.png') }}" alt="" class="img-fluid w-100">
                </div>
            </div>
            <div class="banner-kota col-11 ">
                <div id="page-container" class="enable-page-overlay side-scroll page-header-modern main-content-boxed">
                    <main id="main-container">
                        <div class="content p-0">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="block">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">Pie</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option"
                                                        data-toggle="block-option" data-action="state_toggle"
                                                        data-action-mode="demo">
                                                    <i class="si si-refresh"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content">
                                            <div class="row items-push-2x text-center invisible" data-toggle="appear">
                                                <div class="col-6 col-md-4">
                                                    <!-- Sparkline Pie 1 Container -->
                                                    <span class="js-slc-pie1">15,25,26,18</span>
                                                    <div class="font-w600 mt-10">
                                                        <i class="fa fa-ticket text-muted mr-5"></i> Tickets
                                                    </div>
                                                </div>
                                                <div class="col-6 col-md-4">
                                                    <!-- Sparkline Pie 2 Container -->
                                                    <span class="js-slc-pie2">1500,1350,1280,800</span>
                                                    <div class="font-w600 mt-10">
                                                        <i class="fa fa-line-chart text-muted mr-5"></i> Earnings
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <!-- Sparkline Pie 3 Container -->
                                                    <span class="js-slc-pie3">350,390,420,375</span>
                                                    <div class="font-w600 mt-10">
                                                        <i class="fa fa-suitcase text-muted mr-5"></i> Sales
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <!-- Bars Chart -->
                                    <div class="block">
                                        <div class="block-header block-header-default">
                                            <h3 class="block-title">Bars</h3>
                                            <div class="block-options">
                                                <button type="button" class="btn-block-option"
                                                        data-toggle="block-option" data-action="state_toggle"
                                                        data-action-mode="demo">
                                                    <i class="si si-refresh"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="block-content block-content-full text-center">
                                            <canvas class="js-chartjs-bars"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('assets-admin/js/codebase.core.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/codebase.app.min.js') }}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('assets-admin/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.pie.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.stack.min.js') }}"></script>
    <script src="{{ asset('assets-admin/js/plugins/flot/jquery.flot.resize.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('assets-admin/js/pages/be_comp_charts.min.js') }}"></script>

    <!-- Page JS Helpers (Easy Pie Chart Plugin) -->
    <script>jQuery(function () {
            Codebase.helpers('easy-pie-chart');
        });</script>
@endsection