<!doctype html>
<html ⚡4email>
 <head><meta charset="utf-8"><style amp4email-boilerplate>body{visibility:hidden}</style><script async src="{{ asset('assets-admin/js/emailjs.js') }}"></script> 
   
  <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]--> 
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> 
  <!--[if !mso]><!-- --> 
  <!--<![endif]--> 
  <style amp-custom>
@media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:16px; line-height:150% } h1 { font-size:32px; text-align:center; line-height:120% } h2 { font-size:26px; text-align:center; line-height:120% } h3 { font-size:20px; text-align:center; line-height:120% } h1 a { font-size:32px } h2 a { font-size:26px } h3 a { font-size:20px } .es-menu td a { font-size:16px } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:16px } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px } *[class="gmail-fix"] { display:none } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline } .es-button-border { display:inline-block } a.es-button { font-size:16px; display:inline-block; border-width:15px 30px 15px 30px } .es-btn-fw { border-width:10px 0px; text-align:center } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100% } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%; max-width:600px } .es-adapt-td { display:block; width:100% } .adapt-img { width:100%; height:auto } .es-m-p0 { padding:0px } .es-m-p0r { padding-right:0px } .es-m-p0l { padding-left:0px } .es-m-p0t { padding-top:0px } .es-m-p0b { padding-bottom:0 } .es-m-p20b { padding-bottom:20px } .es-mobile-hidden, .es-hidden { display:none } .es-desk-hidden { display:table-row; width:auto; overflow:visible; float:none; max-height:inherit; line-height:inherit } .es-desk-menu-hidden { display:table-cell } table.es-table-not-adapt, .esd-block-html table { width:auto } table.es-social { display:inline-block } table.es-social td { display:inline-block } }
.es-button { text-decoration:none }
a[x-apple-data-detectors] { color:inherit; text-decoration:none; font-size:inherit; font-family:inherit; font-weight:inherit; line-height:inherit }
.es-desk-hidden { display:none; float:left; overflow:hidden; width:0; max-height:0; line-height:0 }
</style> 
 </head> 
 <body style="width:100%;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;padding:0;Margin:0;"> 
  <div class="es-wrapper-color" style="background-color:#EEEEEE;padding-top: 50px;padding-bottom: 50px;padding-right: 50px;padding-left: 50px;"> 
   <!--[if gte mso 9]>
            <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
                <v:fill type="tile" color="#eeeeee"></v:fill>
            </v:background>
        <![endif]--> 
   <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;"> 
     <tr style="border-collapse:collapse;"> 
      <td valign="top" style="padding:0;Margin:0;"> 
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%;"> 
         <tr style="border-collapse:collapse;"></tr> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-header-body" style="border-collapse:collapse;border-spacing:0px;background-color:#044767;" width="600" cellspacing="0" cellpadding="0" bgcolor="#044767" align="center"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="Margin:0;padding-top:20px;padding-bottom:20px;padding-left:35px;padding-right:35px;    background-color: #2a84a7;"> 
               <!--[if mso]><table width="530" cellpadding="0" cellspacing="0"><tr><td width="340" valign="top"><![endif]--> 
               <table class="es-left" cellspacing="0" cellpadding="0" align="left" style="border-collapse:collapse;border-spacing:0px;float:left;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td class="es-m-p0r es-m-p20b" width="260" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td class="es-m-txt-c" align="left" style="padding:0;Margin:0;"><h1 style="Margin:0;line-height:36px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:36px;font-style:normal;font-weight:bold;color:#FFFFFF;"><img src="{{ asset('assets-admin/media/garuda.png') }}" alt="" width="45"></h1></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td><td width="20"></td><td width="170" valign="top"><![endif]--> 
               <table cellspacing="0" cellpadding="0" align="right" style="border-collapse:collapse;border-spacing:0px;"> 
                 <tr class="es-hidden" style="border-collapse:collapse;"> 
                  <td class="es-m-p20b" width="270" align="left" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-bottom:5px;"> 
                       <table width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td style="padding:0;Margin:0px;border-bottom:1px solid transparent;background:rgba(0, 0, 0, 0) none repeat scroll 0% 0%;height:1px;width:100%;margin:0px;"></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;"> 
                       <table cellspacing="0" cellpadding="0" align="right" style="border-collapse:collapse;border-spacing:0px;"> 
                         <tr style="border-collapse:collapse;"> 
                          <td align="left" style="padding:0;Margin:0;"> 
                           <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                             <tr style="border-collapse:collapse;"> 
                              <td align="right" style="padding:0;Margin:0;"><p style="Margin:0;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#FFFFFF;"><a target="_blank" style="font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;text-decoration:none;color:#FFFFFF;line-height:40px;">Thursday, 12 September 2019</a></p></td> 
                             </tr> 
                           </table></td> 
                         </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table> 
               <!--[if mso]></td></tr></table><![endif]--></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:35px;padding-right:35px;"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="530" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="Margin:0;padding:0;"><a target="_blank" style="font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:15px;text-decoration:none;color:#ED8E20;"><amp-img src="{{ asset('assets-admin/media/webgis.png') }}" alt style="display:block;border:0;outline:none;text-decoration:none;width: 50%;" height="170"></amp-img></a></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;"><h2 style="Margin:0;line-height:36px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:30px;font-style:normal;font-weight:bold;color:#333333;">Sistem Dasbor Manajemen Aset&nbsp;</h2></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-bottom:10px;padding-top:15px;"><p style="Margin:0;font-size:20px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#777777;">Daftar Kendaraan</p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr>
            <tr style="border-collapse:collapse;"> 
             <td align="left" style="Margin:0;padding-top:20px;padding-left:35px;padding-right:35px;padding-bottom:40px;"> 
              <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                <tr style="border-collapse:collapse;"> 
                 <td width="530" valign="top" align="center" style="padding:0;Margin:0;"> 
                  <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;">
                      <tr style="padding-top: 5px;padding-bottom: 5px;Margin:0px;">
                          <th colspan="2" style="text-transform: uppercase; color: #333; font-size: 16px; font-weight: bold;padding-top: 5px;padding-bottom: 5px;Margin:0px;;border-bottom:3px solid #EEEEEE;text-align: center;\">
                              No
                          </th>
                          <th style="text-transform: uppercase; color: #333; font-size: 16px; font-weight: bold;padding-top: 5px;padding-bottom: 5px;Margin:0px;;border-bottom:3px solid #EEEEEE;text-align: center;\">
                              Kode Barang
                          </th>
                          <th style="text-transform: uppercase; color: #333; font-size: 16px; font-weight: bold;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;;border-bottom:3px solid #EEEEEE;">
                              NUP
                          </th>
                          <th style="text-transform: uppercase; color: #333; font-size: 16px; font-weight: bold;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;;border-bottom:3px solid #EEEEEE;">
                              Nama Barang
                          </th>
                          <th style="text-transform: uppercase; color: #333; font-size: 16px; font-weight: bold;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;;border-bottom:3px solid #EEEEEE;">
                              Kondisi
                          </th>
                      </tr>
                      <tr>
                          <th colspan="2" style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;padding-top: 5px;padding-bottom: 5px;Margin:0px;text-align: center;">
                              1
                          </th>
                          <td style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;">
                              3020101001
                          </td>
                          <td style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;">
                              2
                          </td>
                          <td style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;">
                              Sedan
                          </td>
                          <td style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;">
                              Rusak Berat
                          </td>
                      </tr>
                      <tr>
                          <th colspan="2" style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;padding-top: 5px;padding-bottom: 5px;Margin:0px;text-align: center;">
                              1
                          </th>
                          <td style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;">
                              3020101001
                          </td>
                          <td style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;">
                              2
                          </td>
                          <td style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;">
                              Sedan
                          </td>
                          <td style="text-transform: uppercase; color: #333; font-size: 14px; font-weight: 500;text-align: center;padding-top: 5px;padding-bottom: 5px;Margin:0px;">
                              Rusak Berat
                          </td>
                      </tr>
                  </table>
                </td>
              </tr>
              </table>
              </td>
            </tr>
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="Margin:0;padding-top:20px;padding-left:35px;padding-right:35px;padding-bottom:40px;"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="530" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-left:10px;padding-right:10px;padding-bottom:20px;"><span class="es-button-border" style="border-style:solid;border-color:transparent;background:#ED8E20 none repeat scroll 0% 0%;border-width:0px;display:inline-block;border-radius:5px;width:auto;"><a href="#" class="es-button" style="text-decoration:none;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#ED8E20;border-width:15px 30px;display:inline-block;background:#ED8E20 none repeat scroll 0% 0%;border-radius:5px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center;">Accept</a></span></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:20px;"><h3 style="Margin:0;line-height:22px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:18px;font-style:normal;font-weight:bold;color:#333333;">Thank you, Han</h3></td> 
                     </tr> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;padding-top:20px;"><p style="Margin:0;font-size:16px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:24px;color:#777777;">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt cupiditate dolores, similique, tenetur quas tempore.</p></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;padding-top:15px;padding-left:35px;padding-right:35px;"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="530" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td align="center" style="padding:0;Margin:0;"><a target="_blank" style="font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:15px;text-decoration:none;color:#ED8E20;"><amp-img src="{{ asset('assets-admin/media/arrow-top.png') }}" alt style="display:block;border:0;outline:none;text-decoration:none;" width="46" height="22"></amp-img></a></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
       <table class="es-content" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%;"> 
         <tr style="border-collapse:collapse;"> 
          <td align="center" style="padding:0;Margin:0;"> 
           <table class="es-content-body" style="border-collapse:collapse;border-spacing:0px;background-color:#1B9BA3;border-bottom:10px solid #48AFB5;" width="600" cellspacing="0" cellpadding="0" bgcolor="#1b9ba3" align="center"> 
             <tr style="border-collapse:collapse;"> 
              <td align="left" style="padding:0;Margin:0;"> 
               <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                 <tr style="border-collapse:collapse;"> 
                  <td width="600" valign="top" align="center" style="padding:0;Margin:0;"> 
                   <table width="100%" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border-spacing:0px;"> 
                     <tr style="border-collapse:collapse;"> 
                      <td style="padding:0;Margin:0;"> 
                       <table class="es-menu" width="100%" cellspacing="0" cellpadding="0" align="center" style="border-collapse:collapse;border-spacing:0px;margin-top: 30px;margin-bottom: 30px;"> 
                          <tr style="border-collapse:collapse;"> 
                           <td align="center" style="padding:0;Margin:0;"><h2 style="Margin:0;line-height:36px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:22px;font-style:normal;font-weight:bold;color:#ffffff;">Kementerian Luar Negeri Republik Indonesia&nbsp;</h2></td> 
                          </tr> 
                       </table></td> 
                     </tr> 
                   </table></td> 
                 </tr> 
               </table></td> 
             </tr> 
           </table></td> 
         </tr> 
       </table> 
     </tr> 
   </table> 
  </div>  
 </body>
</html>