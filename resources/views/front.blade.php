@extends('layouts.base')

@section('content')
    <div class="main-menu mx-0 row w-100 d-flex justify-content-center align-items-center">
        @if (Auth::user()->hasRole(['super_admin', 'admin', 'pimpinan', 'staff_kendaraan', 'staff_bangunan', 'admin_kendaraan','admin_bangunan','admin_aset','admin_pajak_asuransi' ]))
        <div class="col-5 col-md-3 mr-1 mr-md-3">
            <div class="">
                <a href="https://kendaraan.kemlu.go.id/" title="Jemputanku"><img
                        src="{{ asset('assets/images/jemputanku.png') }}" alt="" class="w-100 img-fluid"></a>
                <div class="title-front">JemputanKu</div>
            </div>
        </div>
            <div class="col-5 col-md-3 ml-1 ml-md-3">
            <div class="">
                <a href="{{ url('/assets-management') }}" title="Manajemen Asset"><img
                        src="{{ asset('assets/images/asset-white.png') }}" alt="" class="w-100 img-fluid"></a>
                <div class="title-front">Manajemen Aset</div>
            </div>
        </div>
    </div>
    @endif
    @if (Auth::user()->hasRole(['admin_satker']))
    <div class="main-menu mx-0 row w-100 d-flex justify-content-center align-items-center">
        <div class="col-5 col-md-3 ml-1 ml-md-3">
    <div class="">
        <a href="{{ url('/assets-management') }}" title="Manajemen Asset"><img
                src="{{ asset('assets/images/asset-white.png') }}" alt="" class="w-10 img-fluid"></a>
        <div class="title-front">Manajemen Aset</div>
    </div>
        </div>
    </div>
@endif
@endsection
