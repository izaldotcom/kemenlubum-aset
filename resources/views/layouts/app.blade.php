
<html>
<head>
    <meta charset="UTF-8">
    <title>Dasbor</title>
    @include('layouts.htmlheader')
</head>
<body class="body-inverse">
<div id="app">
    <header class="header-body d-flex align-items-center">
        <a href="{{ url('/') }}" class="img-garuda">
            <img src="{{ asset('assets/images/garuda.png') }}" width="50">
        </a>
        <div class="header-command">
            <div class="title-content">Sistem Manajemen Aset</div>
            <div class="title-content">Kementerian Luar Negeri Republik Indonesia</div>
        </div>
        <div class="button-bar">
            <a href="{{ url('/') }}" class="btn p-0">
                <img src="{{ asset('assets/images/btn-dashboard.png') }}"
                     width="115">
            </a>
        </div>
    </header>
    <div class="main-inverse">
        @yield('content')
    </div>
</div>
<!-- /.wrapper -->
<script type="text/javascript">
    window.base_url   = "{{ url('/') }}";
    window.base_path  = "{{ asset('/') }}";
    window.csrf_token = "{{ csrf_token() }}";
</script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@include('layouts.basescripts')
</body>
</html>
