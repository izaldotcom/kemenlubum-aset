<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/jquery.rwdImageMaps.min.js') }}" type="text/javascript"></script>

{{-- 
<script src="{{ asset('assets/js/dnSlide.js') }}"></script>
<script src="{{ asset('assets-admin/js/expandJS.js') }}"></script>
<script>
    $.fn.equalHeights = function(){
        var max_height = 0;
        $(this).each(function(){
            max_height = Math.max($(this).height(), max_height);
        });
        $(this).each(function(){
            $(this).height(max_height);
        });
    };
    jQuery(document).ready(function($) {
            $(".dnSlide-main").each(function(index, el) {
                var setting = {
                    "response" : true ,
                    afterClickBtnFn :function(i){ console.log(i); }
                };
                switch (index) {
                    case 0:
                        setting.verticalAlign = "center" ;
                        setting.switching     = "custom" ;
                        setting.precentWidth  = "50%" ;
                        var api = $(el).dnSlide(setting).data( "dnSlide" ); 
                        // $(".hide").on("click",function(){
                        //     api.hide(function(){
                        //         alert('HIDEEN！！！');
                        //     });
                        // });
                        // $(".show").on("click",function(){
                        //     api.show(function(){
                        //         alert('SHOW！！！');
                        //     });
                        // });
                        break;
                    case 1:
                        setting.autoPlay  =  true ;
                        $(el).dnSlide(setting); 
                        break;
                    case 2:
                        setting.verticalAlign = "bottom" ;
                        $(el).dnSlide(setting); 
                        break;
                    default:
                        $(el).dnSlide(setting); 
                        break;
                }
            });
        });
</script> --}}
<script>
    // $(document).ready(function () {
    //     $('body').on('click', '.vue-map-container .show-marker-info', function(){
    //         alert('hello');
    //     });
    // });
    $(document).ready(function () {
        $("input#cari-menu").focusin(function () {
            $(".dropdown-search").addClass('open-dropdown');
            $('.dropdown-search').hover(function () {
                    $(".dropdown-search").addClass('open-dropdown');
                },
                function () {
                    $(".dropdown-search").removeClass('open-dropdown');
                });
        });
        $("input#cari-menu").click(function () {
            $(".dropdown-search").addClass('open-dropdown');
            $('.dropdown-search').hover(function () {
                    $(".dropdown-search").addClass('open-dropdown');
                },
                function () {
                    $(".dropdown-search").removeClass('open-dropdown');
                });
        });
    });
    $(document).ready(function () {
        $("input#cari-menu-filter").focusin(function () {
            $(".dropdown-search").addClass('open-dropdown-filter');
            $('.dropdown-search').hover(function () {
                    $(".dropdown-search-filter").addClass('open-dropdown-filter');
                },
                function () {
                    $(".dropdown-search-filter").removeClass('open-dropdown-filter');
                });
        });
        $("input#cari-menu-filter").click(function () {
            $(".dropdown-search-filter").addClass('open-dropdown-filter');
            $('.dropdown-search-filter').hover(function () {
                    $(".dropdown-search-filter").addClass('open-dropdown-filter');
                },
                function () {
                    $(".dropdown-search-filter").removeClass('open-dropdown-filter');
                });
        });
    });
    $(document).ready(function () {
        $('.open-sidebar').on('click', function () {
            // $(this).toggleClass('active');
            // $('.sidebar-maps').toggleClass('active');
            $('#scrollbar').scrollTop(0);
        });
        // $('#view-list-more').on('click', function () {
        //     $('.content-list-maps').show();
        //     $('.content-filter-country').hide();
        //     $('#scrollbar').scrollTop(0);
        //     $('.content-filter-maps').hide();
        // });
        // $('.close-list-maps').on('click', function () {
        //     $('.content-list-maps').hide();
        //     $('.content-filter-country').hide();
        //     $('#scrollbar').scrollTop(0);
        //     $('.content-filter-maps').show();
        // });
        // $('.open-country').on('click', function () {
        //     $('.content-list-maps').hide();
        //      $('.content-filter-maps').hide();
        //     $('#scrollbar').scrollTop(0);
        //     $('.content-filter-country').show();
        // });
        // $('.list-detail-country').on('click', function () {
        //     $('.content-list-maps').show();
        //     $('.content-filter-country').hide();
        //     $('#scrollbar').scrollTop(0);
        //     $('.content-filter-maps').hide();
        // });
    });
    $(document).ready(function (e) {
        // $('img[usemap]').rwdImageMaps();

        $('.menu-dashboard').on('click', function () {
            $(".menu-home").toggleClass('open-menu');
        });
    });

    var h = $(window).height() - 40;
    // $('#map-landscape').css({"opacity" : "0", "width": "0"})
    // $('#map-potrait').css({"opacity" : "0", "width": "0"})

    var width  = $(window).width();
    var height = $(window).height();

    if (width < height) {
        $('#map-landscape').css({"opacity": "0", "width": "0"})
        $('#map-darkmode').css({"opacity": "0", "width": "0"})
        $('#map-potrait').css({"opacity": "1", "width": "75%", "transform": "scale(1,1)"})
        $('#map-potrait-dark').css({"opacity": "1", "width": "75%", "transform": "scale(1,1)"})
        $('.img-ssl').addClass("mobile").css({'opacity': 1})
    } else {
        $('#map-potrait').css({"opacity": "0", "width": "0"})
        $('#map-potrait-dark').css({"opacity": "0", "width": "0"})
        $('#map-landscape').css({"opacity": "1", "width": "", "transform": "scale(1,1)"})
        $('#map-darkmode').css({"opacity": "1", "width": "", "transform": "scale(1,1)"})
        $('.img-ssl').removeClass("mobile").css({'opacity': 1})
    }

    window.onresize = function (event) {
        // $('#map-landscape').css({"opacity" : "0", "width": "0"})
        // $('#map-potrait').css({"opacity" : "0", "width": "0"})

        var width  = $(window).width();
        var height = $(window).height();

        if (width < height) {
            $('#map-landscape').css({"opacity": "0", "width": "0"})
            $('#map-darkmode').css({"opacity": "0", "width": "0"})
            $('#map-potrait').css({"opacity": "1", "width": "75%", "transform": "scale(1,1)"})
            $('#map-potrait-dark').css({"opacity": "1", "width": "75%", "transform": "scale(1,1)"})
            $('.img-ssl').addClass("mobile").css({'opacity': 1})
        } else {
            $('#map-potrait').css({"opacity": "0", "width": "0"})
            $('#map-potrait-dark').css({"opacity": "0", "width": "0"})
            $('#map-landscape').css({"opacity": "1", "width": "", "transform": "scale(1,1)"})
            $('#map-darkmode').css({"opacity": "1", "width": "", "transform": "scale(1,1)"})
            $('.img-ssl').removeClass("mobile").css({'opacity': 1})
        }
    }
</script>