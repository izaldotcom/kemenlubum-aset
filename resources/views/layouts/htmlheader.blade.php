<link rel="shortcut icon" href="{{ asset('assets/images/garuda.png') }}">
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
{{--<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"--}}
{{--      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">--}}
<link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
{{-- <link href="{{ asset('assets/css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css"/> --}}
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('assets/css/keyframe.css') }}" rel="stylesheet" type="text/css"/>
@if ($dark_mode_front == 'true')
    <link rel="stylesheet" id="css-main" href="{{ asset('assets/css/style-darkmode.css') }}">
@endif

@yield('styles')
