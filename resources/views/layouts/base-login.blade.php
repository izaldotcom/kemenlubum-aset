
<html>
<head>
    <meta charset="UTF-8">
    <title>Dasbor</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="csrf-token" content="{{csrf_token()}}">
    @include('layouts.htmlheader')
    @yield('styles')
</head>
<body class="body-inverse">
<div id="app">
    <header class="header-body d-flex align-items-center">
        <a href="{{ url('/') }}" class="img-garuda">
            <img src="{{ asset('assets/images/garuda.png') }}" width="50">
        </a>
        <div class="header-command header-login">
            <div class="title-content">Sistem Manajemen Aset</div>
            <div class="title-content">Kementerian Luar Negeri Republik Indonesia</div>
        </div>
        <div class="img-ssl header-login">
            {{--            <img src="{{ asset('assets/images/ssl-logo.png') }}">--}}
        </div>
        {{-- <div class="login-bar">
            <a href="{{ url('/login') }}">
                <button class="btn btn-success px-4">Login</button>
            </a>
        </div> --}}
        {{-- <div id="search-bar" class="search-bar">
            <input id="cari-menu" class="form-search" placeholder="Cari Menu">
            <div class="dropdown-search">
                <a href="{{ url('/searches') }}" class="dropdown-item no-hide"><i class="fa fa-home mr-3"></i>Rumah
                    Negara</a>
                <a href="{{ url('/searches') }}" class="dropdown-item no-hide"><i class="fa fa-building mr-3"></i>Gedung</a>
            </div>
        </div> --}}
    </header>
    <div class="main-inverse">
        @yield('content')
    </div>
</div>
<!-- /.wrapper -->
<script type="text/javascript">
    window.base_url   = "{{ url('/') }}";
    window.base_path  = "{{ asset('/') }}";
    window.csrf_token = "{{ csrf_token() }}";
</script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@include('layouts.basescripts')
@yield('scripts')
</body>
</html>
