
<head>
    <meta charset="UTF-8">
    <title>Dasbor</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta
    <meta name="csrf-token" content="{{csrf_token()}}">
    @include('layouts.htmlheader')
    @yield('styles')
</head>
<body class="body-inverse">
<div id="app">
    <header class="header-body d-flex align-items-center">
        <a href="{{ url('/') }}" class="img-garuda">
            <img src="{{ asset('assets/images/garuda.png') }}" width="50">
        </a>
        <div class="header-command">
            <div class="title-content">Sistem Manajemen Aset</div>
            <div class="title-content">Kementerian Luar Negeri Republik Indonesia</div>
        </div>
        <div class="img-ssl">
            {{--            <img src="{{ asset('assets/images/ssl-logo.png') }}">--}}
        </div>
        <div class="login-bar">
            <a href="{{ url('/login') }}">
                <button class="btn btn-success px-4">
                    @if (Auth::check())
                        {{ Auth::user()->name }}
                    @else
                        Admin
                    @endif
                </button>
            </a>
        </div>
        <dark-mode-front></dark-mode-front>
        {{-- <div id="search-bar" class="search-bar">
            <input id="cari-menu" class="form-search" placeholder="Cari Menu">
            <div class="dropdown-search">
                <a href="{{ url('/searches') }}" class="dropdown-item no-hide"><i class="fa fa-home mr-3"></i>Rumah
                    Negara</a>
                <a href="{{ url('/searches') }}" class="dropdown-item no-hide"><i class="fa fa-building mr-3"></i>Gedung</a>
            </div>
        </div> --}}
    </header>
    <div id="printedElement" style="display:none;">
    </div>
    <div class="main-inverse">
        @yield('content')
    </div>
</div>
<style>
    .header-body .login-bar {
        right: 100px;
        height: 35px;
    }

    .header-body > .img-ssl {
        right: 210px;
    }

    .header-body > .img-ssl {
        left: 130px;
        right: unset;
    }

    @media (max-width: 767px) {
        .header-body .login-bar {
            position: fixed;
            right: 16px;
            height: 33px;
        }

        .form-dark {
            display: none;
        }
    }
</style>
<!-- /.wrapper -->
<script type="text/javascript">
    window.base_url   = "{{ url('/') }}";
    window.base_path  = "{{ asset('/') }}";
    window.csrf_token = "{{ csrf_token() }}";
</script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
@include('layouts.basescripts')
@yield('scripts')
</body>
</html>
