import moment from 'moment'
Vue.filter('uppercase', function (value) {
  return value.toUpperCase()
})
Vue.filter('capitalize', function (value) {
  // return value.toUpperCase()
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})
Vue.filter('formatCurrency', function (value, c, d, t) {
  if(!value || value == '' || value == null || value == 0) return value;
  var n = value,
   c = isNaN(c = Math.abs(c)) ? 0 : c,
   d = d == undefined ? "." : d,
   t = t == undefined ? "," : t,
   s = n < 0 ? "-" : "",
   i = parseFloat(n = Math.abs(+n || 0).toFixed(c)) + "",
   j = (j = i.length) > 3 ? j % 3 : 0;

   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
});
Vue.filter('formatCurrencyOrder', function (value, c, d, t) {
  if(!value || value == '' || value == null || value == 0) return value;
  if(typeof value == 'string'){
    value = parseFloat(value);
  }
  if(value.toString().length > 3){
    var tmp = value.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    return tmp.replace('.00','');
  }
  return value;
});
Vue.filter('formatAgo1', function (value) {
    var now = (new Date()).getTime();
    var yesterday = moment().subtract(1, 'day');
    if (value > now - 24 * 3600 * 1000) {
        return moment(value).format('LT');
    } else if (moment(value).isSame(yesterday, 'day')) {
        return "Yesterday";
    } else {
        return moment(value).format('MM/DD');
    }
})

Vue.filter('fullDate',function(value){
  return moment(value).format('MM/DD/YYYY at HH:MM');
})

Vue.filter('fullDateDay',function (value) {
  if(value == null){
    value = new Date();
  }

  return moment(value).format('ddd, DD MMM YYYY');
})

Vue.filter('fullDateDayTime',function (value) {
  if(value == null){
    value = new Date();
  }

  return moment(value).format('ddd, DD MMM YYYY HH:mm');
})

Vue.filter('formatTime1', function (value) {
    return moment(value).format('LT');
})
Vue.filter('truncateText', function (value, length, spacecharpos) {
    if (!value) return '';
    if (spacecharpos) {
      if (!/\s|-/g.test(value.substr(0,spacecharpos))) {
        value = value.substr(0,spacecharpos) + ' ' + value.substr(spacecharpos);
      }
    }
    return value.length > length ? value.substr(0,length) + '…' : value;
})
Vue.filter('convertLinkMessage', function(msg) {
  var returnString = '';
  try {
    msg = msg.replace(/</g, '&lt;');
    msg = msg.replace(/>/g, '&gt;');
  } catch(e) {
    msg = '';
  }
  var urlexp = new RegExp('(http|ftp|https)://[a-z0-9\-_]+(\.[a-z0-9\-_]+)+([a-z0-9\-\.,@\?^=%&;:/~\+#]*[a-z0-9\-@\?^=%&;/~\+#])?', 'i');
  if (urlexp.test(msg)) {
    returnString += '<img src="'+base_path+'/static/img/icon-link.svg" style="margin-right: 6px;"><a href="' + msg + '" target="_blank">' + msg + '</a>';
  } else {
    returnString += msg;
  }

  return returnString;
})
Vue.filter('arrayToText', function (value) {
    if(!value) return '';
    return value.join(', ').replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
})

Vue.filter('boldSameText', function (str,find) {
    var re = new RegExp(find, 'g');
    return str.replace(re, '<b>'+find+'</b>');
})