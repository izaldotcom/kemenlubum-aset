require('./bootstrap');

import VueLadda from 'vue-ladda'
import Croppa from 'vue-croppa'
import VueIziToast from 'vue-izitoast'
import 'izitoast/dist/css/iziToast.min.css';
import Multiselect from 'vue-multiselect'
import ToggleButton from 'vue-js-toggle-button'
import {Datetime} from 'vue-datetime'
import VueGoogleCharts from 'vue-google-charts'
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'

import * as VueGoogleMaps from 'vue2-google-maps'

import VueCtkDateTimePicker from 'vue-ctk-date-time-picker';
import 'vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css';
import VTooltip from 'v-tooltip';

window.Vue = require('vue');

window.Vue.use(VTooltip);
window.Vue.use(VueIziToast);
window.Vue.use(ToggleButton);
window.Vue.use(Datetime);
window.Vue.use(require('vue-moment'));

require('./components/filters.js');

Vue.component('multiselect', Multiselect);
Vue.component('VueCtkDateTimePicker', VueCtkDateTimePicker);
Vue.component('vue-autonumeric', require('vue-autonumeric'));
Vue.component('vue-ladda', VueLadda);
Vue.component('datetime', Datetime);
Vue.component('ladda-reference', require('./components/LaddaReference'));
Vue.component('error-reference', require('./components/ErrorReference'));

Vue.component('ajax-table', require('./components/AjaxTable.vue'));
Vue.component('ajax-tables', require('./components/AjaxTables.vue'));
Vue.component('sorter', require('./components/Sorter'));

Vue.component('dark-mode-toogle', require('./components/setting/DarkModeToogle'));
Vue.component('form-change-password', require('./components/setting/FormChangePassword'));
Vue.component('form-general-setting', require('./components/setting/FormGeneralSetting'));
Vue.component('form-input-excel', require('./components/import/FormImportExcel'));
Vue.component('log-import-excel', require('./components/import/LogImportExcel'));

Vue.component('front-maps', require('./components/maps/FrontMaps'));
Vue.component('front-maps-chart', require('./components/maps/FrontMapsChart'));
Vue.component('front-maps-chart-dashboard', require('./components/maps/FrontChartMapsDs'));
Vue.component('front-maps-filter', require('./components/maps/FrontMapsFilter'));
Vue.component('front-maps-filter-new', require('./components/maps/FrontMapsFilterNew'));
Vue.component('dark-mode-front', require('./components/setting/DarkModeFront'));

Vue.component('form-upload-penghapusan', require('./components/penghapusan/FormUploadPenghapusan'));
Vue.component('detail-penghapusan', require('./components/penghapusan/DetailPenghapusan'));
Vue.component('detail-penghapusan-pimpinan', require('./components/penghapusan/DetailPenghapusanPimpinan'));
Vue.component('detail-penghapusan-icon', require('./components/penghapusan/DetailPenghapusanIcon'));
Vue.component('detail-penghapusan-icon-new', require('./components/penghapusan/DetailPenghapusanIconnew'));
Vue.component('detail-penghapusan-label-new'), require('./components/penghapusan/DetailPenghapusanLabelNew')
Vue.component('detail-penghapusan-label', require('./components/penghapusan/DetailPenghapusanLabel'));
Vue.component('pilih-kendaraan-penghapusan', require('./components/penghapusan/PilihKendaraanPenghapusan'));

Vue.component('tr-data-wilayah', require('./components/tr/DataWilayah'));
Vue.component('tr-data-rumah-negara', require('./components/tr/DataRumahNegara'));
Vue.component('tr-data-kendaraan', require('./components/tr/DataKendaraan'));
Vue.component('tr-data-perwakilan-kendaraan', require('./components/tr/DataPerwakilanKendaraan'));
Vue.component('tr-data-perwakilan-tanah', require('./components/tr/DataPerwakilanTanah'))
Vue.component('tr-data-perwakilan-bangunan', require('./components/tr/DataPerwakilanBangunan'))
Vue.component('tr-data-gedung', require('./components/tr/DataGedung'));
Vue.component('tr-data-tanah', require('./components/tr/DataTanah'));
Vue.component('tr-data-sekjen', require('./components/tr/DataSekjen'));
Vue.component('tr-data-user', require('./components/tr/DataUser'));
Vue.component('tr-data-barang', require('./components/tr/DataBarang'));
Vue.component('tr-data-satker', require('./components/tr/DataSatker'));
Vue.component('tr-data-photos', require('./components/tr/DataPhotos'));
Vue.component('tr-data-kendaraan-dinas', require('./components/tr/DataKendaraanDinas'));
Vue.component('tr-data-kendaraan-add-penghapusan', require('./components/tr/DataKendaraanAddPenghapusan'));
Vue.component('tr-data-kendaraan-penghapusan', require('./components/tr/DataKendaraanPenghapusan'));
Vue.component('tr-data-pengajuan-penghapusan', require('./components/tr/DataPengajuanPenghapusan'));

Vue.component('modal-add-kendaraan-dinas', require('./components/modal/kendaraanDinas/ModalAddKendaraanDinas'));
Vue.component('modal-edit-kendaraan-dinas', require('./components/modal/kendaraanDinas/ModalEditKendaraanDinas'));
Vue.component('modal-delete-kendaraan-dinas', require('./components/modal/kendaraanDinas/ModalDeleteKendaraanDinas'));

Vue.component('modal-add-sekjen', require('./components/modal/sekjen/ModalAddSekjen'));
Vue.component('modal-edit-sekjen', require('./components/modal/sekjen/ModalEditSekjen'));
Vue.component('modal-delete-sekjen', require('./components/modal/sekjen/ModalDeleteSekjen'));

Vue.component('modal-add-user', require('./components/modal/user/ModalAddUser'));
Vue.component('modal-edit-user', require('./components/modal/user/ModalEditUser'));
Vue.component('modal-reset-password', require('./components/modal/user/ModalResetPassword'));
Vue.component('modal-pemilihan-perwakilan-kendaraan', require('./components/perwakilan/perwakilanKendaraan'));
Vue.component('modal-pemilihan-perwakilan-bangunan', require('./components/perwakilan/perwakilanBangunan'));
Vue.component('modal-pemilihan-perwakilan-tanah', require('./components/perwakilan/perwakilanTanah'));

Vue.component('modal-edit-satker', require('./components/modal/satker/ModalEditSatker'));

Vue.component('modal-edit-penghapusan-panitia', require('./components/modal/penghapusan/ModalEditPenghapusanPanitia'));
Vue.component('modal-edit-penghapusan-signature', require('./components/modal/penghapusan/ModalEditPenghapusanSignature'));
Vue.component('modal-edit-penghapusan-pimpinan', require('./components/modal/penghapusan/ModalEditPenghapusanPimpinan'));
Vue.component('modal-create-penghapusan', require('./components/modal/penghapusan/ModalCreatePenghapusan'));
Vue.component('modal-upload-penghapusan-detail', require('./components/modal/penghapusan/ModalUploadPenghapusanDetail'));
Vue.component('modal-template-penghapusan-detail', require('./components/modal/penghapusan/ModalTemplatePenghapusanDetail'));
Vue.component('modal-add-kendaraan-penghapusan', require('./components/modal/penghapusan/ModalAddKendaraanPenghapusan'));

Vue.component('modal-edit-long-lat', require('./components/modal/ModalEditLongLat'));
Vue.component('modal-edit-photos', require('./components/modal/ModalEditPhotos'));

Vue.component('maps-kemlu', require('./components/maps/MapsKemlu.vue'));
Vue.component('backend-maps-filter', require('./components/maps/BackendMapsFilter'));
// Vue.component('multiselect', Multiselect);

Vue.use(Croppa);
Vue.use(VueGoogleMaps, {
    load: {
        key      : 'AIzaSyAQYsVW5wtF2ecHktKoSBzTo1gQSm8BxME',
        libraries: 'places,drawing',
    }
});
window.Vue.use(VueGoogleCharts);

window.eventHub = new Vue();
window.app      = new Vue({
    el     : '#app',
    data   : {
        base_path         : base_path,
        base_url          : base_url,
        csrf_token        : $("meta[name='csrf-token']").attr("content"),
        params            : {
            id    : '',
            status: '',
            filter: '',
            code  : '',
        },
        rowparams         : {},
        notificationSystem: {
            options: {
                show    : {
                    theme           : 'dark',
                    icon            : 'icon-person',
                    position        : 'topCenter',
                    progressBarColor: 'rgb(0, 255, 184)',
                    buttons         : [
                        ['<button>Ok</button>', function (instance, toast) {
                            alert("Hello world!");
                        }, true],
                        ['<button>Close</button>', function (instance, toast) {
                            instance.hide({
                                transitionOut: 'fadeOutUp',
                                onClosing    : function (instance, toast, closedBy) {
                                    console.info('closedBy: ' + closedBy);
                                }
                            }, toast, 'buttonName');
                        }]
                    ],
                    onOpening       : function (instance, toast) {
                        console.info('callback abriu!');
                    },
                    onClosing       : function (instance, toast, closedBy) {
                        console.info('closedBy: ' + closedBy);
                    }
                },
                ballon  : {
                    balloon : true,
                    position: 'bottomCenter'
                },
                info    : {
                    position: 'bottomLeft'
                },
                success : {
                    position: 'bottomRight'
                },
                warning : {
                    position: 'topLeft'
                },
                error   : {
                    position: 'topRight'
                },
                general : {
                    position: 'topRight'
                },
                question: {
                    timeout  : 20000,
                    close    : false,
                    overlay  : true,
                    toastOnce: true,
                    id       : 'question',
                    zindex   : 999,
                    position : 'center',
                    buttons  : [
                        ['<button><b>YES</b></button>', function (instance, toast) {
                            instance.hide({transitionOut: 'fadeOut'}, toast, 'button');
                        }, true],
                        ['<button>NO</button>', function (instance, toast) {
                            instance.hide({transitionOut: 'fadeOut'}, toast, 'button');
                        }]
                    ],
                    onClosing: function (instance, toast, closedBy) {
                        console.info('Closing | closedBy: ' + closedBy);
                    },
                    onClosed : function (instance, toast, closedBy) {
                        console.info('Closed | closedBy: ' + closedBy);
                    }
                }
            }
        },
    },
    methods: {
        makeNotification: function (message, title, type = "success") {
            if (type == "success")
                this.$toast.success(message, title, this.notificationSystem.options.general);
            if (type == "danger")
                this.$toast.error(message, title, this.notificationSystem.options.general);
            if (type == "warning")
                this.$toast.warning(message, title, this.notificationSystem.options.general);
            if (type == "info")
                this.$toast.info(message, title, this.notificationSystem.options.general);
        }
    },
    mounted: function () {
        this.$nextTick(function () {
            // this.$toast.show('Welcome!', 'Hey', this.notificationSystem.options.show);
            // this.$toast.show('Welcome!', 'Hey', this.notificationSystem.options.ballon);
            // this.$toast.info('Welcome!', 'Hello', this.notificationSystem.options.info);
            // this.$toast.success('Successfully inserted record!', 'OK', this.notificationSystem.options.success);
            // this.$toast.warning('You forgot important data', 'Caution', this.notificationSystem.options.warning);
            // this.$toast.error('Illegal operation', 'Error', this.notificationSystem.options.error);
            // this.$toast.question('Are you sure about that?', 'Hey', this.notificationSystem.options.question);
        }.bind(this));
    },
});
