require('./bootstrap');

import VueLadda from 'vue-ladda'
import Croppa from 'vue-croppa'
import VueIziToast from 'vue-izitoast'
import 'izitoast/dist/css/iziToast.min.css';
import Multiselect from 'vue-multiselect'
import ToggleButton from 'vue-js-toggle-button'
import VueHtmlToPaper from 'vue-html-to-paper';
import VueGoogleCharts from 'vue-google-charts'
import * as VueGoogleMaps from 'vue2-google-maps'
import carousel from 'vue-owl-carousel'

window.Vue = require('vue');
window.Vue.use(VueHtmlToPaper);
window.Vue.use(VueIziToast);
window.Vue.use(ToggleButton);

window.Vue.use(require('vue-moment'));

require('./components/filters.js');
Vue.component('vue-ladda', VueLadda);
Vue.component('ladda-reference', require('./components/LaddaReference'));
Vue.component('error-reference', require('./components/ErrorReference'));

Vue.component('ajax-table', require('./components/AjaxTable.vue'));
Vue.component('sorter', require('./components/Sorter'));

Vue.component('form-change-password', require('./components/setting/FormChangePassword'));

Vue.component('front-maps', require('./components/maps/FrontMaps'));
Vue.component('front-maps-chart', require('./components/maps/FrontMapsChart'));
Vue.component('front-maps-filter', require('./components/maps/FrontMapsFilter'));
Vue.component('front-maps-filter-new', require('./components/maps/FrontMapsFilterNew'));
Vue.component('dark-mode-front', require('./components/setting/DarkModeFront'));




Vue.component('multiselect', Multiselect);

window.Vue.use(Croppa);
window.Vue.use(VueGoogleCharts);
window.Vue.use(carousel);
window.Vue.use(VueGoogleMaps, {
    load: {
        key      : 'AIzaSyAQYsVW5wtF2ecHktKoSBzTo1gQSm8BxME',
        libraries: 'places,drawing',
    }
});
const options = {
    name: '_blank',
    specs: [
      'fullscreen=no',
      'titlebar=no',
      'scrollbars=no'
    ],
    styles: [
      'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
      'https://unpkg.com/kidlat-css/css/kidlat.css'
    ]
  }

window.Vue.use(VueHtmlToPaper, options);

window.eventHub = new Vue();
window.app      = new Vue({
    el     : '#app',
    data   : {
        base_path         : base_path,
        base_url          : base_url,
        csrf_token        : $("meta[name='csrf-token']").attr("content"),
        params            : {
            status: '',
        return: {
        globalOptions: {
                name: '_blank',
                specs: [
                  'fullscreen=yes',
                  'titlebar=yes',
                  'scrollbars=no'
                ],
                styles: [
                  'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
                  'https://unpkg.com/kidlat-css/css/kidlat.css'
                ]
              },
              localLandScapeOptions: {
                styles: [
                  'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
                  'https://unpkg.com/kidlat-css/css/kidlat.css',
                  './landscape.css'
                ]
              }
            }
        },


        rowparams         : {},
        notificationSystem: {
            options: {
                show    : {
                    theme           : 'dark',
                    icon            : 'icon-person',
                    position        : 'topCenter',
                    progressBarColor: 'rgb(0, 255, 184)',
                    buttons         : [
                        ['<button>Ok</button>', function (instance, toast) {
                            alert("Hello world!");
                        }, true],
                        ['<button>Close</button>', function (instance, toast) {
                            instance.hide({
                                transitionOut: 'fadeOutUp',
                                onClosing    : function (instance, toast, closedBy) {
                                    console.info('closedBy: ' + closedBy);
                                }
                            }, toast, 'buttonName');
                        }]
                    ],
                    onOpening       : function (instance, toast) {
                        console.info('callback abriu!');
                    },
                    onClosing       : function (instance, toast, closedBy) {
                        console.info('closedBy: ' + closedBy);
                    }
                },
                ballon  : {
                    balloon : true,
                    position: 'bottomCenter'
                },
                info    : {
                    position: 'bottomLeft'
                },
                success : {
                    position: 'bottomRight'
                },
                warning : {
                    position: 'topLeft'
                },
                error   : {
                    position: 'topRight'
                },
                general : {
                    position: 'topRight'
                },
                question: {
                    timeout  : 20000,
                    close    : false,
                    overlay  : true,
                    toastOnce: true,
                    id       : 'question',
                    zindex   : 999,
                    position : 'center',
                    buttons  : [
                        ['<button><b>YES</b></button>', function (instance, toast) {
                            instance.hide({transitionOut: 'fadeOut'}, toast, 'button');
                        }, true],
                        ['<button>NO</button>', function (instance, toast) {
                            instance.hide({transitionOut: 'fadeOut'}, toast, 'button');
                        }]
                    ],
                    onClosing: function (instance, toast, closedBy) {
                        console.info('Closing | closedBy: ' + closedBy);
                    },
                    onClosed : function (instance, toast, closedBy) {
                        console.info('Closed | closedBy: ' + closedBy);
                    }
                }
            }
        },
    },
    methods: {
        makeNotification: function (message, title, type = "success") {
            if (type == "success")
                this.$toast.success(message, title, this.notificationSystem.options.general);
            if (type == "danger")
                this.$toast.error(message, title, this.notificationSystem.options.general);
            if (type == "warning")
                this.$toast.warning(message, title, this.notificationSystem.options.general);
            if (type == "info")
                this.$toast.info(message, title, this.notificationSystem.options.general);
        }
    },
    mounted: function () {
        this.$nextTick(function () {
            // this.$toast.show('Welcome!', 'Hey', this.notificationSystem.options.show);
            // this.$toast.show('Welcome!', 'Hey', this.notificationSystem.options.ballon);
            // this.$toast.info('Welcome!', 'Hello', this.notificationSystem.options.info);
            // this.$toast.success('Successfully inserted record!', 'OK', this.notificationSystem.options.success);
            // this.$toast.warning('You forgot important data', 'Caution', this.notificationSystem.options.warning);
            // this.$toast.error('Illegal operation', 'Error', this.notificationSystem.options.error);
            // this.$toast.question('Are you sure about that?', 'Hey', this.notificationSystem.options.question);
        }.bind(this));
    },
});
